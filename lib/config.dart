const APP_NAME = 'TudoNutri';
const APP_FIRST_NAME = 'Tudo';
const APP_SECOND_NAME = 'Nutri';

// const DOMAIN_URL = 'https://wordpress.iqonic.design/product/wp/kivicare';
//const DOMAIN_URL = 'https://apps.iqonic.design/kivicare';
const DOMAIN_URL = 'http://10.0.2.2:80/last_tudonutri';
// Domínio local

//const DOMAIN_URL = 'https://portaltudonutri.brunesdigital.com.br';

///const DOMAIN_URL = 'https://tudonutrihomolog.brunesdigital.com.br';

//const DOMAIN_URL = 'http://192.168.1.86/wp_plugins/kivicare/index.php';
const BASE_URL = '$DOMAIN_URL/wp-json/';

const IQONIC_PACKAGE_NAME =
    "com.iqonic.kivicare"; // Do not change this Package Name.
const DEFAULT_LANGUAGE = 'pt';
var COPY_RIGHT_TEXT =
    '© ${DateTime.now().year}. Desenvolvido por ♡ by Brunes IT';

const TERMS_AND_CONDITION_URL = 'https://tudonutri/terms-of-use/';
const PRIVACY_POLICY_URL = 'https://tudonutri/privacy-policy/';
const SUPPORT_URL = 'https://tudonutri.support/';
const CODE_CANYON_URL =
    "https://codecanyon.net/item/kivicare-flutter-app-clinic-patient-management-system/30970616";
const MAIL_TO = "admtudonutri@tudonutri.com.br";

// Pagination
const PER_PAGE = 20;
