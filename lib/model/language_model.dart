import 'package:flutter/material.dart';

import '../utils/images.dart';

class Language {
  int id;
  String name;
  String languageCode;
  String flag;

  Language(this.id, this.name, this.languageCode, this.flag);

  static List<Language> getLanguages() {
    return <Language>[
      Language(0, 'Brazil', 'pt', flagsIcBrazil),
      Language(1, 'English', 'en', flagsIcUs),
      Language(2, 'Arabic', 'ar', flagsIcAr),
      Language(3, 'German', 'de', flagsIcGermany),
      Language(4, 'French', 'fr', flagsIcFrench),
    ];
  }

  static List<String> languages() {
    List<String> list = [];

    getLanguages().forEach((element) {
      list.add(element.languageCode);
    });

    return list;
  }

  static List<Locale> languagesLocale() {
    List<Locale> list = [];

    getLanguages().forEach((element) {
      list.add(Locale(element.languageCode, ''));
    });

    return list;
  }
}
