class RestrictAppointmentModel {
  String? post;
  String? pre;

  RestrictAppointmentModel({this.post, this.pre});

  factory RestrictAppointmentModel.fromJson(Map<String, dynamic> json) {
    return RestrictAppointmentModel(
      post: json['post'],
      pre: json['pre'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post'] = this.post;
    data['pre'] = this.pre;
    return data;
  }
}
