class BillModel {
  num? total;
  num? patientId;
  List<BillData>? listOfBill;

  BillModel({this.total, this.listOfBill, this.patientId});

  factory BillModel.fromJson(Map<String, dynamic> json) {
    return BillModel(
      total: json['total'],
      listOfBill: json['data'] != null ? (json['data'] as List).map((i) => BillData.fromJson(i)).toList() : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    if (this.listOfBill != null) {
      data['data'] = this.listOfBill!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BillData {
  num? billId;
  num? encounterId;
  String? billingDate;
  num? totalAmount;
  num? amountToPay;
  num? discount;
  String? patientName;
  String? doctorName;
  bool? paymentStatus;

  BillData({
    this.billId,
    this.amountToPay,
    this.billingDate,
    this.discount,
    this.paymentStatus,
    this.patientName,
    this.encounterId,
    this.doctorName,
    this.totalAmount,
  });

  String get getPaymentStatusInString => this.paymentStatus == true ? '1' : '0';

  factory BillData.fromJson(Map<String, dynamic> json) {
    return BillData(
        billId: json['id'],
        encounterId: json['encounter_id'],
        billingDate: json['bill_date'],
        doctorName: json['doctor_name'],
        patientName: json['patient_name'],
        totalAmount: json['total_amount'],
        discount: json['discount'],
        amountToPay: json['amount_to_pay'],
        paymentStatus: json['payment_status']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['id'] = this.billId;
    data['encounter_id'] = this.encounterId;
    data['bill_date'] = this.billingDate;
    data['doctor_name'] = this.doctorName;
    data['patient_name'] = this.patientName;
    data['discount'] = this.discount;
    data['amount_to_pay'] = this.amountToPay;
    data['payment_status'] = this.paymentStatus;

    return data;
  }
}
