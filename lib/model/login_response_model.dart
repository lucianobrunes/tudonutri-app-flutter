import 'package:kivicare_flutter/model/prescription_module.dart';
import 'package:kivicare_flutter/model/speciality_model.dart';

import 'clinic_list_model.dart';
import 'encounter_module.dart';
import 'module_config_model.dart';
import 'qualification_model.dart';

class LoginResponseModel {
  String? address;
  String? apiKey;
  String? apiSecret;
  String? city;
  List<Clinic>? clinic;
  List<Object>? commonSetting;
  String? country;
  String? dob;
  bool? enableTeleMed;
  List<EncounterModule>? encounterModules;
  String? firstName;
  String? gender;
  String? googleClientId;
  bool? isKiviCareProOnName;
  bool? isTeleMedActive;
  String? isEnableDoctorGCal;
  String? isEnableGoogleCal;
  String? isPatientEnable;
  String? lastName;
  String? mobileNumber;
  List<ModuleConfig>? moduleConfig;
  String? noOfExperience;
  List<Object>? notification;
  String? postalCode;
  List<PrescriptionModule>? prescriptionModule;
  String? price;
  String? priceType;
  String? profileImage;
  List<Qualification>? qualifications;
  String? role;
  List<SpecialtyModel>? specialties;
  String? token;
  String? userDisplayName;
  String? userEmail;
  int? userId;
  String? userNicename;
  String? zoomId;

  LoginResponseModel(
      {this.address,
      this.apiKey,
      this.apiSecret,
      this.city,
      this.clinic,
      this.commonSetting,
      this.country,
      this.dob,
      this.enableTeleMed,
      this.encounterModules,
      this.firstName,
      this.gender,
      this.googleClientId,
      this.isKiviCareProOnName,
      this.isTeleMedActive,
      this.isEnableDoctorGCal,
      this.isEnableGoogleCal,
      this.isPatientEnable,
      this.lastName,
      this.mobileNumber,
      this.moduleConfig,
      this.noOfExperience,
      this.notification,
      this.postalCode,
      this.prescriptionModule,
      this.price,
      this.priceType,
      this.profileImage,
      this.qualifications,
      this.role,
      this.specialties,
      this.token,
      this.userDisplayName,
      this.userEmail,
      this.userId,
      this.userNicename,
      this.zoomId});

  factory LoginResponseModel.fromJson(Map<String, dynamic> json) {
    return LoginResponseModel(
      address: json['address'],
      apiKey: json['api_key'],
      apiSecret: json['api_secret'],
      city: json['city'],
      //clinic: json['clinic'] != null ? (json['clinic'] as List).map((i) => Clinic.fromJson(i)).toList() : null,
      //   common_setting: json['common_setting'] != null ? (json['common_setting'] as List).map((i) => Object.fromJson(i)).toList() : null,
      country: json['country'],
      dob: json['dob'],
      enableTeleMed: json['enableTeleMed'],
      encounterModules: json['enocunter_modules'] != null ? (json['enocunter_modules'] as List).map((i) => EncounterModule.fromJson(i)).toList() : null,
      firstName: json['first_name'],
      gender: json['gender'],
      googleClientId: json['google_client_id'],
      isKiviCareProOnName: json['isKiviCareProOnName'],
      isTeleMedActive: json['isTeleMedActive'],
      isEnableDoctorGCal: json['is_enable_doctor_gcal'],
      isEnableGoogleCal: json['is_enable_google_cal'],
      isPatientEnable: json['is_patient_enable'],
      lastName: json['last_name'],
      mobileNumber: json['mobile_number'],
      moduleConfig: json['module_config'] != null ? (json['module_config'] as List).map((i) => ModuleConfig.fromJson(i)).toList() : null,
      noOfExperience: json['no_of_experience'],
      //  notification: json['notification'] != null ? (json['notification'] as List).map((i) => Object.fromJson(i)).toList() : null,
      postalCode: json['postal_code'],
      prescriptionModule: json['prescription_module'] != null ? (json['prescription_module'] as List).map((i) => PrescriptionModule.fromJson(i)).toList() : null,
      price: json['price'],
      priceType: json['price_type'],
      profileImage: json['profile_image'],
      qualifications: json['qualifications'] != null ? (json['qualifications'] as List).map((i) => Qualification.fromJson(i)).toList() : null,
      role: json['role'],
      specialties: json['specialties'] != null ? (json['specialties'] as List).map((i) => SpecialtyModel.fromJson(i)).toList() : null,
      token: json['token'],
      userDisplayName: json['user_display_name'],
      userEmail: json['user_email'],
      userId: json['user_id'],
      userNicename: json['user_nicename'],
      zoomId: json['zoom_id'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['address'] = this.address;
    data['api_key'] = this.apiKey;
    data['api_secret'] = this.apiSecret;
    data['city'] = this.city;
    data['country'] = this.country;
    data['dob'] = this.dob;
    data['enableTeleMed'] = this.enableTeleMed;
    data['first_name'] = this.firstName;
    data['gender'] = this.gender;
    data['google_client_id'] = this.googleClientId;
    data['isKiviCareProOnName'] = this.isKiviCareProOnName;
    data['isTeleMedActive'] = this.isTeleMedActive;
    data['is_enable_doctor_gcal'] = this.isEnableDoctorGCal;
    data['is_enable_google_cal'] = this.isEnableGoogleCal;
    data['is_patient_enable'] = this.isPatientEnable;
    data['last_name'] = this.lastName;
    data['mobile_number'] = this.mobileNumber;
    data['no_of_experience'] = this.noOfExperience;
    data['postal_code'] = this.postalCode;
    data['price'] = this.price;
    data['price_type'] = this.priceType;
    data['profile_image'] = this.profileImage;
    data['role'] = this.role;
    data['token'] = this.token;
    data['user_display_name'] = this.userDisplayName;
    data['user_email'] = this.userEmail;
    data['user_id'] = this.userId;
    data['user_nicename'] = this.userNicename;
    data['zoom_id'] = this.zoomId;
    if (this.clinic != null) {
      data['clinic'] = this.clinic!.map((v) => v.toJson()).toList();
    }
    /* if (this.common_setting != null) {
            data['common_setting'] = this.common_setting.map((v) => v.toJson()).toList();
        }*/
    if (this.encounterModules != null) {
      data['enocunter_modules'] = this.encounterModules!.map((v) => v.toJson()).toList();
    }
    if (this.moduleConfig != null) {
      data['module_config'] = this.moduleConfig!.map((v) => v.toJson()).toList();
    }
    /*  if (this.notification != null) {
            data['notification'] = this.notification.map((v) => v.toJson()).toList();
        }*/
    if (this.prescriptionModule != null) {
      data['prescription_module'] = this.prescriptionModule!.map((v) => v.toJson()).toList();
    }
    if (this.qualifications != null) {
      data['qualifications'] = this.qualifications!.map((v) => v.toJson()).toList();
    }
    if (this.specialties != null) {
      data['specialties'] = this.specialties!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
