class ServiceListModel {
  List<ServiceData>? serviceData;
  int? total;

  ServiceListModel({this.serviceData, this.total});

  factory ServiceListModel.fromJson(Map<String, dynamic> json) {
    return ServiceListModel(
      serviceData: json['data'] != null ? (json['data'] as List).map((i) => ServiceData.fromJson(i)).toList() : null,
      total: json['total'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    if (this.serviceData != null) {
      data['data'] = this.serviceData!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ServiceData {
  String? charges;
  String? doctorId;
  String? duration;
  String? id;
  String? image;
  String? mappingTableId;
  bool? multiple;
  String? name;
  String? serviceId;
  String? status;
  bool? telemedService;
  String? type;
  String? label;
  bool isCheck;

  ServiceData({this.charges, this.doctorId, this.isCheck = false, this.duration, this.id, this.image, this.mappingTableId, this.multiple, this.name, this.serviceId, this.status, this.telemedService, this.type, this.label});

  factory ServiceData.fromJson(Map<String, dynamic> json) {
    return ServiceData(
      charges: json['charges'],
      doctorId: json['doctor_id'],
      duration: json['duration'],
      id: json['id'],
      image: json['image'],
      mappingTableId: json['mapping_table_id'],
      multiple: json['multiple'],
      name: json['name'],
      serviceId: json['service_id'],
      status: json['status'],
      telemedService: json['telemed_service'],
      type: json['type'],
      label: json['label'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['charges'] = this.charges;
    data['doctor_id'] = this.doctorId;
    data['duration'] = this.duration;
    data['id'] = this.id;
    data['image'] = this.image;
    data['mapping_table_id'] = this.mappingTableId;
    data['multiple'] = this.multiple;
    data['name'] = this.name;
    data['service_id'] = this.serviceId;
    data['status'] = this.status;
    data['telemed_service'] = this.telemedService;
    data['type'] = this.type;
    data['label'] = this.label;
    return data;
  }
}
