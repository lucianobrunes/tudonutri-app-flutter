class TelemedModel {
  TelemedData? telemedData;
  String? message;

  TelemedModel({this.telemedData, this.message});

  factory TelemedModel.fromJson(Map<String, dynamic> json) {
    return TelemedModel(
      telemedData: json['data'] != null ? TelemedData.fromJson(json['data']) : null,
      message: json['message'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    if (this.telemedData != null) {
      data['data'] = this.telemedData!.toJson();
    }
    return data;
  }
}

class TelemedData {
  String? apiKey;
  String? apiSecret;
  bool? enableTelemed;
  String? zoomId;

  TelemedData({
    this.apiKey,
    this.apiSecret,
    this.enableTelemed,
    this.zoomId,
  });

  factory TelemedData.fromJson(Map<String, dynamic> json) {
    return TelemedData(
      apiKey: json['api_key'],
      apiSecret: json['api_secret'],
      enableTelemed: json['enableTeleMed'],
      zoomId: json['zoom_id'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_key'] = this.apiKey;
    data['api_secret'] = this.apiSecret;
    data['enableTeleMed'] = this.enableTelemed;
    data['zoom_id'] = this.zoomId;
    return data;
  }
}
