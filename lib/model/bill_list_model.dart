class BillListModel {
  List<BillListData>? billListData;

  BillListModel({this.billListData});

  factory BillListModel.fromJson(Map<String, dynamic> json) {
    return BillListModel(
      billListData: json['data'] != null ? (json['data'] as List).map((i) => BillListData.fromJson(i)).toList() : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.billListData != null) {
      data['data'] = this.billListData!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BillListData {
  int? actualAmount;
  String? clinicName;
  String? createdAt;
  int? discount;

  String? doctorName;
  int? id;
  String? patientName;
  bool? paymentStatus;
  int? totalAmount;
  num? encounterId;

  BillListData({
    this.actualAmount,
    this.clinicName,
    this.createdAt,
    this.discount,
    this.doctorName,
    this.id,
    this.patientName,
    this.paymentStatus,
    this.totalAmount,
    this.encounterId,
  });

  factory BillListData.fromJson(Map<String, dynamic> json) {
    return BillListData(
        id: json['id'],
        actualAmount: json['actual_amount'],
        clinicName: json['clinic_name'],
        createdAt: json['created_at'],
        discount: json['discount'],
        doctorName: json['doctor_name'],
        patientName: json['patient_name'],
        paymentStatus: json['payment_status'],
        totalAmount: json['total_amount'],
        encounterId: json['encounter_id']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['actual_amount'] = this.actualAmount;
    data['clinic_name'] = this.clinicName;
    data['created_at'] = this.createdAt;
    data['discount'] = this.discount;
    data['doctor_name'] = this.doctorName;
    data['id'] = this.id;
    data['patient_name'] = this.patientName;
    data['payment_status'] = this.paymentStatus;
    data['total_amount'] = this.totalAmount;
    data['encounter_id'] = this.encounterId;
    return data;
  }
}
