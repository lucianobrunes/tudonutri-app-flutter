import 'package:kivicare_flutter/main.dart';
import 'package:nb_utils/nb_utils.dart';

import '../model/upcoming_appointment_model.dart';

const DEMO_URL = 'https://kivicare-demo.iqonic.design/';

const DEFAULT_SERVICE_IMAGE_URL =
    'https://img.freepik.com/free-photo/orange-background_23-2147674307.jpg?w=900&t=st=1682404076~exp=1682404676~hmac=d2eaa5ed2910063112dbfa9cdfb15e1d967ac221b518ca7d1dd47f805ea3a7dd';

// ignore: non_constant_identifier_names
List<String> RTLLanguage = ['ar'];

List<WeeklyAppointment> emptyGraphList = [
  WeeklyAppointment(x: locale.lblMonday, y: 0),
  WeeklyAppointment(x: locale.lblTuesday, y: 0),
  WeeklyAppointment(x: locale.lblWednesday, y: 0),
  WeeklyAppointment(x: locale.lblThursday, y: 0),
  WeeklyAppointment(x: locale.lblFriday, y: 0),
  WeeklyAppointment(x: locale.lblSaturday, y: 0),
  WeeklyAppointment(x: locale.lblSunday, y: 0),
];

List<String> appointmentStatusList = [
  locale.lblAll,
  locale.lblLatest,
  locale.lblCompleted,
  locale.lblCancelled,
  locale.lblPast
];

List<String> userRoles = [locale.lblDoctor, locale.lblClinic];

// Loader Size
double loaderSize = 30;

// EmailsWakelock

// EmailsWakelock
//local
// const receptionistEmail = "calvin@kivicare.com";
// const doctorEmail = "doctor@kivicare.com";
// const patientEmail = "mike@kivicare.com";

// Live
const receptionistEmail = "receptionist_jenny@activelife.com";
const doctorEmail = "doctor_jchristopher@activelife.com";
const patientEmail = "evadavid@activelife.com";

//Demo Password
const loginPassword = "123456";

/* Theme Mode Type */
const THEME_MODE_LIGHT = 0;
const THEME_MODE_DARK = 1;
const THEME_MODE_SYSTEM = 2;

/* DateFormats */
const FORMAT_12_HOUR = 'hh:mm a';
const ONLY_HOUR_MINUTE = 'HH:mm';
const TIME_WITH_SECONDS = 'hh:mm:ss';
const DISPLAY_DATE_FORMAT = 'dd-MMM-yyyy';
const SAVE_DATE_FORMAT = 'yyyy-MM-dd';
const CONFIRM_APPOINTMENT_FORMAT = "EEEE, MMM dd yyyy";

const tokenStream = 'tokenStream';

const TOTAL_PATIENT = "Total Patient";
const TOTAL_DOCTOR = "Total Doctors";
const TOTAL_SERVICE = 'Total Service';

const TOTAL_PACIENTE = "Total Pacientes";
const TOTAL_PROFISSIONAL = "Total Profissionais";
const TOTAL_SERVICO = 'Total Serviço';

// Static DATA
const SPECIALIZATION = "specialization";
const SERVICE_TYPE = "service_type";

// Holidays
const DOCTOR = "doctor";
const CLINIC = "clinic";

const CLINIC_ID = 'CLINIC_ID';

// User Roles
const UserRoleDoctor = 'doctor';
const UserRolePatient = 'patient';
const UserRoleReceptionist = 'receptionist';

// Appointment Status
const BookedStatus = 'Booked';
const CheckOutStatus = 'Check out';
const CheckInStatus = 'Check in';
const CancelledStatus = 'Cancelled';

const BookedStatusInt = 1;
const CheckOutStatusInt = 3;
const CheckInStatusInt = 4;
const CancelledStatusInt = 0;

const ClosedEncounterStatus = 'Closed';
const ActiveEncounterStatus = 'Active';
const InActiveEncounterStatus = 'Inactive';

const ClosedEncounterStatusInt = 0;
const ActiveEncounterStatusInt = 1;
const InActiveEncounterStatusInt = 2;

// Shared preferences keys
const USER_NAME = 'USER_NAME';
const USER_PASSWORD = 'USER_PASSWORD';
const IS_LOGGED_IN = 'IS_LOGGED_IN';
const IS_REMEMBER_ME = 'IS_REMEMBER_ME';
const TOKEN = 'TOKEN';
const LANGUAGE = 'LANGUAGE';
const SELECTED_LANGUAGE = "SELECTED_LANGUAGE";
const USER_ID = "USER_ID";
const USER_DATA = "USER_DATA";
const FIRST_NAME = "FIRST_NAME";
const LAST_NAME = "LAST_NAME";
const USER_EMAIL = "USER_EMAIL";
const USER_LOGIN = "USER_LOGIN";
const USER_MOBILE = 'USER_MOBILE';
const USER_GENDER = 'USER_GENDER';
const SELECTED_PROFILE_INDEX = 'SELECTED_PROFILE_INDEX';

const USER_DISPLAY_NAME = "USER_DISPLAY_NAME";
const PROFILE_IMAGE = "PROFILE_IMAGE";
const DEMO_DOCTOR = "DEMO_DOCTOR";
const DEMO_RECEPTIONIST = "DEMO_RECEPTIONIST";
const DEMO_PATIENT = "DEMO_PATIENT";
const PASSWORD = "PASSWORD";
const USER_ROLE = "USER_ROLE";
const USER_CLINIC = "USER_CLINIC";
const USER_TELEMED_ON = "USER_TELEMED_ON";
const USER_PRO_ENABLED = "USER_PRO_ENABLED";
const USER_ENCOUNTER_MODULES = "USER_ENCOUNTER_MODULES";
const USER_PRESCRIPTION_MODULE = "USER_PRESCRIPTION_MODULE";
const USER_MODULE_CONFIG = "USER_MODULE_CONFIG";
const USER_ENABLE_GOOGLE_CAL = "USER_ENABLE_GOOGLE_CAL";
const DOCTOR_ENABLE_GOOGLE_CAL = "DOCTOR_ENABLE_GOOGLE_CAL";
const SET_TELEMED_TYPE = "SET_TELEMED_TYPE";
const GLOBAL_DATE_FORMAT = "GLOBAL_DATE_FORMAT";
const USER_MEET_SERVICE = "USER_MEET_SERVICE";
const USER_ZOOM_SERVICE = "USER_ZOOM_SERVICE";
const RESTRICT_APPOINTMENT_POST = "RESTRICT_APPOINTMENT_POST";
const RESTRICT_APPOINTMENT_PRE = "RESTRICT_APPOINTMENT_PRE";
const CURRENCY = "CURRENCY";
const CURRENCY_POST_FIX = "CURRENCY_POST_FIX";
const IS_WALKTHROUGH_FIRST = "IS_WALKTHROUGH_FIRST";
const ON = "on";
const OFF = "off";
const SAVE_BASE_URL = "SAVE_BASE_URL";
const GOOGLE_USER_NAME = "GOOGLE_USER_NAME";
const GOOGLE_EMAIL = "GOOGLE_EMAIL";
const GOOGLE_PHOTO_URL = "GOOGLE_PHOTO_URL";

const PROBLEM = "problema";
const OBSERVATION = "observação";
const NOTE = "nota";
const PRESCRIPTION = 'prescrição';
const REPORT = 'relatório';

const UPDATE = "UPDATE";
const DELETE = "DELETE";
const APP_UPDATE = "APP_UPDATE";
const CHANGE_DATE = "CHANGE_DATE";
const DEMO_EMAILS = 'demoEmails';

int titleTextSize = 18;
int fragmentTextSize = 22;

const packageName = "com.iqonic.kivicare";

Future<bool> get isIqonicProduct async => await getPackageName() == packageName;

///LiveStream keys
const UPDATE_APPOINTMENTS = 'UPDATE_APPOINTMENTS';
const UPDATE_ENCOUNTERS = 'UPDATE_ENCOUNTERS';
const UPDATE_BILLS = 'UPDATE_BILLS';
const UPDATE_SESSIONS = 'UPDATE_SESSIONS';

const DOCTOR_YES_NO = ''; // Input Dr. here if that case

const MSG_ADICIONAR_SERVICO = "Serviço adicionado com sucesso";
const MSG_ATUALIZAR_SERVICO = "Serviço atualizado com sucesso";
const MSG_EXCLUIR_SERVICO = "Serviço excluído com sucesso";
