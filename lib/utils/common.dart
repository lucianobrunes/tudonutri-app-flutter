import 'dart:convert';
import 'dart:io';

import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_custom_tabs/flutter_custom_tabs.dart' as custom_tabs;
import 'package:flutter_custom_tabs/flutter_custom_tabs.dart';
import 'package:html/parser.dart';
import 'package:intl/intl.dart';
import 'package:kivicare_flutter/config.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/screens/patient/components/html_widget.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/date_extensions.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:path_provider/path_provider.dart';
import 'package:signature/signature.dart';
import 'package:url_launcher/url_launcher.dart';

String parseHtmlString(String? htmlString) {
  return parse(parse(htmlString).body!.text).documentElement!.text;
}

Future<void> commonLaunchUrl(String address,
    {LaunchMode launchMode = LaunchMode.inAppWebView}) async {
  await launchUrl(Uri.parse(address), mode: launchMode).catchError((e) {
    toast('${locale.lblInvalidURL}: $address');
  });
}

Future<void> commonLaunchUrl2(String address,
    {LaunchMode launchMode = LaunchMode.inAppWebView}) async {
  await launchUrl(Uri.parse(address), mode: launchMode).catchError((e) {
    toast('${locale.lblInvalidURL}: $address');
  });
}

void launchCall(String? url) {
  if (url.validate().isNotEmpty) {
    if (isIOS)
      commonLaunchUrl('tel://' + url!,
          launchMode: LaunchMode.externalApplication);
    else
      commonLaunchUrl('tel:' + url!,
          launchMode: LaunchMode.externalApplication);
  }
}

void launchMail(String url) {
  if (url.validate().isNotEmpty) {
    commonLaunchUrl('$MAIL_TO$url', launchMode: LaunchMode.externalApplication);
  }
}

void checkIfLink(BuildContext context, String value, {String? title}) {
  if (value.validate().isEmpty) return;

  String temp = parseHtmlString(value.validate());
  if (temp.startsWith("https") || temp.startsWith("http")) {
    launchUrlCustomTab(temp.validate());
  } else if (temp.validateEmail()) {
    launchMail(temp);
  } else if (temp.validatePhone() || temp.startsWith('+')) {
    launchCall(temp);
  } else {
    HtmlWidget(postContent: value).launch(context);
  }
}

void launchUrlCustomTab(String? url) {
  if (url.validate().isNotEmpty) {
    custom_tabs.launch(
      url!,
      customTabsOption: custom_tabs.CustomTabsOption(
        enableDefaultShare: true,
        enableInstantApps: true,
        enableUrlBarHiding: true,
        showPageTitle: true,
        toolbarColor: primaryColor,
      ),
      safariVCOption: custom_tabs.SafariViewControllerOption(
        preferredBarTintColor: primaryColor,
        preferredControlTintColor: Colors.white,
        barCollapsingEnabled: true,
        entersReaderIfAvailable: true,
        dismissButtonStyle: SafariViewControllerDismissButtonStyle.close,
      ),
    );
  }
}

InputDecoration inputDecoration(
    {required BuildContext context,
    Widget? prefixIcon,
    String? labelText,
    String? hintText,
    Widget? suffixIcon}) {
  return InputDecoration(
    contentPadding: EdgeInsets.all(10),
    labelText: labelText,
    labelStyle: secondaryTextStyle(),
    alignLabelWithHint: true,
    prefixIcon: prefixIcon,
    suffixIcon: suffixIcon,
    filled: true,
    fillColor: context.cardColor,
    disabledBorder: OutlineInputBorder(
        borderRadius: radius(),
        borderSide: BorderSide(color: Colors.transparent, width: 0.0)),
    border: OutlineInputBorder(
        borderRadius: radius(),
        borderSide: BorderSide(color: Colors.transparent, width: 0.0)),
    enabledBorder: OutlineInputBorder(
        borderRadius: radius(),
        borderSide: BorderSide(color: Colors.transparent, width: 0.0)),
    focusedErrorBorder: OutlineInputBorder(
        borderRadius: radius(),
        borderSide: BorderSide(color: Colors.red, width: 0.0)),
    errorBorder: OutlineInputBorder(
        borderRadius: radius(),
        borderSide: BorderSide(color: Colors.red, width: 1.0)),
    focusedBorder: OutlineInputBorder(
        borderRadius: radius(),
        borderSide: BorderSide(color: primaryColor, width: 0.0)),
  );
}

bool get isRTL => RTLLanguage.contains(appStore.selectedLanguage);

DateTime getDay(DateTime date) {
  return DateTime.parse(DateFormat(SAVE_DATE_FORMAT).format(date));
}

String getStatus(String num) {
  String status = "";
  if (num == '0') {
    return status = locale.lblCancelled;
  } else if (num == '1') {
    return status = locale.lblBooked;
  } else if (num == '3') {
    return status = locale.lblCheckOut;
  } else if (num == '4') {
    return status = locale.lblCheckIn;
  }
  return status;
}

Color getAppointStatusColor(String? num) {
  Color colors = Color(0xFFd0150f);
  if (num == '0') {
    return colors = Color(0xFFd0150f);
  } else if (num == '1') {
    return colors = Color(0xFF23a359);
  } else if (num == '3') {
    return colors = Color(0xFF23a359);
  } else if (num == '4') {
    return colors = Color(0xFF23a359);
  }
  return colors;
}

String getEncounterStatus(String? num) {
  String status = "";
  if (num == '0') {
    return status = locale.lblClosed;
  } else if (num == '1') {
    return status = locale.lblActive;
  } else if (num == '2') {
    return status = locale.lblInActive;
  }
  return status;
}

String getUserActivityStatus(String? num) {
  String status = "";
  if (num == '0') {
    return status = locale.lblActive;
  } else if (num == '1') {
    return status = locale.lblInActive;
  }
  return status;
}

String getServiceActivityStatus(String? num) {
  String status = "";
  if (num == '1') {
    return status = locale.lblActive;
  } else if (num == '0') {
    return status = locale.lblInActive;
  }
  return status;
}

Color getServiceActivityStatusColor(String? num) {
  Color colors = Color(0xFFd0150f);
  if (num == '1') {
    return colors = Color(0xFF23a359);
  } else if (num == '0') {
    return colors = Color(0xFFd0150f);
  }
  return colors;
}

Color getUserActivityStatusColor(String? num) {
  Color colors = Color(0xFFd0150f);
  if (num == '0') {
    return colors = Color(0xFF23a359);
  } else if (num == '1') {
    return colors = Color(0xFFd0150f);
  }
  return colors;
}

Color getEncounterStatusColor(String? num) {
  Color colors = Color(0xFFd0150f);
  if (num == '0') {
    return colors = Color(0xFFd0150f);
  } else if (num == '1') {
    return colors = Color(0xFF23a359);
  }
  return colors;
}

String getPaymentStatus(String? num) {
  String status = "";
  if (num == '0') {
    return status = locale.lblUnPaid;
  } else if (num == '1') {
    return status = locale.lblPaid;
  }
  return status;
}

String getClinicStatus(String? num) {
  String? status;
  if (num == '0') {
    return status = locale.lblClose;
  } else if (num == '1') {
    return status = locale.lblOpen;
  }
  return status.validate(value: '');
}

Color getHolidayStatusColor(bool num) {
  Color colors;
  if (num) {
    colors = Color(0xFF23a359);
  } else {
    colors = Color(0xFFd0150f);
  }
  return colors;
}

Color getPaymentStatusColor(String? num) {
  Color colors = Color(0xFFd0150f);
  if (num == '0') {
    return colors = Color(0xFFd0150f);
  } else if (num == '1') {
    return colors = Color(0xFF23a359);
  }
  return colors;
}

Color getClinicStatusColor(String? num) {
  Color colors = Color(0xFFd0150f);
  if (num == '0') {
    return colors = Color(0xFFd0150f);
  } else if (num == '1') {
    return colors = Color(0xFF23a359);
  }
  return colors;
}

bool isDoctor() => userStore.userRole == UserRoleDoctor;

bool isPatient() => userStore.userRole == UserRolePatient;

bool isReceptionist() => userStore.userRole == UserRoleReceptionist;

bool isProEnabled() => getBoolAsync(USER_PRO_ENABLED);

void setDynamicStatusBarColor({Color? color}) {
  if (color != null) {
    setStatusBarColor(appStore.isDarkModeOn ? scaffoldColorDark : color);
  } else if (appStore.isDarkModeOn) {
    setStatusBarColor(scaffoldColorDark);
  } else {
    setStatusBarColor(Colors.white);
  }
}

Future setupRemoteConfig() async {
  final FirebaseRemoteConfig remoteConfig = FirebaseRemoteConfig.instance;
  await remoteConfig.setConfigSettings(RemoteConfigSettings(
    fetchTimeout: const Duration(seconds: 10),
    minimumFetchInterval: const Duration(hours: 1),
  ));
  await FirebaseRemoteConfig.instance.fetchAndActivate().catchError((e) {
    log("Firebase Remote Failed");
    return false;
  });

  appStore.setDemoEmails();
}

class HttpOverridesSkipCertificate extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) =>
      super.createHttpClient(context)
        ..badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
}

Future<File?> getSignatureInFile(BuildContext context,
    {required SignatureController controller,
    int? height,
    int? width,
    required String fileName}) async {
  if (controller.isEmpty) return null;
  Uint8List? unit = await controller.toPngBytes(
      height: height ?? 150, width: width ?? context.width().toInt());

  Directory? directory = await getExternalStorageDirectory();

  await Directory('${directory!.path}/signature').create(recursive: true);
  File('${directory.path}/signature/$fileName.png')
      .writeAsBytesSync(unit!.buffer.asInt8List());
  return File(Directory('${directory.path}/signature/$fileName.png').path);
}

//region Image DECODE AND ENCODE
dynamic convertImageToBase64(File file) {
  List<int> imageBytes = file.readAsBytesSync();
  String base64Image = base64Encode(imageBytes);

  return 'data:image/png;base64,$base64Image';
}

dynamic decodeImageFromBase64(String encoded) {
  String decoded = utf8.decode(base64Url.decode(encoded));
  return decoded;
}

Uint8List? getImageFromBase64(String value) {
  UriData? data = Uri.parse(value.validate()).data;

  return data?.contentAsBytes();
}
//endregion

String get getAppointmentDate => appointmentAppStore.selectedAppointmentDate
    .getFormattedDate(SAVE_DATE_FORMAT);

String traduzDiasSemana(String? daysEn) {
  //replace substring of the given string
  String result1 = daysEn!.replaceAll("mon", "Seg");
  String result2 = result1.replaceAll("tue", "Ter");
  String result3 = result2.replaceAll("wed", "Qua");
  String result4 = result3.replaceAll("thu", "Qui");
  String result5 = result4.replaceAll("fri", "Sex");
  String result6 = result5.replaceAll("sat", "Sáb");
  String result7 = result6.replaceAll("sun", "Dom");
  return result7;
}

List<String> getListaDiasTraduzidos(List<String> days) {
  List<String> listaDiasTraduzidos = [];
  for (var day in days) {
    listaDiasTraduzidos.add(getNomeDiaTraduzido(day));
  }
  return listaDiasTraduzidos;
}

String getNomeDiaTraduzido(String name) {
  String nome = "";
  switch (name.toLowerCase()) {
    case "mon":
      nome = "Seg";
      break;
    case "tue":
      nome = "Ter";
      break;
    case "wed":
      nome = "Qua";
      break;
    case "thu":
      nome = "Qui";
      break;
    case "fri":
      nome = "Sex";
      break;
    case "sat":
      nome = "Sáb";
      break;
    case "sun":
      nome = "Dom";
      break;
  }
  return nome;
}
