import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

import 'package:kivicare_flutter/config.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/upcoming_appointment_model.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:nb_utils/nb_utils.dart';

Future<void> defaultValue() async {
  appStore.setBaseUrl(getStringAsync(SAVE_BASE_URL, defaultValue: BASE_URL));
  appStore.setDemoDoctor(getStringAsync(DEMO_DOCTOR));
  appStore.setDemoReceptionist(getStringAsync(DEMO_RECEPTIONIST));
  appStore.setDemoPatient(getStringAsync(DEMO_PATIENT));
  appStore.setCurrency(getStringAsync(CURRENCY));

  if (appStore.isLoggedIn) {
    userStore.setUserId(getIntAsync(USER_ID));
    userStore.setFirstName(getStringAsync(FIRST_NAME));
    userStore.setLastName(getStringAsync(LAST_NAME));
    userStore.setUserEmail(getStringAsync(USER_EMAIL));
    userStore.setUserDisplayName(getStringAsync(USER_DISPLAY_NAME));
    userStore.setUserProfile(getStringAsync(PROFILE_IMAGE, defaultValue: ""));
    userStore.setUserGender(getStringAsync(USER_GENDER));
    userStore.setRole(getStringAsync(USER_ROLE));
    userStore.setUserMobileNumber(getStringAsync(USER_MOBILE));
    //userStore.setClinicId(userStore.userClinicId.toString());
    userStore.setClinicId(getStringAsync(CLINIC_ID).toString());
    appStore.setUserProEnabled(getBoolAsync(USER_PRO_ENABLED));
    appStore.setUserTelemedOn(getBoolAsync(USER_TELEMED_ON));
    appStore.setUserEnableGoogleCal(getStringAsync(USER_ENABLE_GOOGLE_CAL));
    appStore.setUserDoctorGoogleCal(getStringAsync(DOCTOR_ENABLE_GOOGLE_CAL));
    appStore.setGlobalDateFormat(getStringAsync(GLOBAL_DATE_FORMAT));
    appStore.setRestrictAppointmentPost(getIntAsync(RESTRICT_APPOINTMENT_POST));
    appStore.setRestrictAppointmentPre(getIntAsync(RESTRICT_APPOINTMENT_PRE));
  }
}

String getRoleWiseAppointmentName({required String name}) {
  if (isPatient() || isReceptionist()) {
    return 'Dr. $name';
  } else {
    return name;
  }
}

String getRoleWiseName({required String name}) {
  if (isDoctor()) {
    return '${name.prefixText(value: 'Dr. ')}';
  } else {
    return name;
  }
}

String getRoleWiseAppointmentFirstText(UpcomingAppointmentModel upcomingData) {
  if (isDoctor() || isReceptionist())
    return upcomingData.patientName.validate();
  else
    return upcomingData.doctorName.validate().prefixText(value: 'Dr. ');
}

SystemUiOverlayStyle defaultSystemUiOverlayStyle(BuildContext context, {Color? color, Brightness? statusBarIconBrightness}) {
  return SystemUiOverlayStyle(statusBarColor: color ?? context.primaryColor, statusBarIconBrightness: statusBarIconBrightness ?? Brightness.light);
}

String getEndPoint({required String endPoint, int? perPages, int? page, List<String>? params}) {
  String perPage = "?limit=${perPages ?? PER_PAGE}";
  String pages = "&page=$page";

  if (page != null && params.validate().isEmpty) {
    return "$endPoint$perPage$pages";
  } else if (page != null && params.validate().isNotEmpty) {
    return "$endPoint$perPage$pages&${params.validate().join('&')}";
  } else if (page == null && params != null) {
    return "$endPoint?${params.join('&')}";
  }
  log('-------------$endPoint---------------');
  return "$endPoint";
}

void getDisposeStatusBarColor({Color? colors}) {
  setStatusBarColor(colors ?? (appStore.isDarkModeOn.validate() ? scaffoldColorDark : scaffoldColorLight));
}

Future<List<File>> getMultipleImageSource({bool isCamera = true}) async {
  final pickedImage = await ImagePicker().pickMultiImage();
  return pickedImage.map((e) => File(e.path)).toList();
}

Future<File> getCameraImage({bool isCamera = true}) async {
  final pickedImage = await ImagePicker().pickImage(source: isCamera ? ImageSource.camera : ImageSource.gallery);
  return File(pickedImage!.path.validate());
}

Widget addRemoveToggle({bool status = false, required Function onTap}) {
  return Align(
    alignment: Alignment.centerRight,
    child: Icon(
      status ? Icons.remove : Icons.add,
      color: status ? Colors.red : primaryColor,
    ).onTap(onTap).paddingBottom(16),
  );
}

Map<T, List<S>> groupBy<S, T>(Iterable<S> values, T Function(S) key) {
  var map = <T, List<S>>{};
  for (var element in values) {
    (map[key(element)] ??= []).add(element);
  }
  return map;
}

DateTime getDateTime({required String date, String? time}) {
  DateTime? dates = DateTime.tryParse(date.validate());
  DateTime? times = DateTime.tryParse(time.validate());
  return DateTime(dates!.year, dates.month, dates.day, times!.hour, times.minute, times.second);
}
