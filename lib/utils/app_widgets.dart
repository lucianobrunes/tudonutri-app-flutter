import 'package:flutter/material.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/demo_login_model.dart';
import 'package:kivicare_flutter/model/upcoming_appointment_model.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:nb_utils/nb_utils.dart';

Widget googleCalendar(BuildContext context) {
  return Container(
    padding: EdgeInsets.all(16),
    width: 260,
    alignment: Alignment.center,
    decoration:
        boxDecorationDefault(borderRadius: radius(), color: context.cardColor),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image.asset(ic_google_calendar,
            height: 32, width: 32, fit: BoxFit.cover),
        16.width,
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              appStore.userDoctorGoogleCal == ON
                  ? '${locale.lblConnectedWith} ${appStore.googleEmail}'
                  : "${locale.lblConnectWithGoogle}",
              style: secondaryTextStyle(),
              textAlign: TextAlign.center,
            ),
            8.height,
            Text("${locale.lblGoogleCalendarConfiguration}",
                style: boldTextStyle(size: 16)),
          ],
        ).expand(),
      ],
    ),
  );
}

List<LanguageDataModel> languageList() {
  return [
    LanguageDataModel(
        id: 0,
        name: locale.lblHindi,
        languageCode: 'pt',
        fullLanguageCode: 'pt-BR',
        flag: flagsIcBrazil),
    LanguageDataModel(
        id: 1,
        name: locale.lblEnglish,
        languageCode: 'en',
        fullLanguageCode: 'en-US',
        flag: flagsIcUs),
    LanguageDataModel(
        id: 2,
        name: locale.lblArabic,
        languageCode: 'ar',
        fullLanguageCode: 'ar-AR',
        flag: flagsIcAr),
    LanguageDataModel(
        id: 3,
        name: locale.lblGerman,
        languageCode: 'de',
        fullLanguageCode: 'de-DE',
        flag: flagsIcGermany),
    LanguageDataModel(
        id: 4,
        name: locale.lblFrench,
        languageCode: 'fr',
        fullLanguageCode: 'fr-FR',
        flag: flagsIcFrench),
  ];
}

List<DemoLoginModel> demoLoginList() {
  List<DemoLoginModel> demoLoginListData = [];
  demoLoginListData
      .add(DemoLoginModel(loginTypeImage: "images/icons/user.png"));
  demoLoginListData
      .add(DemoLoginModel(loginTypeImage: "images/icons/receptionistIcon.png"));
  demoLoginListData
      .add(DemoLoginModel(loginTypeImage: "images/icons/doctorIcon.png"));

  return demoLoginListData;
}

List<String> getServicesImages() {
  List<String> images = [];

  for (int i = 1; i < 6; i++) {
    images.add("images/services_icon/services$i.png");
  }
  return images;
}

List<WeeklyAppointment> emptyGraphListMonthly = [
  WeeklyAppointment(x: "W1", y: 0),
  WeeklyAppointment(x: "W2", y: 0),
  WeeklyAppointment(x: "W3", y: 0),
  WeeklyAppointment(x: "W4", y: 0),
  WeeklyAppointment(x: "W5", y: 0),
];

List<WeeklyAppointment> emptyGraphListYearly = [
  WeeklyAppointment(x: locale.lblJan, y: 0),
  WeeklyAppointment(x: locale.lblFeb, y: 0),
  WeeklyAppointment(x: locale.lblMar, y: 0),
  WeeklyAppointment(x: locale.lblApr, y: 0),
  WeeklyAppointment(x: locale.lblMay, y: 0),
  WeeklyAppointment(x: locale.lblJun, y: 0),
  WeeklyAppointment(x: locale.lblJul, y: 0),
  WeeklyAppointment(x: locale.lblAug, y: 0),
  WeeklyAppointment(x: locale.lblSep, y: 0),
  WeeklyAppointment(x: locale.lblOct, y: 0),
  WeeklyAppointment(x: locale.lblNov, y: 0),
  WeeklyAppointment(x: locale.lblDec, y: 0),
];
