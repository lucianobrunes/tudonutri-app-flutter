import 'package:kivicare_flutter/model/dashboard_model.dart';
import 'package:kivicare_flutter/model/upcoming_appointment_model.dart';
import 'package:kivicare_flutter/model/user_model.dart';

DashboardModel? cachedDashboardModel;

List<UpcomingAppointmentModel>? cachedDoctorAppointment;

List<UserModel>? cachedDoctorPatient;
List<UserModel>? cachedDoctorList;
