import 'dart:async';

import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/no_connection_screen.dart';
import 'package:nb_utils/nb_utils.dart';

import '../main.dart';

class InternetConnectivity {
  static StreamSubscription<ConnectivityResult>? subscription;

  static void startMonitoringConnectivity({required BuildContext context}) {
    subscription = Connectivity().onConnectivityChanged.listen((result) {
      if (result == ConnectivityResult.none) {
        NoConnectionScreen().launch(getContext);
      } else {
        finish(getContext);
        toast(locale.lblConnectionReEstablished);
      }
    });
  }

  static void stopMonitoringConnectivity() {
    subscription!.cancel(); // Stop monitoring the connectivity
  }
}

class NetworkConnectivityWidget extends StatefulWidget {
  final Widget defaultWidget;

  NetworkConnectivityWidget({required this.defaultWidget});

  @override
  _NetworkConnectivityWidgetState createState() => _NetworkConnectivityWidgetState();
}

class _NetworkConnectivityWidgetState extends State<NetworkConnectivityWidget> {
  ConnectivityResult? _connectionStatus;

  @override
  void initState() {
    super.initState();
    initConnectivity();
  }

  Future<void> initConnectivity() async {
    ConnectivityResult? result;
    try {
      result = await Connectivity().checkConnectivity();
    } catch (e) {
      print(e.toString());
    }

    if (!mounted) {
      return;
    }

    setState(() {
      _connectionStatus = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<ConnectivityResult>(
      stream: Connectivity().onConnectivityChanged,
      builder: (BuildContext context, AsyncSnapshot<ConnectivityResult> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return _buildConnectingWidget();
        }
        if (snapshot.hasError) {
          return _buildErrorWidget();
        }

        final ConnectivityResult connectivityResult = snapshot.data!;

        if (connectivityResult == ConnectivityResult.none) {
          return NoDataWidget();
        }

        return widget.defaultWidget;
      },
    );
  }

  Widget _buildConnectingWidget() {
    return Scaffold(
      appBar: AppBar(
        title: Text('${locale.lblConnecting}...'),
      ),
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _buildErrorWidget() {
    return Scaffold(
      appBar: AppBar(
        title: Text(locale.lblError),
      ),
      body: Center(
        child: Text('${locale.lblAnErrorOccurredWhileCheckingInternetConnectivity}'),
      ),
    );
  }

  Widget _buildConnectedWidget() {
    return Scaffold(
      appBar: AppBar(
        title: Text(locale.lblConnected),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              '${locale.lblNetworkStatus}:',
              style: TextStyle(fontSize: 20),
            ),
            SizedBox(height: 16),
            Text(
              _getConnectionStatusText(),
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }

  String _getConnectionStatusText() {
    switch (_connectionStatus) {
      case ConnectivityResult.none:
        return locale.lblOffline;
      case ConnectivityResult.wifi:
        return '${locale.lblConnected} ${locale.lblToWifi} ';
      case ConnectivityResult.mobile:
        return '${locale.lblConnected} ${locale.lblToMobileData}';
      default:
        return locale.lblUnknown;
    }
  }
}
