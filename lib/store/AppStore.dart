import 'dart:convert';

import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:kivicare_flutter/config.dart';
import 'package:kivicare_flutter/locale/app_localizations.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:mobx/mobx.dart';
import 'package:nb_utils/nb_utils.dart';

part 'AppStore.g.dart';

class AppStore = AppStoreBase with _$AppStore;

abstract class AppStoreBase with Store {
  @observable
  bool isDarkModeOn = false;

  @observable
  bool isLoading = false;

  @observable
  bool isLoggedIn = false;

  @observable
  bool isBookedFromDashboard = false;

  @observable
  String mStatus = "All";

  @observable
  int? restrictAppointmentPost;

  @observable
  int? restrictAppointmentPre;

  @observable
  String? currency;

  @observable
  String? currencyPreFix;

  @observable
  String? tempBaseUrl;

  @observable
  bool? userTelemedOn;

  @observable
  bool? userProEnabled;

  @observable
  String? userEnableGoogleCal;

  @observable
  String? userDoctorGoogleCal;

  @observable
  String? googleEmail;

  @observable
  String? telemedType;

  @observable
  String? globalDateFormat;

  @observable
  String selectedLanguageCode = DEFAULT_LANGUAGE;

  @observable
  String? demoDoctor;

  @observable
  String? demoReceptionist;

  @observable
  String? demoPatient;

  @observable
  bool? userMeetService;

  @observable
  bool? zoomService;

  @observable
  String selectedLanguage = DEFAULT_LANGUAGE;

  @observable
  List<dynamic> demoEmails = [];

  @observable
  int? clinicId;

  @action
  void setDemoEmails() {
    String temp = FirebaseRemoteConfig.instance.getString(DEMO_EMAILS);
    log(temp);

    if (temp.isNotEmpty && temp.isJson()) {
      demoEmails = jsonDecode(temp) as List<dynamic>;
    } else {
      log('');
    }
  }

  Future<void> setDarkMode(bool aIsDarkMode) async {
    isDarkModeOn = aIsDarkMode;

    if (isDarkModeOn) {
      textPrimaryColorGlobal = textPrimaryDarkColor;
      textSecondaryColorGlobal = textSecondaryWhiteColor;
      defaultLoaderBgColorGlobal = cardSelectedColor;
      selectedColor = Color(0xF4B4A4A);
    } else {
      textPrimaryColorGlobal = textPrimaryLightColor;
      textSecondaryColorGlobal = textSecondaryLightColor;
      defaultLoaderBgColorGlobal = Colors.white;
      selectedColor = getColorFromHex('#e6ecfa');
    }
  }

  @action
  Future<void> setLoggedIn(bool value) async {
    setValue(IS_LOGGED_IN, value);
    isLoggedIn = value;
  }

  @action
  Future<void> setBookedFromDashboard(bool value) async => isBookedFromDashboard = value;

  @action
  Future<void> setDemoDoctor(String value, {bool initialize = false}) async {
    if (initialize) setValue(DEMO_DOCTOR, value);
    demoDoctor = value;
  }

  @action
  Future<void> setDemoReceptionist(String value, {bool initialize = false}) async {
    if (initialize) setValue(DEMO_RECEPTIONIST, value);
    demoReceptionist = value;
  }

  @action
  Future<void> setDemoPatient(String value, {bool initialize = false}) async {
    if (initialize) setValue(DEMO_PATIENT, value);
    demoPatient = value;
  }

  @action
  Future<void> setGoogleEmail(String value, {bool initialize = false}) async {
    if (initialize) setValue(GOOGLE_EMAIL, value);
    googleEmail = value;
  }

  @action
  Future<void> setRestrictAppointmentPost(int value, {bool initialize = false}) async {
    if (initialize) setValue(RESTRICT_APPOINTMENT_POST, value);
    restrictAppointmentPost = value;
  }

  @action
  Future<void> setRestrictAppointmentPre(int value, {bool initialize = false}) async {
    if (initialize) setValue(RESTRICT_APPOINTMENT_PRE, value);
    restrictAppointmentPre = value;
  }

  @action
  Future<void> setLoading(bool value) async => isLoading = value;

  @action
  Future<void> setCurrency(String value, {bool initialize = false}) async {
    if (initialize) setValue(CURRENCY, value);

    currency = value;
  }

  @action
  Future<void> setCurrencyPostfix(String value, {bool initialize = false}) async {
    if (initialize) setValue(CURRENCY_POST_FIX, value);

    currency = value;
  }

  @action
  Future<void> setBaseUrl(String value, {bool initialize = false}) async {
    if (initialize) setValue(SAVE_BASE_URL, value);
    log("Current Base Url :  $value");
    tempBaseUrl = value;
  }

  @action
  Future<void> setUserTelemedOn(bool value, {bool initialize = false}) async {
    if (initialize) setValue(USER_TELEMED_ON, value);

    userTelemedOn = value;
  }

  @action
  Future<void> setUserProEnabled(bool value, {bool initialize = false}) async {
    if (initialize) setValue(USER_PRO_ENABLED, value);

    userProEnabled = value;
  }

  Future<void> setUserEnableGoogleCal(String value, {bool initialize = false}) async {
    if (initialize) setValue(USER_ENABLE_GOOGLE_CAL, value);

    userEnableGoogleCal = value;
  }

  Future<void> setUserDoctorGoogleCal(String value, {bool initialize = false}) async {
    if (initialize) setValue(DOCTOR_ENABLE_GOOGLE_CAL, value);

    userDoctorGoogleCal = value;
  }

  Future<void> setTelemedType(String value, {bool initialize = false}) async {
    if (initialize) setValue(SET_TELEMED_TYPE, value);

    telemedType = value;
  }

  Future<void> setGlobalDateFormat(String value, {bool initialize = false}) async {
    if (initialize) setValue(GLOBAL_DATE_FORMAT, value);

    globalDateFormat = value;
  }

  Future<void> setUserMeetService(bool value, {bool initialize = false}) async {
    if (initialize) setValue(USER_MEET_SERVICE, value);

    userMeetService = value;
  }

  Future<void> setUserZoomService(bool value, {bool initialize = false}) async {
    if (initialize) setValue(USER_ZOOM_SERVICE, value);

    zoomService = value;
  }

  @action
  Future<void> setLanguage(String val, {BuildContext? context}) async {
    selectedLanguageCode = val;
    selectedLanguageDataModel = getSelectedLanguageModel();

    await setValue(SELECTED_LANGUAGE_CODE, selectedLanguageCode);

    locale = await AppLocalizations().load(Locale(selectedLanguageCode));
  }

  @action
  void setStatus(String aStatus) => mStatus = aStatus;
}
