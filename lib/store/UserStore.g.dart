// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'UserStore.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$UserStore on UserStoreBase, Store {
  late final _$userEmailAtom = Atom(name: 'UserStoreBase.userEmail', context: context);

  @override
  String? get userEmail {
    _$userEmailAtom.reportRead();
    return super.userEmail;
  }

  @override
  set userEmail(String? value) {
    _$userEmailAtom.reportWrite(value, super.userEmail, () {
      super.userEmail = value;
    });
  }

  late final _$profileImageAtom = Atom(name: 'UserStoreBase.profileImage', context: context);

  @override
  String? get profileImage {
    _$profileImageAtom.reportRead();
    return super.profileImage;
  }

  @override
  set profileImage(String? value) {
    _$profileImageAtom.reportWrite(value, super.profileImage, () {
      super.profileImage = value;
    });
  }

  late final _$userIdAtom = Atom(name: 'UserStoreBase.userId', context: context);

  @override
  int? get userId {
    _$userIdAtom.reportRead();
    return super.userId;
  }

  @override
  set userId(int? value) {
    _$userIdAtom.reportWrite(value, super.userId, () {
      super.userId = value;
    });
  }

  late final _$firstNameAtom = Atom(name: 'UserStoreBase.firstName', context: context);

  @override
  String? get firstName {
    _$firstNameAtom.reportRead();
    return super.firstName;
  }

  @override
  set firstName(String? value) {
    _$firstNameAtom.reportWrite(value, super.firstName, () {
      super.firstName = value;
    });
  }

  late final _$lastNameAtom = Atom(name: 'UserStoreBase.lastName', context: context);

  @override
  String? get lastName {
    _$lastNameAtom.reportRead();
    return super.lastName;
  }

  @override
  set lastName(String? value) {
    _$lastNameAtom.reportWrite(value, super.lastName, () {
      super.lastName = value;
    });
  }

  late final _$userRoleAtom = Atom(name: 'UserStoreBase.userRole', context: context);

  @override
  String? get userRole {
    _$userRoleAtom.reportRead();
    return super.userRole;
  }

  @override
  set userRole(String? value) {
    _$userRoleAtom.reportWrite(value, super.userRole, () {
      super.userRole = value;
    });
  }

  late final _$userDisplayNameAtom = Atom(name: 'UserStoreBase.userDisplayName', context: context);

  @override
  String? get userDisplayName {
    _$userDisplayNameAtom.reportRead();
    return super.userDisplayName;
  }

  @override
  set userDisplayName(String? value) {
    _$userDisplayNameAtom.reportWrite(value, super.userDisplayName, () {
      super.userDisplayName = value;
    });
  }

  late final _$userMobileNumberAtom = Atom(name: 'UserStoreBase.userMobileNumber', context: context);

  @override
  String? get userMobileNumber {
    _$userMobileNumberAtom.reportRead();
    return super.userMobileNumber;
  }

  @override
  set userMobileNumber(String? value) {
    _$userMobileNumberAtom.reportWrite(value, super.userMobileNumber, () {
      super.userMobileNumber = value;
    });
  }

  late final _$userGenderAtom = Atom(name: 'UserStoreBase.userGender', context: context);

  @override
  String? get userGender {
    _$userGenderAtom.reportRead();
    return super.userGender;
  }

  @override
  set userGender(String? value) {
    _$userGenderAtom.reportWrite(value, super.userGender, () {
      super.userGender = value;
    });
  }

  @override
  String toString() {
    return '''
userEmail: ${userEmail},
profileImage: ${profileImage},
userId: ${userId},
firstName: ${firstName},
lastName: ${lastName},
userRole: ${userRole},
userDisplayName: ${userDisplayName},
userMobileNumber: ${userMobileNumber},
userGender: ${userGender}
    ''';
  }
}
