// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AppStore.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$AppStore on AppStoreBase, Store {
  late final _$isDarkModeOnAtom = Atom(name: 'AppStoreBase.isDarkModeOn', context: context);

  @override
  bool get isDarkModeOn {
    _$isDarkModeOnAtom.reportRead();
    return super.isDarkModeOn;
  }

  @override
  set isDarkModeOn(bool value) {
    _$isDarkModeOnAtom.reportWrite(value, super.isDarkModeOn, () {
      super.isDarkModeOn = value;
    });
  }

  late final _$isLoadingAtom = Atom(name: 'AppStoreBase.isLoading', context: context);

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  late final _$isLoggedInAtom = Atom(name: 'AppStoreBase.isLoggedIn', context: context);

  @override
  bool get isLoggedIn {
    _$isLoggedInAtom.reportRead();
    return super.isLoggedIn;
  }

  @override
  set isLoggedIn(bool value) {
    _$isLoggedInAtom.reportWrite(value, super.isLoggedIn, () {
      super.isLoggedIn = value;
    });
  }

  late final _$isBookedFromDashboardAtom = Atom(name: 'AppStoreBase.isBookedFromDashboard', context: context);

  @override
  bool get isBookedFromDashboard {
    _$isBookedFromDashboardAtom.reportRead();
    return super.isBookedFromDashboard;
  }

  @override
  set isBookedFromDashboard(bool value) {
    _$isBookedFromDashboardAtom.reportWrite(value, super.isBookedFromDashboard, () {
      super.isBookedFromDashboard = value;
    });
  }

  late final _$mStatusAtom = Atom(name: 'AppStoreBase.mStatus', context: context);

  @override
  String get mStatus {
    _$mStatusAtom.reportRead();
    return super.mStatus;
  }

  @override
  set mStatus(String value) {
    _$mStatusAtom.reportWrite(value, super.mStatus, () {
      super.mStatus = value;
    });
  }

  late final _$restrictAppointmentPostAtom = Atom(name: 'AppStoreBase.restrictAppointmentPost', context: context);

  @override
  int? get restrictAppointmentPost {
    _$restrictAppointmentPostAtom.reportRead();
    return super.restrictAppointmentPost;
  }

  @override
  set restrictAppointmentPost(int? value) {
    _$restrictAppointmentPostAtom.reportWrite(value, super.restrictAppointmentPost, () {
      super.restrictAppointmentPost = value;
    });
  }

  late final _$restrictAppointmentPreAtom = Atom(name: 'AppStoreBase.restrictAppointmentPre', context: context);

  @override
  int? get restrictAppointmentPre {
    _$restrictAppointmentPreAtom.reportRead();
    return super.restrictAppointmentPre;
  }

  @override
  set restrictAppointmentPre(int? value) {
    _$restrictAppointmentPreAtom.reportWrite(value, super.restrictAppointmentPre, () {
      super.restrictAppointmentPre = value;
    });
  }

  late final _$currencyAtom = Atom(name: 'AppStoreBase.currency', context: context);

  @override
  String? get currency {
    _$currencyAtom.reportRead();
    return super.currency;
  }

  @override
  set currency(String? value) {
    _$currencyAtom.reportWrite(value, super.currency, () {
      super.currency = value;
    });
  }

  late final _$tempBaseUrlAtom = Atom(name: 'AppStoreBase.tempBaseUrl', context: context);

  @override
  String? get tempBaseUrl {
    _$tempBaseUrlAtom.reportRead();
    return super.tempBaseUrl;
  }

  @override
  set tempBaseUrl(String? value) {
    _$tempBaseUrlAtom.reportWrite(value, super.tempBaseUrl, () {
      super.tempBaseUrl = value;
    });
  }

  late final _$userTelemedOnAtom = Atom(name: 'AppStoreBase.userTelemedOn', context: context);

  @override
  bool? get userTelemedOn {
    _$userTelemedOnAtom.reportRead();
    return super.userTelemedOn;
  }

  @override
  set userTelemedOn(bool? value) {
    _$userTelemedOnAtom.reportWrite(value, super.userTelemedOn, () {
      super.userTelemedOn = value;
    });
  }

  late final _$userProEnabledAtom = Atom(name: 'AppStoreBase.userProEnabled', context: context);

  @override
  bool? get userProEnabled {
    _$userProEnabledAtom.reportRead();
    return super.userProEnabled;
  }

  @override
  set userProEnabled(bool? value) {
    _$userProEnabledAtom.reportWrite(value, super.userProEnabled, () {
      super.userProEnabled = value;
    });
  }

  late final _$userEnableGoogleCalAtom = Atom(name: 'AppStoreBase.userEnableGoogleCal', context: context);

  @override
  String? get userEnableGoogleCal {
    _$userEnableGoogleCalAtom.reportRead();
    return super.userEnableGoogleCal;
  }

  @override
  set userEnableGoogleCal(String? value) {
    _$userEnableGoogleCalAtom.reportWrite(value, super.userEnableGoogleCal, () {
      super.userEnableGoogleCal = value;
    });
  }

  late final _$userDoctorGoogleCalAtom = Atom(name: 'AppStoreBase.userDoctorGoogleCal', context: context);

  @override
  String? get userDoctorGoogleCal {
    _$userDoctorGoogleCalAtom.reportRead();
    return super.userDoctorGoogleCal;
  }

  @override
  set userDoctorGoogleCal(String? value) {
    _$userDoctorGoogleCalAtom.reportWrite(value, super.userDoctorGoogleCal, () {
      super.userDoctorGoogleCal = value;
    });
  }

  late final _$googleUsernameAtom = Atom(name: 'AppStoreBase.googleUsername', context: context);

  late final _$googleEmailAtom = Atom(name: 'AppStoreBase.googleEmail', context: context);

  @override
  String? get googleEmail {
    _$googleEmailAtom.reportRead();
    return super.googleEmail;
  }

  @override
  set googleEmail(String? value) {
    _$googleEmailAtom.reportWrite(value, super.googleEmail, () {
      super.googleEmail = value;
    });
  }

  late final _$googlePhotoURLAtom = Atom(name: 'AppStoreBase.googlePhotoURL', context: context);

  late final _$telemedTypeAtom = Atom(name: 'AppStoreBase.telemedType', context: context);

  @override
  String? get telemedType {
    _$telemedTypeAtom.reportRead();
    return super.telemedType;
  }

  @override
  set telemedType(String? value) {
    _$telemedTypeAtom.reportWrite(value, super.telemedType, () {
      super.telemedType = value;
    });
  }

  late final _$globalDateFormatAtom = Atom(name: 'AppStoreBase.globalDateFormat', context: context);

  @override
  String? get globalDateFormat {
    _$globalDateFormatAtom.reportRead();
    return super.globalDateFormat;
  }

  @override
  set globalDateFormat(String? value) {
    _$globalDateFormatAtom.reportWrite(value, super.globalDateFormat, () {
      super.globalDateFormat = value;
    });
  }

  late final _$selectedLanguageCodeAtom = Atom(name: 'AppStoreBase.selectedLanguageCode', context: context);

  @override
  String get selectedLanguageCode {
    _$selectedLanguageCodeAtom.reportRead();
    return super.selectedLanguageCode;
  }

  @override
  set selectedLanguageCode(String value) {
    _$selectedLanguageCodeAtom.reportWrite(value, super.selectedLanguageCode, () {
      super.selectedLanguageCode = value;
    });
  }

  late final _$demoDoctorAtom = Atom(name: 'AppStoreBase.demoDoctor', context: context);

  @override
  String? get demoDoctor {
    _$demoDoctorAtom.reportRead();
    return super.demoDoctor;
  }

  @override
  set demoDoctor(String? value) {
    _$demoDoctorAtom.reportWrite(value, super.demoDoctor, () {
      super.demoDoctor = value;
    });
  }

  late final _$demoReceptionistAtom = Atom(name: 'AppStoreBase.demoReceptionist', context: context);

  @override
  String? get demoReceptionist {
    _$demoReceptionistAtom.reportRead();
    return super.demoReceptionist;
  }

  @override
  set demoReceptionist(String? value) {
    _$demoReceptionistAtom.reportWrite(value, super.demoReceptionist, () {
      super.demoReceptionist = value;
    });
  }

  late final _$demoPatientAtom = Atom(name: 'AppStoreBase.demoPatient', context: context);

  @override
  String? get demoPatient {
    _$demoPatientAtom.reportRead();
    return super.demoPatient;
  }

  @override
  set demoPatient(String? value) {
    _$demoPatientAtom.reportWrite(value, super.demoPatient, () {
      super.demoPatient = value;
    });
  }

  late final _$userMeetServiceAtom = Atom(name: 'AppStoreBase.userMeetService', context: context);

  @override
  bool? get userMeetService {
    _$userMeetServiceAtom.reportRead();
    return super.userMeetService;
  }

  @override
  set userMeetService(bool? value) {
    _$userMeetServiceAtom.reportWrite(value, super.userMeetService, () {
      super.userMeetService = value;
    });
  }

  late final _$zoomServiceAtom = Atom(name: 'AppStoreBase.zoomService', context: context);

  @override
  bool? get zoomService {
    _$zoomServiceAtom.reportRead();
    return super.zoomService;
  }

  @override
  set zoomService(bool? value) {
    _$zoomServiceAtom.reportWrite(value, super.zoomService, () {
      super.zoomService = value;
    });
  }

  late final _$selectedLanguageAtom = Atom(name: 'AppStoreBase.selectedLanguage', context: context);

  @override
  String get selectedLanguage {
    _$selectedLanguageAtom.reportRead();
    return super.selectedLanguage;
  }

  @override
  set selectedLanguage(String value) {
    _$selectedLanguageAtom.reportWrite(value, super.selectedLanguage, () {
      super.selectedLanguage = value;
    });
  }

  late final _$demoEmailsAtom = Atom(name: 'AppStoreBase.demoEmails', context: context);

  @override
  List<dynamic> get demoEmails {
    _$demoEmailsAtom.reportRead();
    return super.demoEmails;
  }

  @override
  set demoEmails(List<dynamic> value) {
    _$demoEmailsAtom.reportWrite(value, super.demoEmails, () {
      super.demoEmails = value;
    });
  }

  late final _$setLoggedInAsyncAction = AsyncAction('AppStoreBase.setLoggedIn', context: context);

  @override
  Future<void> setLoggedIn(bool value) {
    return _$setLoggedInAsyncAction.run(() => super.setLoggedIn(value));
  }

  late final _$setBookedFromDashboardAsyncAction = AsyncAction('AppStoreBase.setBookedFromDashboard', context: context);

  @override
  Future<void> setBookedFromDashboard(bool value) {
    return _$setBookedFromDashboardAsyncAction.run(() => super.setBookedFromDashboard(value));
  }

  late final _$setUserEmailAsyncAction = AsyncAction('AppStoreBase.setUserEmail', context: context);

  late final _$setUserProfileAsyncAction = AsyncAction('AppStoreBase.setUserProfile', context: context);

  late final _$setDemoDoctorAsyncAction = AsyncAction('AppStoreBase.setDemoDoctor', context: context);

  @override
  Future<void> setDemoDoctor(String value, {bool initialize = false}) {
    return _$setDemoDoctorAsyncAction.run(() => super.setDemoDoctor(value, initialize: initialize));
  }

  late final _$setDemoReceptionistAsyncAction = AsyncAction('AppStoreBase.setDemoReceptionist', context: context);

  @override
  Future<void> setDemoReceptionist(String value, {bool initialize = false}) {
    return _$setDemoReceptionistAsyncAction.run(() => super.setDemoReceptionist(value, initialize: initialize));
  }

  late final _$setDemoPatientAsyncAction = AsyncAction('AppStoreBase.setDemoPatient', context: context);

  @override
  Future<void> setDemoPatient(String value, {bool initialize = false}) {
    return _$setDemoPatientAsyncAction.run(() => super.setDemoPatient(value, initialize: initialize));
  }

  late final _$setGoogleUsernameAsyncAction = AsyncAction('AppStoreBase.setGoogleUsername', context: context);

  late final _$setGoogleEmailAsyncAction = AsyncAction('AppStoreBase.setGoogleEmail', context: context);

  @override
  Future<void> setGoogleEmail(String value, {bool initialize = false}) {
    return _$setGoogleEmailAsyncAction.run(() => super.setGoogleEmail(value, initialize: initialize));
  }

  late final _$setGooglePhotoURLAsyncAction = AsyncAction('AppStoreBase.setGooglePhotoURL', context: context);

  late final _$setRestrictAppointmentPostAsyncAction = AsyncAction('AppStoreBase.setRestrictAppointmentPost', context: context);

  @override
  Future<void> setRestrictAppointmentPost(int value, {bool initialize = false}) {
    return _$setRestrictAppointmentPostAsyncAction.run(() => super.setRestrictAppointmentPost(value, initialize: initialize));
  }

  late final _$setRestrictAppointmentPreAsyncAction = AsyncAction('AppStoreBase.setRestrictAppointmentPre', context: context);

  @override
  Future<void> setRestrictAppointmentPre(int value, {bool initialize = false}) {
    return _$setRestrictAppointmentPreAsyncAction.run(() => super.setRestrictAppointmentPre(value, initialize: initialize));
  }

  late final _$setUserIdAsyncAction = AsyncAction('AppStoreBase.setUserId', context: context);

  late final _$setFirstNameAsyncAction = AsyncAction('AppStoreBase.setFirstName', context: context);

  late final _$setLastNameAsyncAction = AsyncAction('AppStoreBase.setLastName', context: context);

  late final _$setLoadingAsyncAction = AsyncAction('AppStoreBase.setLoading', context: context);

  @override
  Future<void> setLoading(bool value) {
    return _$setLoadingAsyncAction.run(() => super.setLoading(value));
  }

  late final _$setRoleAsyncAction = AsyncAction('AppStoreBase.setRole', context: context);

  late final _$setUserDisplayNameAsyncAction = AsyncAction('AppStoreBase.setUserDisplayName', context: context);

  late final _$setUserMobileNumberAsyncAction = AsyncAction('AppStoreBase.setUserMobileNumber', context: context);

  late final _$setUserGenderAsyncAction = AsyncAction('AppStoreBase.setUserGender', context: context);

  late final _$setCurrencyAsyncAction = AsyncAction('AppStoreBase.setCurrency', context: context);

  @override
  Future<void> setCurrency(String value, {bool initialize = false}) {
    return _$setCurrencyAsyncAction.run(() => super.setCurrency(value, initialize: initialize));
  }

  late final _$setBaseUrlAsyncAction = AsyncAction('AppStoreBase.setBaseUrl', context: context);

  @override
  Future<void> setBaseUrl(String value, {bool initialize = false}) {
    return _$setBaseUrlAsyncAction.run(() => super.setBaseUrl(value, initialize: initialize));
  }

  late final _$setUserTelemedOnAsyncAction = AsyncAction('AppStoreBase.setUserTelemedOn', context: context);

  @override
  Future<void> setUserTelemedOn(bool value, {bool initialize = false}) {
    return _$setUserTelemedOnAsyncAction.run(() => super.setUserTelemedOn(value, initialize: initialize));
  }

  late final _$setUserProEnabledAsyncAction = AsyncAction('AppStoreBase.setUserProEnabled', context: context);

  @override
  Future<void> setUserProEnabled(bool value, {bool initialize = false}) {
    return _$setUserProEnabledAsyncAction.run(() => super.setUserProEnabled(value, initialize: initialize));
  }

  late final _$setLanguageAsyncAction = AsyncAction('AppStoreBase.setLanguage', context: context);

  @override
  Future<void> setLanguage(String val, {BuildContext? context}) {
    return _$setLanguageAsyncAction.run(() => super.setLanguage(val, context: context));
  }

  late final _$AppStoreBaseActionController = ActionController(name: 'AppStoreBase', context: context);

  @override
  void setDemoEmails() {
    final _$actionInfo = _$AppStoreBaseActionController.startAction(name: 'AppStoreBase.setDemoEmails');
    try {
      return super.setDemoEmails();
    } finally {
      _$AppStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setStatus(String aStatus) {
    final _$actionInfo = _$AppStoreBaseActionController.startAction(name: 'AppStoreBase.setStatus');
    try {
      return super.setStatus(aStatus);
    } finally {
      _$AppStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
isDarkModeOn: ${isDarkModeOn},
isLoading: ${isLoading},
isLoggedIn: ${isLoggedIn},
isBookedFromDashboard: ${isBookedFromDashboard},
mStatus: ${mStatus},
restrictAppointmentPost: ${restrictAppointmentPost},
restrictAppointmentPre: ${restrictAppointmentPre},
currency: ${currency},
tempBaseUrl: ${tempBaseUrl},
userTelemedOn: ${userTelemedOn},
userProEnabled: ${userProEnabled},
userEnableGoogleCal: ${userEnableGoogleCal},
userDoctorGoogleCal: ${userDoctorGoogleCal},

googleEmail: ${googleEmail},

telemedType: ${telemedType},
globalDateFormat: ${globalDateFormat},
selectedLanguageCode: ${selectedLanguageCode},
demoDoctor: ${demoDoctor},
demoReceptionist: ${demoReceptionist},
demoPatient: ${demoPatient},
userMeetService: ${userMeetService},
zoomService: ${zoomService},
selectedLanguage: ${selectedLanguage},
demoEmails: ${demoEmails}
    ''';
  }
}
