import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/cached_image_widget.dart';
import 'package:kivicare_flutter/components/name_initials_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/screens/doctor/screens/edit_doctor_profile_screen.dart';
import 'package:kivicare_flutter/screens/receptionist/screens/edit_profile_screen.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:nb_utils/nb_utils.dart';

class DashboardTopProfileWidget extends StatelessWidget {
  const DashboardTopProfileWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (isDoctor())
          EditDoctorProfileScreen().launch(context);
        else {
          EditProfilesScreen().launch(context);
        }
      },
      child: Container(
        decoration: boxDecorationDefault(color: context.primaryColor, border: Border.all(color: white, width: 4), shape: BoxShape.circle),
        padding: userStore.profileImage.validate().isNotEmpty ? EdgeInsets.zero : EdgeInsets.all(12),
        margin: EdgeInsets.only(right: 16),
        child: userStore.profileImage.validate().isNotEmpty
            ? CachedImageWidget(
                url: userStore.profileImage.validate(),
                fit: BoxFit.cover,
                height: 46,
                width: 46,
                circle: true,
                alignment: Alignment.center,
              )
            : NameInitialsWidget(
                firstName: userStore.firstName.validate(value: 'K'),
                lastName: userStore.lastName.validate(value: 'V'),
                color: Colors.white,
              ),
      ),
    );
  }
}
