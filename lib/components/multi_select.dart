import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:kivicare_flutter/components/body_widget.dart';
import 'package:kivicare_flutter/components/cached_image_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/components/price_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/service_model.dart';
import 'package:kivicare_flutter/network/service_repository.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:nb_utils/nb_utils.dart';

// ignore: must_be_immutable
class MultiSelectWidget extends StatefulWidget {
  final int? id;
  List<String>? selectedServicesId;

  MultiSelectWidget({this.id, this.selectedServicesId});

  @override
  _MultiSelectWidgetState createState() => _MultiSelectWidgetState();
}

class _MultiSelectWidgetState extends State<MultiSelectWidget> {
  TextEditingController search = TextEditingController();

  List<ServiceData> servicesList = [];

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    getData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void getData() async {
    appStore.setLoading(true);
    await getServiceResponseAPI(id: appointmentAppStore.mDoctorSelected?.iD ?? userStore.userId.validate()).then((value) {
      servicesList = value.serviceData.validate();
      if (widget.selectedServicesId != null) {
        multiSelectStore.clearList();
        servicesList.forEach((element) {
          if (widget.selectedServicesId.validate().contains(element.id)) {
            element.isCheck = true;
            multiSelectStore.addSingleItem(element, isClear: false);
          }
        });
      }
      setState(() {});
    }).catchError((e) {
      toast(e.toString(), print: true);
    });
    appStore.setLoading(false);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  Future multiSelectDialog() async {
    await showInDialog(
      context,
      contentPadding: EdgeInsets.all(0),
      builder: (p0) {
        return Container(
          width: context.width(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('${locale.lblNote}: ', style: boldTextStyle()).paddingSymmetric(horizontal: 16, vertical: 16),
              ...[
                TextIcon(
                  prefix: ic_telemed.iconImage(),
                  text: locale.lblTelemedServiceIsAvailable,
                  spacing: 16,
                  textStyle: primaryTextStyle(size: 14),
                ).paddingSymmetric(horizontal: 8),
                TextIcon(
                  prefix: ic_multi_select.iconImage(),
                  spacing: 16,
                  text: locale.lblMultipleSelectionIsAvailableForThisService,
                  expandedText: true,
                  maxLine: 2,
                  textStyle: primaryTextStyle(size: 14),
                ).paddingSymmetric(horizontal: 8),
              ],
              32.height,
              Align(
                alignment: Alignment.bottomRight,
                child: TextButton(
                  onPressed: () {
                    finish(context);
                  },
                  child: Text(locale.lblClose, style: primaryTextStyle(color: primaryColor, size: 14)),
                ),
              ).paddingRight(8),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(
        locale.lblSelectServices,
        textColor: Colors.white,
        systemUiOverlayStyle: defaultSystemUiOverlayStyle(context),
        actions: [
          IconButton(
            onPressed: () {
              multiSelectDialog();
            },
            icon: Icon(Icons.info_outline_rounded, color: white),
          ).paddingRight(8),
        ],
      ),
      body: Body(
        child: Stack(
          children: [
            Observer(builder: (context) {
              return NoDataFoundWidget().center().visible(!appStore.isLoading && servicesList.validate().isEmpty);
            }),
            AnimatedListView(
              itemCount: servicesList.length,
              shrinkWrap: true,
              padding: EdgeInsets.only(top: 0, bottom: 80, right: 16, left: 16),
              itemBuilder: (context, index) {
                ServiceData data = servicesList[index];
                if (data.name.isEmptyOrNull) return Offstage();
                return Padding(
                  padding: EdgeInsets.symmetric(vertical: 8),
                  child: Theme(
                    data: ThemeData(unselectedWidgetColor: primaryColor),
                    child: CheckboxListTile(
                      contentPadding: EdgeInsets.all(8),
                      checkboxShape: RoundedRectangleBorder(borderRadius: radius(4)),
                      controlAffinity: ListTileControlAffinity.trailing,
                      tileColor: context.cardColor,
                      secondary: CachedImageWidget(url: data.image.validate(), height: 50, circle: true),
                      shape: RoundedRectangleBorder(borderRadius: radius()),
                      value: data.isCheck,
                      title: Row(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${data.name.capitalizeFirstLetter().validate()}",
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: primaryTextStyle(),
                              ),
                              4.height,
                              PriceWidget(price: data.charges.validate(), textStyle: boldTextStyle()),
                            ],
                          ).expand(),
                          8.width,
                          Row(
                            children: [
                              if (data.telemedService.validate()) ic_telemed.iconImage(size: 20),
                              if (data.telemedService.validate() && data.multiple.validate()) 8.width,
                              if (data.multiple.validate()) ic_multi_select.iconImage(size: 20),
                            ],
                          ),
                        ],
                      ),
                      onChanged: (v) async {
                        if (data.multiple.validate()) {
                          servicesList.forEach((element) {
                            if (element.multiple.validate() == false && element.isCheck) {
                              element.isCheck = false;
                              multiSelectStore.removeItem(element);
                            }
                          });
                          data.isCheck = !data.isCheck;
                          if (data.isCheck) {
                            multiSelectStore.addSingleItem(data, isClear: false);
                          } else {
                            multiSelectStore.removeItem(data);
                          }
                        } else {
                          if (servicesList.where((element) => element.isCheck.validate()).length > 1) {
                            await multiSelectDialog();
                          }
                          servicesList.forEach((element) {
                            if (element.isCheck) {
                              element.isCheck = false;
                            }
                          });
                          data.isCheck = !data.isCheck;
                          if (data.isCheck) {
                            multiSelectStore.addSingleItem(data, isClear: true);
                          } else {
                            multiSelectStore.removeItem(data);
                          }
                        }
                        setState(() {});
                      },
                    ),
                  ),
                );
              },
            ).paddingTop(16),
            /* AppTextField(
              decoration: inputDecoration(context: context, labelText: locale.lblSearch),
              controller: search,
              onChanged: onSearchTextChanged,
              autoFocus: false,
              textInputAction: TextInputAction.go,
              textFieldType: TextFieldType.OTHER,
              suffix: Icon(Icons.search, size: 20),
            ).paddingOnly(bottom: 0, right: 16, left: 16, top: 16),*/
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.done),
        onPressed: () {
          finish(context);
        },
      ),
    );
  }
}
