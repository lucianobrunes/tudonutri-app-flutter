import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/body_widget.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/encounter_model.dart';
import 'package:kivicare_flutter/network/encounter_repository.dart';
import 'package:kivicare_flutter/screens/encounter/component/encounter_component.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:nb_utils/nb_utils.dart';

class PatientEncounterListScreen extends StatefulWidget {
  @override
  _PatientEncounterScreenListState createState() => _PatientEncounterScreenListState();
}

class _PatientEncounterScreenListState extends State<PatientEncounterListScreen> {
  Future<List<EncounterModel>>? future;

  List<EncounterModel> encounterList = [];

  int total = 0;
  int page = 1;
  bool isLastPage = false;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    future = getPatientEncounterList(id: (isReceptionist() || isDoctor()) ? null : userStore.userId.validate(), page: page, encounterList: encounterList, getTotalPatient: (b) => total = b, lastPageCallback: (b) => isLastPage = b);
  }

  Future<void> deleteEncounter(int encounterId) async {
    Map request = {"encounter_id": encounterId};

    appStore.setLoading(true);

    await deleteEncounterData(request).then((value) {
      appStore.setLoading(false);
      toast(value['message']);
      init();
      setState(() {});
    }).catchError((e) {
      log(e.toString());
    });
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    getDisposeStatusBarColor();
    super.dispose();
  }

  Widget buildBodyWidget() {
    return Body(
      child: SnapHelperWidget<List<EncounterModel>>(
        future: future,
        loadingWidget: LoaderWidget(),
        onSuccess: (snap) {
          if (snap.isEmpty) {
            return NoDataFoundWidget(text: locale.lblNoEncounterFound);
          }

          return AnimatedListView(
            physics: AlwaysScrollableScrollPhysics(),
            itemCount: snap.length,
            disposeScrollController: true,
            padding: EdgeInsets.only(bottom: 80, top: 16, right: 16, left: 16),
            onSwipeRefresh: () async {
              page = 1;
              init();
              return await 2.seconds.delay;
            },
            onNextPage: () {
              if (!isLastPage) {
                page++;
                init();
                setState(() {});
              }
            },
            shrinkWrap: true,
            listAnimationType: listAnimationType,
            slideConfiguration: SlideConfiguration(verticalOffset: 400),
            itemBuilder: (_, index) {
              return EncounterComponent(
                data: snap[index],
                deleteEncounter: deleteEncounter,
                callForRefresh: () {
                  init();
                  setState(() {});
                },
              );
            },
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(
        "${locale.lblPatientsEncounter} ${total != 0 ? "($total)" : ""}",
        textColor: Colors.white,
        systemUiOverlayStyle: defaultSystemUiOverlayStyle(context),
      ),
      body: buildBodyWidget(),
      // floatingActionButton: (isReceptionist() || isDoctor())
      //     ? FloatingActionButton(
      //         child: Icon(Icons.add),
      //         onPressed: () {
      //           AddEncounterScreen().launch(context).then((value) {
      //             init();
      //             setState(() {});
      //           });
      //         })
      //     : Offstage(),
    );
  }
}
