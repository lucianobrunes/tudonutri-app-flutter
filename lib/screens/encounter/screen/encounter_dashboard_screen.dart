import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/body_widget.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/components/status_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/encounter_model.dart';
import 'package:kivicare_flutter/network/dashboard_repository.dart';
import 'package:kivicare_flutter/network/encounter_repository.dart';
import 'package:kivicare_flutter/screens/doctor/screens/bill_details_screen.dart';
import 'package:kivicare_flutter/screens/doctor/screens/generate_bill_screen.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/enums.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:kivicare_flutter/screens/encounter/component/encounter_expandable_view.dart';

class EncounterDashboardScreen extends StatefulWidget {
  final String? encounterId;

  EncounterDashboardScreen({this.encounterId});

  @override
  State<EncounterDashboardScreen> createState() =>
      _EncounterDashboardScreenState();
}

class _EncounterDashboardScreenState extends State<EncounterDashboardScreen> {
  Future<EncounterModel>? future;

  FilePickerResult? result;
  File? file;

  String encounterStatus = '';

  bool isProblem = false,
      isNotes = false,
      isObservation = false,
      isPrescription = false,
      isMedicalReport = false;

  DateTime current = DateTime.now();

  @override
  void initState() {
    super.initState();
    init();
  }

  Future<void> init() async {
    future = getEncounterDetailsDashBoardAPI(
        encounterId: widget.encounterId.toInt());

    /*  appStore.setLoading(true);
    await getEncounterDetailsDashBoardAPI(widget.id.toInt()).then((value) async {
      bool canUpdate = value.paymentStatus != "paid";
      encounterData = value;

      if (isProEnabled()) {
        paymentStatus = value.paymentStatus;
        setState(() {});
      }
      appStore.setLoading(false);
    }).catchError((e) {
      log(e.toString());
      appStore.setLoading(false);
    });*/
  }

  void closeEncounter({String? appointmentId, bool isCheckOut = false}) async {
    Map<String, dynamic> request = {
      "encounter_id": widget.encounterId,
      "appointment_id": appointmentId.validate(),
    };

    log(isCheckOut);
    if (isCheckOut) {
      request.putIfAbsent("appointment_status", () => CheckOutStatusInt);
    } else {
      request.putIfAbsent("appointment_status", () => CheckInStatusInt);
    }

    appStore.setLoading(true);

    await encounterClose(request).then((value) {
      appStore.setLoading(false);
      toast(value.message);
      finish(context, true);
      //redirectionCase(context, message: value.message.validate(), finishCount: 1);
    }).catchError((e) {
      appStore.setLoading(false);
      toast(e.toString());
    });
  }

  void _handleCloseEncounterClick(
      {required EncounterModel encounterData, bool isCheckOut = false}) async {
    if (encounterData.isBilling.validate()) {
      await GenerateBillScreen(data: encounterData)
          .launch(context)
          .then((value) {
        if (value != null) {
          if (value == "paid") {
            closeEncounter(
                isCheckOut: value == "paid",
                appointmentId: encounterData.appointmentId);
            //closeEncounter(isCheckOut: (isCheckOut && value == "paid"), appointmentId: encounterData.appointmentId);
          }
          if (value == 'unpaid') {
            toast(locale.lblPlease +
                " ${locale.lblUpdate}" +
                ' ${locale.lblPaymentStatus}');
            init();
            setState(() {});
          }
        }
      });
    } else {
      showConfirmDialogCustom(
        context,
        title: locale.lblEncounterWillBeClosed,
        dialogType: DialogType.CONFIRMATION,
        onAccept: (p0) {
          closeEncounter(
              isCheckOut: isCheckOut,
              appointmentId: encounterData.appointmentId);
        },
      );
    }
  }

  Widget buildEncounterDetailWidget({required EncounterModel encounterData}) {
    return SizedBox(
      height: encounterData.status == ClosedEncounterStatusInt.toString()
          ? context.height()
          : context.height() - 180,
      child: AnimatedScrollView(
        padding: EdgeInsets.fromLTRB(16, 16, 16, 120),
        onSwipeRefresh: () async {
          appStore.setLoading(true);
          init();
          appStore.setLoading(false);
          return await 2.seconds.delay;
        },
        children: [
          Align(
            alignment: Alignment.centerRight,
            child: StatusWidget(
              status: encounterData.status.validate(),
              isEncounterStatus: true,
            ),
          ),
          12.height,
          encounterDetail(encounterData: encounterData)
              .paddingSymmetric(vertical: 6),
          16.height,
          //region Others
          buildExpandableWidget(
            title: PROBLEM,
            isExpanded: isProblem,
            encounterData: encounterData,
            encounterType: EncounterTypeEnum.OTHERS,
            encounterTypeValue: EncounterTypeValues.PROBLEM,
            onTap: () {
              isProblem = !isProblem;
              setState(() {});
            },
          ),
          16.height,
          buildExpandableWidget(
            title: OBSERVATION,
            encounterData: encounterData,
            isExpanded: isObservation,
            encounterTypeValue: EncounterTypeValues.OBSERVATION,
            encounterType: EncounterTypeEnum.OTHERS,
            onTap: () {
              isObservation = !isObservation;
              setState(() {});
            },
          ),
          16.height,
          buildExpandableWidget(
            title: NOTE,
            isExpanded: isNotes,
            encounterData: encounterData,
            encounterTypeValue: EncounterTypeValues.NOTE,
            encounterType: EncounterTypeEnum.OTHERS,
            onTap: () {
              isNotes = !isNotes;
              setState(() {});
            },
          ),
          16.height,
          //endregion
          //region Prescriptions
          buildExpandableWidget(
            title: PRESCRIPTION,
            isExpanded: isPrescription,
            encounterData: encounterData,
            encounterType: EncounterTypeEnum.PRESCRIPTIONS,
            onTap: () {
              isPrescription = !isPrescription;
              setState(() {});
            },
          ),
          16.height,
          //endregion
          //region Medical Report
          buildExpandableWidget(
            title: REPORT,
            encounterData: encounterData,
            encounterType: EncounterTypeEnum.REPORTS,
            isExpanded: isMedicalReport,
            onTap: () {
              isMedicalReport = !isMedicalReport;
              setState(() {});
            },
          ),
          //endregion
        ],
      ),
    );
  }

  Widget encounterDetail({required EncounterModel encounterData}) {
    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            decoration: boxDecorationDefault(color: context.cardColor),
            padding: EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(locale.lblName, style: secondaryTextStyle(size: 12)),
                Text(encounterData.patientName.validate(),
                    style: boldTextStyle()),
                8.height,
                Text(locale.lblEmail, style: secondaryTextStyle(size: 12)),
                Text(encounterData.patientEmail.validate(),
                    style: boldTextStyle()),
                8.height,
                Text(locale.lblEncounterDate,
                    style: secondaryTextStyle(size: 12)),
                Text(encounterData.encounterDate.validate(),
                    style: boldTextStyle()),
              ],
            ),
          ).expand(),
          16.width,
          Container(
            decoration: boxDecorationDefault(color: context.cardColor),
            padding: EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(locale.lblClinicName, style: secondaryTextStyle(size: 12)),
                Text(encounterData.clinicName.validate(),
                    style: boldTextStyle()),
                8.height,
                Text(locale.lblDoctorName, style: secondaryTextStyle(size: 12)),
                Text(encounterData.doctorName.validate(),
                    style: boldTextStyle()),
                8.height,
                Text(locale.lblDescription,
                    style: secondaryTextStyle(size: 12)),
                Text(encounterData.description.validate(value: " -- "),
                    style: boldTextStyle()),
              ],
            ),
          ).expand(),
        ],
      ),
    );
  }

  Widget buildExpandableWidget({
    required String title,
    required EncounterTypeEnum encounterType,
    required EncounterModel encounterData,
    required bool isExpanded,
    required Function() onTap,
    EncounterTypeValues? encounterTypeValue,
  }) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 2, horizontal: 16),
        decoration: boxDecorationDefault(
            color: context.cardColor, borderRadius: radius()),
        child: AnimatedCrossFade(
          firstChild: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(title.capitalizeFirstLetter(),
                  style: secondaryTextStyle(size: 16)),
              (isExpanded ? ic_arrow_up : ic_arrow_down)
                  .iconImage(size: 16, color: context.iconColor)
                  .paddingAll(14)
                  .onTap(onTap),
            ],
          ),
          secondChild: Stack(
            children: [
              Column(
                children: [
                  Divider(thickness: 1, color: context.dividerColor, height: 2),
                  4.height,
                  EncounterExpandableView(
                    encounterType: title,
                    encounterData: encounterData,
                    callForRefresh: () async {
                      await init();
                      setState(() {});
                    },
                  ),
                ],
              ).paddingTop(40),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(title.capitalizeFirstLetter(),
                      style: secondaryTextStyle(size: 16)),
                  (isExpanded ? ic_arrow_up : ic_arrow_down)
                      .iconImage(size: 16, color: context.iconColor)
                      .paddingAll(14)
                      .onTap(onTap),
                ],
              ),
            ],
          ),
          crossFadeState:
              isExpanded ? CrossFadeState.showSecond : CrossFadeState.showFirst,
          duration: 600.milliseconds,
          firstCurve: Curves.bounceInOut,
        ),
      ),
    );
  }

  Widget buildEncounterBottomWidget({required EncounterModel encounterData}) {
    if ((encounterData.paymentStatus == null ||
            encounterData.paymentStatus != "paid") &&
        (isDoctor() || isReceptionist()))
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          8.height,
          Text(
              '\t${locale.lblNote}: ${locale.lblToCloseTheEncounterInvoicePaymentIsMandatory}',
              style: secondaryTextStyle(size: 10, color: appSecondaryColor)),
          8.height,
          Row(
            children: [
              AppButton(
                text: locale.lblClose,
                color: context.cardColor,
                textStyle: primaryTextStyle(color: Colors.red),
                shapeBorder: RoundedRectangleBorder(
                    borderRadius: radius(),
                    side: BorderSide(color: Colors.red)),
                onTap: () async {
                  _handleCloseEncounterClick(encounterData: encounterData);
                },
              ).expand(),
              16.width,
              AppButton(
                text: encounterData.paymentStatus == null
                    ? '${locale.lblClose} & ${locale.lblCheckOut}'
                    : encounterData.paymentStatus ==
                            locale.lblPaid.toLowerCase()
                        ? locale.lblCheckOut
                        : '${locale.lblUpdate} ${locale.lblPaymentStatus}',
                color: context.cardColor,
                shapeBorder: RoundedRectangleBorder(
                    borderRadius: radius(),
                    side: BorderSide(color: Colors.red)),
                textStyle: primaryTextStyle(color: Colors.red),
                onTap: () {
                  _handleCloseEncounterClick(
                      isCheckOut: true, encounterData: encounterData);
                },
              ),
            ],
          ),
        ],
      );
    else {
      return SizedBox.shrink();
    }
  }

  Widget buildBodyWidget() {
    return Body(
      child: FutureBuilder<EncounterModel>(
        future: future,
        builder: (context, snap) {
          if (snap.hasData) {
            return Stack(
              children: [
                buildEncounterDetailWidget(encounterData: snap.requireData)
                    .paddingBottom(30),
                if (snap.requireData.status !=
                    ClosedEncounterStatusInt.toString())
                  Positioned(
                      bottom: 16,
                      right: 16,
                      left: 16,
                      child: buildEncounterBottomWidget(
                              encounterData: snap.requireData)
                          .paddingTop(8)),
              ],
            );
          }
          return snapWidgetHelper(snap, loadingWidget: LoaderWidget());
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      extendBody: false,
      extendBodyBehindAppBar: false,
      appBar: appBarWidget(
        locale.lblEncounterDashboard,
        titleTextStyle: boldTextStyle(color: textPrimaryDarkColor, size: 18),
        color: appPrimaryColor,
        elevation: 0,
        systemUiOverlayStyle: defaultSystemUiOverlayStyle(context),
        textColor: Colors.white,
      ),
      body: buildBodyWidget(),
      floatingActionButton: encounterStatus == '0'
          ? Container(
              padding: EdgeInsets.symmetric(horizontal: 32, vertical: 8),
              decoration: boxDecorationDefault(color: appSecondaryColor),
              child: Text(locale.lblBillDetails, style: primaryTextStyle()),
            ).onTap(() => BillDetailsScreen(
                  encounterId: widget.encounterId.validate().toInt())
              .launch(context))
          : Offstage(),
    );
  }
}
