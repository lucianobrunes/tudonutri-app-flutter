import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/body_widget.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/encounter_model.dart';
import 'package:kivicare_flutter/model/user_model.dart';
import 'package:kivicare_flutter/network/encounter_repository.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:nb_utils/nb_utils.dart';

import 'package:kivicare_flutter/screens/encounter/screen/add_encounter_screen.dart';
import 'package:kivicare_flutter/screens/encounter/component/encounter_list_component.dart';

class EncounterListScreen extends StatefulWidget {
  final UserModel? patientData;

  EncounterListScreen({this.patientData});

  @override
  _EncounterListScreenState createState() => _EncounterListScreenState();
}

class _EncounterListScreenState extends State<EncounterListScreen> {
  Future<List<EncounterModel>>? future;

  List<EncounterModel> patientEncounterList = [];

  int total = 0;
  int page = 1;
  bool isLastPage = false;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    future = getPatientEncounterList(id: widget.patientData!.iD, page: page, encounterList: patientEncounterList, getTotalPatient: (b) => total = b, lastPageCallback: (b) => isLastPage = b);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    getDisposeStatusBarColor();
    super.dispose();
  }

  Widget body() {
    return SnapHelperWidget<List<EncounterModel>>(
      future: future,
      loadingWidget: LoaderWidget(),
      onSuccess: (snap) {
        if (snap.isEmpty) return NoDataFoundWidget(text: locale.lblNoEncounterFound.capitalizeEachWord());

        return AnimatedScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          disposeScrollController: true,
          listAnimationType: listAnimationType,
          slideConfiguration: SlideConfiguration(verticalOffset: 400),
          onSwipeRefresh: () async {
            page = 1;
            init();
            return await 2.seconds.delay;
          },
          onNextPage: () {
            if (!isLastPage) {
              page++;
              init();
              setState(() {});
            }
          },
          children: [
            16.height,
            Text(locale.lblSwipeMassage, style: secondaryTextStyle(size: 10, color: appSecondaryColor)).paddingOnly(left: 20),
            AnimatedListView(
              physics: NeverScrollableScrollPhysics(),
              itemCount: snap.length,
              padding: EdgeInsets.fromLTRB(16, 0, 16, 80),
              shrinkWrap: true,
              itemBuilder: (context, index) {
                EncounterModel data = snap[index];
                return EncounterListComponent(data: data, patientData: widget.patientData).paddingSymmetric(vertical: 8);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(
        widget.patientData!.displayName.validate().capitalizeEachWord() + " " + locale.lblEncounters,
        textColor: Colors.white,
        elevation: 0,
        systemUiOverlayStyle: defaultSystemUiOverlayStyle(context),
      ),
      body: Body(child: body()),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          await AddEncounterScreen(patientId: widget.patientData!.iD).launch(context).then((value) {
            if (value ?? false) {
              setState(() {});
              init();
            }
          });
        },
      ),
    );
  }
}
