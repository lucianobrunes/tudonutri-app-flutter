import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/role_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/clinic_list_model.dart';
import 'package:kivicare_flutter/model/encounter_model.dart';
import 'package:kivicare_flutter/model/user_model.dart';
import 'package:kivicare_flutter/network/encounter_repository.dart';
import 'package:kivicare_flutter/screens/appointment/components/patient_search_screen.dart';
import 'package:kivicare_flutter/screens/appointment/screen/step1_clinic_selection_screen.dart';
import 'package:kivicare_flutter/screens/appointment/screen/step2_doctor_selection_screen.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/date_extensions.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:nb_utils/nb_utils.dart';

class AddEncounterScreen extends StatefulWidget {
  final EncounterModel? patientEncounterData;
  final int? patientId;

  AddEncounterScreen({this.patientEncounterData, this.patientId});

  @override
  _AddEncounterScreenState createState() => _AddEncounterScreenState();
}

class _AddEncounterScreenState extends State<AddEncounterScreen> {
  GlobalKey<FormState> formKey = GlobalKey();

  TextEditingController selectedClinicNameCont = TextEditingController();
  TextEditingController selectedDoctorNameCont = TextEditingController();
  TextEditingController selectedPatientNameCont = TextEditingController();
  TextEditingController encounterDateCont = TextEditingController();
  TextEditingController encounterDescriptionCont = TextEditingController();

  EncounterModel? patientEncounterData;

  late Clinic selectedClinic;

  late UserModel selectedDoctor;

  late UserModel selectedPatient;

  DateTime current = DateTime.now();

  int? clinicId;

  bool isUpdate = false;
  bool isFirstTime = true;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    isUpdate = widget.patientEncounterData != null;
    if (isUpdate) {
      patientEncounterData = widget.patientEncounterData;
      selectedPatientNameCont.text = widget.patientEncounterData!.patientName.validate();
      selectedClinicNameCont.text = widget.patientEncounterData!.clinicName.validate();
      selectedDoctorNameCont.text = widget.patientEncounterData!.doctorName.validate().prefixText(value: 'Dr. ');
      encounterDateCont.text = patientEncounterData!.encounterDate!.getFormattedDate(SAVE_DATE_FORMAT);
      current = DateTime.parse(patientEncounterData!.encounterDate!);
      encounterDescriptionCont.text = patientEncounterData!.description.validate();
      clinicId = patientEncounterData!.clinicId.toInt();
    }
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  void saveEncounter() async {
    appStore.setLoading(true);

    Map request = {
      "date": encounterDateCont.text,
      "description": encounterDescriptionCont.text,
      "status": "1",
    };

    if (isUpdate) {
      request.putIfAbsent('patient_id', () => widget.patientEncounterData!.patientId.validate());
    } else {
      request.putIfAbsent('patient_id', () => selectedPatient.userId);
    }

    if (isDoctor()) {
      if (isProEnabled()) {
        request.putIfAbsent("clinic_id", () => selectedClinic.id);
      } else {
        request.putIfAbsent("clinic_id", () => userStore.userClinicId);
      }
    } else if (isReceptionist()) {
      request.putIfAbsent("clinic_id", () => userStore.userClinicId);
      request.putIfAbsent("doctor_id", () => selectedDoctor.iD);
    }

    addEncounterData(request).then((value) {
      toast(locale.lblAddedNewEncounter);
      finish(context, true);
    }).catchError((e) {
      toast(e);
    });
    appStore.setLoading(false);
  }

  void updateEncounter() async {
    appStore.setLoading(true);

    Map request = {
      "id": patientEncounterData!.id,
      "date": encounterDateCont.text,
      "patient_id": widget.patientId,
      "description": encounterDescriptionCont.text,
      "status": "1",
    };

    if (isDoctor()) {
      if (isProEnabled()) {
        request.putIfAbsent("clinicId", () => selectedClinic.id.validate());
      } else {
        request.putIfAbsent("clinicId", () => userStore.userClinicId);
      }
    }

    if (isReceptionist()) {
      request.putIfAbsent("clinicId", () => userStore.userClinicId);
      request.putIfAbsent("doctor_id", () => selectedDoctor.iD);
    }

    addEncounterData(request).then((value) {
      toast(locale.lblEncounterUpdated);
      finish(context, true);
    }).catchError((e) {
      toast(e.toString());
    });
    appStore.setLoading(false);
  }

  @override
  void dispose() {
    getDisposeStatusBarColor();
    super.dispose();
  }

  void datePicker() async {
    DateTime? dateTime = await showDatePicker(
      context: context,
      initialDate: current,
      locale: Locale(appStore.selectedLanguage),
      firstDate: DateTime.now(),
      lastDate: DateTime(2101),
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: appStore.isDarkModeOn
              ? ThemeData.dark()
              : ThemeData.light().copyWith(
                  primaryColor: Color(0xFF4974dc),
                  hintColor: const Color(0xFF4974dc),
                  colorScheme: ColorScheme.light(primary: const Color(0xFF4974dc)),
                ),
          child: child!.cornerRadiusWithClipRRect(defaultRadius).paddingSymmetric(horizontal: 32, vertical: 100),
        );
      },
    );
    if (dateTime != null) {
      encounterDateCont.text = dateTime.getFormattedDate(SAVE_DATE_FORMAT);
      current = dateTime;
      setState(() {});
    }
  }

  Widget buildBodyWidget() {
    return Form(
      key: formKey,
      autovalidateMode: isFirstTime ? AutovalidateMode.disabled : AutovalidateMode.onUserInteraction,
      child: AnimatedScrollView(
        padding: EdgeInsets.all(16),
        children: [
          if (isProEnabled())
            RoleWidget(
              isShowDoctor: true,
              child: Column(
                children: [
                  AppTextField(
                    textFieldType: TextFieldType.NAME,
                    controller: selectedPatientNameCont,
                    readOnly: true,
                    decoration: inputDecoration(context: context, labelText: locale.lblSelectPatient),
                    onTap: () async {
                      await PatientSearchScreen().launch(context).then((value) {
                        log(selectedPatient.userDisplayName.validate());
                        if (value != null) {
                          selectedPatient = value;
                          selectedPatientNameCont.text = selectedPatient.userDisplayName.validate();
                          setState(() {});
                        }
                      });
                    },
                  ),
                  16.height,
                  AppTextField(
                    controller: selectedClinicNameCont,
                    textFieldType: TextFieldType.NAME,
                    readOnly: true,
                    decoration: inputDecoration(context: context, labelText: locale.lblSelectClinic),
                    onTap: () async {
                      await Step1ClinicSelectionScreen(sessionOrEncounter: true).launch(context).then((value) {
                        if (value != null) {
                          selectedClinic = value!;
                          selectedClinicNameCont.text = selectedClinic.name.validate();
                        } else {
                          toast('${locale.lblPlease} ${locale.lblSelectClinic}');
                        }
                        setState(() {});
                      }).catchError((e) {
                        toast(e.toString());
                      });
                    },
                  ),
                ],
              ),
            ),
          if (!isDoctor()) 16.height,
          RoleWidget(
            isShowReceptionist: true,
            isShowPatient: true,
            child: AppTextField(
              textFieldType: TextFieldType.NAME,
              controller: selectedDoctorNameCont,
              decoration: inputDecoration(context: context, labelText: locale.lblSelectDoctor),
              readOnly: true,
              onTap: () {
                Step2DoctorSelectionScreen(isNotForAppointment: true).launch(context).then((value) {
                  if (value != null) {
                    selectedDoctor = value;
                    selectedDoctorNameCont.text = selectedDoctor.displayName.validate();
                  } else {
                    toast('${locale.lblPlease} ${locale.lblSelectDoctor}');
                  }
                  setState(() {});
                }).catchError((e) {
                  toast(e.toString());
                });
              },
            ),
          ),
          16.height,
          AppTextField(
            onTap: datePicker,
            controller: encounterDateCont,
            readOnly: true,
            textFieldType: TextFieldType.NAME,
            suffix: Icon(Icons.date_range),
            decoration: inputDecoration(context: context, labelText: locale.lblDate),
          ),
          16.height,
          AppTextField(
            controller: encounterDescriptionCont,
            textFieldType: TextFieldType.MULTILINE,
            isValidationRequired: false,
            maxLines: 14,
            textAlign: TextAlign.start,
            minLines: 5,
            decoration: inputDecoration(context: context, labelText: locale.lblDescription),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(
        isUpdate ? locale.lblEditEncounterDetail : locale.lblAddNewEncounter,
        textColor: Colors.white,
        systemUiOverlayStyle: defaultSystemUiOverlayStyle(context),
      ),
      body: buildBodyWidget(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.done, color: Colors.white),
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();
            showConfirmDialogCustom(
              context,
              dialogType: isUpdate ? DialogType.UPDATE : DialogType.CONFIRMATION,
              primaryColor: context.primaryColor,
              title: locale.lblAreYouSureYouWantToSubmitTheForm,
              onAccept: (p0) {
                isUpdate ? updateEncounter() : saveEncounter();
              },
            );
          } else {
            isFirstTime = !isFirstTime;
            setState(() {});
          }
        },
      ),
    );
  }
}
