import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/role_widget.dart';
import 'package:kivicare_flutter/model/encounter_model.dart';
import 'package:kivicare_flutter/network/prescription_repository.dart';
import 'package:kivicare_flutter/network/report_repository.dart';
import 'package:kivicare_flutter/screens/doctor/screens/add_prescription_screen.dart';
import 'package:kivicare_flutter/screens/doctor/screens/add_report_screen.dart';
import 'package:kivicare_flutter/screens/encounter/component/encounter_type_list_component.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/enums.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/network/encounter_repository.dart';
import 'package:kivicare_flutter/utils/common.dart';

class EncounterExpandableView extends StatefulWidget {
  final String encounterType;
  final EncounterModel encounterData;
  final Function()? callForRefresh;

  EncounterExpandableView({
    required this.encounterType,
    required this.encounterData,
    this.callForRefresh,
    Key? key,
  }) : super(key: key);

  @override
  _EncounterExpandableViewState createState() =>
      _EncounterExpandableViewState();
}

class _EncounterExpandableViewState extends State<EncounterExpandableView> {
  GlobalKey<FormState> formKey = GlobalKey();
  TextEditingController descriptionCont = TextEditingController();

  bool showAdd = false;
  final String encounterTypeTraduzido = "";

  // Future<MedicalHistoryModel>? future;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    log(widget.encounterType);
    descriptionCont.clear();
  }

  void saveDetails() async {
    String enconterTypeEnglish = "";

    switch (widget.encounterType) {
      case PROBLEM:
        enconterTypeEnglish = "problem";
        break;
      case OBSERVATION:
        enconterTypeEnglish = "observation";
        break;
      case NOTE:
        enconterTypeEnglish = "note";
        break;
      case PRESCRIPTION:
        enconterTypeEnglish = "prescription";
        break;
      case REPORT:
        enconterTypeEnglish = "report";
        break;
    }

    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();

      appStore.setLoading(true);

      Map request = {
        "encounter_id": "${widget.encounterData.id}",
        "type": enconterTypeEnglish.toLowerCase(),
        "title": descriptionCont.text.trim(),
      };
      hideKeyboard(context);

      await saveMedicalHistoryData(request).then((value) {
        showAdd = !showAdd;
        descriptionCont.clear();
        widget.callForRefresh?.call();
        toast(locale.lblMedicalHistoryHasBeen +
            " ${locale.lblAddedSuccessfully}");
      }).catchError((e) {
        toast(e.toString());
      });
      setState(() {});
      appStore.setLoading(false);
    }
  }

  Future<void> callForDelete(
      {required EncounterTypeEnum encounterTypeEnum, required int id}) async {
    Map request = {
      "id": "$id",
    };
    switch (encounterTypeEnum) {
      case EncounterTypeEnum.REPORTS:
        await deleteReportAPI(request).then((value) {
          showAdd = !showAdd;
          widget.callForRefresh?.call();
          toast(locale.lblReport + locale.lblDeletedSuccessfully);
        }).catchError((e) {
          toast(e.toString());
        });
        break;
      case EncounterTypeEnum.PRESCRIPTIONS:
        await deletePrescriptionDataAPI(request).then((value) {
          showAdd = !showAdd;
          widget.callForRefresh?.call();
          toast(locale.lblPrescriptionDeleted);
        }).catchError((e) {
          toast(e.toString());
        });
        break;
      case EncounterTypeEnum.OTHERS:
        await deleteMedicalHistoryData(request).then((value) {
          showAdd = !showAdd;
          widget.callForRefresh?.call();
          toast(locale.lblMedicalHistoryHasBeen +
              " " +
              locale.lblDeletedSuccessfully);
        }).catchError((e) {
          toast(e.toString());
        });
        break;
    }
  }

  void deleteDetails(
      {required int id, required EncounterTypeEnum encounterTypeEnum}) async {
    appStore.setLoading(true);
    hideKeyboard(context);
    callForDelete(encounterTypeEnum: encounterTypeEnum, id: id);
    appStore.setLoading(false);
  }

  Future<void> _handleSendEmailPrescriptionData() async {
    appStore.setLoading(true);
    await sendPrescriptionMailAPI(
            encounterId: widget.encounterData.id.validate().toInt())
        .then((value) {
      toast(value.message.toString());
    }).catchError((e) {
      toast(e.toString());
    });
    appStore.setLoading(false);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant EncounterExpandableView oldWidget) {
    super.didUpdateWidget(oldWidget);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    String encounterTypeTraduzido = "";

    switch (widget.encounterType) {
      case PROBLEM:
        encounterTypeTraduzido = "problema";
        break;
      case OBSERVATION:
        encounterTypeTraduzido = "observação";
        break;
      case NOTE:
        encounterTypeTraduzido = "nota";
        break;
      case PRESCRIPTION:
        encounterTypeTraduzido = "prescrição";
        break;
      case REPORT:
        encounterTypeTraduzido = "relatório";
        break;
    }

    return AnimatedScrollView(
      children: [
        EncounterTypeList(
          encounterType: encounterTypeTraduzido,
          encounterData: widget.encounterData,
          callDelete: deleteDetails,
        ),
        RoleWidget(
          isShowDoctor: true,
          isShowReceptionist: true,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              if (widget.encounterType == PRESCRIPTION)
                TextButton(
                    onPressed: _handleSendEmailPrescriptionData,
                    child: Text(locale.lblSendPrescriptionOnMail,
                        style: primaryTextStyle(color: Colors.green))),
              Icon(
                showAdd ? Icons.remove : Icons.add,
                color: showAdd ? Colors.red : primaryColor,
              ).onTap(() {
                if (showAdd) {
                  hideKeyboard(context);
                  descriptionCont.clear();
                }

                if (!showAdd && widget.encounterType == PRESCRIPTION)
                  AddPrescriptionScreen(
                          encounterId: widget.encounterData.id.toInt())
                      .launch(context)
                      .then((value) {
                    if (value != null) if (value) {
                      showAdd = !showAdd;
                      widget.callForRefresh?.call();
                      setState(() {});
                    }
                  });
                if (!showAdd && widget.encounterType == REPORT)
                  AddReportScreen(
                          patientId: widget.encounterData.patientId.toInt())
                      .launch(context)
                      .then((value) {
                    if (value != null) if (value) {
                      showAdd = !showAdd;
                      widget.callForRefresh?.call();
                      setState(() {});
                    }
                  });

                showAdd = !showAdd;

                setState(() {});
              })
            ],
          ).paddingOnly(top: 4, bottom: 16, right: 8),
        ),
        if (showAdd &&
            (widget.encounterType == PROBLEM ||
                widget.encounterType == OBSERVATION ||
                widget.encounterType == NOTE))
          Form(
            key: formKey,
            child: AppTextField(
              controller: descriptionCont,
              textFieldType: TextFieldType.MULTILINE,
              minLines: 1,
              maxLines: 5,
              autoFocus: false,
              errorThisFieldRequired: locale.lblFieldIsRequired,
              decoration: inputDecoration(
                      context: context,
                      labelText: locale.lblEnter + ' ${widget.encounterType}')
                  .copyWith(
                      filled: true, fillColor: context.scaffoldBackgroundColor),
              keyboardType: TextInputType.multiline,
              suffix: IconButton(
                icon: Icon(Icons.send),
                onPressed: saveDetails,
              ),
            ).paddingOnly(right: 4, bottom: 16, left: 4),
          ),
      ],
    );
  }
}
