import 'package:kivicare_flutter/model/encounter_model.dart';
import 'package:kivicare_flutter/model/medical_history_model.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:nb_utils/nb_utils.dart';

List<EncounterType> getEncounterOtherTypeList({required String encounterType, required EncounterModel encounterData}) {
  switch (encounterType) {
    case PROBLEM:
      return encounterData.problem.validate();
    case OBSERVATION:
      return encounterData.observation.validate();
    case NOTE:
      return encounterData.note.validate();
    default:
      return [];
  }
}
