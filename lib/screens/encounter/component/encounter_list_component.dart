import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:kivicare_flutter/components/side_date_widget.dart';
import 'package:kivicare_flutter/components/status_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/encounter_model.dart';
import 'package:kivicare_flutter/model/user_model.dart';
import 'package:kivicare_flutter/network/encounter_repository.dart';
import 'package:kivicare_flutter/screens/encounter/screen/add_encounter_screen.dart';
import 'package:kivicare_flutter/screens/encounter/screen/encounter_dashboard_screen.dart';
import 'package:kivicare_flutter/screens/encounter/screen/patient_encounter_dashboard_screen.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:nb_utils/nb_utils.dart';

class EncounterListComponent extends StatefulWidget {
  final UserModel? patientData;
  final EncounterModel? data;

  EncounterListComponent({required this.data, this.patientData});

  @override
  _EncounterListComponentState createState() => _EncounterListComponentState();
}

class _EncounterListComponentState extends State<EncounterListComponent> {
  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    //
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  void _handleDeleteAction() {
    showConfirmDialogCustom(
      context,
      dialogType: DialogType.DELETE,
      title: locale.lblAreYouSureYouWantTo + ' ' + locale.lblDelete + ' ' + locale.lblEncounterDetails + ' ' + 'Of ${widget.data!.patientName.validate()}',
      onAccept: (p0) {
        Map request = {
          "encounter_id": widget.data!.id,
        };

        appStore.setLoading(true);

        deleteEncounterData(request).then((value) {
          appStore.setLoading(false);
          toast(value);
          init();
          setState(() {});
        });
      },
    );
  }

  void _handleEditAction() async {
    bool? res = await AddEncounterScreen(
      patientEncounterData: widget.data!,
      patientId: widget.patientData!.iD,
    ).launch(context);
    if (res ?? false) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: boxDecorationDefault(borderRadius: radius(), color: context.cardColor),
      padding: EdgeInsets.only(left: 8, top: 10, bottom: 10, right: 8),
      child: Slidable(
        key: ValueKey(widget.data!),
        endActionPane: ActionPane(
          motion: ScrollMotion(),
          children: [
            SlidableAction(
              flex: 1,
              backgroundColor: primaryColor,
              foregroundColor: Colors.white,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(defaultRadius), bottomLeft: Radius.circular(defaultRadius)),
              icon: Icons.edit,
              label: locale.lblEdit,
              onPressed: (BuildContext context) async {
                _handleEditAction();
              },
            ),
            SlidableAction(
              flex: 1,
              borderRadius: BorderRadius.only(topRight: Radius.circular(defaultRadius), bottomRight: Radius.circular(defaultRadius)),
              backgroundColor: Colors.red,
              foregroundColor: Colors.white,
              icon: Icons.delete,
              label: locale.lblDelete,
              onPressed: (BuildContext context) async {
                _handleDeleteAction();
              },
            ),
          ],
        ),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            SideDateWidget(tempDate: DateFormat(SAVE_DATE_FORMAT).parse(widget.data!.encounterDate.validate())),
            Container(
              height: 60,
              child: VerticalDivider(color: Colors.grey.withOpacity(0.5), width: 25, thickness: 1, indent: 4, endIndent: 1),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(widget.data!.clinicName.validate(), style: boldTextStyle(size: 18), maxLines: 2).flexible(),
                    FaIcon(
                      FontAwesomeIcons.gaugeHigh,
                      size: 20,
                      color: appSecondaryColor,
                    ).paddingAll(8).onTap(
                      () {
                        if (isDoctor() || isReceptionist()) {
                          EncounterDashboardScreen(encounterId: widget.data!.id).launch(context);
                        } else
                          PatientEncounterDashboardScreen(id: widget.data!.id).launch(context);
                      },
                    )
                  ],
                ),
                if (!isDoctor())
                  RichTextWidget(
                    list: [
                      TextSpan(text: locale.lblDoctor.validate().suffixText(value: ' : '), style: secondaryTextStyle(size: 14)),
                      TextSpan(text: widget.data!.doctorName.validate().capitalizeEachWord().prefixText(value: 'Dr. '), style: boldTextStyle(size: 14)),
                    ],
                  ),
                if (isDoctor() && !isPatient())
                  RichTextWidget(
                    list: [
                      TextSpan(text: locale.lblPatientName.suffixText(value: ": "), style: secondaryTextStyle(size: 14)),
                      TextSpan(text: widget.data!.patientName.validate().capitalizeEachWord(), style: boldTextStyle(size: 14)),
                    ],
                  ),
                4.height,
                Row(
                  children: [
                    RichTextWidget(
                      list: [
                        TextSpan(text: locale.lblDescription.suffixText(value: ": "), spellOut: true, style: secondaryTextStyle(size: 14)),
                        TextSpan(text: widget.data!.description.validate(value: locale.lblNA).trim(), style: boldTextStyle(size: 14)),
                      ],
                    ).expand(),
                    StatusWidget(
                      status: widget.data!.status.validate(),
                      isEncounterStatus: true,
                    )
                  ],
                ),
              ],
            ).expand(),
          ],
        ),
      ),
    );
  }
}
