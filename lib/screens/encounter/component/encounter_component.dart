import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:kivicare_flutter/components/common_row_widget.dart';
import 'package:kivicare_flutter/components/side_date_widget.dart';
import 'package:kivicare_flutter/components/status_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/encounter_model.dart';
import 'package:kivicare_flutter/screens/doctor/screens/bill_details_screen.dart';
import 'package:kivicare_flutter/screens/encounter/screen/encounter_dashboard_screen.dart';
import 'package:kivicare_flutter/screens/encounter/screen/patient_encounter_dashboard_screen.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:nb_utils/nb_utils.dart';

class EncounterComponent extends StatelessWidget {
  final EncounterModel data;
  final Function(int)? deleteEncounter;
  final Function()? callForRefresh;

  EncounterComponent({required this.data, this.deleteEncounter, this.callForRefresh});

  @override
  Widget build(BuildContext context) {
    return Slidable(
      key: ValueKey(data.id),
      endActionPane: ActionPane(
        extentRatio: 0.7,
        motion: ScrollMotion(),
        children: [
          if (data.status == ClosedEncounterStatusInt.toString())
            SlidableAction(
              backgroundColor: primaryColor,
              foregroundColor: Colors.white,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(defaultRadius), bottomLeft: Radius.circular(defaultRadius)),
              icon: FontAwesomeIcons.moneyBill,
              label: locale.lblBillDetails,
              onPressed: (BuildContext context) {
                BillDetailsScreen(encounterId: data.id.validate().toInt()).launch(context);
              },
            ),
          SlidableAction(
            borderRadius: BorderRadius.only(topRight: Radius.circular(defaultRadius), bottomRight: Radius.circular(defaultRadius)),
            backgroundColor: Colors.red,
            foregroundColor: Colors.white,
            icon: Icons.delete,
            label: locale.lblDelete,
            onPressed: (BuildContext context) {
              showConfirmDialogCustom(
                context,
                dialogType: DialogType.DELETE,
                title: locale.lblAreYouSureYouWantTo + ' ' + locale.lblDelete + ' ' + locale.lblEncounterDetails + ' ' + 'Of ${data.patientName}',
                onAccept: (p0) {
                  deleteEncounter?.call(data.id.toInt());
                },
              );
            },
          ),
        ],
      ),
      child: Container(
        padding: EdgeInsets.all(12),
        decoration: boxDecorationDefault(color: context.cardColor, borderRadius: radius()),
        child: Column(
          children: [
            Row(
              children: [
                SideDateWidget(tempDate: DateFormat(SAVE_DATE_FORMAT).parse(data.encounterDate.validate())),
                Container(
                  height: 60,
                  child: VerticalDivider(color: Colors.grey.withOpacity(0.5), width: 25, thickness: 1, indent: 4, endIndent: 1),
                ),
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text("${data.clinicName.validate()}", style: boldTextStyle()).expand(),
                        16.width,
                        FaIcon(FontAwesomeIcons.gaugeHigh, color: appSecondaryColor, size: 20).withWidth(20).onTap(() {
                          if (isPatient()) {
                            PatientEncounterDashboardScreen(id: data.id, isPaymentDone: data.status == '0' ? true : false).launch(context);
                          } else {
                            EncounterDashboardScreen(encounterId: data.id).launch(context).then((value) {
                              if (value) callForRefresh?.call();
                            });
                          }
                        })
                      ],
                    ),
                    4.height,
                    CommonRowWidget(title: locale.lblDoctor, value: data.doctorName.validate().prefixText(value: 'Dr. '), valueColor: primaryColor, valueSize: 14),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(locale.lblDescription + ' : ', style: secondaryTextStyle()),
                            4.width,
                            Text(
                              data.description.validate(value: locale.lblNA).trim(),
                              style: boldTextStyle(size: 14),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ).expand(),
                            StatusWidget(
                              status: data.status.validate(),
                              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                              isEncounterStatus: true,
                            ),
                          ],
                        ).expand(),
                      ],
                    ),
                  ],
                ).expand()
              ],
            ),
          ],
        ),
      ),
    ).paddingSymmetric(vertical: 8);
  }
}
