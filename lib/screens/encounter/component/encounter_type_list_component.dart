import 'package:flutter/material.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/encounter_model.dart';
import 'package:kivicare_flutter/model/medical_history_model.dart';
import 'package:kivicare_flutter/model/prescription_model.dart';
import 'package:kivicare_flutter/model/report_model.dart';
import 'package:kivicare_flutter/network/prescription_repository.dart';
import 'package:kivicare_flutter/screens/encounter/component/encounter_functions.dart';
import 'package:kivicare_flutter/screens/encounter/component/encounter_prescription_component.dart';
import 'package:kivicare_flutter/screens/encounter/component/encounter_type_%20component.dart';
import 'package:kivicare_flutter/screens/encounter/component/report_component.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/enums.dart';
import 'package:nb_utils/nb_utils.dart';

class EncounterTypeList extends StatefulWidget {
  final String encounterType;
  final EncounterModel encounterData;
  final void Function(
      {required int id,
      required EncounterTypeEnum encounterTypeEnum}) callDelete;

  EncounterTypeList(
      {required this.encounterData,
      required this.encounterType,
      required this.callDelete});

  @override
  State<EncounterTypeList> createState() => _EncounterTypeListState();
}

class _EncounterTypeListState extends State<EncounterTypeList> {
  List<ReportData> reportList = [];

  List<PrescriptionData> prescriptionList = [];

  @override
  void initState() {
    super.initState();
    prescriptionList = widget.encounterData.prescription.validate();
    reportList = widget.encounterData.reportData.validate();
  }

  Widget buildChild() {
    switch (widget.encounterType) {
      case PROBLEM:
        return buildEncounterTypeOthersList();
      case OBSERVATION:
        return buildEncounterTypeOthersList();
      case NOTE:
        return buildEncounterTypeOthersList();
      case PRESCRIPTION:
        return buildEncounterPrescriptionList();
      case REPORT:
        return buildEncounterReportList();

      default:
        return SizedBox.shrink();
    }
  }

  Widget buildEncounterReportList() {
    if (reportList.isEmpty)
      return NoDataWidget(
          title: locale.lblNo +
              " " +
              "${widget.encounterType}" +
              " ${locale.lblFound}");
    return AnimatedWrap(
      spacing: 16,
      runSpacing: 12,
      itemCount: reportList.length,
      itemBuilder: (context, index) {
        ReportData reportData = reportList[index];

        return ReportComponent(
          reportData: reportData,
          isForMyReportScreen: false,
          deleteReportData: () {
            showConfirmDialogCustom(
              context,
              title: locale.lblAreYouSureYouWantTo +
                  locale.lblDelete +
                  " ${widget.encounterType} ?",
              dialogType: DialogType.DELETE,
              onAccept: (p0) {
                reportList.remove(reportData);
                setState(() {});
                widget.callDelete.call(
                    id: reportData.id.validate().toInt(),
                    encounterTypeEnum: EncounterTypeEnum.REPORTS);
              },
            );
          },
        );
      },
    ).paddingSymmetric(vertical: 8);
  }

  Widget buildEncounterPrescriptionList() {
    if (prescriptionList.length == 0)
      return NoDataWidget(
          title: locale.lblNo +
              " " +
              "${widget.encounterType.capitalizeFirstLetter()}" +
              " ${locale.lblFound.capitalizeFirstLetter()}");
    return AnimatedWrap(
      spacing: 16,
      runSpacing: 12,
      itemCount: prescriptionList.length,
      itemBuilder: (context, index) {
        PrescriptionData prescriptionData = prescriptionList[index];

        return EncounterPrescriptionComponent(
          prescriptionData: prescriptionData,
          onTap: () {
            showConfirmDialogCustom(
              context,
              title: locale.lblAreYouSureYouWantTo +
                  " " +
                  locale.lblDelete +
                  " ${prescriptionData.name.capitalizeFirstLetter()} ?",
              dialogType: DialogType.DELETE,
              onAccept: (p0) {
                prescriptionList.remove(prescriptionData);
                setState(() {});
                widget.callDelete.call(
                    id: prescriptionData.id.toInt(),
                    encounterTypeEnum: EncounterTypeEnum.PRESCRIPTIONS);
              },
            );
          },
        );
      },
    ).paddingSymmetric(vertical: 8);
  }

  Widget buildEncounterTypeOthersList() {
    String encounterTypeTraduzido = "";
    switch (widget.encounterType) {
      case PROBLEM:
        encounterTypeTraduzido = "problema";
        break;
      case OBSERVATION:
        encounterTypeTraduzido = "observação";
        break;
      case NOTE:
        encounterTypeTraduzido = "nota";
        break;
      case PRESCRIPTION:
        encounterTypeTraduzido = "prescrição";
        break;
      case REPORT:
        encounterTypeTraduzido = "relatório";
        break;
    }
    if (getEncounterOtherTypeList(
            encounterType: encounterTypeTraduzido,
            encounterData: widget.encounterData)
        .isEmpty)
      return NoDataWidget(
          title: locale.lblNo +
              " " +
              "${widget.encounterType}" +
              " ${locale.lblFound}");
    return AnimatedWrap(
      spacing: 16,
      runSpacing: 12,
      itemCount: getEncounterOtherTypeList(
              encounterType: widget.encounterType,
              encounterData: widget.encounterData)
          .length,
      itemBuilder: (context, index) {
        EncounterType data = getEncounterOtherTypeList(
            encounterType: widget.encounterType,
            encounterData: widget.encounterData)[index];

        return EncounterTypeComponent(
          data: data,
          onTap: () {
            showConfirmDialogCustom(
              context,
              title: locale.lblAreYouSureYouWantTo +
                  " " +
                  locale.lblDelete +
                  " ${data.title.capitalizeFirstLetter()} ?",
              dialogType: DialogType.DELETE,
              onAccept: (p0) {
                getEncounterOtherTypeList(
                        encounterType: widget.encounterType,
                        encounterData: widget.encounterData)
                    .remove(data);
                setState(() {});
                widget.callDelete.call(
                    id: data.id.toInt(),
                    encounterTypeEnum: EncounterTypeEnum.OTHERS);
                //deleteDetails(data.id.toInt(), index);
              },
            );
          },
        );
      },
    ).paddingSymmetric(vertical: 8);
  }

  @override
  Widget build(BuildContext context) {
    return buildChild();
  }
}
