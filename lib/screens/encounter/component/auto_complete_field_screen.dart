import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/network/prescription_repository.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:nb_utils/nb_utils.dart';

class AutoCompleteFieldScreen extends StatefulWidget {
  final String encounterId;
  final bool isFrequency;

  final Function(String name) onTap;

  const AutoCompleteFieldScreen({Key? key, required this.encounterId, required this.isFrequency, required this.onTap}) : super(key: key);

  @override
  State<AutoCompleteFieldScreen> createState() => _AutoCompleteFieldScreenState();
}

class _AutoCompleteFieldScreenState extends State<AutoCompleteFieldScreen> {
  Future<List<String>>? future;

  TextEditingController prescriptionCont = TextEditingController();

  String? selectedValues;

  bool isFirstTime = true;

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    future = getPrescriptionNameAndFrequencyAPI(id: '', isFrequency: widget.isFrequency);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  Widget getBody() {
    return FutureBuilder<List<String>>(
      future: future,
      builder: (context, snap) {
        if (snap.hasData) {
          if (snap.requireData.isEmpty) {
            return AppTextField(
              textFieldType: TextFieldType.OTHER,
              controller: prescriptionCont,
              onFieldSubmitted: (p0) {
                widget.onTap.call(p0);
              },
              decoration: inputDecoration(context: context, labelText: widget.isFrequency ? locale.lblFrequency : locale.lblName),
            );
          }

          return RawAutocomplete<String>(
            onSelected: widget.onTap,
            optionsViewBuilder: (context, onSelected, options) {
              return Material(
                color: Colors.transparent,
                child: Blur(
                  borderRadius: radius(),
                  child: AnimatedScrollView(
                    reverse: false,
                    physics: AlwaysScrollableScrollPhysics(),
                    listAnimationType: isFirstTime ? listAnimationType : ListAnimationType.None,
                    children: options.map(
                      (opt) {
                        return InkWell(
                          onTap: () {
                            onSelected(opt);
                          },
                          child: Container(
                            width: context.width(),
                            decoration: boxDecorationDefault(color: context.primaryColor),
                            padding: EdgeInsets.all(12),
                            margin: EdgeInsets.only(right: 26, top: 4, bottom: 4),
                            child: Text(opt, style: boldTextStyle(color: Colors.white)),
                          ),
                        );
                      },
                    ).toList(),
                  ),
                ),
              );
            },
            optionsBuilder: (TextEditingValue textEditingValue) {
              log(textEditingValue.text.isNotEmpty);
              if (textEditingValue.text.isNotEmpty) {
                isFirstTime = false;
                setState(() {});
              }
              List<String> matches = <String>[];
              matches.addAll(snap.requireData);
              matches.retainWhere((s) => s.toLowerCase().contains(textEditingValue.text.toLowerCase()));
              return matches;
            },
            fieldViewBuilder: (context, textEditingController, focusNode, onFieldSubmitted) {
              return AppTextField(
                textFieldType: TextFieldType.OTHER,
                focus: focusNode,
                controller: textEditingController,
                onTap: () {
                  widget.onTap.call(textEditingController.text);
                },
                onFieldSubmitted: (p0) {
                  widget.onTap.call(textEditingController.text);
                },
                decoration: inputDecoration(context: context, labelText: widget.isFrequency ? locale.lblFrequency : locale.lblName),
              );
            },
          );
        }
        return snapWidgetHelper(
          snap,
          errorWidget: NoDataFoundWidget(text: errorMessage),
          loadingWidget: AppTextField(
            textFieldType: TextFieldType.NAME,
            decoration: inputDecoration(context: context, labelText: widget.isFrequency ? locale.lblFrequency : locale.lblName),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return getBody();
  }
}
