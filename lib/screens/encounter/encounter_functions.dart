import 'package:kivicare_flutter/network/report_repository.dart';
import 'package:nb_utils/nb_utils.dart';

Future<bool> deleteReport({required String id}) async {
  return await deleteReportAPI({
    "id": "$id",
  }).then((value) {
    return true;
  }).catchError((e) {
    toast(e.toString());
    return false;
  });
}
