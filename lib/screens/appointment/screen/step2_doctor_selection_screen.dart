import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/user_model.dart';
import 'package:kivicare_flutter/network/doctor_list_repository.dart';
import 'package:kivicare_flutter/screens/appointment/appointment_functions.dart';
import 'package:kivicare_flutter/screens/receptionist/screens/doctor/component/doctor_list_component.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:nb_utils/nb_utils.dart';

// ignore: must_be_immutable
class Step2DoctorSelectionScreen extends StatefulWidget {
  final int? clinicId;
  bool isNotForAppointment;

  Step2DoctorSelectionScreen({this.clinicId, this.isNotForAppointment = false});

  @override
  _Step2DoctorSelectionScreenState createState() => _Step2DoctorSelectionScreenState();
}

class _Step2DoctorSelectionScreenState extends State<Step2DoctorSelectionScreen> {
  Future<List<UserModel>>? future;

  List<UserModel> doctorList = [];

  int total = 0;
  int page = 1;

  bool isLastPage = false;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    future = getDoctorListWithPagination(clinicId: widget.clinicId, page: page, doctorList: doctorList, getTotalDoctor: (b) => total = b, lastPageCallback: (b) => isLastPage = b);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() async {
    appointmentAppStore.setSelectedDoctor(null);
    super.dispose();
  }

  Widget buildBodyWidget() {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Stack(
        children: [
          widget.isNotForAppointment.validate() == false ? stepCountWidget(name: locale.lblChooseYourDoctor, currentCount: isPatient() ? 2 : 1, totalCount: isReceptionist() ? 2 : 3, percentage: 0.50) : Offstage(),
          SnapHelperWidget<List<UserModel>>(
            future: future,
            loadingWidget: LoaderWidget(),
            onSuccess: (snap) {
              if (snap.isEmpty) {
                return NoDataFoundWidget(text: locale.lblNoDataFound);
              }
              return AnimatedListView(
                itemCount: snap.length,
                padding: EdgeInsets.only(bottom: 60),
                shrinkWrap: true,
                onNextPage: () {
                  if (!isLastPage) {
                    page++;
                    init();
                  }
                },
                itemBuilder: (context, index) {
                  UserModel data = snap[index];

                  return GestureDetector(
                    onTap: () {
                      if (appointmentAppStore.mDoctorSelected != null ? appointmentAppStore.mDoctorSelected!.iD.validate() == data.iD.validate() : false) {
                        appointmentAppStore.setSelectedDoctor(null);
                      } else {
                        appointmentAppStore.setSelectedDoctor(data);
                      }
                    },
                    child: Observer(builder: (context) {
                      return DoctorListComponent(data: data, isSelected: appointmentAppStore.mDoctorSelected?.iD.validate() == data.iD.validate());
                    }),
                  );
                },
              );
            },
          ).paddingTop(widget.isNotForAppointment.validate() ? 0 : 80),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(
        widget.isNotForAppointment.validate() ? locale.lblSelectDoctor : locale.lblAddNewAppointment,
        textColor: Colors.white,
        systemUiOverlayStyle: defaultSystemUiOverlayStyle(context),
      ),
      body: buildBodyWidget(),
      floatingActionButton: FloatingActionButton(
        child: Icon(widget.isNotForAppointment.validate() ? Icons.done : Icons.arrow_forward_outlined),
        onPressed: () {
          if (appointmentAppStore.mDoctorSelected == null) {
            toast(locale.lblSelectOneDoctor);
          } else {
            if (widget.isNotForAppointment.validate())
              finish(context, appointmentAppStore.mDoctorSelected);
            else
              doctorNavigation(context, clinicId: widget.clinicId.validate().toInt(), doctorId: appointmentAppStore.mDoctorSelected!.iD.validate());
          }
        },
      ),
    );
  }
}
