import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:kivicare_flutter/components/body_widget.dart';
import 'package:kivicare_flutter/components/multi_select.dart';
import 'package:kivicare_flutter/components/price_widget.dart';
import 'package:kivicare_flutter/components/role_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/service_model.dart';
import 'package:kivicare_flutter/model/upcoming_appointment_model.dart';
import 'package:kivicare_flutter/model/user_model.dart';
import 'package:kivicare_flutter/screens/appointment/appointment_functions.dart';
import 'package:kivicare_flutter/screens/appointment/components/appointment_date_component.dart';
import 'package:kivicare_flutter/screens/appointment/components/appointment_slots.dart';
import 'package:kivicare_flutter/screens/appointment/components/confirm_appointment_screen.dart';
import 'package:kivicare_flutter/screens/appointment/components/file_upload_component.dart';
import 'package:kivicare_flutter/screens/appointment/components/patient_search_screen.dart';
import 'package:kivicare_flutter/screens/doctor/screens/service/add_service_screen.dart';
import 'package:kivicare_flutter/screens/patient/components/selected_clinic_widget.dart';
import 'package:kivicare_flutter/screens/patient/components/selected_doctor_widget.dart';
import 'package:kivicare_flutter/screens/receptionist/screens/patient/add_patient_screen.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:nb_utils/nb_utils.dart';

class Step3FinalSelectionScreen extends StatefulWidget {
  final int? id;
  final int? clinicId;
  final int? doctorId;
  final UpcomingAppointmentModel? data;

  Step3FinalSelectionScreen(
      {this.id, this.data, this.doctorId, required this.clinicId});

  @override
  State<Step3FinalSelectionScreen> createState() =>
      _Step3FinalSelectionScreenState();
}

class _Step3FinalSelectionScreenState extends State<Step3FinalSelectionScreen> {
  GlobalKey<FormState> formKey = GlobalKey();

  TextEditingController descriptionCont = TextEditingController(text: '');
  TextEditingController patientNameCont = TextEditingController();
  TextEditingController servicesCont = TextEditingController();

  bool isUpdate = false;
  bool isFirstTime = true;

  UserModel? user;

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    multiSelectStore.clearList();

    isUpdate = widget.data != null;

    if (isUpdate) {
      appointmentAppStore
          .setSelectedPatient(widget.data!.patientName.validate());
      appointmentAppStore
          .setSelectedPatientId(widget.data!.patientId.validate().toInt());
      appointmentAppStore.setSelectedTime(widget.data!.getAppointmentTime);

      if (widget.data!.patientName.validate().isNotEmpty) {
        patientNameCont.text = widget.data!.patientName.validate();
      }
      if (widget.data!.visitType.validate().isNotEmpty) {
        widget.data!.visitType.validate().forEach((element) {
          multiSelectStore.selectedService.add(ServiceData(
              id: element.id,
              name: element.serviceName,
              serviceId: element.serviceId));
        });

        servicesCont.text = "${multiSelectStore.selectedService.length} " +
            locale.lblServicesSelected;
      }

      descriptionCont.text = widget.data!.description.validate();

      if (widget.data!.appointmentReport.validate().isNotEmpty) {
        appointmentAppStore.addReportListString(
            data: widget.data!.appointmentReport.validate());
        appointmentAppStore.addReportData(
            data: widget.data!.appointmentReport
                .validate()
                .map((e) => PlatformFile(name: e.url, size: 220))
                .toList());
      }
    }
    setState(() {});
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  //region Widgets
  Widget buildProgressWidget() {
    return stepCountWidget(
      name: locale.lblSelectDateTime,
      currentCount: (isDoctor() || isReceptionist()) ? 2 : 3,
      totalCount: (isDoctor() || isReceptionist()) ? 2 : 3,
      percentage: 1,
    );
  }

  Widget buildClinicAndDoctorWidget() {
    return Row(
      children: [
        if (isProEnabled())
          SelectedClinicComponent(clinicId: widget.clinicId.validate())
              .expand(),
        if (isProEnabled()) 16.width,
        SelectedDoctorWidget(
          clinicId: widget.clinicId.validate(),
          doctorId: isUpdate
              ? widget.data!.doctorId.toInt()
              : widget.doctorId.validate(),
        ).expand(),
      ],
    );
  }

  Widget buildServicesWidget() {
    return Observer(
      builder: (context) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AppTextField(
              controller: servicesCont,
              textFieldType: TextFieldType.NAME,
              decoration: inputDecoration(
                context: context,
                labelText: locale.lblSelectServices,
                suffixIcon:
                    Icon(Icons.arrow_drop_down, color: context.iconColor),
              ).copyWith(
                border: multiSelectStore.selectedService.isNotEmpty
                    ? OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: radiusOnly(
                            topLeft: defaultRadius, topRight: defaultRadius))
                    : null,
                enabledBorder: multiSelectStore.selectedService.isNotEmpty
                    ? OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: radiusOnly(
                            topLeft: defaultRadius, topRight: defaultRadius))
                    : null,
                focusedBorder: multiSelectStore.selectedService.isNotEmpty
                    ? OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: radiusOnly(
                            topLeft: defaultRadius, topRight: defaultRadius))
                    : null,
              ),
              readOnly: true,
              onTap: () async {
                await MultiSelectWidget(
                  selectedServicesId: multiSelectStore.selectedService
                      .map((element) => element.id.validate())
                      .toList(),
                ).launch(context, pageRouteAnimation: PageRouteAnimation.Fade);

                servicesCont.text =
                    '${multiSelectStore.selectedService.length} ${locale.lblServicesSelected}';
              },
            ),
            if (multiSelectStore.selectedService.isNotEmpty)
              Container(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                decoration: boxDecorationDefault(
                    color: context.cardColor,
                    borderRadius: radiusOnly(
                        bottomLeft: defaultRadius, bottomRight: defaultRadius)),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text(
                                locale.lblService.getApostropheString(
                                    count:
                                        multiSelectStore.selectedService.length,
                                    apostrophe: false),
                                style: boldTextStyle(size: 12))
                            .expand(),
                        Text(
                            locale.lblCharges.getApostropheString(
                                count: multiSelectStore.selectedService.length,
                                apostrophe: false),
                            style: boldTextStyle(size: 12)),
                      ],
                    ),
                    Divider(),
                    Column(
                      children: List.generate(
                        multiSelectStore.selectedService.length,
                        (index) {
                          ServiceData data =
                              multiSelectStore.selectedService[index];
                          return Row(
                            children: [
                              Marquee(
                                      child: Text(data.name.validate(),
                                          style: primaryTextStyle(size: 14)))
                                  .expand(),
                              PriceWidget(
                                  price: data.charges
                                      .validate()
                                      .toDouble()
                                      .toStringAsFixed(1),
                                  textSize: 14),
                            ],
                          ).paddingBottom(2);
                        },
                      ),
                    ),
                    Divider(),
                    Row(
                      children: [
                        Text(locale.lblTotal, style: boldTextStyle(size: 14))
                            .expand(),
                        PriceWidget(
                            price: multiSelectStore.selectedService
                                .sumByDouble((p0) => p0.charges.toDouble())
                                .toString(),
                            textSize: 14),
                      ],
                    ),
                  ],
                ),
              ),
            if ((multiSelectStore.selectedService.isEmpty) &&
                (isDoctor() || isReceptionist()))
              Align(
                alignment: Alignment.centerRight,
                child: Text(locale.lblAddService,
                        style: secondaryTextStyle(
                            color: appPrimaryColor, size: 10))
                    .paddingOnly(top: 10)
                    .onTap(() => AddServiceScreen().launch(context)),
              )
          ],
        );
      },
    );
  }

  Widget buildPatientWidget() {
    return Column(
      children: [
        AppTextField(
          controller: patientNameCont,
          textFieldType: TextFieldType.OTHER,
          validator: (patient) {
            if (patient!.trim().isEmpty) return locale.lblPatientNameIsRequired;
            return null;
          },
          decoration: inputDecoration(
            context: context,
            labelText: locale.lblPatientName,
            suffixIcon: ic_user
                .iconImage(size: 10, color: context.iconColor)
                .paddingAll(14),
          ),
          readOnly: true,
          onTap: () async {
            PatientSearchScreen(selectedData: user)
                .launch(context)
                .then((value) {
              if (value != null) {
                user = value as UserModel;
                appointmentAppStore.setSelectedPatientId(user!.iD.validate());
                appointmentAppStore.setSelectedPatient(user!.displayName);
                patientNameCont.text = user!.displayName.validate();
              }
            });
          },
        ),
        if (patientNameCont.text.isEmptyOrNull)
          Align(
            alignment: Alignment.centerRight,
            child: Text(locale.lblAddNewPatient,
                    style: secondaryTextStyle(color: appPrimaryColor, size: 10))
                .paddingOnly(top: 10)
                .onTap(
                  () => AddPatientScreen().launch(context).then(
                    (value) {
                      if (value ?? false) {
                        init();
                        setState(() {});
                      }
                    },
                  ),
                ),
          )
      ],
    );
  }

  //endregion

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(locale.lblConfirmAppointment,
          systemUiOverlayStyle: defaultSystemUiOverlayStyle(context),
          textColor: Colors.white),
      body: Body(
        child: Stack(
          children: [
            Form(
              key: formKey,
              autovalidateMode: isFirstTime
                  ? AutovalidateMode.disabled
                  : AutovalidateMode.onUserInteraction,
              child: AnimatedScrollView(
                padding: EdgeInsets.fromLTRB(16, 16, 16, 60),
                listAnimationType: ListAnimationType.None,
                children: [
                  buildProgressWidget(),
                  16.height,
                  buildClinicAndDoctorWidget(),
                  16.height,
                  RoleWidget(
                    isShowDoctor: true,
                    isShowReceptionist: true,
                    child: buildPatientWidget(),
                  ),
                  RoleWidget(
                    isShowDoctor: true,
                    isShowReceptionist: true,
                    child: 16.height,
                  ),
                  buildServicesWidget(),
                  16.height,
                  AppointmentDateComponent(
                    initialDate: isUpdate
                        ? DateTime.parse(
                            widget.data!.appointmentStartDate.validate())
                        : null,
                  ),
                  16.height,
                  AppointmentSlots(
                    date: isUpdate
                        ? widget.data?.getAppointmentSaveDate
                        : getAppointmentDate,
                    clinicId: isUpdate
                        ? widget.data?.clinicId.validate()
                        : widget.clinicId.validate().toString(),
                    doctorId: isUpdate
                        ? widget.data?.doctorId.validate()
                        : widget.doctorId.validate().toString(),
                    appointmentTime: isUpdate
                        ? widget.data?.getAppointmentTime.validate()
                        : '',
                  ),
                  16.height,
                  AppTextField(
                    maxLines: 15,
                    minLines: 5,
                    controller: descriptionCont,
                    isValidationRequired: false,
                    textFieldType: TextFieldType.MULTILINE,
                    decoration: inputDecoration(
                        context: context, labelText: locale.lblDescription),
                  ),
                  16.height,
                  FileUploadComponent().paddingBottom(32),
                ],
              ),
            ),
            Positioned(
              left: 16,
              right: 16,
              bottom: 16,
              child: AppButton(
                text:
                    //'${locale.lblBook} ${locale.lblAppointments.split('s').first}',
                    '${locale.lblBook} ${locale.lblAppointment}',
                onTap: () async {
                  appointmentAppStore.setDescription(descriptionCont.text);
                  if (formKey.currentState!.validate()) {
                    formKey.currentState!.save();
                    await showInDialog(
                      context,
                      barrierDismissible: false,
                      backgroundColor: context.cardColor,
                      builder: (p0) {
                        return ConfirmAppointmentScreen(
                            appointmentId: widget.data?.id.toInt() ?? null);
                      },
                    );
                  } else {
                    isFirstTime = !isFirstTime;
                    setState(() {});
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
