import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/clinic_list_model.dart';
import 'package:kivicare_flutter/network/clinic_repository.dart';
import 'package:kivicare_flutter/screens/appointment/appointment_functions.dart';
import 'package:kivicare_flutter/screens/appointment/components/clinic_list_component.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:nb_utils/nb_utils.dart';

class Step1ClinicSelectionScreen extends StatefulWidget {
  final bool? sessionOrEncounter;

  Step1ClinicSelectionScreen({Key? key, this.sessionOrEncounter}) : super(key: key);

  @override
  State<Step1ClinicSelectionScreen> createState() => _Step1ClinicSelectionScreenState();
}

class _Step1ClinicSelectionScreenState extends State<Step1ClinicSelectionScreen> {
  Future<List<Clinic>>? future;

  List<Clinic> clinicList = [];

  int page = 1;
  int totalClinic = 0;

  bool isLastPage = false;

  @override
  void initState() {
    super.initState();
    appointmentAppStore.setDescription(null);
    appointmentAppStore.setSelectedPatient(null);
    appointmentAppStore.setSelectedClinic(null);
    appointmentAppStore.setSelectedTime(null);
    appointmentAppStore.setSelectedPatientId(null);
    appointmentAppStore.setSelectedDoctor(null);
    appointmentAppStore.clearAll();
    multiSelectStore.clearList();
    init();
  }

  void init() async {
    future = getClinicListAPI(page: page, appointmentList: clinicList, getTotalPatient: (p0) => totalClinic = p0, lastPageCallback: (p0) => isLastPage = p0);
    setState(() {});
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  void dispose() {
    appointmentAppStore.setSelectedClinic(null);

    super.dispose();
  }

  Widget buildBodyWidget() {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Stack(
        children: [
          if (widget.sessionOrEncounter == null)
            stepCountWidget(
              name: locale.lblChooseYourClinic,
              currentCount: 1,
              totalCount: isDoctor() ? 2 : 3,
              percentage: isDoctor() ? 0.33 : 0.50,
            ),
          SnapHelperWidget<List<Clinic>>(
            future: future,
            loadingWidget: LoaderWidget(),
            onSuccess: (snap) {
              return AnimatedListView(
                itemCount: snap.length,
                onNextPage: () {
                  if (!isLastPage) {
                    page++;
                    init();
                  }
                },
                padding: EdgeInsets.only(bottom: 60),
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  Clinic data = snap[index];

                  return GestureDetector(
                    onTap: () {
                      if (appointmentAppStore.mClinicSelected != null ? appointmentAppStore.mClinicSelected!.id.validate() == data.id.validate() : false) {
                        appointmentAppStore.setSelectedClinic(null);
                      } else {
                        appointmentAppStore.setSelectedClinic(data);
                      }
                    },
                    child: Observer(
                      builder: (context) {
                        bool isSelected = appointmentAppStore.mClinicSelected != null ? appointmentAppStore.mClinicSelected!.id.validate() == data.id.validate() : false;
                        return ClinicListComponent(data: data, isSelected: isSelected);
                      },
                    ),
                  );
                },
              );
            },
          ).paddingTop(widget.sessionOrEncounter != null ? 0 : 80),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(widget.sessionOrEncounter != null ? locale.lblSelectClinic : locale.lblAddNewAppointment, systemUiOverlayStyle: defaultSystemUiOverlayStyle(context), textColor: Colors.white),
      body: buildBodyWidget(),
      floatingActionButton: FloatingActionButton(
        child: Icon(widget.sessionOrEncounter != null ? Icons.done : Icons.arrow_forward_outlined),
        onPressed: () {
          if (appointmentAppStore.mClinicSelected == null)
            toast(locale.lblSelectOneClinic);
          else {
            if (widget.sessionOrEncounter != null)
              finish(context, appointmentAppStore.mClinicSelected);
            else
              clinicNavigation(context, clinicId: appointmentAppStore.mClinicSelected!.id.validate().toInt());
          }
        },
      ),
    );
  }
}
