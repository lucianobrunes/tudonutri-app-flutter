import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/date_extensions.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:moment_dart/moment_dart.dart';
import 'package:nb_utils/nb_utils.dart';

class AppointmentDateComponent extends StatefulWidget {
  final DateTime? initialDate;

  AppointmentDateComponent({this.initialDate});

  @override
  State<AppointmentDateComponent> createState() => _AppointmentDateComponentState();
}

class _AppointmentDateComponentState extends State<AppointmentDateComponent> {
  TextEditingController appointmentDateCont = TextEditingController();

  DateTime? selectedDate;
  bool isUpdate = false;

  @override
  void initState() {
    super.initState();
    isUpdate = widget.initialDate != null;
    appointmentDateCont.text = DateTime.now().getFormattedDate(SAVE_DATE_FORMAT);

    if (isUpdate) {
      appointmentDateCont.text = widget.initialDate!.getFormattedDate(SAVE_DATE_FORMAT);
      selectedDate = widget.initialDate!;
      appointmentAppStore.setSelectedAppointmentDate(widget.initialDate!);
    } else {
      selectedDate = DateTime.now().add(appStore.restrictAppointmentPre.days);
      //appointmentDateCont.text = selectedDate!.getFormattedDate(SAVE_DATE_FORMAT);
      appointmentAppStore.setSelectedAppointmentDate(selectedDate!);
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return AppTextField(
      controller: appointmentDateCont,
      textFieldType: TextFieldType.OTHER,
      decoration: inputDecoration(context: context, labelText: locale.lblAppointmentDate, suffixIcon: ic_calendar.iconImage(size: 10, color: context.iconColor).paddingAll(14)),
      readOnly: true,
      onTap: () async {
        selectedDate = await showDatePicker(
          context: context,
          initialDate: selectedDate!,
          firstDate: DateTime.now().add(appStore.restrictAppointmentPre.days),
          lastDate: DateTime.now().add(appStore.restrictAppointmentPost.days),
          helpText: locale.lblSelectAppointmentDate,
        );

        if (selectedDate != null) {
          appointmentDateCont.text = DateFormat(SAVE_DATE_FORMAT).format(selectedDate!);
          appointmentAppStore.setSelectedAppointmentDate(selectedDate!);
          LiveStream().emit(CHANGE_DATE, true);
          setState(() {});
        }
      },
      validator: (s) {
        if (s!.trim().isEmpty) return locale.lblFieldIsRequired;
        return null;
      },
    );
  }
}
