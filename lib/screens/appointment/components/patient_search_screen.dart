import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/body_widget.dart';
import 'package:kivicare_flutter/components/cached_image_widget.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/user_model.dart';
import 'package:kivicare_flutter/network/patient_list_repository.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:nb_utils/nb_utils.dart';

import '../../../locale/language_pt.dart';

class PatientSearchScreen extends StatefulWidget {
  final UserModel? selectedData;

  PatientSearchScreen({Key? key, this.selectedData}) : super(key: key);

  @override
  State<PatientSearchScreen> createState() => _PatientSearchScreenState();
}

class _PatientSearchScreenState extends State<PatientSearchScreen> {
  Future<List<UserModel>>? future;

  List<UserModel> patientList = [];

  int total = 0;
  int page = 1;
  bool isLastPage = false;
  bool isFirst = true;

  UserModel? selectedData;

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    future = getPatientListAPI(
        page: page,
        patientList: patientList,
        getTotalPatient: (b) => total = b,
        lastPageCallback: (b) => isLastPage = b);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget('${locale.lblPatients}',
          textColor: Colors.white,
          systemUiOverlayStyle: defaultSystemUiOverlayStyle(context)),
      body: SnapHelperWidget<List<UserModel>>(
        future: future,
        loadingWidget: LoaderWidget(),
        onSuccess: (snap) {
          if (widget.selectedData != null && isFirst) {
            selectedData = snap.firstWhere((element) =>
                element.iD.validate() == widget.selectedData!.iD.validate());
            isFirst = false;
          }
          if (snap.isEmpty) {
            return NoDataFoundWidget(text: locale.lblNoPatientFound);
          }

          return Body(
            child: AnimatedScrollView(
              padding: EdgeInsets.fromLTRB(16, 16, 16, 80),
              disposeScrollController: true,
              listAnimationType: ListAnimationType.None,
              physics: AlwaysScrollableScrollPhysics(),
              slideConfiguration: SlideConfiguration(verticalOffset: 400),
              onSwipeRefresh: () async {
                page = 1;
                init();
                return await 2.seconds.delay;
              },
              onNextPage: () {
                if (!isLastPage) {
                  page++;
                  init();
                  setState(() {});
                }
              },
              children: [
                Text(
                    (locale is LanguagePt)
                        ? '$TOTAL_PACIENTE ($total)'
                        : '$TOTAL_PATIENT ($total)',
                    style: boldTextStyle(size: 18)),
                10.height,
                AnimatedListView(
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: snap.length,
                  shrinkWrap: true,
                  itemBuilder: (_, index) {
                    UserModel data = snap[index];
                    return Theme(
                      data: ThemeData(unselectedWidgetColor: primaryColor),
                      child: RadioListTile<UserModel>(
                        controlAffinity: ListTileControlAffinity.trailing,
                        tileColor: context.cardColor,
                        secondary: CachedImageWidget(
                            url: data.profileImage.validate(),
                            height: 40,
                            radius: 20,
                            circle: true,
                            fit: BoxFit.cover),
                        shape: RoundedRectangleBorder(borderRadius: radius()),
                        value: data,
                        title: Text(
                            data.displayName.capitalizeFirstLetter().validate(),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: primaryTextStyle()),
                        onChanged: (v) {
                          selectedData = v;
                          setState(() {});
                        },
                        groupValue: selectedData,
                      ),
                    ).paddingSymmetric(vertical: 8);
                  },
                ),
              ],
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.done),
        onPressed: () {
          finish(context, selectedData);
        },
      ),
    );
  }
}
