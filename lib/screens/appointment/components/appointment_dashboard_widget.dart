import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/cached_image_widget.dart';
import 'package:kivicare_flutter/components/status_widget.dart';
import 'package:kivicare_flutter/model/upcoming_appointment_model.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:nb_utils/nb_utils.dart';

class AppointmentDashboardComponent extends StatelessWidget {
  final UpcomingAppointmentModel upcomingData;

  const AppointmentDashboardComponent({required this.upcomingData});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      width: context.width() - 32,
      decoration: boxDecorationDefault(color: context.cardColor, borderRadius: radius()),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CachedImageWidget(url: upcomingData.patientProfileImg.validate(), height: 60, radius: 30, fit: BoxFit.cover, circle: true),
              16.width,
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(upcomingData.id.validate().prefixText(value: '#'), style: secondaryTextStyle(color: primaryColor)),
                  2.height,
                  Text(getRoleWiseAppointmentFirstText(upcomingData), style: boldTextStyle()),
                  if (upcomingData.getVisitTypesInString.validate().isNotEmpty) 4.height,
                  if (upcomingData.getVisitTypesInString.validate().isNotEmpty) Text(upcomingData.getVisitTypesInString.validate(), style: secondaryTextStyle()),
                ],
              ).expand(),
              StatusWidget(
                isAppointmentStatus: true,
                status: upcomingData.status.validate(),
              ),
            ],
          ),
          16.height,
          Row(
            children: [
              Container(
                decoration: boxDecorationDefault(borderRadius: radius(), color: context.scaffoldBackgroundColor),
                padding: EdgeInsets.all(8),
                child: Text(
                  "${upcomingData.appointmentStartDate.validate().getFormattedDate(CONFIRM_APPOINTMENT_FORMAT)} : ${upcomingData.getDisplayAppointmentTime.validate()}",
                  style: secondaryTextStyle(),
                ).fit(fit: BoxFit.contain),
              ).expand(),
              16.width,
              if (!upcomingData.videoConsultation.validate())
                Container(
                  padding: EdgeInsets.all(8),
                  decoration: boxDecorationDefault(borderRadius: radius(), color: primaryColor),
                  child: ic_telemed.iconImage(color: Colors.white, size: 18),
                ),
            ],
          ),
          8.height
        ],
      ),
    );
  }
}
