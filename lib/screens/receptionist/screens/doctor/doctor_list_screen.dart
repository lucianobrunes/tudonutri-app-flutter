import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/body_widget.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/user_model.dart';
import 'package:kivicare_flutter/network/doctor_list_repository.dart';
import 'package:kivicare_flutter/screens/receptionist/screens/doctor/component/doctor_list_component.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/cached_value.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:nb_utils/nb_utils.dart';
import 'add_doctor_screen.dart';

class DoctorListScreen extends StatefulWidget {
  @override
  _DoctorListScreenState createState() => _DoctorListScreenState();
}

class _DoctorListScreenState extends State<DoctorListScreen> {
  Future<List<UserModel>>? future;

  List<UserModel> doctorList = [];

  int total = 0;
  int page = 1;

  bool isLastPage = false;

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    future = getDoctorListWithPagination(clinicId: userStore.userClinicId.validate().toInt(), page: page, doctorList: doctorList, getTotalDoctor: (b) => total = b, lastPageCallback: (b) => isLastPage = b);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget buildBodyWidget() {
    return FutureBuilder<List<UserModel>>(
      future: future,
      initialData: cachedDoctorList,
      builder: (context, snap) {
        if (snap.hasData) {
          if (snap.data.validate().isEmpty) {
            return NoDataFoundWidget(text: locale.lblNoDataFound);
          }

          return AnimatedScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            disposeScrollController: true,
            padding: EdgeInsets.fromLTRB(16, 16, 16, 80),
            listAnimationType: ListAnimationType.None,
            slideConfiguration: SlideConfiguration(verticalOffset: 400),
            onSwipeRefresh: () async {
              page = 1;
              init();
              return await 2.seconds.delay;
            },
            onNextPage: () {
              if (!isLastPage) {
                page++;
                init();
                setState(() {});
              }
            },
            children: [
              Text("${locale.lblClinic.getApostropheString()} ${locale.lblDoctor.getApostropheString(apostrophe: false)}($total)", style: boldTextStyle(size: fragmentTextSize)),
              8.height,
              AnimatedWrap(
                listAnimationType: listAnimationType,
                itemCount: snap.data.validate().length,
                itemBuilder: (context, index) => DoctorListComponent(
                  data: snap.data.validate()[index],
                  callForRefreshAfterDelete: () {
                    init();
                    setState(() {});
                  },
                ),
              ),
            ],
          );
        }
        return snapWidgetHelper(snap, loadingWidget: LoaderWidget());
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: isReceptionist() ? null : appBarWidget(locale.lblClinicDoctor, textColor: Colors.white, systemUiOverlayStyle: defaultSystemUiOverlayStyle(context)),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add, color: Colors.white),
          onPressed: () async {
            await AddDoctorScreen().launch(context).then(
              (value) {
                if (value ?? false) {
                  init();
                  setState(() {});
                }
              },
            );
          },
        ),
      ),
    );
  }
}
