import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:kivicare_flutter/app_theme.dart';
import 'package:kivicare_flutter/components/body_widget.dart';
import 'package:kivicare_flutter/components/gender_selection_component.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/qualification_model.dart';
import 'package:kivicare_flutter/model/static_data_model.dart';
import 'package:kivicare_flutter/network/auth_repository.dart';
import 'package:kivicare_flutter/screens/doctor/screens/add_qualification_screen.dart';
import 'package:kivicare_flutter/screens/receptionist/components/multi_select_specialization.dart';
import 'package:kivicare_flutter/screens/receptionist/components/qualification_item_widget.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/date_extensions.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:nb_utils/nb_utils.dart';

class AddDoctorScreen extends StatefulWidget {
  final int? doctorId;

  const AddDoctorScreen({Key? key, this.doctorId}) : super(key: key);

  @override
  State<AddDoctorScreen> createState() => _AddDoctorScreenState();
}

class _AddDoctorScreenState extends State<AddDoctorScreen> {
  GlobalKey<FormState> formKey = GlobalKey();

  UniqueKey genderKey = UniqueKey();

  TextEditingController firstNameCont = TextEditingController();
  TextEditingController lastNameCont = TextEditingController();
  TextEditingController emailCont = TextEditingController();
  TextEditingController userLogin = TextEditingController();
  TextEditingController contactNumberCont = TextEditingController();
  TextEditingController dobCont = TextEditingController();
  TextEditingController genderCont = TextEditingController();
  TextEditingController specializationCont = TextEditingController();
  TextEditingController addressCont = TextEditingController();
  TextEditingController cityCont = TextEditingController();
  TextEditingController postalCodeCont = TextEditingController();
  TextEditingController countryCont = TextEditingController();
  TextEditingController experienceCont = TextEditingController();
  TextEditingController signatureCont = TextEditingController();

  FocusNode firstNameFocus = FocusNode();
  FocusNode lastNameFocus = FocusNode();
  FocusNode emailFocus = FocusNode();
  FocusNode contactNumberFocus = FocusNode();
  FocusNode dobFocus = FocusNode();
  FocusNode genderFocus = FocusNode();
  FocusNode specializationFocus = FocusNode();
  FocusNode addressFocus = FocusNode();
  FocusNode cityFocus = FocusNode();
  FocusNode countryFocus = FocusNode();
  FocusNode postalCodeFocus = FocusNode();
  FocusNode experienceFocus = FocusNode();

  DateTime selectedDate = DateTime.now();
  List<Qualification> qualificationList = [];

  bool isUpdate = false;
  bool isFirstTime = true;

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    multiSelectStore.clearStaticList();
    isUpdate = widget.doctorId != null;

    if (isUpdate) {
      appStore.setLoading(true);

      getSingleUserDetailAPI(widget.doctorId.validate()).then((value) {
        firstNameCont.text = value.firstName.validate();
        lastNameCont.text = value.lastName.validate();
        userLogin.text = value.userLogin.validate();
        emailCont.text = value.userEmail.validate();
        contactNumberCont.text = value.mobileNumber.validate();
        dobCont.text = value.dob.validate();
        selectedDate = DateTime.parse(value.dob.validate());
        genderCont.text = value.gender.validate();
        addressCont.text = value.address.validate();
        cityCont.text = value.city.validate();
        postalCodeCont.text = value.postalCode.validate();
        countryCont.text = value.country.validate();
        experienceCont.text = value.noOfExperience.validate();
        signatureCont.text = value.signatureImg.validate();
        qualificationList = value.qualifications.validate();
        value.specialties.validate().forEach((e) {
          multiSelectStore.selectedStaticData.add(StaticData(id: e.id, label: e.label));
        });

        genderKey = UniqueKey();
        setState(() {});
      }).catchError((e) {
        log("GET DOCTOR ERROR : ${e.toString()} ");
      });
    }
    appStore.setLoading(false);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  //region Widgets
  Widget buildBasicDetailWidget() {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(locale.lblBasicDetails, style: boldTextStyle(color: context.primaryColor, size: 18)),
          Divider(color: viewLineColor),
          Wrap(
            runSpacing: 16,
            children: [
              Row(
                children: [
                  AppTextField(
                    controller: firstNameCont,
                    focus: firstNameFocus,
                    nextFocus: lastNameFocus,
                    textFieldType: TextFieldType.NAME,
                    decoration: inputDecoration(context: context, labelText: locale.lblFirstName),
                  ).expand(),
                  10.width,
                  AppTextField(
                    controller: lastNameCont,
                    focus: lastNameFocus,
                    nextFocus: emailFocus,
                    textFieldType: TextFieldType.NAME,
                    decoration: inputDecoration(context: context, labelText: locale.lblLastName),
                  ).expand(),
                ],
              ),
              AppTextField(
                controller: emailCont,
                focus: emailFocus,
                nextFocus: contactNumberFocus,
                textFieldType: TextFieldType.EMAIL,
                decoration: inputDecoration(context: context, labelText: locale.lblEmail),
              ),
              Row(
                children: [
                  AppTextField(
                    controller: contactNumberCont,
                    focus: contactNumberFocus,
                    nextFocus: dobFocus,
                    inputFormatters: [LengthLimitingTextInputFormatter(10)],
                    textFieldType: TextFieldType.PHONE,
                    decoration: inputDecoration(context: context, labelText: locale.lblContactNumber),
                  ).expand(),
                  16.width,
                  AppTextField(
                    controller: dobCont,
                    focus: dobFocus,
                    nextFocus: addressFocus,
                    readOnly: true,
                    validator: (s) {
                      if (s!.trim().isEmpty) return locale.lblFieldIsRequired;
                      return null;
                    },
                    textFieldType: TextFieldType.OTHER,
                    decoration: inputDecoration(context: context, labelText: locale.lblDOB),
                    onTap: () {
                      pickDateTimePicker().then((value) {});
                      if (dobCont.text.isNotEmpty) {
                        FocusScope.of(context).requestFocus(addressFocus);
                      }
                    },
                  ).expand(),
                ],
              ),
              GenderSelectionComponent(
                key: genderKey,
                type: genderCont.text,
                onTap: (value) {
                  genderCont.text = value;
                },
              ),
              GestureDetector(
                onTap: () {
                  MultiSelectSpecialization(selectedServicesId: multiSelectStore.selectedStaticData.validate().map((element) => element!.id.validate()).toList()).launch(context);
                },
                child: Container(
                  padding: EdgeInsets.fromLTRB(10, 16, 16, 16),
                  width: context.width(),
                  decoration: boxDecorationDefault(borderRadius: radius(), color: context.cardColor),
                  child: Observer(
                    builder: (_) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(locale.lblSpecialization, style: secondaryTextStyle()),
                          if (multiSelectStore.selectedStaticData.validate().isNotEmpty) 16.height,
                          Wrap(
                            spacing: 8,
                            runSpacing: 8,
                            children: List.generate(
                              multiSelectStore.selectedStaticData.length,
                              (index) {
                                StaticData data = multiSelectStore.selectedStaticData[index]!;
                                return Chip(
                                  label: Text(data.label.validate(), style: primaryTextStyle()),
                                  backgroundColor: context.cardColor,
                                  deleteIcon: Icon(Icons.clear, size: 18),
                                  deleteIconColor: Colors.red,
                                  onDeleted: () {
                                    multiSelectStore.removeStaticItem(data);
                                  },
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(defaultRadius), side: BorderSide(color: viewLineColor)),
                                );
                              },
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ),
              ),
              AppTextField(
                controller: experienceCont,
                focus: experienceFocus,
                textFieldType: TextFieldType.OTHER,
                keyboardType: TextInputType.number,
                decoration: inputDecoration(context: context, labelText: locale.lblExperience),
              ),
            ],
          ).paddingTop(8)
        ],
      ),
    );
  }

  Widget buildAddressWidget() {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(locale.lblAddressDetail, style: boldTextStyle(color: context.primaryColor, size: 18)),
          Divider(color: viewLineColor),
          Wrap(
            runSpacing: 16,
            children: [
              AppTextField(
                controller: addressCont,
                focus: addressFocus,
                nextFocus: cityFocus,
                isValidationRequired: false,
                textFieldType: TextFieldType.MULTILINE,
                minLines: 2,
                maxLines: 4,
                decoration: inputDecoration(context: context, labelText: locale.lblAddress).copyWith(alignLabelWithHint: true),
              ),
              Row(
                children: [
                  AppTextField(
                    controller: countryCont,
                    focus: countryFocus,
                    nextFocus: postalCodeFocus,
                    textFieldType: TextFieldType.OTHER,
                    decoration: inputDecoration(context: context, labelText: locale.lblCountry),
                  ).expand(),
                  16.width,
                  AppTextField(
                    controller: cityCont,
                    focus: cityFocus,
                    nextFocus: countryFocus,
                    textFieldType: TextFieldType.OTHER,
                    decoration: inputDecoration(context: context, labelText: locale.lblCity),
                  ).expand(),
                ],
              ),
              AppTextField(
                controller: postalCodeCont,
                focus: postalCodeFocus,
                textFieldType: TextFieldType.OTHER,
                decoration: inputDecoration(context: context, labelText: locale.lblPostalCode),
              ),
            ],
          ).paddingTop(8)
        ],
      ),
    );
  }

  Widget buildQualificationWidget() {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(locale.lblQualification.getApostropheString(apostrophe: false), style: boldTextStyle(color: context.primaryColor, size: 18)).expand(),
              TextButton(
                onPressed: () async {
                  await showModalBottomSheet(
                    context: context,
                    isScrollControlled: true,
                    builder: (context) => Padding(
                      padding: MediaQuery.of(context).viewInsets,
                      child: AddQualificationScreen(qualificationList: qualificationList),
                    ),
                  ).then((value) {
                    if (value != null) qualificationList = value;
                  });
                  setState(() {});
                  // await AddQualificationScreen(qualificationList: qualificationList).launch(context);
                },
                child: Text(locale.lblAddNewQualification, style: secondaryTextStyle()),
              )
            ],
          ),
          Divider(color: viewLineColor, height: 0),
          8.height,
          if (qualificationList.validate().isNotEmpty)
            AnimatedListView(
              shrinkWrap: true,
              itemCount: qualificationList.validate().length,
              itemBuilder: (context, index) => QualificationItemWidget(
                data: qualificationList[index],
                onEdit: () async {
                  await showModalBottomSheet(
                    context: context,
                    isScrollControlled: true,
                    builder: (context) => Padding(
                      padding: MediaQuery.of(context).viewInsets,
                      child: AddQualificationScreen(qualification: qualificationList[index], qualificationList: qualificationList),
                    ),
                  );
                  setState(() {});
                },
              ),
            )
          else
            NoDataFoundWidget(iconSize: 100, text: locale.lblNoQualificationsFound).center(),
        ],
      ),
    );
  }

  Widget buildSignatureWidget() {
    if (signatureCont.text.validate().isEmpty) return Offstage();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(locale.lblSignature, style: boldTextStyle()).paddingSymmetric(horizontal: 16),
        if (signatureCont.text.validate().isNotEmpty) Image.memory(getImageFromBase64(signatureCont.text.validate())!).cornerRadiusWithClipRRect(defaultRadius).paddingAll(16),
      ],
    );
  }

  //endregion

  Future<DateTime?> pickDateTimePicker({DateTime? currentDate}) async {
    DateTime? date = await showDatePicker(
      context: context,
      initialDate: currentDate ?? selectedDate,
      builder: (context, child) {
        return Theme(
          data: appStore.isDarkModeOn
              ? AppTheme.darkTheme
              : ThemeData(
                  colorScheme: ColorScheme.fromSeed(seedColor: primaryColor),
                ),
          child: child!,
        );
      },
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
    );
    if (DateTime.now().year - date!.year < 18) {
      Fluttertoast.cancel();
      toast(
        locale.lblMinimumAgeRequired + locale.lblCurrentAgeIs + ' ${DateTime.now().year - date.year}',
        bgColor: errorBackGroundColor,
        textColor: errorTextColor,
      );
      pickDateTimePicker(currentDate: date);
      return null;
    } else {
      selectedDate = date;
      dobCont.text = date.getFormattedDate(SAVE_DATE_FORMAT);
      return date;
    }
  }

  void saveDoctorDetails() async {
    Map<String, dynamic> request = {
      "first_name": firstNameCont.text.toString(),
      "last_name": lastNameCont.text.toString(),
      "user_email": emailCont.text.toString(),
      "user_login": userLogin.text.toString(),
      "mobile_number": contactNumberCont.text.toString(),
      "dob": dobCont.text.toString(),
      "gender": genderCont.text.toString(),
      "clinic_id": "${userStore.userClinicId}",
      "specialties": jsonEncode(multiSelectStore.selectedStaticData),
      "no_of_experience": experienceCont.text,
      "address": addressCont.text.toString(),
      "country": countryCont.text.toString(),
      "city": cityCont.text.toString(),
      "postal_code": postalCodeCont.text,
      'qualifications': jsonEncode(qualificationList),
    };
    if (isUpdate) {
      request.putIfAbsent("ID", () => widget.doctorId.validate());
      request.putIfAbsent("user_login", () => userLogin.text);
    }

    await addUpdateDoctorDetailsAPI(data: request).then((value) {
      //
    }).catchError((e) {
      log(e.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(
        isUpdate ? locale.lblEditProfile : locale.lblAddDoctorProfile,
        color: appPrimaryColor,
        elevation: 0,
        systemUiOverlayStyle: defaultSystemUiOverlayStyle(context),
        textColor: Colors.white,
      ),
      body: Body(
        child: Stack(
          children: [
            Form(
              key: formKey,
              child: AnimatedScrollView(
                padding: EdgeInsets.only(bottom: 80),
                listAnimationType: ListAnimationType.None,
                children: [
                  buildBasicDetailWidget(),
                  buildAddressWidget(),
                  buildQualificationWidget(),
                  if (isUpdate) buildSignatureWidget(),
                ],
              ),
            ),
            Positioned(
              right: 0,
              left: 0,
              bottom: 0,
              child: Container(
                color: context.scaffoldBackgroundColor,
                padding: EdgeInsets.all(16),
                child: AppButton(
                  text: locale.lblSave,
                  onTap: () {
                    if (formKey.currentState!.validate()) {
                      formKey.currentState!.save();
                      showConfirmDialogCustom(
                        context,
                        primaryColor: context.primaryColor,
                        width: context.width() * 0.7,
                        height: context.height() * 0.2,
                        title: locale.lblAreYouSureYouWantTo + " ${(isUpdate) ? locale.lblUpdate : locale.lblAdd} " + locale.lblDoctorDetails,
                        positiveText: locale.lblYes,
                        negativeText: locale.lblCancel,
                        onAccept: (p0) {
                          saveDoctorDetails();
                        },
                      );
                    } else {
                      isFirstTime = !isFirstTime;
                      setState(() {});
                    }
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
