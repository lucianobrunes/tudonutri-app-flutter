import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kivicare_flutter/components/cached_image_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/components/role_widget.dart';
import 'package:kivicare_flutter/components/view_all_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/network/doctor_list_repository.dart';
import 'package:kivicare_flutter/screens/patient/components/doctor_detail_widget.dart';
import 'package:kivicare_flutter/screens/patient/screens/review/component/review_widget.dart';
import 'package:kivicare_flutter/screens/patient/screens/review/rating_view_all_screen.dart';
import 'package:kivicare_flutter/screens/receptionist/screens/doctor/add_doctor_screen.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:nb_utils/nb_utils.dart';

import '../../../../model/user_model.dart';

// ignore: must_be_immutable
class DoctorDetailScreen extends StatefulWidget {
  UserModel doctorData;

  DoctorDetailScreen({Key? key, required this.doctorData}) : super(key: key);

  @override
  _DoctorDetailScreenState createState() => _DoctorDetailScreenState();
}

class _DoctorDetailScreenState extends State<DoctorDetailScreen> {
  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    //
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  Future<void> deleteDoctor(int doctorId) async {
    showConfirmDialogCustom(
      context,
      dialogType: DialogType.DELETE,
      title: locale.lblAreYouWantToDeleteDoctor,
      onAccept: (p0) {
        Map<String, dynamic> request = {
          "doctor_id": doctorId,
        };

        appStore.setLoading(true);

        deleteDoctorAPI(request).then((value) {
          toast(locale.lblDoctorDeleted);
          finish(context, true);
        }).catchError((e) {
          toast(e.toString());
        });

        appStore.setLoading(false);
      },
    );
  }

  Widget buildBasicDetailsWidget() {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('${locale.lblBasicDetails}: ', style: boldTextStyle()),
          8.height,
          if (widget.doctorData.displayName.validate().isNotEmpty)
            DoctorDetailWidget(
              width: context.width(),
              image: ic_user,
              bgColor:
                  appStore.isDarkModeOn ? cardDarkColor : context.cardColor,
              title: locale.lblName,
              subTitle: "${widget.doctorData.displayName.validate()}",
            ),
          10.height,
          if (widget.doctorData.userEmail.validate().isNotEmpty)
            DoctorDetailWidget(
              width: context.width(),
              image: ic_message,
              bgColor:
                  appStore.isDarkModeOn ? cardDarkColor : context.cardColor,
              title: locale.lblEmail,
              subTitle: "${widget.doctorData.userEmail.validate()}",
            ),
          10.height,
          Wrap(
            spacing: 10,
            runSpacing: 16,
            children: [
              Visibility(
                visible: widget.doctorData.mobileNumber.validate().isNotEmpty,
                child: DoctorDetailWidget(
                  image: ic_phone,
                  bgColor:
                      appStore.isDarkModeOn ? cardDarkColor : context.cardColor,
                  title: locale.lblContact,
                  subTitle: "${widget.doctorData.mobileNumber.validate()}",
                ),
              ),
              Visibility(
                visible:
                    !widget.doctorData.noOfExperience.validate().isNotEmpty,
                child: DoctorDetailWidget(
                  image: ic_experience,
                  bgColor:
                      appStore.isDarkModeOn ? cardDarkColor : context.cardColor,
                  title: locale.lblExperience,
                  subTitle: "${widget.doctorData.noOfExperience.validate()} " +
                      locale.lblYearsExperience,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget buildAvailableWeekDaysWidget() {
    String? daysEn = widget.doctorData.available;
    String? diasPtBr = traduzDiasSemana(daysEn);
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(locale.lblAvailableOn, style: boldTextStyle(size: 16)),
          16.height,
          widget.doctorData.available != null
              ? AnimatedWrap(
                  spacing: 16,
                  runSpacing: 10,
                  itemCount: widget.doctorData.available!.split(",").length,
                  listAnimationType: listAnimationType,
                  itemBuilder: (context, index) {
                    return Container(
                      width: context.width() / 4 - 20,
                      alignment: Alignment.center,
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                      decoration: boxDecorationDefault(
                          color: appStore.isDarkModeOn
                              ? cardDarkColor
                              : context.cardColor),
                      child: Text(
                        diasPtBr.split(",")[index].capitalizeFirstLetter(),
                        style: boldTextStyle(color: primaryColor, size: 14),
                      ),
                    );
                  },
                )
              : NoDataFoundWidget(iconSize: 120).center(),
        ],
      ),
    );
  }

  Widget buildSpecialityWidget() {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('${locale.lblSpecialities}: ', style: boldTextStyle()),
          16.height,
          Wrap(
            children: List.generate(
              widget.doctorData.specialties!.length,
              (index) => Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    decoration: boxDecorationDefault(
                        shape: BoxShape.circle, color: primaryColor),
                    height: 8,
                    width: 8,
                  ),
                  8.width,
                  Text(widget.doctorData.specialties![index].label.validate(),
                      style: primaryTextStyle()),
                  16.width,
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildReviewWidget() {
    if (isProEnabled()) return Offstage();
    return Container(
      padding: EdgeInsets.fromLTRB(16, 8, 16, 16),
      child: Column(
        children: [
          ViewAllLabel(
            label: locale.lblRatingsAndReviews,
            subLabel: locale.lblKnowWhatYourPatientsSaysAboutYou,
            viewAllShowLimit: 5,
            list: widget.doctorData.ratingList.validate(),
            onTap: () {
              RatingViewAllScreen(doctorId: widget.doctorData.iD.validate())
                  .launch(context);
            },
          ),
          16.height,
          if (widget.doctorData.ratingList.validate().isNotEmpty)
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: widget.doctorData.ratingList.validate().length,
              itemBuilder: (context, index) => ReviewWidget(
                  data: widget.doctorData.ratingList.validate()[index]),
            )
          else
            NoDataFoundWidget(
                    iconSize: 120, text: "Oops " + locale.lblNoReviewsFound)
                .center(),
        ],
      ),
    );
  }

  Widget buildProfileWidget() {
    return Hero(
      tag: widget.doctorData.iD.validate(),
      child: CachedImageWidget(
        height: 150,
        width: 150,
        fit: BoxFit.cover,
        url: widget.doctorData.profileImage.validate(),
        circle: true,
      ).center().paddingTop(20).paddingBottom(20),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(
        locale.lblDoctorDetails,
        textColor: Colors.white,
        systemUiOverlayStyle: defaultSystemUiOverlayStyle(context),
        actions: [
          RoleWidget(
            isShowReceptionist: true,
            child: IconButton(
              icon: FaIcon(Icons.edit, size: 20, color: Colors.white),
              onPressed: () async {
                await AddDoctorScreen(doctorId: widget.doctorData.iD.validate())
                    .launch(context)
                    .then(
                      (value) => (value) {
                        if (value != null) {
                          widget.doctorData = value;
                        }
                      },
                    );
              },
            ),
          ),
          RoleWidget(
            isShowReceptionist: true,
            child: IconButton(
              icon: FaIcon(Icons.delete, size: 20, color: Colors.red),
              onPressed: () => deleteDoctor(widget.doctorData.iD.validate()),
            ).paddingRight(16),
          ),
        ],
      ),
      body: AnimatedScrollView(
        listAnimationType: ListAnimationType.None,
        padding: EdgeInsets.only(bottom: 16),
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          buildProfileWidget(),
          buildBasicDetailsWidget(),
          buildSpecialityWidget(),
          buildAvailableWeekDaysWidget(),
          buildReviewWidget(),
        ],
      ),
    );
  }
}
