import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kivicare_flutter/components/cached_image_widget.dart';
import 'package:kivicare_flutter/model/user_model.dart';
import 'package:kivicare_flutter/screens/receptionist/screens/doctor/doctor_details_screen.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:nb_utils/nb_utils.dart';

import '../../../../../main.dart';

class DoctorListComponent extends StatelessWidget {
  final UserModel data;
  final bool? isSelected;
  final Function()? callForRefreshAfterDelete;

  DoctorListComponent({required this.data, this.isSelected, this.callForRefreshAfterDelete});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 8, bottom: 8),
      //width: context.width() / 2 - 24,
      padding: EdgeInsets.all(12),
      decoration: boxDecorationDefault(
        borderRadius: radius(),
        color: context.cardColor,
        border: Border.all(color: isSelected.validate(value: false) ? context.primaryColor : context.cardColor),
      ),

      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Hero(tag: data.iD.validate(), child: CachedImageWidget(height: 76, width: 76, fit: BoxFit.cover, url: data.profileImage.validate(), circle: true)),
          24.width,
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              6.height,
              Text(data.displayName.validate().prefixText(value: 'Dr. '), style: boldTextStyle(size: 16)),
              6.height,
              if (data.specialties.validate().isNotEmpty) Text(data.getDoctorSpeciality, maxLines: 2, style: secondaryTextStyle(size: 14)),
              6.height,
              if (data.noOfExperience.validate().isNotEmpty) Text("${data.noOfExperience.validate()} ${locale.lblYearsOfExperience}", maxLines: 2, style: secondaryTextStyle(size: 14)),
            ],
          ).expand(),
          AppButton(
            padding: EdgeInsets.symmetric(vertical: 2),
            width: 80,
            text: locale.lblViewDetails,
            shapeBorder: RoundedRectangleBorder(borderRadius: radius()),
            textStyle: primaryTextStyle(color: Colors.white),
            color: context.primaryColor,
            onTap: () {
              DoctorDetailScreen(
                doctorData: data,
              ).launch(context, pageRouteAnimation: PageRouteAnimation.Fade, duration: 800.milliseconds).then(
                (isDoctorDeleted) {
                  if (isDoctorDeleted ?? false) {
                    finish(context, callForRefreshAfterDelete?.call());
                  }
                },
              );
            },
          )
        ],
      ),
    );
  }
}

/*child: Column(
        children: [
          Hero(tag: data.iD.validate(), child: CachedImageWidget(height: 60, width: 60, fit: BoxFit.cover, url: data.profileImage.validate(), circle: true)),
          6.height,
          Text(data.displayName.validate().prefixText(value: 'Dr. '), style: boldTextStyle(size: 16)),
          if (data.specialties.validate().isNotEmpty) Text(data.getDoctorSpeciality, maxLines: 2, style: secondaryTextStyle(size: 12)),
          2.height,
          if (data.noOfExperience.validate().isNotEmpty) Text("${data.noOfExperience.validate()} ${locale.lblYearsOfExperience}", maxLines: 2, style: secondaryTextStyle(size: 12)),
          8.height,
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: EdgeInsets.all(8),
                decoration: boxDecorationDefault(color: appPrimaryColor.withOpacity(0.15)),
                child: ic_phone.iconImage(color: appPrimaryColor, size: 18),
              ).onTap(() {}),
              8.width,
              Container(
                padding: EdgeInsets.all(8),
                decoration: boxDecorationDefault(color: appSecondaryColor.withOpacity(0.20)),
                child: Icon(Icons.email_outlined, color: appSecondaryColor, size: 18),
              ).onTap(() {}),
            ],
          ),
          8.height,
          AppButton(
            padding: EdgeInsets.symmetric(horizontal: 8),
            text: locale.lblViewDetails,
            shapeBorder: RoundedRectangleBorder(borderRadius: radius()),
            textStyle: primaryTextStyle(color: Colors.white),
            color: context.primaryColor,
            onTap: () {
              DoctorDetailScreen(
                doctorData: data,
              ).launch(context, pageRouteAnimation: PageRouteAnimation.Fade, duration: 800.milliseconds).then(
                (isDoctorDeleted) {
                  if (isDoctorDeleted ?? false) {
                    finish(context, callForRefreshAfterDelete?.call());
                  }
                },
              );
            },
          )
        ],
      ),*/
