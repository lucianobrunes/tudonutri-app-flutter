import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kivicare_flutter/app_theme.dart';
import 'package:kivicare_flutter/components/body_widget.dart';
import 'package:kivicare_flutter/components/cached_image_widget.dart';
import 'package:kivicare_flutter/components/custom_image_picker.dart';
import 'package:kivicare_flutter/components/gender_selection_component.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/network/auth_repository.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/date_extensions.dart';
import 'package:kivicare_flutter/utils/extensions/enums.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:nb_utils/nb_utils.dart';

class EditProfilesScreen extends StatefulWidget {
  const EditProfilesScreen({Key? key}) : super(key: key);

  @override
  State<EditProfilesScreen> createState() => _EditProfilesScreenState();
}

class _EditProfilesScreenState extends State<EditProfilesScreen> {
  GlobalKey<FormState> globalKey = GlobalKey();
  UniqueKey genderKey = UniqueKey();

  TextEditingController firstNameCont = TextEditingController();
  TextEditingController lastNameCont = TextEditingController();
  TextEditingController emailCont = TextEditingController();
  TextEditingController contactNumberCont = TextEditingController();
  TextEditingController dobCont = TextEditingController();
  TextEditingController genderCont = TextEditingController();
  TextEditingController addressCont = TextEditingController();
  TextEditingController cityCont = TextEditingController();
  TextEditingController postalCodeCont = TextEditingController();
  TextEditingController countryCont = TextEditingController();

  FocusNode firstNameFocus = FocusNode();
  FocusNode lastNameFocus = FocusNode();
  FocusNode emailFocus = FocusNode();
  FocusNode contactNumberFocus = FocusNode();
  FocusNode dobFocus = FocusNode();
  FocusNode genderFocus = FocusNode();
  FocusNode specializationFocus = FocusNode();
  FocusNode addressFocus = FocusNode();
  FocusNode cityFocus = FocusNode();
  FocusNode countryFocus = FocusNode();
  FocusNode postalCodeFocus = FocusNode();
  File? selectedProfileImage;

  DateTime selectedDate = DateTime.now();
  bool isUpdate = false;

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    getUserData();
  }

  Future<void> getUserData() async {
    appStore.setLoading(true);

    await getSingleUserDetailAPI(userStore.userId.validate()).then((value) {
      firstNameCont.text = value.firstName.validate();
      lastNameCont.text = value.lastName.validate();
      emailCont.text = value.userEmail.validate();
      contactNumberCont.text = value.mobileNumber.validate();
      dobCont.text = value.dob.validate();
      selectedDate = DateTime.parse(value.dob.validate());
      genderCont.text = value.gender.validate();
      genderKey = UniqueKey();
      addressCont.text = value.address.validate();
      cityCont.text = value.city.validate();
      postalCodeCont.text = value.postalCode.validate();
      countryCont.text = value.country.validate();
      setState(() {});
    }).catchError((e) {
      //
    });

    appStore.setLoading(false);
  }

  Future<DateTime?> pickDateTimePicker({DateTime? currentDate}) async {
    DateTime? date = await showDatePicker(
      context: context,
      initialDate: currentDate ?? selectedDate,
      builder: (context, child) {
        return Theme(
          data: appStore.isDarkModeOn
              ? AppTheme.darkTheme
              : ThemeData(
                  colorScheme: ColorScheme.fromSeed(seedColor: primaryColor),
                ),
          child: child!,
        );
      },
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
    );
    if (DateTime.now().year - date!.year < 18) {
      Fluttertoast.cancel();
      toast(
        locale.lblMinimumAgeRequired + locale.lblCurrentAgeIs + ' ${DateTime.now().year - date.year}',
        bgColor: errorBackGroundColor,
        textColor: errorTextColor,
      );
      pickDateTimePicker(currentDate: date);
      return null;
    } else {
      selectedDate = date;
      dobCont.text = date.getFormattedDate(SAVE_DATE_FORMAT);
      return date;
    }
  }

  Future<void> saveUserData() async {
    Map<String, dynamic> request = {
      "ID": getIntAsync(USER_ID),
      "user_login": getStringAsync(USER_LOGIN),
      "first_name": firstNameCont.text.toString(),
      "last_name": lastNameCont.text.toString(),
      "user_email": emailCont.text.toString(),
      "mobile_number": contactNumberCont.text.toString(),
      "dob": dobCont.text.toString(),
      "gender": genderCont.text.toString(),
      "clinic_id": "${userStore.userClinicId}",
      "address": addressCont.text.toString(),
      "country": countryCont.text.toString(),
      "city": cityCont.text.toString(),
      "postal_code": postalCodeCont.text,
    };

    updateProfileAPI(data: request, profileImage: selectedProfileImage).then((value) {}).catchError((e) {
      log(e.toString());
    });
  }

  Widget buildBasicDetailWidget() {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(locale.lblBasicDetails, style: boldTextStyle(color: context.primaryColor, size: 18)),
          Divider(color: viewLineColor),
          Wrap(
            runSpacing: 16,
            children: [
              Stack(
                clipBehavior: Clip.none,
                children: <Widget>[
                  Container(
                    decoration: boxDecorationDefault(color: appStore.isDarkModeOn ? cardDarkColor : context.scaffoldBackgroundColor, shape: BoxShape.circle),
                    child: selectedProfileImage != null
                        ? Image.file(File(selectedProfileImage!.path), height: 126, width: 126, fit: BoxFit.cover, alignment: Alignment.center).cornerRadiusWithClipRRect(180)
                        : CachedImageWidget(url: userStore.profileImage.validate(), height: 126, fit: BoxFit.cover, circle: true),
                  ),
                  Positioned(
                    bottom: -4,
                    right: 0,
                    child: Container(
                      padding: EdgeInsets.all(8),
                      decoration: boxDecorationDefault(color: appPrimaryColor, shape: BoxShape.circle, border: Border.all(color: white, width: 3)),
                      child: ic_camera.iconImage(size: 14, color: Colors.white),
                    ).onTap(() async {
                      await showInDialog(
                        context,
                        contentPadding: EdgeInsets.symmetric(vertical: 16),
                        title: Text(locale.lblChooseAction, style: boldTextStyle()),
                        builder: (p0) {
                          return FilePickerDialog(isSelected: (false));
                        },
                      ).then((file) async {
                        if (file != null) {
                          if (file == GalleryFileTypes.CAMERA) {
                            await getCameraImage().then((value) {
                              selectedProfileImage = value;
                              setState(() {});
                            });
                          } else if (file == GalleryFileTypes.GALLERY) {
                            await getCameraImage(isCamera: false).then((value) {
                              selectedProfileImage = value;
                              setState(() {});
                            });
                          }
                        }
                      });
                    }),
                  )
                ],
              ).center().paddingBottom(16),
              Row(
                children: [
                  AppTextField(
                    controller: firstNameCont,
                    focus: firstNameFocus,
                    nextFocus: lastNameFocus,
                    textFieldType: TextFieldType.NAME,
                    decoration: inputDecoration(context: context, labelText: locale.lblFirstName),
                  ).expand(),
                  10.width,
                  AppTextField(
                    controller: lastNameCont,
                    focus: lastNameFocus,
                    nextFocus: emailFocus,
                    textFieldType: TextFieldType.NAME,
                    decoration: inputDecoration(context: context, labelText: locale.lblLastName),
                  ).expand(),
                ],
              ),
              AppTextField(
                controller: emailCont,
                focus: emailFocus,
                nextFocus: contactNumberFocus,
                textFieldType: TextFieldType.EMAIL,
                decoration: inputDecoration(context: context, labelText: locale.lblEmail),
              ),
              Row(
                children: [
                  AppTextField(
                    controller: contactNumberCont,
                    focus: contactNumberFocus,
                    nextFocus: dobFocus,
                    inputFormatters: [LengthLimitingTextInputFormatter(10)],
                    textFieldType: TextFieldType.PHONE,
                    decoration: inputDecoration(context: context, labelText: locale.lblContactNumber),
                  ).expand(),
                  16.width,
                  AppTextField(
                    controller: dobCont,
                    focus: dobFocus,
                    nextFocus: addressFocus,
                    readOnly: true,
                    validator: (s) {
                      if (s!.trim().isEmpty) return locale.lblFieldIsRequired;
                      return null;
                    },
                    textFieldType: TextFieldType.OTHER,
                    decoration: inputDecoration(context: context, labelText: locale.lblDOB),
                    onTap: () {
                      pickDateTimePicker().then((value) {});
                      if (dobCont.text.isNotEmpty) {
                        FocusScope.of(context).requestFocus(addressFocus);
                      }
                    },
                  ).expand(),
                ],
              ),
              GenderSelectionComponent(
                key: genderKey,
                type: genderCont.text,
                onTap: (value) {
                  genderCont.text = value;
                },
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget buildAddressWidget() {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(locale.lblAddressDetail, style: boldTextStyle(color: context.primaryColor, size: 18)),
          Divider(color: viewLineColor),
          8.height,
          Wrap(
            runSpacing: 16,
            children: [
              AppTextField(
                controller: addressCont,
                focus: addressFocus,
                nextFocus: cityFocus,
                isValidationRequired: false,
                textFieldType: TextFieldType.MULTILINE,
                minLines: 2,
                maxLines: 4,
                decoration: inputDecoration(context: context, labelText: locale.lblAddress).copyWith(alignLabelWithHint: true),
              ),
              Row(
                children: [
                  AppTextField(
                    controller: countryCont,
                    focus: countryFocus,
                    nextFocus: postalCodeFocus,
                    textFieldType: TextFieldType.OTHER,
                    decoration: inputDecoration(context: context, labelText: locale.lblCountry),
                  ).expand(),
                  16.width,
                  AppTextField(
                    controller: cityCont,
                    focus: cityFocus,
                    nextFocus: countryFocus,
                    textFieldType: TextFieldType.OTHER,
                    decoration: inputDecoration(context: context, labelText: locale.lblCity),
                  ).expand(),
                ],
              ),
              AppTextField(
                controller: postalCodeCont,
                focus: postalCodeFocus,
                textFieldType: TextFieldType.OTHER,
                decoration: inputDecoration(context: context, labelText: locale.lblPostalCode),
              ),
            ],
          )
        ],
      ),
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(
        locale.lblEditProfile,
        color: appPrimaryColor,
        elevation: 0,
        systemUiOverlayStyle: defaultSystemUiOverlayStyle(context),
        textColor: Colors.white,
      ),
      body: Body(
        child: Stack(
          children: [
            Form(
              key: globalKey,
              child: AnimatedScrollView(
                listAnimationType: ListAnimationType.None,

                padding: EdgeInsets.only(bottom: 80),
                children: [
                  buildBasicDetailWidget(),
                  buildAddressWidget(),
                ],
              ),
            ),
            Positioned(
              right: 0,
              left: 0,
              bottom: 0,
              child: Container(
                color: context.scaffoldBackgroundColor,
                padding: EdgeInsets.all(16),
                child: AppButton(
                  text: locale.lblSave,
                  onTap: () {
                    if (globalKey.currentState!.validate()) {
                      globalKey.currentState!.save();
                      saveUserData();
                    }
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
