import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/body_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/network/auth_repository.dart';
import 'package:kivicare_flutter/network/patient_list_repository.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/date_extensions.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:nb_utils/nb_utils.dart';

class AddPatientScreen extends StatefulWidget {
  final int? userId;

  AddPatientScreen({this.userId});

  @override
  _AddPatientScreenState createState() => _AddPatientScreenState();
}

class _AddPatientScreenState extends State<AddPatientScreen> {
  GlobalKey<FormState> formKey = GlobalKey();
  UniqueKey genderKey = UniqueKey();

  DateTime? birthDate;

  String? bloodGroup;
  String? userLogin = "";

  bool isUpdate = false;
  bool isFirstTime = true;

  List<String> bloodGroupList = ['A+', 'B+', 'AB+', 'O+', 'A-', 'B-', 'AB-', 'O-'];

  TextEditingController firstNameCont = TextEditingController();
  TextEditingController lastNameCont = TextEditingController();
  TextEditingController emailCont = TextEditingController();
  TextEditingController contactNumberCont = TextEditingController();
  TextEditingController dOBCont = TextEditingController();
  TextEditingController addressCont = TextEditingController();
  TextEditingController cityCont = TextEditingController();
  TextEditingController countryCont = TextEditingController();
  TextEditingController postalCodeCont = TextEditingController();
  TextEditingController genderCont = TextEditingController();

  FocusNode lastNameFocus = FocusNode();
  FocusNode emailFocus = FocusNode();
  FocusNode contactNumberFocus = FocusNode();
  FocusNode dOBFocus = FocusNode();
  FocusNode genderFocus = FocusNode();
  FocusNode addressFocus = FocusNode();
  FocusNode cityFocus = FocusNode();
  FocusNode countryFocus = FocusNode();
  FocusNode postalCodeFocus = FocusNode();

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    isUpdate = widget.userId != null;
    birthDate = DateTime.now();
    if (isUpdate) {
      appStore.setLoading(true);

      await getSingleUserDetailAPI(widget.userId).then((value) {
        firstNameCont.text = value.firstName.validate();
        lastNameCont.text = value.lastName.validate();
        emailCont.text = value.userEmail.validate();
        contactNumberCont.text = value.mobileNumber.validate();
        addressCont.text = value.address.validate(value: '');
        cityCont.text = value.city.validate();
        countryCont.text = value.country.validate();
        postalCodeCont.text = value.postalCode.validate();
        userLogin = value.userLogin.validate();
        genderCont.text = value.gender.validate();
        dOBCont.text = value.dob.validate();
        if (!value.dob.isEmptyOrNull) birthDate = DateTime.parse(value.dob.validate(value: ' '));
        if (!value.bloodGroup.isEmptyOrNull) bloodGroup = value.bloodGroup.validate(value: '');
        setState(() {});
      }).catchError((e) {
        toast(e.toString());
      });
      appStore.setLoading(false);
    }
  }

  void addNewPatientDetail() async {
    appStore.setLoading(true);

    Map request = {
      "first_name": firstNameCont.text.validate(),
      "last_name": lastNameCont.text.validate(),
      "user_email": emailCont.text.validate(),
      "mobile_number": contactNumberCont.text.validate(),
      "gender": genderCont.text.toLowerCase(),
      "dob": birthDate!.getFormattedDate(SAVE_DATE_FORMAT).validate(),
      "address": addressCont.text.validate(),
      "city": cityCont.text.validate(),
      "country": countryCont.text.validate(),
      "postal_code": postalCodeCont.text.validate(),
      "blood_group": bloodGroup.validate(),
    };
    request.putIfAbsent('clinic_id', () => userStore.userClinicId.validate());

    await addNewPatientDataAPI(request).then((value) {
      finish(context, true);
      toast(locale.lblNewPatientAddedSuccessfully);
    }).catchError((e) {
      toast(e.toString());
    });
    appStore.setLoading(false);
  }

  void updatePatientDetail() async {
    appStore.setLoading(true);

    Map request = {
      "ID": widget.userId,
      "first_name": firstNameCont.text.validate(),
      "last_name": lastNameCont.text.validate(),
      "user_email": emailCont.text.validate(),
      "mobile_number": contactNumberCont.text.validate(),
      "gender": genderCont.text.validate(),
      "dob": birthDate != null ? birthDate!.getFormattedDate(SAVE_DATE_FORMAT).validate() : null,
      "address": addressCont.text.validate(),
      "city": cityCont.text.validate(),
      "country": countryCont.text.validate(),
      "postal_code": postalCodeCont.text.validate(),
      "blood_group": bloodGroup.validate(),
      "user_login": userLogin,
    };

    await updatePatientDataAPI(request).then((value) {
      toast(locale.lblPatientDetailUpdatedSuccessfully);
      finish(context, true);
    }).catchError((e) {
      toast(e.toString());
    });
    appStore.setLoading(false);
  }

  void savePatientDetails() async {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      showConfirmDialogCustom(
        context,
        primaryColor: context.primaryColor,
        width: context.width() * 0.7,
        height: context.height() * 0.2,
        title: "${locale.lblAreYouSureYouWantTo} ${(isUpdate) ? locale.lblUpdate : locale.lblAdd} ${locale.lblPatientDetails}",
        positiveText: locale.lblYes,
        negativeText: locale.lblCancel,
        onAccept: (p0) {
          isUpdate ? updatePatientDetail() : addNewPatientDetail();
        },
      );
    } else {
      isFirstTime = false;
      setState(() {});
    }
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    getDisposeStatusBarColor();
    super.dispose();
  }

  Future<void> dateBottomSheet(context, {DateTime? bDate}) async {
    await showModalBottomSheet(
      context: context,
      builder: (BuildContext e) {
        return Container(
          height: 245,
          color: appStore.isDarkModeOn ? Colors.black : Colors.white,
          child: Column(
            children: [
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(locale.lblCancel, style: boldTextStyle()).onTap(() {
                      finish(context);
                    }),
                    Text(locale.lblDone, style: boldTextStyle()).onTap(() {
                      if (DateTime.now().year - birthDate!.year < 18) {
                        toast(locale.lblMinimumAgeRequired + locale.lblCurrentAgeIs + ' ${DateTime.now().year - birthDate!.year}');
                      } else {
                        finish(context);
                        dOBCont.text = birthDate!.getFormattedDate(SAVE_DATE_FORMAT).toString();
                      }
                    })
                  ],
                ).paddingOnly(top: 8, left: 8, right: 8, bottom: 8),
              ),
              Container(
                height: 200,
                child: CupertinoTheme(
                  data: CupertinoThemeData(textTheme: CupertinoTextThemeData(dateTimePickerTextStyle: primaryTextStyle(size: 20))),
                  child: CupertinoDatePicker(
                    minimumDate: DateTime(1900, 1, 1),
                    minuteInterval: 1,
                    initialDateTime: bDate == null ? DateTime.now() : bDate,
                    mode: CupertinoDatePickerMode.date,
                    onDateTimeChanged: (DateTime dateTime) {
                      birthDate = dateTime;
                      setState(() {});
                    },
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget buildBasicDetailWidget() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          16.height,
          Text(locale.lblBasicInformation, style: boldTextStyle(size: titleTextSize, color: context.primaryColor)),
          Divider(color: viewLineColor),
          8.height,
          Row(
            children: [
              AppTextField(
                controller: firstNameCont,
                nextFocus: lastNameFocus,
                textFieldType: TextFieldType.NAME,
                errorThisFieldRequired: locale.lblFirstNameIsRequired,
                decoration: inputDecoration(context: context, labelText: locale.lblFirstName),
              ).expand(),
              16.width,
              AppTextField(
                controller: lastNameCont,
                nextFocus: emailFocus,
                errorThisFieldRequired: locale.lblLastNameIsRequired,
                textFieldType: TextFieldType.NAME,
                decoration: inputDecoration(context: context, labelText: locale.lblLastName),
              ).expand(),
            ],
          ),
          16.height,
          AppTextField(
            controller: emailCont,
            nextFocus: contactNumberFocus,
            textFieldType: TextFieldType.EMAIL,
            errorThisFieldRequired: locale.lblEmailIsRequired,
            decoration: inputDecoration(context: context, labelText: locale.lblEmail, suffixIcon: ic_message.iconImage(size: 10, color: context.iconColor).paddingAll(14)),
          ),
          16.height,
          AppTextField(
            nextFocus: dOBFocus,
            controller: contactNumberCont,
            textFieldType: TextFieldType.PHONE,
            maxLength: 10,
            isValidationRequired: true,
            buildCounter: (context, {int? currentLength, bool? isFocused, maxLength}) {
              return null;
            },
            validator: (value) {
              if (contactNumberCont.text.length < 10) return locale.lblPleaseCheckYourNumber;
            },
            decoration: inputDecoration(context: context, labelText: locale.lblContactNumber, suffixIcon: ic_phone.iconImage(size: 10, color: context.iconColor).paddingAll(14)),
          ),
          16.height,
          AppTextField(
            controller: dOBCont,
            focus: dOBFocus,
            nextFocus: genderFocus,
            textFieldType: TextFieldType.OTHER,
            readOnly: true,
            decoration: inputDecoration(context: context, labelText: locale.lblDOB, suffixIcon: ic_calendar.iconImage(size: 10, color: context.iconColor).paddingAll(14)),
            onTap: () {
              if (dOBCont.text.isNotEmpty) {
                dateBottomSheet(context, bDate: birthDate);
              } else {
                dateBottomSheet(context);
              }
            },
          ),
          16.height,
        ],
      ),
    );
  }

  Widget buildGenderSelectionWidget() {
    return Container(
      padding: EdgeInsets.all(16),
      decoration: boxDecorationDefault(color: context.cardColor),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(locale.lblGender1, style: secondaryTextStyle()),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              RadioListTile<String>(
                value: locale.lblMale.toLowerCase(),
                groupValue: genderCont.text,
                title: Text(locale.lblMale, style: secondaryTextStyle()),
                visualDensity: VisualDensity(horizontal: -4.0),
                contentPadding: EdgeInsets.zero,
                onChanged: (gender) {
                  genderCont.text = gender.validate();
                  setState(() {});
                },
              ).expand(),
              RadioListTile<String>(
                value: locale.lblFemale.toLowerCase(),
                groupValue: genderCont.text,
                title: Text(locale.lblFemale, style: secondaryTextStyle()),
                visualDensity: VisualDensity(horizontal: -4.0),
                contentPadding: EdgeInsets.zero,
                onChanged: (gender) {
                  genderCont.text = gender.validate();
                  setState(() {});
                },
              ).expand(),
              16.width,
              RadioListTile<String>(
                value: locale.lblOther.toLowerCase(),
                groupValue: genderCont.text,
                title: Text(locale.lblOther, style: secondaryTextStyle()),
                visualDensity: VisualDensity(horizontal: -4.0),
                contentPadding: EdgeInsets.zero,
                onChanged: (gender) {
                  genderCont.text = gender.validate();
                  setState(() {});
                },
              ).expand()
            ],
          )
        ],
      ),
    ).paddingSymmetric(horizontal: 16);
  }

  Widget buildAddressWidget() {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(locale.lblAddressDetail, style: boldTextStyle(size: titleTextSize, color: context.primaryColor)),
          Divider(color: viewLineColor),
          8.height,
          AppTextField(
            controller: addressCont,
            focus: addressFocus,
            nextFocus: cityFocus,
            isValidationRequired: false,
            textFieldType: TextFieldType.MULTILINE,
            minLines: 3,
            maxLines: 3,
            textInputAction: TextInputAction.newline,
            decoration: inputDecoration(context: context, labelText: locale.lblAddress),
          ),
          16.height,
          AppTextField(
            controller: cityCont,
            focus: cityFocus,
            nextFocus: countryFocus,
            textFieldType: TextFieldType.OTHER,
            decoration: inputDecoration(
              context: context,
              labelText: locale.lblCity,
              suffixIcon: Icon(Icons.location_on_outlined, size: 16, color: appStore.isDarkModeOn ? context.iconColor : Colors.black26),
            ),
          ),
          16.height,
          Row(
            children: [
              AppTextField(
                controller: countryCont,
                focus: countryFocus,
                nextFocus: postalCodeFocus,
                textFieldType: TextFieldType.OTHER,
                decoration: inputDecoration(
                  context: context,
                  labelText: locale.lblCountry,
                  suffixIcon: Icon(Icons.location_on_outlined, size: 16, color: appStore.isDarkModeOn ? context.iconColor : Colors.black26),
                ),
              ).expand(),
              16.width,
              AppTextField(
                controller: postalCodeCont,
                focus: postalCodeFocus,
                textFieldType: TextFieldType.OTHER,
                decoration: inputDecoration(context: context, labelText: locale.lblPostalCode, suffixIcon: Icon(Icons.location_on_outlined, size: 16, color: appStore.isDarkModeOn ? context.iconColor : Colors.black26)),
              ).expand(),
            ],
          ),
        ],
      ),
    );
  }

  Widget buildBloodGroupWidget() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      margin: EdgeInsets.only(bottom: 16),
      child: DropdownButtonFormField(
        value: bloodGroup,
        icon: SizedBox.shrink(),
        isExpanded: true,
        dropdownColor: context.cardColor,
        items: List.generate(
          bloodGroupList.length,
          (index) => DropdownMenuItem(value: bloodGroupList[index], child: Text("${bloodGroupList[index]}", style: primaryTextStyle())),
        ),
        decoration: inputDecoration(context: context, labelText: locale.lblBloodGroup, suffixIcon: ic_arrow_down.iconImage(size: 10, color: context.iconColor).paddingAll(14)),
        onChanged: (dynamic value) {
          bloodGroup = value;
          setState(() {});
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(
        isUpdate ? locale.lblEditPatientDetail : locale.lblAddNewPatient,
        textColor: Colors.white,
        systemUiOverlayStyle: defaultSystemUiOverlayStyle(context),
      ),
      body: Body(
        child: Stack(
          children: [
            Form(
              key: formKey,
              autovalidateMode: isFirstTime ? AutovalidateMode.disabled : AutovalidateMode.onUserInteraction,
              child: AnimatedScrollView(
                padding: EdgeInsets.only(bottom: 80),
                listAnimationType: ListAnimationType.None,
                children: [
                  buildBasicDetailWidget(),
                  buildBloodGroupWidget(),
                  buildGenderSelectionWidget(),
                  buildAddressWidget(),
                ],
              ),
            ),
            Positioned(
              right: 0,
              left: 0,
              bottom: 0,
              child: Container(
                color: context.scaffoldBackgroundColor,
                padding: EdgeInsets.all(16),
                child: AppButton(text: locale.lblSave, onTap: savePatientDetails),
              ),
            )
          ],
        ),
      ),
    );
  }
}
