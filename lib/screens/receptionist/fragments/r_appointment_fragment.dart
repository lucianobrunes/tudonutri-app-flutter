import 'dart:async';

import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/app_loader.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/upcoming_appointment_model.dart';
import 'package:kivicare_flutter/network/appointment_repository.dart';
import 'package:kivicare_flutter/screens/appointment/appointment_functions.dart';
import 'package:kivicare_flutter/screens/appointment/components/appointment_widget.dart';
import 'package:kivicare_flutter/screens/doctor/fragments/appointment_fragment.dart';
import 'package:kivicare_flutter/utils/cached_value.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/date_extensions.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:nb_utils/nb_utils.dart';

class RAppointmentFragment extends StatefulWidget {
  @override
  State<RAppointmentFragment> createState() => _RAppointmentFragmentState();
}

class _RAppointmentFragmentState extends State<RAppointmentFragment> {
  Future<List<UpcomingAppointmentModel>>? future;

  List<UpcomingAppointmentModel> appointmentList = [];

  StreamSubscription? updateAppointmentApi;

  DateTime current = DateTime.now();

  int selectIndex = 0;
  int totalAppointment = 0;
  int page = 1;

  String status = '1';

  bool isLastPage = false;

  @override
  void initState() {
    super.initState();
    updateAppointmentApi = appointmentStreamController.stream.listen((streamData) {
      page = 1;
      init();
      setState(() {});
    });
    init();
  }

  void init() async {
    future = getReceptionistAppointmentList(
      appointmentList: appointmentList,
      status: appStore.mStatus,
      page: page,
      getTotalAppointment: (p0) => totalAppointment = p0,
      lastPageCallback: (p0) => isLastPage,
    );
  }

  Widget buildStatusWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("${locale.lblClinic.getApostropheString(apostrophe: true)} ${locale.lblAppointments}", style: boldTextStyle(size: fragmentTextSize)).paddingSymmetric(horizontal: 16),
        HorizontalList(
          itemCount: appointmentStatusList.length,
          padding: EdgeInsets.symmetric(horizontal: 16),
          itemBuilder: (context, index) {
            bool isSelected = selectIndex == index;
            return GestureDetector(
              onTap: () {
                selectIndex = index;
                setState(() {});

                if (index == 0) {
                  appStore.setStatus(locale.lblAll);
                } else if (index == 1) {
                  appStore.setStatus('1');
                } else if (index == 2) {
                  appStore.setStatus('3');
                } else if (index == 3) {
                  appStore.setStatus('0');
                } else if (index == 4) {
                  appStore.setStatus(locale.lblPast);
                }
                page = 1;
                init();
              },
              child: Container(
                padding: EdgeInsets.only(top: 8, bottom: 8, left: 12, right: 12),
                decoration: boxDecorationDefault(color: isSelected ? context.primaryColor : context.cardColor, borderRadius: radius()),
                child: Text(appointmentStatusList[index], style: primaryTextStyle(color: isSelected ? Colors.white : textPrimaryColorGlobal, size: 14)),
              ),
            );
          },
        ).paddingTop(16),
      ],
    );
  }

  Widget buildBodyWidget() {
    return Stack(
      children: [
        buildStatusWidget(),
        SnapHelperWidget<List<UpcomingAppointmentModel>>(
          future: future,
          initialData: cachedDoctorAppointment,
          loadingWidget: Offstage(),
          onSuccess: (snap) {
            if (snap.isEmpty) {
              return Builder(
                builder: (context) {
                  return NoDataFoundWidget(text: locale.lblNoAppointmentsFound).center().visible(!appStore.isLoading);
                },
              );
            }

            return AnimatedScrollView(
              padding: EdgeInsets.fromLTRB(16, 0, 16, 80),
              disposeScrollController: true,
              onSwipeRefresh: () async {
                page = 1;
                init();

                setState(() {});
                return await 2.seconds.delay;
              },
              onNextPage: () {
                if (!isLastPage) {
                  page++;
                  init();
                  setState(() {});
                }
              },
              listAnimationType: listAnimationType,
              slideConfiguration: SlideConfiguration(verticalOffset: 400),
              children: snap.map((upcomingData) => AppointmentWidget(upcomingData: upcomingData).paddingSymmetric(vertical: 8)).toList(),
            );
          },
        ).paddingTop(100),
        AppLoader(),
      ],
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    if (updateAppointmentApi != null) {
      updateAppointmentApi!.cancel().then((value) {
        log("============== Stream Cancelled ==============");
      });
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          appointmentWidgetNavigation(context);
        },
      ),
      body: buildBodyWidget(),
    );
  }
}
