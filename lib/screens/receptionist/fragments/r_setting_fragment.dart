import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/app_common_dialog.dart';
import 'package:kivicare_flutter/components/app_setting_widget.dart';
import 'package:kivicare_flutter/components/cached_image_widget.dart';
import 'package:kivicare_flutter/components/setting_third_page.dart';
import 'package:kivicare_flutter/components/theme_selection_dialog.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/response_model.dart';
import 'package:kivicare_flutter/network/google_repository.dart';
import 'package:kivicare_flutter/screens/auth/change_password_screen.dart';
import 'package:kivicare_flutter/screens/doctor/screens/holiday/holiday_list_screen.dart';
import 'package:kivicare_flutter/screens/doctor/screens/service/service_list_screen.dart';
import 'package:kivicare_flutter/screens/doctor/screens/sessions/doctor_session_list_screen.dart';
import 'package:kivicare_flutter/screens/language_screen.dart';
import 'package:kivicare_flutter/screens/patient/screens/my_bill_records_screen.dart';
import 'package:kivicare_flutter/screens/encounter/screen/patient_encounter_list_screen.dart';
import 'package:kivicare_flutter/screens/receptionist/screens/edit_profile_screen.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/app_widgets.dart';
import 'package:kivicare_flutter/utils/cached_value.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:nb_utils/nb_utils.dart';

class RSettingFragment extends StatefulWidget {
  @override
  _RSettingFragmentState createState() => _RSettingFragmentState();
}

class _RSettingFragmentState extends State<RSettingFragment> with SingleTickerProviderStateMixin {
  TabController? tabController;

  int? currentIndex = 0;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    tabController = new TabController(initialIndex: 0, length: 3, vsync: this);
    currentIndex = getIntAsync(THEME_MODE_INDEX);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  // region widget

  Widget buildREditProfileWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Stack(
          clipBehavior: Clip.none,
          alignment: Alignment.bottomRight,
          children: [
            CachedImageWidget(url: userStore.profileImage.validate(), height: 90, circle: true, fit: BoxFit.cover),
            Positioned(
              bottom: -8,
              left: 0,
              right: -60,
              child: GestureDetector(
                onTap: () async {
                  await EditProfilesScreen().launch(context).then((value) {
                    if (value ?? false) {
                      setState(() {});
                      buildREditProfileWidget();
                    }
                  });
                },
                child: Container(
                  padding: EdgeInsets.all(4),
                  decoration: boxDecorationDefault(border: Border.all(color: white, width: 3), shape: BoxShape.circle, color: appPrimaryColor),
                  child: ic_edit.iconImage(size: 20, color: Colors.white, fit: BoxFit.contain),
                ),
              ),
            ),
          ],
        ),
        22.height,
        Text(getRoleWiseName(name: "${userStore.firstName.validate()} ${userStore.lastName.validate()}"), style: boldTextStyle()),
        Text(userStore.userEmail.validate(), style: primaryTextStyle()),
        28.height,
        if (isReceptionist() && appStore.userEnableGoogleCal == ON && isProEnabled())
          googleCalendar(context).onTap(
            () async {
              if (appStore.userDoctorGoogleCal != ON) {
                await authService.signInWithGoogle().then((user) async {
                  if (user != null) {
                    appStore.setLoading(true);

                    Map<String, dynamic> request = {
                      'code': await user.getIdToken().then((value) => value),
                    };

                    await connectGoogleCalendarAPI(request: request).then((value) async {
                      ResponseModel data = value;

                      appStore.setUserDoctorGoogleCal(ON);

                      appStore.setGoogleEmail(user.email.validate(), initialize: true);

                      toast(data.message);
                      appStore.setLoading(false);
                      setState(() {});
                    }).catchError((e) {
                      toast(e.toString());
                      appStore.setLoading(false);
                    });
                  }
                }).catchError((e) {
                  appStore.setLoading(false);
                  toast(e.toString());
                });
              } else {
                appStore.setLoading(true);
                showConfirmDialogCustom(
                  context,
                  onAccept: (c) async {
                    await disconnectGoogleCalendarAPI().then((value) {
                      appStore.setUserDoctorGoogleCal(OFF);

                      appStore.setGoogleEmail("", initialize: true);

                      appStore.setLoading(false);
                      toast(value.message.validate());
                    }).catchError((e) {
                      appStore.setLoading(false);
                      toast(e.toString());
                    });
                  },
                  title: locale.lblAreYouSureYouWantToDisconnect,
                  dialogType: DialogType.CONFIRMATION,
                  positiveText: locale.lblYes,
                );
              }
            },
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
          ),
      ],
    );
  }

  Widget buildRFirstWidget() {
    return Wrap(
      alignment: WrapAlignment.start,
      spacing: 16,
      runSpacing: 16,
      children: [
        AppSettingWidget(
          name: locale.lblServices,
          image: ic_services,
          widget: ServiceListScreen(),
          subTitle: locale.lblServicesYouProvide,
        ),
        AppSettingWidget(
          name: locale.lblHoliday,
          image: ic_holiday,
          widget: HolidayScreen(),
          subTitle: locale.lblScheduledHolidays,
        ),
        AppSettingWidget(
          name: locale.lblEncounters,
          image: ic_services,
          widget: PatientEncounterListScreen(),
          subTitle: locale.lblYourAllEncounters,
        ),
        AppSettingWidget(
          name: locale.lblSessions,
          image: ic_calendar,
          widget: DoctorSessionListScreen(),
          subTitle: locale.lblAvailableSession,
        ),
        AppSettingWidget(
          name: locale.lblBillRecords,
          image: ic_bill,
          widget: MyBillRecordsScreen(),
          subTitle: locale.lblGetYourAllBillsHere,
        ),
      ],
    );
  }

  Widget buildRSecondWidget() {
    return Wrap(
      spacing: 16,
      runSpacing: 16,
      children: [
        AppSettingWidget(
          name: locale.lblSelectTheme,
          image: ic_darkMode,
          subTitle: locale.lblChooseYourAppTheme,
          onTap: () {
            showInDialog(
              context,
              contentPadding: EdgeInsets.zero,
              shape: dialogShape(),
              builder: (context) {
                return AppCommonDialog(
                  title: locale.lblSelectTheme,
                  child: ThemeSelectionDialog(),
                );
              },
            );
          },
        ),
        AppSettingWidget(
          name: locale.lblChangePassword,
          image: ic_unlock,
          widget: ChangePasswordScreen(),
        ),
        AppSettingWidget(
          name: locale.lblLanguage,
          isLanguage: true,
          subTitle: selectedLanguageDataModel!.name.validate(),
          image: selectedLanguageDataModel!.flag.validate(),
          onTap: () async {
            await LanguageScreen().launch(context).then((value) {
              setState(() {});
            });
          },
        ),
      ],
    );
  }

  //endregion

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraint) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraint.maxHeight),
            child: IntrinsicHeight(
              child: Column(
                children: [
                  50.height,
                  buildREditProfileWidget(),
                  TabBar(
                    controller: tabController,
                    indicatorSize: TabBarIndicatorSize.tab,
                    splashBorderRadius: radius(),
                    labelColor: appStore.isDarkModeOn ? Colors.white : primaryColor,
                    isScrollable: false,
                    labelStyle: boldTextStyle(),
                    dividerColor: Colors.transparent,
                    unselectedLabelStyle: primaryTextStyle(),
                    unselectedLabelColor: appStore.isDarkModeOn ? gray : textSecondaryColorGlobal,
                    indicator: ShapeDecoration(shape: RoundedRectangleBorder(borderRadius: radius()), color: context.cardColor),
                    tabs: [
                      Tab(icon: Text(locale.lblGeneralSetting, textAlign: TextAlign.center)),
                      Tab(icon: Text(locale.lblAppSettings, textAlign: TextAlign.center)),
                      Tab(icon: Text(locale.lblOther, textAlign: TextAlign.center)),
                    ],
                  ).paddingAll(16).center(),
                  Container(
                    height: constraint.maxHeight,
                    child: TabBarView(
                      controller: tabController,
                      physics: BouncingScrollPhysics(),
                      children: [
                        buildRFirstWidget().paddingAll(16),
                        buildRSecondWidget().paddingAll(16),
                        SettingThirdPage().paddingAll(16),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
