import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/user_model.dart';
import 'package:kivicare_flutter/network/doctor_list_repository.dart';
import 'package:kivicare_flutter/screens/receptionist/screens/doctor/component/doctor_list_component.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:nb_utils/nb_utils.dart';

class MultiSelectDoctorDropDown extends StatefulWidget {
  final int? clinicId;
  final List<int>? selectedServicesId;
  final Function(List<UserModel> selectedDoctor)? onSubmit;

  MultiSelectDoctorDropDown({this.clinicId, this.selectedServicesId, this.onSubmit});

  @override
  _MultiSelectDoctorDropDownState createState() => _MultiSelectDoctorDropDownState();
}

class _MultiSelectDoctorDropDownState extends State<MultiSelectDoctorDropDown> {
  Future<List<UserModel>>? future;

  List<UserModel> doctorList = [];

  int total = 0;
  int page = 1;

  bool isLastPage = false;
  bool isFirst = true;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    future = getDoctorListWithPagination(clinicId: widget.clinicId, page: page, doctorList: doctorList, getTotalDoctor: (b) => total = b, lastPageCallback: (b) => isLastPage = b);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() async {
    super.dispose();
  }

  Widget buildBodyWidget() {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Stack(
        children: [
          SnapHelperWidget<List<UserModel>>(
            future: future,
            loadingWidget: LoaderWidget(),
            onSuccess: (snap) {
              if (widget.selectedServicesId != null && isFirst) {
                snap.forEach((element) {
                  if (widget.selectedServicesId!.contains(element.iD)) {
                    element.isCheck = true;
                  }
                });
                isFirst = false;
              }
              if (snap.isEmpty) {
                return NoDataFoundWidget(text: locale.lblNoDataFound);
              }

              return AnimatedListView(
                itemCount: snap.length,
                padding: EdgeInsets.only(bottom: 60),
                shrinkWrap: true,
                onNextPage: () {
                  if (!isLastPage) {
                    page++;
                    isFirst = true;
                    init();
                  }
                },
                itemBuilder: (context, index) {
                  UserModel data = snap[index];

                  return GestureDetector(
                    onTap: () {
                      data.isCheck = !data.isCheck;
                      setState(() {});
                    },
                    child: DoctorListComponent(data: data, isSelected: data.isCheck),
                  );
                },
              );
            },
          ).paddingTop(0),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(locale.lblSelectDoctor, textColor: Colors.white, systemUiOverlayStyle: defaultSystemUiOverlayStyle(context)),
      body: buildBodyWidget(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.done),
        onPressed: () {
          widget.onSubmit!.call(doctorList.where((element) => element.isCheck == true).toList());
          finish(context);
        },
      ),
    );
  }
}
