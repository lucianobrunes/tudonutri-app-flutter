import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/body_widget.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/holiday_model.dart';
import 'package:kivicare_flutter/network/holiday_repository.dart';
import 'package:kivicare_flutter/screens/doctor/screens/holiday/add_holiday_screen.dart';
import 'package:kivicare_flutter/screens/doctor/screens/holiday/components/holiday_widget.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:nb_utils/nb_utils.dart';

class HolidayScreen extends StatefulWidget {
  @override
  _HolidayScreenState createState() => _HolidayScreenState();
}

class _HolidayScreenState extends State<HolidayScreen> {
  Future<HolidayModel>? future;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    future = getHolidayResponseAPI();
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    getDisposeStatusBarColor();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant HolidayScreen oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  Widget buildBodyWidget() {
    return FutureBuilder<HolidayModel>(
      future: future,
      builder: (_, snap) {
        if (snap.hasData) {
          if (snap.data!.holidayData.validate().isEmpty) return NoDataFoundWidget().center();

          return AnimatedScrollView(
            padding: EdgeInsets.fromLTRB(16, 16, 16, 80),
            disposeScrollController: true,
            listAnimationType: ListAnimationType.None,
            physics: AlwaysScrollableScrollPhysics(),
            slideConfiguration: SlideConfiguration(verticalOffset: 400),
            onSwipeRefresh: () async {
              init();
              return await 2.seconds.delay;
            },
            children: [
              AnimatedWrap(
                spacing: 16,
                runSpacing: 16,
                children: snap.requireData.holidayData
                    .validate()
                    .map(
                      (holidayData) => HolidayWidget(data: holidayData).paddingBottom(16).onTap(
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        () async {
                          await AddHolidayScreen(holidayData: holidayData).launch(context, pageRouteAnimation: PageRouteAnimation.Slide);
                          init();
                          setState(() {});
                        },
                      ),
                    )
                    .toList(),
              )
            ],
          );
        }
        return snapWidgetHelper(snap, errorWidget: NoDataFoundWidget(text: errorMessage), loadingWidget: LoaderWidget());
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(locale.lblHolidays, textColor: Colors.white, systemUiOverlayStyle: defaultSystemUiOverlayStyle(context)),
      body: Body(
        child: buildBodyWidget(),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          await AddHolidayScreen().launch(context, pageRouteAnimation: PageRouteAnimation.Slide).then((value) {
            if (value != null) {
              if (value) {
                init();
                setState(() {});
              }
            }
          });
        },
      ),
    );
  }
}
