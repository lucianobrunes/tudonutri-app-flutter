import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/status_widget.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:nb_utils/nb_utils.dart';

import '../../../../../components/image_border_component.dart';
import '../../../../../main.dart';
import '../../../../../model/holiday_model.dart';
import '../../../../../utils/common.dart';

class HolidayWidget extends StatelessWidget {
  final HolidayData data;

  HolidayWidget({required this.data});

  @override
  Widget build(BuildContext context) {
    int totalDays = (DateTime.parse(data.endDate!).difference(DateTime.parse(data.startDate!))).inDays;
    int pendingDays = DateTime.parse(data.endDate!).difference(DateTime.now()).inDays;
    bool isPending = (DateTime.parse(data.endDate!).isAfter(DateTime.now()));

    return Container(
      width: context.width() / 2 - 24,
      decoration: boxDecorationDefault(borderRadius: radius(), color: context.cardColor, border: Border.all(color: context.dividerColor)),
      child: Stack(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ImageBorder(src: data.userProfileImage.validate(), height: 34),
                      8.width,
                      Marquee(
                        child: Text(isDoctor() ? data.userName.validate().prefixText(value: 'Dr. ') : data.userName.validate(), style: boldTextStyle(size: 16)),
                      ).expand(),
                    ],
                  ),
                  8.height,
                  Text('${data.startDate.validate().getFormattedDate(DISPLAY_DATE_FORMAT).validate()}', style: primaryTextStyle(size: 14)),
                  Text('—', style: primaryTextStyle()),
                  Text('${data.endDate.validate().getFormattedDate(DISPLAY_DATE_FORMAT).validate()}', style: primaryTextStyle(size: 14)),
                  10.height,
                  Text(locale.lblAfter + ' ${pendingDays == 0 ? '1' : pendingDays} ' + locale.lblDays, style: boldTextStyle(size: 16)).visible(isPending),
                  Text(locale.lblWasOffFor + ' ${totalDays == 0 ? '1' : totalDays} ' + locale.lblDays, style: boldTextStyle(size: 16)).visible(!isPending),
                ],
              ).expand(),
            ],
          ).paddingAll(16),
          Positioned(
            top: 0,
            left: 0,
            bottom: 0,
            child: StatusWidget(
              status: '',
              padding: EdgeInsets.symmetric(horizontal: 4, vertical: 4),
              borderRadius: radiusOnly(topLeft: defaultRadius, bottomLeft: defaultRadius),
              backgroundColor: getHolidayStatusColor(isPending).withOpacity(0.5),
            ),
          ),
        ],
      ),
    );
  }
}
