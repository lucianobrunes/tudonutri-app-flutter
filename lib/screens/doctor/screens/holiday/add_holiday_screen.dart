import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/body_widget.dart';
import 'package:kivicare_flutter/components/role_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/holiday_model.dart';
import 'package:kivicare_flutter/model/user_model.dart';
import 'package:kivicare_flutter/network/holiday_repository.dart';
import 'package:kivicare_flutter/screens/appointment/screen/step2_doctor_selection_screen.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/date_extensions.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:nb_utils/nb_utils.dart';

class AddHolidayScreen extends StatefulWidget {
  final HolidayData? holidayData;

  AddHolidayScreen({this.holidayData});

  @override
  _AddHolidayScreenState createState() => _AddHolidayScreenState();
}

class _AddHolidayScreenState extends State<AddHolidayScreen> {
  GlobalKey<FormState> formKey = GlobalKey();

  TextEditingController dateCont = TextEditingController();
  TextEditingController selectedDoctorCont = TextEditingController();

  DateTimeRange? picked = DateTimeRange(start: DateTime.now(), end: DateTime.now());

  bool isUpdate = false;
  bool isFirstTime = true;

  int? totalLeaveInDays;

  UserModel? doctorCont;

  String? moduleCont;

  void addHoliday() async {
    appStore.setLoading(true);
    Map request = {};

    if (moduleCont == "doctor" || moduleCont == null) {
      request = {
        "start_date": picked!.start.getFormattedDate(SAVE_DATE_FORMAT),
        "end_date": picked!.end.getFormattedDate(SAVE_DATE_FORMAT),
        "module_type": DOCTOR,
        "module_id": "${doctorCont == null ? getIntAsync(USER_ID) : doctorCont!.iD.validate()}",
        "description": "",
      };
    } else {
      request = {
        "start_date": picked!.start.getFormattedDate(SAVE_DATE_FORMAT),
        "end_date": picked!.end.getFormattedDate(SAVE_DATE_FORMAT),
        "module_type": CLINIC,
        "module_id": userStore.userClinicId,
        "description": "",
      };
    }

    await addHolidayDataAPI(request).then((value) {
      toast(value.message);
      finish(context, true);
    }).catchError((e) {
      toast(e.toString());
    });

    appStore.setLoading(false);
  }

  void updateHoliday() async {
    appStore.setLoading(true);
    Map request = {};

    if (moduleCont == "doctor" || moduleCont == null) {
      request = {
        "id": widget.holidayData!.id,
        "start_date": picked!.start.getFormattedDate(SAVE_DATE_FORMAT),
        "end_date": picked!.end.getFormattedDate(SAVE_DATE_FORMAT),
        "module_type": DOCTOR,
        "module_id": "${doctorCont == null ? getIntAsync(USER_ID) : doctorCont!.iD.validate()}",
        "description": "",
      };
    } else {
      request = {
        "id": widget.holidayData!.id,
        "start_date": picked!.start.getFormattedDate(SAVE_DATE_FORMAT),
        "end_date": picked!.end.getFormattedDate(SAVE_DATE_FORMAT),
        "module_type": CLINIC,
        "module_id": userStore.userClinicId,
        "description": "",
      };
    }

    await addHolidayDataAPI(request).then((value) {
      toast(value.message);
      finish(context, true);
    }).catchError((e) {
      toast(e);
    });
    appStore.setLoading(false);
  }

  void deleteHoliday() async {
    Map request = {
      "id": widget.holidayData!.id,
    };
    appStore.setLoading(true);

    await deleteHolidayDataAPI(request).then((value) {
      finish(context, true);
      toast(value.message);
    }).catchError((e) {
      toast(e.toString());
    });

    appStore.setLoading(false);
  }

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    isUpdate = widget.holidayData != null;
    if (isUpdate) {
      listAppStore.doctorList.forEach((element) {
        if (element!.iD.toString() == widget.holidayData!.moduleId) {
          doctorCont = element;
          selectedDoctorCont.text = doctorCont!.displayName.validate();
        }
      });

      if (!isDoctor()) {
        moduleCont = widget.holidayData!.moduleType;
      }
      picked = DateTimeRange(
        start: DateTime.parse(widget.holidayData!.startDate.validate()),
        end: DateTime.parse(widget.holidayData!.endDate.validate()),
      );
      dateCont.text = "${widget.holidayData!.startDate!.getFormattedDate(SAVE_DATE_FORMAT)} - ${widget.holidayData!.endDate!.getFormattedDate(SAVE_DATE_FORMAT)}";
      totalLeaveInDays = DateTime.parse(widget.holidayData!.endDate!).difference(DateTime.parse(widget.holidayData!.startDate!)).inDays;
      setState(() {});
    }
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    super.dispose();
  }

  void showDatePicker() async {
    picked = await showDateRangePicker(
      firstDate: DateTime.now(),
      initialDateRange: picked,
      helpText: locale.lblScheduleDate,
      context: context,
      locale: Locale(appStore.selectedLanguage),
      lastDate: DateTime(2101),
      builder: (context, child) {
        return Theme(
          data: ThemeData(
            primaryColor: primaryColor,
            textTheme: TextTheme(bodyMedium: TextStyle(color: Colors.black)),
            colorScheme: ColorScheme.fromSwatch().copyWith(primary: primaryColor, onSurface: Colors.black),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                constraints: BoxConstraints(
                  maxWidth: context.width() * 0.9,
                  maxHeight: context.height() * 0.60,
                ),
                child: child.cornerRadiusWithClipRRect(defaultRadius),
              )
            ],
          ),
        );
      },
    ).catchError((e) {
      toast(locale.lblCantEditDate);
    });
    if (picked != null) {
      dateCont.text = "${picked!.start.getFormattedDate(SAVE_DATE_FORMAT)} - ${picked!.end.getFormattedDate(SAVE_DATE_FORMAT)}";
      totalLeaveInDays = (picked!.end.difference(picked!.start)).inDays;
      setState(() {});
    }
  }

  Widget buildBodyWidget() {
    return Body(
      child: Form(
        key: formKey,
        autovalidateMode: isFirstTime ? AutovalidateMode.disabled : AutovalidateMode.onUserInteraction,
        child: AnimatedScrollView(
          listAnimationType: ListAnimationType.None,
          padding: EdgeInsets.fromLTRB(16, 20, 16, 16),
          children: [
            RoleWidget(
              isShowReceptionist: true,
              child: DropdownButtonFormField<String>(
                isExpanded: true,
                icon: SizedBox.shrink(),
                dropdownColor: context.cardColor,
                value: moduleCont,
                items: List.generate(
                  userRoles.length,
                  (index) => DropdownMenuItem<String>(
                    child: Text(userRoles[index].capitalizeFirstLetter(), style: primaryTextStyle()),
                    value: index == 0 ? DOCTOR : CLINIC,
                  ),
                ),
                onChanged: (value) {
                  moduleCont = value;
                  setState(() {});
                },
                decoration: inputDecoration(context: context, labelText: locale.lblHolidayOf, suffixIcon: ic_arrow_down.iconImage(size: 10, color: context.iconColor).paddingAll(14)),
                validator: (v) {
                  if (v == null) return locale.lblModuleIsRequired;
                  return null;
                },
              ),
            ),
            16.height,
            if (moduleCont == locale.lblDoctor.toLowerCase())
              RoleWidget(
                isShowReceptionist: true,
                child: AppTextField(
                  textFieldType: TextFieldType.NAME,
                  controller: selectedDoctorCont,
                  decoration: inputDecoration(context: context, labelText: locale.lblSelectDoctor),
                  readOnly: true,
                  onTap: () {
                    Step2DoctorSelectionScreen(isNotForAppointment: true).launch(context).then((value) {
                      if (value != null) {
                        doctorCont = value;
                        selectedDoctorCont.text = doctorCont!.displayName.validate();
                      } else {
                        toast(locale.lblPleaseSelectDoctor);
                      }
                      setState(() {});
                    }).catchError((e) {
                      toast(e.toString());
                    });
                  },
                ),
              ),
            if (moduleCont == locale.lblDoctor.toLowerCase()) 16.height,
            AppTextField(
              onTap: showDatePicker,
              controller: dateCont,
              textFieldType: TextFieldType.NAME,
              isValidationRequired: true,
              suffix: Icon(Icons.date_range_outlined, color: appStore.isDarkModeOn ? context.iconColor : Colors.black),
              decoration: inputDecoration(context: context, labelText: locale.lblScheduleDate),
              readOnly: true,
            ),
            if (totalLeaveInDays != null) 12.height,
            if (totalLeaveInDays != null)
              Align(
                alignment: Alignment.centerRight,
                child: Text(locale.lblLeaveFor + ' $totalLeaveInDays ' + locale.lblDays, style: secondaryTextStyle(color: primaryColor)),
              ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(
        isUpdate ? locale.lblEditHolidays : locale.lblAddHoliday,
        textColor: Colors.white,
        systemUiOverlayStyle: defaultSystemUiOverlayStyle(context),
        actions: [
          if (isUpdate)
            IconButton(
              icon: Icon(Icons.delete, color: Colors.red),
              onPressed: () async {
                showConfirmDialogCustom(
                  context,
                  dialogType: DialogType.DELETE,
                  title: locale.lblAreYouSureToDelete,
                  onAccept: (p0) {
                    deleteHoliday();
                  },
                );
              },
            )
        ],
      ),
      body: buildBodyWidget(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.done, color: Colors.white),
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();
            showConfirmDialogCustom(
              context,
              dialogType: isUpdate ? DialogType.UPDATE : DialogType.CONFIRMATION,
              primaryColor: context.primaryColor,
              title: locale.lblAllTheAppointmentOnSelectedDateWillBeCancelled,
              onAccept: (p0) {
                isUpdate ? updateHoliday() : addHoliday();
              },
            );
          } else {
            isFirstTime = !isFirstTime;
            setState(() {});
          }
        },
      ),
    );
  }
}
