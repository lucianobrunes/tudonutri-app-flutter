import 'package:flutter/material.dart';
import 'package:kivicare_flutter/network/doctor_list_repository.dart';
import 'package:kivicare_flutter/screens/doctor/screens/sessions/add_session_screen.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:nb_utils/nb_utils.dart';

import 'package:kivicare_flutter/components/cached_image_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/doctor_schedule_model.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:kivicare_flutter/utils/common.dart';

class SessionWidget extends StatelessWidget {
  final SessionData data;

  const SessionWidget({required this.data});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: boxDecorationDefault(
          borderRadius: radius(), color: context.cardColor),
      margin: EdgeInsets.only(top: 8, bottom: 8),
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              CachedImageWidget(
                url: data.doctorImage.validate(),
                height: 70,
                width: 70,
                fit: BoxFit.cover,
                radius: 120,
              ),
              16.width,
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(data.doctors.validate().prefixText(value: "Dr. "),
                      style: boldTextStyle()),
                  2.height,
                  Text(data.clinicName.validate(), style: secondaryTextStyle()),
                  6.height,
                  Text(
                    '${getListaDiasTraduzidos(data.days.validate()).join(" - ")}'
                        .toUpperCase(),
                    style: secondaryTextStyle(
                        size: 12, color: context.primaryColor),
                  )
                ],
              ).expand(),
            ],
          ),
          16.height,
          Container(
            decoration: boxDecorationDefault(
                borderRadius: radius(), color: context.scaffoldBackgroundColor),
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ic_morning
                          .iconImage(size: 18, color: Colors.orange)
                          .paddingOnly(top: 4),
                      12.width,
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(locale.lblMorning, style: secondaryTextStyle()),
                          4.height,
                          Text(data.morningTime,
                              style: boldTextStyle(size: 14)),
                        ],
                      ).expand(),
                    ],
                  ),
                ).expand(),
                8.width,
                Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ic_evening
                          .iconImage(size: 18, color: Colors.red)
                          .paddingOnly(top: 4),
                      12.width,
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(locale.lblEvening, style: secondaryTextStyle()),
                          4.height,
                          Text(data.eveningTime,
                              style: boldTextStyle(size: 14)),
                        ],
                      ).expand(),
                    ],
                  ),
                ).expand()
              ],
            ),
          ),
        ],
      ),
    ).onTap(() async {
      await getSelectedDoctorAPI(
          clinicId: data.clinicId.toInt(), doctorId: data.doctorId.toInt());
      AddSessionsScreen(sessionData: data).launch(context);
    });
  }
}
