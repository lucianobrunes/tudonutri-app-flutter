import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/body_widget.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/doctor_schedule_model.dart';
import 'package:kivicare_flutter/network/doctor_sessions_repository.dart';
import 'package:kivicare_flutter/screens/doctor/screens/sessions/add_session_screen.dart';
import 'package:kivicare_flutter/screens/doctor/screens/sessions/components/session_widget.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:nb_utils/nb_utils.dart';

class DoctorSessionListScreen extends StatefulWidget {
  @override
  _DoctorSessionListScreenState createState() => _DoctorSessionListScreenState();
}

class _DoctorSessionListScreenState extends State<DoctorSessionListScreen> {
  Future<DoctorSessionModel>? future;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    future = getDoctorSessionDataAPI(clinicData: userStore.userClinicId.validate().toInt());
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    getDisposeStatusBarColor();
    super.dispose();
  }

  Widget body() {
    return Container(
      child: FutureBuilder<DoctorSessionModel>(
        future: future,
        builder: (_, snap) {
          if (snap.hasData) {
            if (snap.data!.sessionData.validate().isEmpty) return NoDataFoundWidget(text: locale.lblNoDataFound).center();

            return AnimatedScrollView(
              padding: EdgeInsets.fromLTRB(16, 16, 16, 80),
              disposeScrollController: true,
              listAnimationType: listAnimationType,
              physics: AlwaysScrollableScrollPhysics(),
              slideConfiguration: SlideConfiguration(verticalOffset: 400),
              onSwipeRefresh: () async {
                init();
                return await 2.seconds.delay;
              },
              children: snap.data!.sessionData.validate().map<Widget>((sessionData) => SessionWidget(data: sessionData)).toList(),
            );
          }
          return snapWidgetHelper(snap, errorWidget: NoDataFoundWidget(text: errorMessage), loadingWidget: LoaderWidget());
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(locale.lblDoctorSessions, textColor: Colors.white, systemUiOverlayStyle: defaultSystemUiOverlayStyle(context)),
      body: Body(
        child: body(),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          await AddSessionsScreen().launch(context).then((value) {
            if (value != null) {
              if (value ?? false) {
                init();
                setState(() {});
              }
            }
          });
        },
      ),
    );
  }
}
