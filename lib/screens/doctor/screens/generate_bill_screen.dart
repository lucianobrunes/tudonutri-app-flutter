import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/body_widget.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/patient_bill_model.dart';
import 'package:kivicare_flutter/network/bill_repository.dart';
import 'package:kivicare_flutter/screens/doctor/screens/add_bill_item_screen.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:nb_utils/nb_utils.dart';

import '../../../model/encounter_model.dart';
import '../../../utils/constants.dart';

// ignore: must_be_immutable
class GenerateBillScreen extends StatefulWidget {
  EncounterModel data;

  GenerateBillScreen({required this.data});

  @override
  _GenerateBillScreenState createState() => _GenerateBillScreenState();
}

class _GenerateBillScreenState extends State<GenerateBillScreen> {
  Future<PatientBillModule>? future;

  late EncounterModel patientEncounterData;

  TextEditingController totalCont = TextEditingController();
  TextEditingController discountCont = TextEditingController();
  TextEditingController payableCont = TextEditingController();

  int payableText = 0;
  int? paymentStatusValue;

  List<BillItem> billItemData = [];

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    patientEncounterData = widget.data;

    if (patientEncounterData.paymentStatus != null) {
      paymentStatusValue = patientEncounterData.paymentStatus.toInt();
    }
    future = getBillDetailsAPI(encounterId: widget.data.id.toInt());
  }

  void saveFrom() {
    if (billItemData.isNotEmpty) {
      appStore.setLoading(true);

      Map<String, dynamic> request = {
        "id":
            "${patientEncounterData.billId == null ? "" : patientEncounterData.billId}",
        "encounter_id":
            "${patientEncounterData.id == null ? "" : patientEncounterData.id}",
        "appointment_id":
            "${patientEncounterData.appointmentId == null ? "" : patientEncounterData.appointmentId}",
        "total_amount": "${totalCont.text.validate()}",
        "discount": "${discountCont.text.validate()}",
        "actual_amount": "${payableCont.text.validate()}",
        "payment_status": paymentStatusValue == 0 ? "unpaid" : "paid",
        "billItems": billItemData,
      };

      log("----------------------------$request--------------------------------------------");

      addPatientBillAPI(request).then((value) {
        finish(context, paymentStatusValue == 1 ? "paid" : "unpaid");
        toast(locale.lblBillAddedSuccessfully);
        // LiveStream().emit(UPDATE, true);
      }).catchError((e) {
        toast(e.toString());
      });

      appStore.setLoading(false);
    } else {
      toast(locale.lblAtLeastSelectOneBillItem);
    }
  }

  void getTotal() {
    payableText = 0;

    billItemData.forEach((element) {
      payableText +=
          (element.price.validate().toInt() * element.qty.validate().toInt());
    });

    totalCont.text = payableText.toString();
    payableCont.text = payableText.toString();

    setTotalPayable(discountCont.text);
  }

  void setTotalPayable(String v) {
    if (v.isDigit()) {
      payableCont.text = "${payableText - v.toInt()}";
    }
    if (v.trim().isEmpty) {
      payableCont.text = payableText.toString();
    }
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget buildInvoiceWidget() {
    return Stack(
      children: [
        Column(
          children: [
            8.height,
            Container(
              padding: EdgeInsets.symmetric(horizontal: 4, vertical: 8),
              decoration: boxDecorationDefault(
                color: appStore.isDarkModeOn ? cardDarkColor : selectedColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8)),
              ),
              child: Row(
                children: [
                  Text(locale.lblSRNo,
                          style: boldTextStyle(size: 12, color: primaryColor),
                          textAlign: TextAlign.center)
                      .fit(fit: BoxFit.none)
                      .expand(),
                  Text("${locale.lblSERVICES}",
                          style: boldTextStyle(size: 12, color: primaryColor),
                          textAlign: TextAlign.start)
                      .fit(fit: BoxFit.none)
                      .expand(flex: 1),
                  Text("         " + locale.lblPRICE,
                          style: boldTextStyle(size: 12, color: primaryColor),
                          textAlign: TextAlign.end)
                      .fit(fit: BoxFit.none)
                      .expand(),
                  Text("      " + locale.lblQUANTITY,
                          style: boldTextStyle(size: 12, color: primaryColor),
                          textAlign: TextAlign.end)
                      .fit(fit: BoxFit.none)
                      .expand(),
                  Text("      " + locale.lblTOTAL,
                          style: boldTextStyle(size: 12, color: primaryColor),
                          textAlign: TextAlign.end)
                      .fit(fit: BoxFit.none)
                      .expand(flex: 1),
                ],
              ),
            ),
            Container(
              constraints: BoxConstraints(maxHeight: context.height() * 0.39),
              padding: EdgeInsets.symmetric(vertical: 16),
              decoration: boxDecorationDefault(
                color: context.cardColor,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8),
                    bottomRight: Radius.circular(8)),
              ),
              child: billItemData.isEmpty
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(locale.lblNoRecordsFound,
                            style: secondaryTextStyle(color: Colors.red)),
                      ],
                    )
                  : ListView.separated(
                      shrinkWrap: true,
                      itemCount: billItemData.length,
                      itemBuilder: (context, index) {
                        BillItem data = billItemData[index];
                        int total = data.price.validate().toInt() *
                            data.qty.validate().toInt();

                        return Row(
                          children: [
                            Text('${index + 1}',
                                    style: primaryTextStyle(size: 12),
                                    textAlign: TextAlign.center)
                                .expand(),
                            Text('     ${data.label.validate()}',
                                    style: primaryTextStyle(size: 12),
                                    textAlign: TextAlign.left)
                                .expand(flex: 2),
                            Text('${appStore.currency}${data.price.validate()}',
                                    style: primaryTextStyle(size: 12),
                                    textAlign: TextAlign.start)
                                .paddingRight(8)
                                .expand(),
                            Text('${data.qty.validate()}',
                                    style: primaryTextStyle(size: 12),
                                    textAlign: TextAlign.center)
                                .expand(),
                            Text('${appStore.currency}$total',
                                    style: primaryTextStyle(size: 12),
                                    textAlign: TextAlign.center)
                                .paddingRight(8)
                                .expand(flex: 1),
                          ],
                        );
                      },
                      separatorBuilder: (context, index) {
                        return Divider(color: viewLineColor);
                      },
                    ),
            ),
            24.height,
          ],
        ).paddingAll(16),
        Positioned(
          bottom: 90,
          right: 0,
          left: 0,
          child: Container(
            padding: EdgeInsets.all(16),
            margin: EdgeInsets.symmetric(horizontal: 16),
            width: context.width(),
            decoration: boxDecorationDefault(
              borderRadius: radius(),
              color: context.cardColor,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(locale.lblTotal, style: secondaryTextStyle()),
                    Spacer(),
                    Text("${appStore.currency}${totalCont.text.toString()}",
                        style: boldTextStyle(), textAlign: TextAlign.right),
                  ],
                ),
                16.height,
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(locale.lblDiscount, style: secondaryTextStyle()),
                    Spacer(),
                    Container(
                      child: AppTextField(
                        controller: discountCont,
                        textFieldType: TextFieldType.PHONE,
                        keyboardType: TextInputType.number,
                        decoration:
                            inputDecoration(context: context, labelText: '')
                                .copyWith(
                                    filled: true,
                                    fillColor: context.scaffoldBackgroundColor),
                        onChanged: setTotalPayable,
                        onFieldSubmitted: setTotalPayable,
                      ),
                    ).expand(),
                  ],
                ),
                16.height,
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(locale.lblPayableAmount, style: secondaryTextStyle()),
                    Spacer(),
                    Text("${appStore.currency}${payableCont.text.toString()}",
                        style: boldTextStyle(), textAlign: TextAlign.right),
                  ],
                ),
                16.height,
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(locale.lblPaymentStatus, style: secondaryTextStyle()),
                    Spacer(),
                    TextIcon(
                      onTap: () {
                        showModalBottomSheet(
                          context: context,
                          builder: (context) {
                            return buildPaidWidget();
                          },
                        );
                      },
                      spacing: 0,
                      suffix: Icon(Icons.arrow_drop_down_outlined,
                          color: primaryColor),
                      textStyle: boldTextStyle(
                          color: paymentStatusValue != null
                              ? primaryColor
                              : textPrimaryColorGlobal,
                          size: 14),
                      text: paymentStatusValue != null
                          ? paymentStatusValue == 0
                              ? locale.lblUnPaid
                              : locale.lblPaid
                          : '${locale.lblSelect} ${locale.lblPaymentStatus}',
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        Positioned(
          bottom: 16,
          right: 16,
          left: 16,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              AppButton(
                color: context.scaffoldBackgroundColor,
                padding: EdgeInsets.symmetric(horizontal: 12, vertical: 14),
                shapeBorder: RoundedRectangleBorder(
                    side: BorderSide(color: context.primaryColor),
                    borderRadius: radius()),
                onTap: () {
                  finish(context);
                },
                child: Text(locale.lblCancel.toUpperCase(),
                    style: boldTextStyle(color: context.primaryColor)),
              ),
              16.width,
              AppButton(
                color:
                    appStore.isDarkModeOn ? cardDarkColor : appSecondaryColor,
                child: Text(
                    '${paymentStatusValue == 1 ? locale.lblSaveAndCloseEncounter : locale.lblSave.toUpperCase()}',
                    style: boldTextStyle(color: Colors.white)),
                onTap: () async {
                  if (paymentStatusValue != null) {
                    saveFrom();
                  } else {
                    toast(locale.lblPleaseSelectPaymentStatus);
                  }
                },
              ).expand(flex: 3),
            ],
          ),
        ),
      ],
    );
  }

  Widget body() {
    if (patientEncounterData.billId == null) return buildInvoiceWidget();
    return Body(
      child: FutureBuilder<PatientBillModule>(
        future: future,
        builder: (_, snap) {
          if (snap.hasData) {
            if (billItemData.isEmpty) {
              billItemData.addAll(snap.data!.billItems.validate());
            }

            getTotal();
            return buildInvoiceWidget();
          }
          return snapWidgetHelper(snap, loadingWidget: LoaderWidget());
        },
      ),
    );
  }

  Widget buildPaidWidget() {
    return Container(
      padding: EdgeInsets.all(16),
      color: context.scaffoldBackgroundColor,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 5,
            width: 40,
            decoration: boxDecorationDefault(
                color: context.primaryColor, borderRadius: radius()),
          ).center(),
          32.height,
          Text(locale.lblPaymentStatus, style: boldTextStyle()),
          16.height,
          Column(
            children: [
              RadioListTile(
                value: 1,
                contentPadding: EdgeInsets.zero,
                groupValue: paymentStatusValue,
                title: Text(locale.lblPaid),
                selectedTileColor: context.cardColor,
                onChanged: (int? newValue) {
                  paymentStatusValue = newValue.validate();
                  setState(() {});
                  finish(context);
                },
                activeColor: primaryColor,
                selected: true,
              ),
              RadioListTile(
                value: 0,
                contentPadding: EdgeInsets.zero,
                groupValue: paymentStatusValue,
                selectedTileColor: context.cardColor,
                title: Text(locale.lblUnPaid),
                onChanged: (int? newValue) {
                  paymentStatusValue = newValue.validate();
                  setState(() {});

                  finish(context);
                },
                activeColor: primaryColor,
                selected: true,
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(
        locale.lblGenerateInvoice,
        textColor: Colors.white,
        systemUiOverlayStyle: defaultSystemUiOverlayStyle(context),
        actions: [
          TextIcon(
            text: locale.lblAddBillItem.toUpperCase(),
            textStyle: boldTextStyle(color: Colors.white),
            prefix: Icon(Icons.add, color: white, size: 18),
          ).paddingOnly(right: 16, left: 8).onTap(() async {
            bool? res = await AddBillItemScreen(
                    billId: patientEncounterData.billId.toInt(),
                    billItem: billItemData,
                    doctorId: patientEncounterData.doctorId.toInt())
                .launch(context);
            if (res ?? false) {
              getTotal();
              setState(() {});
            }
          }),
        ],
      ),
      body: body(),
    );
  }
}
