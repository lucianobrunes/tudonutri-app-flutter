import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/body_widget.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/service_model.dart';
import 'package:kivicare_flutter/network/get_service_response_repository.dart';
import 'package:kivicare_flutter/screens/doctor/screens/service/components/service_widget.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:nb_utils/nb_utils.dart';

import 'add_service_screen.dart';

class ServiceListScreen extends StatefulWidget {
  @override
  _ServiceListScreenState createState() => _ServiceListScreenState();
}

class _ServiceListScreenState extends State<ServiceListScreen> {
  Future<List<ServiceData>>? future;

  List<ServiceData> serviceList = [];

  int total = 0;
  int page = 1;
  bool isLastPage = false;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    future = getServiceResponseAPI(
      id: isReceptionist() ? userStore.userClinicId.validate().toInt() : userStore.userId.validate(),
      perPages: 50,
      page: page,
      serviceList: serviceList,
      getTotalService: (b) => total = b,
      lastPageCallback: (b) => isLastPage = b,
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    getDisposeStatusBarColor();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant ServiceListScreen oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  Widget buildBodyWidget() {
    return SnapHelperWidget<List<ServiceData>>(
      future: future,
      loadingWidget: LoaderWidget(),
      onSuccess: (snap) {
        if (snap.isEmpty) {
          return NoDataFoundWidget(text: locale.lblNoDataFound);
        }

        return AnimatedScrollView(
          padding: EdgeInsets.fromLTRB(16, 16, 16, 80),
          disposeScrollController: true,
          listAnimationType: listAnimationType,
          physics: AlwaysScrollableScrollPhysics(),
          slideConfiguration: SlideConfiguration(verticalOffset: 400),
          onSwipeRefresh: () async {
            page = 1;
            init();
            return await 2.seconds.delay;
          },
          onNextPage: () {
            if (!isLastPage) {
              page++;
              init();
              setState(() {});
            }
          },
          children: [
            AnimatedWrap(
              spacing: 16,
              runSpacing: 16,
              direction: Axis.horizontal,
              crossAxisAlignment: WrapCrossAlignment.center,
              itemCount: snap.length,
              itemBuilder: (p0, index) {
                return GestureDetector(
                  onTap: () async {
                    await AddServiceScreen(serviceData: snap[index]).launch(context, pageRouteAnimation: PageRouteAnimation.Slide);
                    setState(() {});
                    init();
                  },
                  child: ServiceWidget(data: snap[index]),
                );
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(
        locale.lblServices,
        textColor: Colors.white,
        systemUiOverlayStyle: defaultSystemUiOverlayStyle(context),
        actions: [],
      ),
      body: Body(
        child: buildBodyWidget(),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          await AddServiceScreen().launch(context);
        },
      ),
    );
  }
}
