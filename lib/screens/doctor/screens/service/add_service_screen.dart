import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/app_loader.dart';
import 'package:kivicare_flutter/components/image_border_component.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/locale/language_pt.dart';
import 'package:kivicare_flutter/model/service_duration_model.dart';
import 'package:kivicare_flutter/network/dashboard_repository.dart';
import 'package:kivicare_flutter/network/service_repository.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:kivicare_flutter/components/cached_image_widget.dart';
import 'package:kivicare_flutter/components/custom_image_picker.dart';
import 'package:kivicare_flutter/components/role_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/service_model.dart';
import 'package:kivicare_flutter/model/static_data_model.dart';
import 'package:kivicare_flutter/model/user_model.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/extensions/enums.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:kivicare_flutter/screens/receptionist/components/multi_select_doctor_drop_down.dart';

class AddServiceScreen extends StatefulWidget {
  final ServiceData? serviceData;

  AddServiceScreen({this.serviceData});

  @override
  State<AddServiceScreen> createState() => _AddServiceScreenState();
}

class _AddServiceScreenState extends State<AddServiceScreen> {
  GlobalKey<FormState> formKey = GlobalKey();

  Future<StaticDataModel>? future;

  bool isUpdate = false;
  bool isFirst = false;
  bool isFirstTime = true;

  bool? isTelemed;
  bool? isActive;
  bool? isMultiSelection;

  ServiceData? serviceData;

  StaticData? category;
  DurationModel? selectedDuration;
  String? categoriaSelecionada;

  List<UserModel?> selectedDoctorList = [];
  List<DurationModel> durationList = getServiceDuration();

  TextEditingController serviceNameCont = TextEditingController();
  TextEditingController serviceCategoryCont = TextEditingController();

  TextEditingController chargesCont = TextEditingController();
  TextEditingController doctorCont = TextEditingController();

  String statusCont = "";
  List<String> doctorIds = [];

  File? selectedProfileImage;

  FocusNode serviceCategoryFocus = FocusNode();
  FocusNode serviceNameFocus = FocusNode();
  FocusNode serviceChargesFocus = FocusNode();
  FocusNode doctorSelectionFocus = FocusNode();
  FocusNode serviceDurationFocus = FocusNode();

  @override
  void initState() {
    super.initState();

    init();
  }

  void init() async {
    isUpdate = widget.serviceData != null;
    isFirst = isUpdate;
    future = getStaticDataResponseAPI(SERVICE_TYPE);

    if (isUpdate) {
      serviceNameCont.text = widget.serviceData!.name.validate();
      chargesCont.text = widget.serviceData!.charges.validate();

      serviceNameCont.text = widget.serviceData!.name.validate();
      selectedDuration = durationList.firstWhere((element) =>
          element.value.toString() == widget.serviceData!.duration.validate());
      isTelemed = widget.serviceData!.telemedService.validate();
      isActive = widget.serviceData!.status!.getBoolInt();
      isMultiSelection = widget.serviceData!.multiple.validate();
      if (widget.serviceData!.image.validate().isNotEmpty)
        selectedProfileImage = File(widget.serviceData!.image.validate());
    }

    setState(() {});
  }

  void _handleClick() {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      if (widget.serviceData != null &&
          widget.serviceData!.type == "system_service") {
        categoriaSelecionada = widget.serviceData!.type;
      }

      showConfirmDialogCustom(
        context,
        dialogType: isUpdate ? DialogType.UPDATE : DialogType.CONFIRMATION,
        primaryColor: context.primaryColor,
        title:
            "${locale.lblAreYouSureYouWantTo} ${isUpdate ? locale.lblUpdate : locale.lblAdd} ${locale.lblService}?",
        onAccept: (c) async {
          Map<String, dynamic> req = {
            "type": categoriaSelecionada,
            //"type": category!.val ue,
            "charges": chargesCont.text,
            "name": serviceNameCont.text,
          };

          if (isUpdate) {
            req.putIfAbsent('id', () => widget.serviceData!.id.validate());
          }
          if (selectedDuration != null) {
            req.putIfAbsent(
                'duration', () => selectedDuration!.value.validate());
          }

          req.putIfAbsent('clinic_id', () => userStore.userClinicId);
          if (isDoctor()) {
            req.putIfAbsent('doctor_id[0]', () => userStore.userId);
          } else {
            selectedDoctorList.forEachIndexed((element, index) {
              req.putIfAbsent(
                  'doctor_id[$index]', () => element!.iD.validate());
            });
          }

          if (isTelemed != null) {
            req.putIfAbsent('is_telemed', () => isTelemed.getIntBool());
          } else {
            Fluttertoast.cancel();
            toast('${locale.lblTelemed} ${locale.lblNotSelected}');
          }
          if (isActive != null) {
            req.putIfAbsent('status', () => isActive.getIntBool());
          } else {
            Fluttertoast.cancel();
            toast('${locale.lblStatus} ${locale.lblNotSelected}');
          }
          if (isMultiSelection != null) {
            req.putIfAbsent(
                'is_multiple_selection', () => isMultiSelection.getIntBool());
          } else {
            Fluttertoast.cancel();
            toast("${locale.lblMultipleSelection} ${locale.lblNotSelected}");
          }

          log("Service Request :${jsonEncode(req)}");

          appStore.setLoading(true);

          await saveServiceAPI(data: req, serviceImage: selectedProfileImage)
              .then((value) {
            if (!isUpdate) {
              if (locale is LanguagePt) {
                toast(MSG_ADICIONAR_SERVICO);
              } else {
                toast(value.message);
              }
            } else {
              if (locale is LanguagePt) {
                toast(MSG_ATUALIZAR_SERVICO);
              } else {
                toast(value.message);
              }
            }
            finish(context, true);
          }).catchError((e) {
            toast(e.toString());
          });
          appStore.setLoading(false);
        },
      );
    } else {
      isFirstTime = !isFirstTime;
      setState(() {});
    }
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  void changeStatus(bool? value) {
    isActive = value.validate();
    setState(() {});
  }

  void changeTelemed(bool? value) {
    isTelemed = value.validate();
    setState(() {});
  }

  void changeMultiSelection(bool? value) {
    isMultiSelection = value.validate();
    setState(() {});
  }

  Widget buildBodyWidget() {
    return Stack(
      children: [
        Form(
          autovalidateMode: isFirstTime
              ? AutovalidateMode.disabled
              : AutovalidateMode.onUserInteraction,
          key: formKey,
          child: AnimatedScrollView(
            padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 88),
            children: [
              Align(
                  alignment: Alignment.center,
                  child: Stack(
                    clipBehavior: Clip.none,
                    children: <Widget>[
                      Container(
                        decoration: boxDecorationDefault(
                            color: appStore.isDarkModeOn
                                ? cardDarkColor
                                : context.scaffoldBackgroundColor,
                            shape: BoxShape.circle),
                        child: selectedProfileImage != null
                            ? selectedProfileImage!.path.validateURL()
                                ? ImageBorder(
                                    src: selectedProfileImage!.path,
                                    height: 120,
                                  )
                                : Image.file(File(selectedProfileImage!.path),
                                        height: 126,
                                        width: 126,
                                        fit: BoxFit.cover,
                                        alignment: Alignment.center)
                                    .cornerRadiusWithClipRRect(180)
                            : CachedImageWidget(
                                url: '',
                                height: 126,
                                width: 126,
                                fit: BoxFit.cover,
                                circle: true),
                      ),
                      Positioned(
                        bottom: 0,
                        right: 0,
                        child: Container(
                          padding: EdgeInsets.all(8),
                          decoration: boxDecorationDefault(
                              color: appPrimaryColor,
                              shape: BoxShape.circle,
                              border: Border.all(color: white, width: 3)),
                          child: ic_camera.iconImage(
                              size: 14, color: Colors.white),
                        ).onTap(() async {
                          await showInDialog(
                            context,
                            contentPadding: EdgeInsets.symmetric(vertical: 16),
                            title: Text(locale.lblChooseAction,
                                style: boldTextStyle()),
                            builder: (p0) {
                              return FilePickerDialog(isSelected: (false));
                            },
                          ).then((file) async {
                            if (file != null) {
                              if (file == GalleryFileTypes.CAMERA) {
                                await getCameraImage().then((value) {
                                  selectedProfileImage = value;
                                  setState(() {});
                                });
                              } else if (file == GalleryFileTypes.GALLERY) {
                                await getCameraImage(isCamera: false)
                                    .then((value) {
                                  selectedProfileImage = value;
                                  setState(() {});
                                });
                              }
                            }
                          });
                        }),
                      )
                    ],
                  ).center().paddingBottom(16)),
              20.height,
              FutureBuilder<StaticDataModel>(
                  future: future,
                  builder: (context, snap) {
                    if (snap.hasData) {
                      // Incluida validacao para system_service pois dava erro ao tentar buscar somente os service_type
                      if (isFirst &&
                          isUpdate &&
                          widget.serviceData!.type != "system_service") {
                        isFirst = false;
                        category = snap.data?.staticData?.firstWhere(
                            (element) =>
                                element!.value ==
                                widget.serviceData!.type.validate());
                      }

                      // Só permitir alterar a categoria se for diferente de system_service (ex: Telemed não pode alterar categoria por ser de sistema)
                      /*if (!isUpdate &&
                          widget.serviceData!.type != "system_service") {*/

                      if (!isUpdate) {
                        return DropdownButtonFormField<String>(
                          focusNode: serviceCategoryFocus,
                          icon: SizedBox.shrink(),
                          isExpanded: true,
                          dropdownColor: context.cardColor,
                          autovalidateMode: isFirstTime
                              ? AutovalidateMode.disabled
                              : AutovalidateMode.onUserInteraction,
                          value: category?.value,
                          items: snap.data!.staticData!
                              .map<DropdownMenuItem<String>>((serviceData) {
                            return DropdownMenuItem<String>(
                              value: serviceData?.value,
                              child: Text(serviceData!.label!,
                                  style: primaryTextStyle()),
                            );
                          }).toList(),
                          onChanged: (value) {
                            categoriaSelecionada = value;
                            if (!isUpdate) {
                              //category?.value = value;
                              setState(() {});
                            }
                          },
                          decoration: inputDecoration(
                            context: context,
                            labelText:
                                '${locale.lblSelect} ${locale.lblCategory}',
                            suffixIcon: ic_arrow_down
                                .iconImage(size: 10, color: context.iconColor)
                                .paddingAll(14),
                          ),
                          validator: (s) {
                            if (s == null) return errorThisFieldRequired;
                            return null;
                          },
                        );
                      } else {
                        if (widget.serviceData != null &&
                            widget.serviceData!.type != "system_service") {
                          return DropdownButtonFormField<String>(
                            focusNode: serviceCategoryFocus,
                            icon: SizedBox.shrink(),
                            isExpanded: true,
                            dropdownColor: context.cardColor,
                            autovalidateMode: isFirstTime
                                ? AutovalidateMode.disabled
                                : AutovalidateMode.onUserInteraction,
                            value: category?.value,
                            items: snap.data!.staticData!
                                .map<DropdownMenuItem<String>>((serviceData) {
                              return DropdownMenuItem<String>(
                                value: serviceData?.value,
                                child: Text(serviceData!.label!,
                                    style: primaryTextStyle()),
                              );
                            }).toList(),
                            onChanged: (value) {
                              categoriaSelecionada = value;
                              if (!isUpdate) {
                                //category?.value = value;
                                setState(() {});
                              }
                            },
                            decoration: inputDecoration(
                              context: context,
                              labelText:
                                  '${locale.lblSelect} ${locale.lblCategory}',
                              suffixIcon: ic_arrow_down
                                  .iconImage(size: 10, color: context.iconColor)
                                  .paddingAll(14),
                            ),
                            validator: (s) {
                              if (s == null) return errorThisFieldRequired;
                              return null;
                            },
                          );
                        }
                      }
                    }

                    return snapWidgetHelper(snap,
                        loadingWidget: LoaderWidget());
                  }),
              16.height,
              AppTextField(
                nextFocus: serviceChargesFocus,
                controller: serviceNameCont,
                textFieldType: TextFieldType.NAME,
                textAlign: TextAlign.justify,
                decoration: inputDecoration(
                  context: context,
                  labelText: locale.lblService + ' ${locale.lblName}',
                  suffixIcon: ic_services
                      .iconImage(size: 10, color: context.iconColor)
                      .paddingAll(14),
                ),
              ),
              16.height,
              AppTextField(
                focus: serviceChargesFocus,
                nextFocus: doctorSelectionFocus,
                controller: chargesCont,
                textFieldType: TextFieldType.NAME,
                keyboardType: TextInputType.number,
                textAlign: TextAlign.justify,
                decoration: inputDecoration(
                  context: context,
                  labelText: locale.lblCharges,
                  suffixIcon: ic_dollar_icon
                      .iconImage(size: 10, color: context.iconColor)
                      .paddingAll(14),
                ),
              ),
              16.height,
              RoleWidget(
                isShowDoctor: true,
                isShowReceptionist: true,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AppTextField(
                      controller: doctorCont,
                      focus: doctorSelectionFocus,
                      textFieldType: TextFieldType.OTHER,
                      errorThisFieldRequired: locale.lblSelectOneDoctor,
                      validator: (value) {
                        if (value == null) return locale.lblSelectOneDoctor;
                        return null;
                      },
                      readOnly: true,
                      onTap: () {
                        MultiSelectDoctorDropDown(
                          clinicId: userStore.userClinicId.toInt(),
                          selectedServicesId: selectedDoctorList
                              .map((e) => e!.iD.validate())
                              .toList(),
                          onSubmit: (selectedDoctor) {
                            selectedDoctorList = selectedDoctor;
                            doctorCont.text = selectedDoctorList.length
                                    .toString() +
                                ' ${locale.lblDoctor.getApostropheString(apostrophe: false)} selected';
                            setState(() {});
                          },
                        ).launch(context);
                      },
                      decoration: inputDecoration(
                        context: context,
                        labelText: locale.lblSelectDoctor,
                        suffixIcon: ic_user
                            .iconImage(size: 10, color: context.iconColor)
                            .paddingAll(14),
                      ).copyWith(alignLabelWithHint: true),
                    ),
                    16.height,
                    Wrap(
                      spacing: 8,
                      runSpacing: 8,
                      children: List.generate(
                        selectedDoctorList.length.toString().isEmpty
                            ? 0
                            : selectedDoctorList.length,
                        (index) {
                          UserModel data = selectedDoctorList[index]!;
                          return Chip(
                            backgroundColor: context.cardColor,
                            shape: StadiumBorder(
                                side: BorderSide(color: appPrimaryColor)),
                            label: Text('${data.displayName}',
                                style:
                                    primaryTextStyle(color: context.iconColor)),
                            avatar: Icon(CupertinoIcons.person,
                                size: 16, color: context.iconColor),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
              if (selectedDoctorList.isNotEmpty) 16.height,
              DropdownButtonHideUnderline(
                child: DropdownButtonFormField<DurationModel>(
                  focusNode: serviceDurationFocus,
                  value: selectedDuration,
                  icon: SizedBox.shrink(),
                  dropdownColor: context.cardColor,
                  autovalidateMode: isFirstTime
                      ? AutovalidateMode.disabled
                      : AutovalidateMode.onUserInteraction,
                  decoration: inputDecoration(
                    context: context,
                    labelText: 'Select ${locale.lblDuration}',
                    suffixIcon: ic_arrow_down
                        .iconImage(size: 10, color: context.iconColor)
                        .paddingAll(14),
                  ),
                  onChanged: (value) {
                    // antes estava para atualizar somente no caso de inclusao
                    selectedDuration = value;
                    setState(() {});
                  },
                  items: durationList
                      .map<DropdownMenuItem<DurationModel>>((duration) {
                    return DropdownMenuItem<DurationModel>(
                      value: duration,
                      child: Text(duration.label.validate(),
                          style: primaryTextStyle()),
                    );
                  }).toList(),
                ),
              ),
              16.height,
              Container(
                decoration: boxDecorationDefault(
                    borderRadius: radius(), color: context.cardColor),
                padding: EdgeInsets.fromLTRB(16, 8, 16, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      locale.lblIsThisTeleMedService,
                      style: primaryTextStyle(color: textSecondaryColorGlobal),
                    ),
                    4.height,
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        RadioListTile<bool>(
                          visualDensity: VisualDensity.compact,
                          value: true,
                          controlAffinity: ListTileControlAffinity.trailing,
                          groupValue: isTelemed,
                          onChanged: changeTelemed,
                          title: Text(locale.lblYes, style: primaryTextStyle()),
                        ).expand(),
                        RadioListTile<bool>(
                          visualDensity: VisualDensity.compact,
                          value: false,
                          controlAffinity: ListTileControlAffinity.trailing,
                          groupValue: isTelemed,
                          onChanged: changeTelemed,
                          title: Text(locale.lblNo, style: primaryTextStyle()),
                        ).expand()
                      ],
                    )
                  ],
                ),
              ),
              16.height,
              Container(
                decoration: boxDecorationDefault(
                    borderRadius: radius(), color: context.cardColor),
                padding: EdgeInsets.fromLTRB(16, 8, 16, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      locale.lblAllowMultiSelectionWhileBooking,
                      style: primaryTextStyle(color: textSecondaryColorGlobal),
                    ),
                    4.height,
                    Row(
                      children: [
                        RadioListTile<bool>(
                          visualDensity: VisualDensity.compact,
                          value: true,
                          controlAffinity: ListTileControlAffinity.trailing,
                          groupValue: isMultiSelection,
                          title: Text(locale.lblYes, style: primaryTextStyle()),
                          onChanged: changeMultiSelection,
                        ).expand(),
                        RadioListTile<bool>(
                          visualDensity: VisualDensity.compact,
                          value: false,
                          controlAffinity: ListTileControlAffinity.trailing,
                          groupValue: isMultiSelection,
                          title: Text(locale.lblNo, style: primaryTextStyle()),
                          onChanged: changeMultiSelection,
                        ).expand(),
                      ],
                    )
                  ],
                ),
              ),
              16.height,
              Container(
                decoration: boxDecorationDefault(
                    borderRadius: radius(), color: context.cardColor),
                padding: EdgeInsets.fromLTRB(16, 8, 16, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      locale.lblSetStatus,
                      style: primaryTextStyle(color: textSecondaryColorGlobal),
                    ),
                    4.height,
                    Row(
                      children: [
                        RadioListTile<bool>(
                          visualDensity: VisualDensity.compact,
                          value: true,
                          controlAffinity: ListTileControlAffinity.trailing,
                          groupValue: isActive,
                          title:
                              Text(locale.lblActive, style: primaryTextStyle()),
                          onChanged: changeStatus,
                        ).expand(),
                        RadioListTile<bool>(
                          visualDensity: VisualDensity.compact,
                          value: false,
                          controlAffinity: ListTileControlAffinity.trailing,
                          groupValue: isActive,
                          title: Text(locale.lblInActive,
                              style: primaryTextStyle()),
                          onChanged: changeStatus,
                        ).expand(),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        AppLoader(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(
        isUpdate ? locale.lblEditService : locale.lblAddService,
        textColor: Colors.white,
        systemUiOverlayStyle: defaultSystemUiOverlayStyle(context),
        actions: [
          if (isUpdate)
            IconButton(
              icon: Icon(Icons.delete, color: Colors.red),
              onPressed: () async {
                showConfirmDialogCustom(
                  context,
                  dialogType: DialogType.DELETE,
                  title: locale.lblAreYouSureToDelete,
                  onAccept: (p0) async {
                    Map<String, dynamic> req = {
                      "id": widget.serviceData!.id.validate(),
                      "doctor_id": widget.serviceData!.doctorId.validate(),
                      "service_mapping_id":
                          widget.serviceData!.mappingTableId.validate(),
                    };

                    appStore.setLoading(true);

                    await deleteServiceDataAPI(req).then((value) {
                      if (locale is LanguagePt) {
                        toast(MSG_EXCLUIR_SERVICO);
                      } else {
                        toast(value.message.validate());
                      }

                      finish(context);
                    }).catchError((e) {
                      toast(e.toString());
                    });

                    appStore.setLoading(false);
                  },
                );
              },
            ),
        ],
      ),
      body: buildBodyWidget(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.done, color: Colors.white),
        onPressed: _handleClick,
      ),
    );
  }
}
