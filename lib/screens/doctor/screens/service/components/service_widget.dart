import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/image_border_component.dart';
import 'package:kivicare_flutter/components/status_widget.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:nb_utils/nb_utils.dart';

import '../../../../../components/cached_image_widget.dart';
import '../../../../../components/price_widget.dart';
import '../../../../../model/service_model.dart';
import '../../../../../utils/colors.dart';
import '../../../../../utils/images.dart';

class ServiceWidget extends StatelessWidget {
  final ServiceData data;

  ServiceWidget({required this.data});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: context.width() / 2 - 24,
          child: Container(
            decoration: boxDecorationDefault(color: context.cardColor),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ImageBorder(src: data.image.validate(), height: 60),
                8.height,
                Text(data.name.validate().capitalizeEachWord(), style: boldTextStyle(size: 16)),
                8.height,
                Row(
                  children: [
                    PriceWidget(price: "${data.charges.validate()}", textSize: 16, textColor: primaryColor).expand(),
                    if (!data.duration.isEmptyOrNull)
                      TextIcon(
                        prefix: ic_timer.iconImage(color: appSecondaryColor, size: 16),
                        text: "${data.duration.validate()} min",
                        textStyle: secondaryTextStyle(),
                      )
                  ],
                ),
              ],
            ).paddingSymmetric(horizontal: 12, vertical: 12),
          ),
        ),
        Positioned(
          right: 8,
          top: 10,
          child: StatusWidget(
            status: data.status.validate(),
            isServiceActivityStatus: true,
          ),
        ),

        // Container(
        //   width: context.width() / 2 - 24,
        //   decoration: boxDecorationDefault(borderRadius: radius(), color: context.cardColor),
        //   padding: EdgeInsets.all(16),
        //   child: Column(
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     children: [
        //       CachedImageWidget(url: data.image.validate(), height: 80, circle: true, fit: BoxFit.contain),
        //       8.height,
        //       Text(data.name.validate(), style: boldTextStyle()),
        //       4.height,
        //       PriceWidget(price: data.charges.validate(), textSize: 18, textColor: primaryColor),
        //       4.height,
        //       TextIcon(
        //         onTap: () {
        //           //
        //         },
        //         edgeInsets: EdgeInsets.all(0),
        //         prefix: Icon(Icons.timer_outlined, color: appSecondaryColor, size: 16),
        //         text: data.duration.validate().isNotEmpty ? "${data.duration.validate()}min" : " - ",
        //         textStyle: secondaryTextStyle(),
        //       ),
        //       8.height,
        //       Row(
        //         children: [
        //           if (data.telemedService.validate()) ic_telemed.iconImage(size: 28),
        //           8.width,
        //           if (data.multiple.validate()) ic_multi_select.iconImage(size: 28),
        //         ],
        //       ),
        //     ],
        //   ),
        // ),
        //Positioned(right: 6, child: CachedImageWidget(url: data.image.validate(), height: 50, circle: true)),
      ],
    );
  }
}
