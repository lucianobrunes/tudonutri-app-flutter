import 'dart:async';

import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/body_widget.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/upcoming_appointment_model.dart';
import 'package:kivicare_flutter/network/appointment_repository.dart';
import 'package:kivicare_flutter/screens/appointment/appointment_functions.dart';
import 'package:kivicare_flutter/screens/appointment/components/appointment_widget.dart';
import 'package:kivicare_flutter/utils/cached_value.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/date_extensions.dart';
import 'package:kivicare_flutter/utils/widgets/calender/date_utils.dart';
import 'package:kivicare_flutter/utils/widgets/calender/flutter_clean_calendar.dart';
import 'package:nb_utils/nb_utils.dart';

StreamController appointmentStreamController = StreamController.broadcast();

class AppointmentFragment extends StatefulWidget {
  const AppointmentFragment({Key? key}) : super(key: key);

  @override
  State<AppointmentFragment> createState() => _AppointmentFragmentState();
}

class _AppointmentFragmentState extends State<AppointmentFragment> {
  Map<DateTime, List> _events = {};

  Future<List<UpcomingAppointmentModel>>? future;

  int page = 1;
  bool isLastPage = false;
  bool isRangeSelected = false;

  String startDate = DateTime(DateTime.now().year, DateTime.now().month, 1).getFormattedDate(SAVE_DATE_FORMAT);
  String endDate = DateTime(DateTime.now().year, DateTime.now().month, Utils.lastDayOfMonth(DateTime.now()).day).getFormattedDate(SAVE_DATE_FORMAT);

  DateTime selectedDate = DateTime.now();
  List<UpcomingAppointmentModel> appointmentListData = [];

  StreamSubscription? updateAppointmentApi;

  @override
  void initState() {
    super.initState();
    updateAppointmentApi = appointmentStreamController.stream.listen((streamData) {
      page = 1;
      init(
        todayDate: selectedDate.getFormattedDate(SAVE_DATE_FORMAT),
        startDate: null,
        endDate: null,
      );
      setState(() {});
    });
    isRangeSelected = true;
    init(startDate: startDate, endDate: endDate);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  void showData(DateTime dateTime) async {
    if (isRangeSelected) return;
    selectedDate = dateTime;
    init(
      todayDate: dateTime.getFormattedDate(SAVE_DATE_FORMAT),
      startDate: null,
      endDate: null,
    );
    setState(() {});
  }

  void init({String? todayDate, String? startDate, String? endDate}) async {
    future = getAppointment(pages: page, perPage: 20, lastPageCallback: (b) => isLastPage = b, todayDate: todayDate, startDate: startDate, endDate: endDate, data: appointmentListData);
  }

  Widget buildListWidget({required AsyncSnapshot<List<UpcomingAppointmentModel>> snap, required List<UpcomingAppointmentModel> data}) {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if ((snap.connectionState != ConnectionState.waiting))
            if (data.isNotEmpty)
              AnimatedListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                listAnimationType: listAnimationType,
                itemCount: data.length,
                itemBuilder: (context, index) {
                  return AppointmentWidget(upcomingData: data[index]).paddingSymmetric(vertical: 8);
                },
              )
            else
              NoDataFoundWidget(text: locale.lblNoAppointmentForToday).center()
          else
            LoaderWidget().paddingTop(80),
        ],
      ),
    );
  }

  Widget buildBodyWidget() {
    return Body(
      child: FutureBuilder<List<UpcomingAppointmentModel>>(
          initialData: cachedDoctorAppointment,
          future: future,
          builder: (context, snap) {
            if (snap.hasData) {
              if (isRangeSelected && (snap.connectionState != ConnectionState.waiting)) {
                _events.clear();
                snap.data.validate().forEachIndexed((element, index) {
                  DateTime date = DateTime.parse(element.appointmentStartDate.validate());
                  _events.addAll({
                    DateTime(date.year, date.month, date.day): [{}]
                  });
                });
                isRangeSelected = false;
              }
            }
            return AnimatedScrollView(
              padding: EdgeInsets.only(bottom: 60),
              onSwipeRefresh: () async {
                isRangeSelected = true;
                page = 1;
                setState(() {});
                init(
                  todayDate: selectedDate.getFormattedDate(SAVE_DATE_FORMAT),
                  startDate: null,
                  endDate: null,
                );

                return await 1.seconds.delay;
              },
              onNextPage: () {
                if (!isLastPage) {
                  page++;
                  init(
                    todayDate: selectedDate.getFormattedDate(SAVE_DATE_FORMAT),
                    startDate: null,
                    endDate: null,
                  );
                  setState(() {});
                }
              },
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(locale.lblTodaySAppointments, style: boldTextStyle(size: fragmentTextSize)),
                        8.width,
                        Marquee(child: Text("(${selectedDate.getDateInString(format: CONFIRM_APPOINTMENT_FORMAT)})", style: boldTextStyle(size: 14, color: context.primaryColor))).expand(),
                      ],
                    ),
                    4.height,
                    Text(locale.lblSwipeMassage, style: secondaryTextStyle(size: 10, color: appSecondaryColor)),
                  ],
                ).paddingOnly(top: 16, right: 16, left: 16),
                Container(
                  margin: EdgeInsets.all(16),
                  decoration: boxDecorationWithRoundedCorners(backgroundColor: context.cardColor),
                  child: CleanCalendar(
                    startOnMonday: true,
                    weekDays: [locale.lblMon, locale.lblTue, locale.lblWed, locale.lblThu, locale.lblFri, locale.lblSat, locale.lblSun],
                    events: _events,
                    onDateSelected: (e) => showData(e),
                    initialDate: selectedDate,
                    onRangeSelected: (Range range) {
                      isRangeSelected = true;
                      page = 1;
                      setState(() {});
                      init(
                        todayDate: null,
                        startDate: range.from.getFormattedDate(SAVE_DATE_FORMAT),
                        endDate: range.to.getFormattedDate(SAVE_DATE_FORMAT),
                      );
                    },
                    isExpandable: true,
                    locale: appStore.selectedLanguage,
                    isExpanded: false,
                    eventColor: appSecondaryColor,
                    selectedColor: primaryColor,
                    todayColor: primaryColor,
                    bottomBarArrowColor: context.iconColor,
                    dayOfWeekStyle: TextStyle(color: appStore.isDarkModeOn ? Colors.white : Colors.black, fontWeight: FontWeight.w800, fontSize: 11),
                  ),
                ),
                buildListWidget(snap: snap, data: snap.data.validate().where((element) => element.getAppointmentSaveDate == selectedDate.getFormattedDate(SAVE_DATE_FORMAT)).toList()),
              ],
            );
          }),
    );
  }

  @override
  void dispose() {
    if (updateAppointmentApi != null) {
      updateAppointmentApi!.cancel().then((value) {
        log("============== Stream Cancelled ==============");
      });
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildBodyWidget(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          appointmentWidgetNavigation(context);
          init();
        },
      ),
    );
  }
}
