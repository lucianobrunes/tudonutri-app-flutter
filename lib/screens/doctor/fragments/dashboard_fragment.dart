import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/dashboard_model.dart';
import 'package:kivicare_flutter/model/upcoming_appointment_model.dart';
import 'package:kivicare_flutter/network/dashboard_repository.dart';
import 'package:kivicare_flutter/screens/appointment/components/appointment_dashboard_widget.dart';
import 'package:kivicare_flutter/screens/doctor/components/dashboard_count_widget.dart';
import 'package:kivicare_flutter/screens/doctor/components/weekly_chart_component.dart';
import 'package:kivicare_flutter/screens/doctor/screens/show_appointment_chart_screen.dart';
import 'package:kivicare_flutter/utils/cached_value.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:nb_utils/nb_utils.dart';

class DashboardFragment extends StatefulWidget {
  @override
  _DashboardFragmentState createState() => _DashboardFragmentState();
}

class _DashboardFragmentState extends State<DashboardFragment> {
  List<DashBoardCountWidget> dashboardCount = [];
  Future<DashboardModel>? future;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    future = getUserDashBoardAPI();
  }

  @override
  void didUpdateWidget(covariant DashboardFragment oldWidget) {
    super.didUpdateWidget(oldWidget);
    setState(() {});
  }

  Widget buildAnalyticsWidget({required DashboardModel data}) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          40.height,
          AnimatedWrap(
            itemCount: 3,
            spacing: 14,
            listAnimationType: listAnimationType,
            itemBuilder: (context, index) {
              if (index == 0) {
                return DashBoardCountWidget(
                  title: locale.lblTodayAppointments,
                  color1: appSecondaryColor,
                  subTitle: locale.lblTotalTodayAppointments,
                  count: data.upcomingAppointmentTotal.validate(),
                  icon: FontAwesomeIcons.calendarCheck,
                );
              } else if (index == 1) {
                return DashBoardCountWidget(
                  title: locale.lblTotalAppointment,
                  color1: appPrimaryColor,
                  subTitle: locale.lblTotalVisitedAppointment,
                  count: data.totalAppointment.validate(),
                  icon: FontAwesomeIcons.calendarCheck,
                );
              } else if (index == 2) {
                return DashBoardCountWidget(
                  title: locale.lblTotalPatient,
                  color1: appSecondaryColor,
                  subTitle: locale.lblTotalVisitedPatients,
                  count: data.totalPatient.validate(),
                  icon: FontAwesomeIcons.userInjured,
                );
              }

              return Offstage();
            },
          )
        ],
      ),
    );
  }

  Widget buildChartWidget({required DashboardModel data}) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          16.height,
          Row(
            children: [
              Text(locale.lblWeeklyAppointments, style: boldTextStyle(size: titleTextSize)).paddingOnly(left: 10).expand(),
              Text(locale.lblMore, style: secondaryTextStyle(color: appSecondaryColor)).onTap(() {
                ShowAppointmentChartScreen().launch(context);
              }),
              16.width,
            ],
          ),
          10.height,
          Container(
            height: 220,
            margin: EdgeInsets.all(8),
            padding: EdgeInsets.only(left: 16, right: 16, top: 24),
            decoration: boxDecorationDefault(borderRadius: radius(), color: context.cardColor),
            child: WeeklyChartComponent(weeklyAppointment: data.weeklyAppointment.validate().isNotEmpty ? data.weeklyAppointment.validate() : emptyGraphList).withWidth(
              context.width(),
            ),
          )
        ],
      ),
    );
  }

  Widget buildAppointmentWidget({required DashboardModel data}) {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(locale.lblTodaySAppointments, style: boldTextStyle(size: 18)),
          8.height,
          if (data.upcomingAppointment.validate().isNotEmpty)
            AnimatedListView(
              shrinkWrap: true,
              itemCount: data.upcomingAppointment.validate().length,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index) {
                UpcomingAppointmentModel value = data.upcomingAppointment.validate()[index];
                return AppointmentDashboardComponent(upcomingData: value).paddingSymmetric(vertical: 8);
              },
            )
          else
            Center(child: NoDataFoundWidget(text: locale.lblNoAppointmentForToday)),
        ],
      ),
    );
  }

  Widget buildBodyWidget({required DashboardModel data}) {
    return AnimatedScrollView(
      children: [
        buildAnalyticsWidget(data: data),
        buildChartWidget(data: data),
        buildAppointmentWidget(data: data),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<DashboardModel>(
        initialData: cachedDashboardModel,
        future: future,
        builder: (_, snap) {
          if (snap.hasData) {
            return buildBodyWidget(data: snap.data!);
          }
          return snapWidgetHelper(
            snap,
            loadingWidget: LoaderWidget(),
            errorBuilder: (p0) {
              return NoDataFoundWidget(text: p0);
            },
          );
        },
      ),
    );
  }
}
