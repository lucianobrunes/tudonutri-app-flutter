import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/body_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/user_model.dart';
import 'package:kivicare_flutter/network/encounter_repository.dart';
import 'package:kivicare_flutter/network/patient_list_repository.dart';
import 'package:kivicare_flutter/screens/doctor/components/patient_widget.dart';
import 'package:kivicare_flutter/screens/doctor/fragments/dashboard_fragment.dart';
import 'package:kivicare_flutter/screens/receptionist/screens/patient/add_patient_screen.dart';
import 'package:kivicare_flutter/utils/cached_value.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:nb_utils/nb_utils.dart';

class PatientFragment extends StatefulWidget {
  @override
  _PatientFragmentState createState() => _PatientFragmentState();
}

class _PatientFragmentState extends State<PatientFragment> {
  Future<List<UserModel>>? future;

  List<UserModel> patientList = [];

  int total = 0;
  int page = 1;

  bool isLastPage = false;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    future = getPatientListAPI(
        page: page,
        patientList: patientList,
        getTotalPatient: (b) => total = b,
        lastPageCallback: (b) => isLastPage = b);
  }

  @override
  void didUpdateWidget(covariant PatientFragment oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    super.dispose();
  }

  void deletePatient(int patientId, String patientDisplayName) {
    Map<String, dynamic> request = {
      "patient_id": patientId,
    };

    appStore.setLoading(true);

    deletePatientData(request).then((value) {
      appStore.setLoading(false);
      toast(locale.lblAllRecordsFor +
          " $patientDisplayName " +
          locale.lblAreDeleted);
      init();
      setState(() {});
    }).catchError((e) {
      appStore.setLoading(false);
      toast(e.toString());
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  Widget buildBodyWidget() {
    return SnapHelperWidget<List<UserModel>>(
      future: future,
      initialData: cachedDoctorPatient,
      loadingWidget: Offstage(),
      onSuccess: (snap) {
        if (snap.isEmpty) {
          return NoDataFoundWidget(text: locale.lblNoDataFound);
        }
        return AnimatedScrollView(
          padding: EdgeInsets.fromLTRB(16, 16, 16, 80),
          disposeScrollController: true,
          listAnimationType: ListAnimationType.None,
          onSwipeRefresh: () async {
            page = 1;
            init();
            return await 2.seconds.delay;
          },
          onNextPage: () {
            if (!isLastPage) {
              page++;
              init();
            }
          },
          children: [
            Text(
              isDoctor()
                  ? "${locale.lblPatients}($total)"
                  : "${locale.lblClinic.getApostropheString()} ${locale.lblPatients}($total)",
              style: boldTextStyle(size: fragmentTextSize),
            ),
            2.height,
            Text(locale.lblSwipeMassage,
                style: secondaryTextStyle(size: 10, color: appSecondaryColor)),
            16.height,
            if (snap.isNotEmpty)
              AnimatedWrap(
                listAnimationType: listAnimationType,
                slideConfiguration: SlideConfiguration(
                    verticalOffset: 200, horizontalOffset: 40),
                itemCount: snap.length,
                itemBuilder: (context, index) => PatientListComponent(
                    patientData: snap[index], callDeletePatient: deletePatient),
              ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Body(child: buildBodyWidget()),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add, color: Colors.white),
          onPressed: () async {
            await AddPatientScreen().launch(context).then(
              (value) {
                if (value ?? false) {
                  init();
                  setState(() {});
                }
              },
            );
          },
        ),
      ),
    );
  }
}
