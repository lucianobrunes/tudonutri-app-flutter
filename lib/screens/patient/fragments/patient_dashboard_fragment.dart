import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/components/view_all_widget.dart';
import 'package:kivicare_flutter/config.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/network/dashboard_repository.dart';
import 'package:kivicare_flutter/screens/appointment/appointment_functions.dart';
import 'package:kivicare_flutter/screens/appointment/components/appointment_dashboard_widget.dart';
import 'package:kivicare_flutter/screens/patient/components/category_widget.dart';
import 'package:kivicare_flutter/screens/patient/components/news_dashboard_widget.dart';
import 'package:kivicare_flutter/screens/patient/models/news_model.dart';
import 'package:kivicare_flutter/screens/patient/screens/service_list_screen.dart';
import 'package:kivicare_flutter/screens/receptionist/screens/doctor/component/doctor_list_component.dart';
import 'package:kivicare_flutter/screens/receptionist/screens/doctor/doctor_list_screen.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:nb_utils/nb_utils.dart';

import '../../../model/dashboard_model.dart';
import '../../../model/service_model.dart';
import '../../../model/upcoming_appointment_model.dart';
import '../../../model/user_model.dart';

class PatientDashBoardFragment extends StatefulWidget {
  @override
  _PatientDashBoardFragmentState createState() => _PatientDashBoardFragmentState();
}

class _PatientDashBoardFragmentState extends State<PatientDashBoardFragment> {
  double secSpace = 32;

  Future<DashboardModel>? future;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    future = getUserDashBoardAPI();
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget buildUpcomingAppointmentWidget({required List<UpcomingAppointmentModel> upcomingAppointment}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
          child: ViewAllLabel(
            label: locale.lblUpcomingAppointments,
            list: upcomingAppointment.validate(),
            viewAllShowLimit: 4,
          ),
        ),
        if (upcomingAppointment.length > 0)
          HorizontalList(
            spacing: 16,
            padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
            itemCount: upcomingAppointment.length,
            itemBuilder: (context, index) {
              return AppointmentDashboardComponent(upcomingData: upcomingAppointment[index]);
            },
          )
        else
          NoDataFoundWidget(
            text: locale.lblNoAppointments,
            iconSize: 40,
            onRetry: () {
              appointmentWidgetNavigation(context);
            },
            retryText: locale.lblBook + " ${locale.lblAppointments}",
          )
      ],
    ).paddingOnly(bottom: 8);
  }

  Widget buildDoctorServiceWidget({required List<ServiceData> service}) {
    if (service.isEmpty) return NoDataFoundWidget(text: locale.lblNoServicesFound);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(16, 16, 16, 8),
          child: ViewAllLabel(
            label: locale.lblClinicServices,
            list: service,
            viewAllShowLimit: 8,
            onTap: () {
              ServiceListScreen().launch(context);
            },
          ),
        ),
        HorizontalList(
          spacing: 20,
          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
          itemCount: service.length,
          itemBuilder: (context, index) {
            return CategoryWidget(data: service[index], index: index);
          },
        ),
      ],
    ).paddingOnly(bottom: 16);
  }

  Widget buildTopDoctorWidget({required List<UserModel> doctorList}) {
    return Padding(
      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
      child: Column(
        children: [
          ViewAllLabel(
            label: locale.lblTopDoctors,
            list: doctorList.validate(),
            viewAllShowLimit: 2,
            onTap: () => DoctorListScreen().launch(context),
          ),
          Wrap(
            runSpacing: 8,
            spacing: 16,
            children: doctorList.map((e) => DoctorListComponent(data: e)).take(2).toList(),
          ).visible(doctorList.isNotEmpty, defaultWidget: NoDataFoundWidget(text: locale.lblNoDataFound, iconSize: 60).center()),
        ],
      ),
    );
  }

  Widget newsComponent({required List<NewsData> newsData}) {
    if (newsData.isEmpty) return Offstage();

    return Padding(
      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ViewAllLabel(
            label: locale.lblExpertsHealthTipsAndAdvice,
            subLabel: locale.lblArticlesByHighlyQualifiedDoctors,
            list: newsData.validate(),
            viewAllShowLimit: 2,
            onTap: () => patientStore.setBottomNavIndex(2),
          ),
          8.height,
          Wrap(
            runSpacing: 16,
            spacing: 16,
            children: List.generate(
              newsData.take(3).length,
              (index) => NewsDashboardWidget(newsData: newsData[index], index: index),
            ),
          )
        ],
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DashboardModel>(
      future: future,
      builder: (context, snap) {
        if (snap.hasData) {
          return AnimatedScrollView(
            padding: EdgeInsets.only(bottom: 80),
            children: [
              buildDoctorServiceWidget(service: snap.data!.serviceList.validate()),
              buildUpcomingAppointmentWidget(upcomingAppointment: snap.data!.upcomingAppointment.validate()),
              16.height,
              buildTopDoctorWidget(doctorList: snap.data!.doctor.validate()),
              16.height,
              newsComponent(newsData: snap.data!.news.validate()),

            ],
          );
        }
        return snapWidgetHelper(snap, loadingWidget: LoaderWidget());
      },
    );
  }
}
