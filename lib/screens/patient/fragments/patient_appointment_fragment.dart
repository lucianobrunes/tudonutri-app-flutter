import 'dart:async';

import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/app_loader.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/network/appointment_repository.dart';
import 'package:kivicare_flutter/screens/appointment/appointment_functions.dart';
import 'package:kivicare_flutter/screens/appointment/components/appointment_widget.dart';
import 'package:kivicare_flutter/screens/doctor/fragments/appointment_fragment.dart';
import 'package:kivicare_flutter/utils/constants.dart';
import 'package:nb_utils/nb_utils.dart';

import '../../../model/upcoming_appointment_model.dart';

class PatientAppointmentFragment extends StatefulWidget {
  @override
  _PatientAppointmentFragmentState createState() =>
      _PatientAppointmentFragmentState();
}

class _PatientAppointmentFragmentState
    extends State<PatientAppointmentFragment> {
  Future<List<UpcomingAppointmentModel>>? future;

  int selectIndex = 0;
  int total = 0;
  int page = 1;
  bool isLastPage = false;

  DateTime current = DateTime.now();

  List<UpcomingAppointmentModel> appointmentList = [];

  StreamSubscription? updateAppointmentApi;

  @override
  void initState() {
    super.initState();
    updateAppointmentApi =
        appointmentStreamController.stream.listen((streamData) {
      page = 1;
      init();
      setState(() {});
    });
    init();
  }

  init() async {
    future = getPatientAppointmentList(userStore.userId.validate(),
        page: page,
        appointmentList: appointmentList,
        getTotalPatient: (b) => total = b,
        lastPageCallback: (b) => isLastPage = b);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void didUpdateWidget(covariant PatientAppointmentFragment oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    if (updateAppointmentApi != null) {
      updateAppointmentApi!.cancel().then((value) {
        log("============== Stream Cancelled ==============");
      });
    }
    super.dispose();
  }

  Widget buildStatusWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('${locale.lblAppointments}',
                style: boldTextStyle(size: fragmentTextSize))
            .paddingSymmetric(horizontal: 16),
        HorizontalList(
          itemCount: appointmentStatusList.length,
          padding: EdgeInsets.symmetric(horizontal: 16),
          itemBuilder: (context, index) {
            bool isSelected = selectIndex == index;
            return GestureDetector(
              onTap: () {
                selectIndex = index;
                setState(() {});

                if (index == 0) {
                  appStore.setStatus(locale.lblAll);
                } else if (index == 1) {
                  appStore.setStatus('-1');
                } else if (index == 2) {
                  appStore.setStatus('3');
                } else if (index == 3) {
                  appStore.setStatus('0');
                } else if (index == 4) {
                  appStore.setStatus(locale.lblPast);
                }

                init();
              },
              child: Container(
                padding:
                    EdgeInsets.only(top: 8, bottom: 8, left: 12, right: 12),
                decoration: boxDecorationDefault(
                    color:
                        isSelected ? context.primaryColor : context.cardColor,
                    borderRadius: radius()),
                child: Text(appointmentStatusList[index],
                    style: primaryTextStyle(
                        color:
                            isSelected ? Colors.white : textPrimaryColorGlobal,
                        size: 14)),
              ),
            );
          },
        ).paddingTop(16),
      ],
    ).paddingOnly(bottom: 16);
  }

  Widget body() {
    return Stack(
      children: [
        buildStatusWidget(),
        SnapHelperWidget<List<UpcomingAppointmentModel>>(
          future: future,
          loadingWidget: Offstage(),
          onSuccess: (snap) {
            if (snap.isEmpty) {
              return NoDataFoundWidget(text: locale.lblNoAppointmentsFound)
                  .center();
            }

            return AnimatedListView(
              physics: AlwaysScrollableScrollPhysics(),
              itemCount: snap.length,
              disposeScrollController: true,
              onSwipeRefresh: () async {
                page = 1;
                init();

                setState(() {});
                return await 2.seconds.delay;
              },
              padding: EdgeInsets.fromLTRB(16, 0, 16, 80),
              onNextPage: () {
                if (!isLastPage) {
                  page++;
                  init();
                  setState(() {});
                }
              },
              shrinkWrap: true,
              listAnimationType: listAnimationType,
              slideConfiguration: SlideConfiguration(verticalOffset: 400),
              itemBuilder: (_, index) =>
                  AppointmentWidget(upcomingData: snap[index])
                      .paddingSymmetric(vertical: 8),
            );
          },
        ).paddingTop(100),
        AppLoader(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: body(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          appointmentWidgetNavigation(context);
        },
      ),
    );
  }
}
