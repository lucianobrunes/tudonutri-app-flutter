import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/app_common_dialog.dart';
import 'package:kivicare_flutter/components/app_setting_widget.dart';
import 'package:kivicare_flutter/components/cached_image_widget.dart';
import 'package:kivicare_flutter/components/setting_third_page.dart';
import 'package:kivicare_flutter/components/theme_selection_dialog.dart';
import 'package:kivicare_flutter/config.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/screens/auth/change_password_screen.dart';
import 'package:kivicare_flutter/screens/language_screen.dart';
import 'package:kivicare_flutter/screens/patient/screens/my_bill_records_screen.dart';
import 'package:kivicare_flutter/screens/patient/screens/patient_clinic_selection_screen.dart';
import 'package:kivicare_flutter/screens/encounter/screen/patient_encounter_list_screen.dart';
import 'package:kivicare_flutter/screens/receptionist/screens/edit_profile_screen.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:nb_utils/nb_utils.dart';

import '../screens/my_report_screen.dart';

class PatientSettingFragment extends StatefulWidget {
  @override
  _PatientSettingFragmentState createState() => _PatientSettingFragmentState();
}

class _PatientSettingFragmentState extends State<PatientSettingFragment> with SingleTickerProviderStateMixin {
  TabController? tabController;

  int? currentIndex = 0;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    tabController = new TabController(initialIndex: 0, length: 3, vsync: this);
    currentIndex = getIntAsync(THEME_MODE_INDEX);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  //region Widgets

  Widget buildEditProfileWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Stack(
          clipBehavior: Clip.none,
          alignment: Alignment.bottomRight,
          children: [
            CachedImageWidget(url: userStore.profileImage.validate(), height: 90, circle: true, fit: BoxFit.cover),
            Positioned(
              bottom: -8,
              left: 0,
              right: -60,
              child: GestureDetector(
                onTap: () => EditProfilesScreen().launch(context),
                child: Container(
                  padding: EdgeInsets.all(4),
                  decoration: boxDecorationWithRoundedCorners(
                    backgroundColor: appPrimaryColor,
                    boxShape: BoxShape.circle,
                    border: Border.all(color: white, width: 3),
                  ),
                  child: Image.asset(ic_edit, height: 20, width: 20, color: Colors.white),
                ),
              ),
            ),
          ],
        ),
        22.height,
        Text(
          getRoleWiseName(name: "${userStore.firstName.validate()} ${userStore.lastName.validate()}"),
          style: boldTextStyle(),
        ),
        Text(userStore.userEmail.validate(), style: primaryTextStyle()),
      ],
    );
  }

  Widget buildFirstWidget() {
    return Wrap(
      alignment: WrapAlignment.start,
      spacing: 16,
      runSpacing: 16,
      children: [
        AppSettingWidget(
          name: locale.lblEncounters,
          image: ic_services,
          widget: PatientEncounterListScreen(),
          subTitle: locale.lblYourEncounters,
        ),
        AppSettingWidget(
          name: locale.lblMedicalReports,
          image: ic_reports,
          widget: MyReportsScreen(),
          subTitle: locale.lblYourReports,
        ),
        AppSettingWidget(
          name: locale.lblBillingRecords,
          image: ic_bill,
          widget: MyBillRecordsScreen(),
          subTitle: locale.lblYourBills,
        ),
        AppSettingWidget(
          name: locale.lblChangeYourClinic,
          image: ic_clinic,
          widget: PatientClinicSelectionScreen(),
          subTitle: locale.lblChooseYourFavouriteClinic,
        ),
      ],
    );
  }

  Widget buildSecondWidget() {
    return Wrap(
      spacing: 16,
      runSpacing: 16,
      children: [
        AppSettingWidget(
          name: locale.lblSelectTheme,
          image: ic_darkMode,
          subTitle: locale.lblChooseYourAppTheme,
          onTap: () {
            showInDialog(
              context,
              contentPadding: EdgeInsets.zero,
              shape: dialogShape(),
              builder: (context) {
                return AppCommonDialog(
                  title: locale.lblSelectTheme,
                  child: ThemeSelectionDialog(),
                );
              },
            );
          },
        ),
        AppSettingWidget(
          name: locale.lblChangePassword,
          image: ic_unlock,
          widget: ChangePasswordScreen(),
        ),
        AppSettingWidget(
          name: locale.lblLanguage,
          isLanguage: true,
          subTitle: selectedLanguageDataModel!.name.validate(),
          image: selectedLanguageDataModel!.flag.validate(),
          onTap: () async => await LanguageScreen().launch(context).then((value) => setState(() {})),
        ),
      ],
    );
  }

  //endregion

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraint) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraint.maxHeight),
            child: IntrinsicHeight(
              child: Column(
                children: <Widget>[
                  50.height,
                  buildEditProfileWidget(),
                  20.height,
                  TabBar(
                    controller: tabController,
                    indicatorSize: TabBarIndicatorSize.tab,
                    splashBorderRadius: radius(),
                    labelColor: appStore.isDarkModeOn ? Colors.white : primaryColor,
                    isScrollable: false,
                    labelStyle: boldTextStyle(),
                    unselectedLabelStyle: primaryTextStyle(),
                    dividerColor: Colors.transparent,
                    unselectedLabelColor: appStore.isDarkModeOn ? gray : textSecondaryColorGlobal,
                    indicator: ShapeDecoration(shape: RoundedRectangleBorder(borderRadius: radius()), color: context.cardColor),
                    tabs: [
                      Tab(icon: Text(locale.lblGeneralSetting, textAlign: TextAlign.center)),
                      Tab(icon: Text(locale.lblAppSettings, textAlign: TextAlign.center)),
                      Tab(icon: Text(locale.lblOther, textAlign: TextAlign.center)),
                    ],
                  ).paddingAll(16).center(),
                  Container(
                    height: constraint.maxHeight,
                    child: TabBarView(
                      controller: tabController,
                      physics: BouncingScrollPhysics(),
                      children: [
                        buildFirstWidget().paddingAll(16),
                        buildSecondWidget().paddingAll(16),
                        SettingThirdPage().paddingAll(16),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
