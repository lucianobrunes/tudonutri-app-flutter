import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/screens/patient/components/clinic_component.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:nb_utils/nb_utils.dart';

import '../../../main.dart';
import '../../../model/clinic_list_model.dart';
import '../../../network/clinic_repository.dart';
import '../../../utils/app_common.dart';

class PatientClinicSelectionScreen extends StatefulWidget {
  @override
  _PatientClinicSelectionScreenState createState() => _PatientClinicSelectionScreenState();
}

class _PatientClinicSelectionScreenState extends State<PatientClinicSelectionScreen> {
  Future<List<Clinic>>? future;

  List<Clinic> clinicList = [];

  int page = 1;
  int totalClinic = 0;
  int selectedIndex = -1;

  bool isLastPage = false;
  bool isChecked = false;

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    future = getClinicListAPI(page: page, appointmentList: clinicList, getTotalPatient: (p0) => totalClinic = p0, lastPageCallback: (p0) => isLastPage = p0);
    setState(() {});
  }

  void switchFavouriteClinic(int selectedClinicId) {
    showConfirmDialogCustom(
      context,
      title: locale.lblAreYouSureYouWantTo + " ${locale.lblChange} ${locale.lblClinic}",
      onAccept: (_) async {
        appStore.setLoading(true);

        Map<String, dynamic> req = {'clinic_id': selectedClinicId};

        await switchClinicApi(req: req).then((value) {
          toast(value['message']);
          if (value['status'] == true) userStore.setClinicId(selectedClinicId.toString());
          setState(() {});
        }).catchError((e) {
          toast(e.toString());
        });
        appStore.setLoading(false);
      },
    );
    setState(() {});
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    getDisposeStatusBarColor();
    super.dispose();
  }

  Widget buildBodyWidget() {
    return FutureBuilder<List<Clinic>>(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return AnimatedScrollView(
            padding: EdgeInsets.fromLTRB(16, 24, 16, 24),
            listAnimationType: ListAnimationType.None,
            children: [
              AnimatedWrap(
                spacing: 16,
                runSpacing: 16,
                itemCount: snapshot.requireData.length,
                itemBuilder: (p0, index) {
                  bool isChecked = snapshot.requireData.indexWhere((element) => element.id.validate() == userStore.userClinicId) == index;

                  return ClinicComponent(
                    clinicData: snapshot.requireData[index],
                    isCheck: isChecked,
                    onTap: (isCheck) {
                      selectedIndex = index;
                      setState(() {});
                      switchFavouriteClinic(snapshot.requireData[index].id.validate().toInt());
                    },
                  );
                },
              ),
            ],
          );
        }

        return snapWidgetHelper(snapshot, loadingWidget: LoaderWidget());
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(
        locale.lblMyClinic,
        textColor: Colors.white,
        systemUiOverlayStyle: defaultSystemUiOverlayStyle(context),
        elevation: 0,
        color: appPrimaryColor,
      ),
      body: buildBodyWidget(),
    );
  }
}
