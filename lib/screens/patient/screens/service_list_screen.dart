import 'package:flutter/material.dart';
import 'package:kivicare_flutter/model/service_model.dart';
import 'package:nb_utils/nb_utils.dart';

import '../../../components/loader_widget.dart';
import '../../../components/no_data_found_widget.dart';
import '../../../main.dart';
import '../../../network/service_repository.dart';
import '../../../utils/app_common.dart';
import '../components/category_widget.dart';

class ServiceListScreen extends StatefulWidget {
  const ServiceListScreen({Key? key}) : super(key: key);

  @override
  State<ServiceListScreen> createState() => _ServiceListScreenState();
}

class _ServiceListScreenState extends State<ServiceListScreen> {
  Future<List<ServiceData>>? future;

  List<ServiceData> serviceList = [];

  int total = 0;
  int page = 1;

  bool isLastPage = false;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    future = getServiceListWithPaginationAPI(page: page, serviceList: serviceList, getTotalService: (b) => total = b, lastPageCallback: (b) => isLastPage = b);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(locale.lblServices, textColor: Colors.white, systemUiOverlayStyle: defaultSystemUiOverlayStyle(context)),
      body: SnapHelperWidget<List<ServiceData>>(
        future: future,
        loadingWidget: LoaderWidget(),
        onSuccess: (snap) {
          if (snap.isEmpty) {
            return NoDataFoundWidget(text: locale.lblNoDataFound);
          }

          return AnimatedScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            disposeScrollController: true,
            padding: EdgeInsets.fromLTRB(16, 16, 16, 80),
            listAnimationType: ListAnimationType.None,
            slideConfiguration: SlideConfiguration(verticalOffset: 400),
            onSwipeRefresh: () async {
              page = 1;
              init();
              return await 2.seconds.delay;
            },
            onNextPage: () {
              if (!isLastPage) {
                page++;
                init();
                setState(() {});
              }
            },
            children: [
              AnimatedListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: groupEmployeesByCountry(snap).keys.length,
                itemBuilder: (context, index) {
                  String title = groupEmployeesByCountry(snap).keys.toList()[index].toString();
                  return SettingSection(
                    title: Text(title.removeAllWhiteSpace().replaceAll('_', ' ').capitalizeFirstLetter(), style: boldTextStyle()),
                    headingDecoration: BoxDecoration(),
                    headerPadding: EdgeInsets.all(4),
                    divider: 16.height,
                    items: [
                      AnimatedWrap(
                        itemCount: groupEmployeesByCountry(snap).values.toList()[index].length,
                        spacing: 16,
                        runSpacing: 16,
                        itemBuilder: (context, i) {
                          return CategoryWidget(data: groupEmployeesByCountry(snap).values.toList()[index][i], index: i, hideMoreButton: false);
                        },
                      ).paddingBottom(24),
                    ],
                  );
                },
              ),
            ],
          );
        },
      ),
    );
  }
}

Map<String, List<ServiceData>> groupEmployeesByCountry(List<ServiceData> employees) {
  return groupBy(employees, (ServiceData e) => e.type.validate());
}
