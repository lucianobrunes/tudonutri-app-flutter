import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/cached_image_widget.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/screens/patient/models/news_model.dart';
import 'package:kivicare_flutter/screens/patient/screens/feeds/feed_details_screen.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:share/share.dart';

import '../../../../network/patient_list_repository.dart';

class FeedFragment extends StatefulWidget {
  @override
  _FeedFragmentState createState() => _FeedFragmentState();
}

class _FeedFragmentState extends State<FeedFragment> {
  Future<NewsModel>? future;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    future = getNewsListAPI();
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  Widget buildListWidget({required NewsData data}) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CachedImageWidget(
            url: data.image.validate(),
            height: 200,
            width: context.width(),
            fit: BoxFit.cover,
          ).cornerRadiusWithClipRRectOnly(topLeft: 8, topRight: 8),
          Container(
            decoration: boxDecorationDefault(borderRadius: radiusOnly(bottomLeft: defaultRadius, bottomRight: defaultRadius), color: context.cardColor),
            width: context.width(),
            padding: EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                8.height,
                Text(data.readableDate.validate(), style: secondaryTextStyle(size: 12)),
                8.height,
                Text(data.postTitle.validate(), style: boldTextStyle(size: 18)),
                8.height,
                Text(parseHtmlString(data.postExcerpt.validate()), style: secondaryTextStyle()),
                22.height,
                Row(
                  children: [
                    Text("By ${data.postAuthorName.validate().capitalizeFirstLetter()}", style: secondaryTextStyle()).expand(),
                    TextIcon(
                      onTap: () {
                        Share.share(data.shareUrl.validate());
                      },
                      text: locale.lblShare,
                      textStyle: secondaryTextStyle(),
                      prefix: ic_share.iconImage(size: 16),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ).onTap(() {
        FeedDetailsScreen(newsData: data).launch(context);
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<NewsModel>(
      future: future,
      builder: (_, snap) {
        if (snap.hasData) {
          return AnimatedListView(
            itemCount: snap.data!.newsData!.length,
            shrinkWrap: true,
            padding: EdgeInsets.only(top: 16, left: 16, bottom: 24, right: 16),
            onSwipeRefresh: () async {
              init();
              return await 2.seconds.delay;
            },
            itemBuilder: (BuildContext context, int index) {
              return buildListWidget(data: snap.data!.newsData![index]);
            },
          ).visible(snap.data!.newsData.validate().isNotEmpty, defaultWidget: Center(child: NoDataFoundWidget(text: locale.lblNoDataFound)));
        }
        return snapWidgetHelper(snap, loadingWidget: LoaderWidget());
      },
    );
  }
}
