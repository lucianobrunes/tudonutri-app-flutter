import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/report_model.dart';
import 'package:kivicare_flutter/network/report_repository.dart';
import 'package:kivicare_flutter/screens/encounter/component/report_component.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:nb_utils/nb_utils.dart';

class MyReportsScreen extends StatefulWidget {
  @override
  _MyReportsScreenState createState() => _MyReportsScreenState();
}

class _MyReportsScreenState extends State<MyReportsScreen> {
  Future<List<ReportData>>? future;

  List<ReportData> reportList = [];

  int total = 0;
  int page = 1;

  bool isLastPage = false;

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    future = getPatientReportListApi(patientId: userStore.userId, page: page, getTotalReport: (b) => total = b, lastPageCallback: (b) => isLastPage = b, reportList: reportList);
  }

  void deleteReportData(String? id) {
    appStore.setLoading(true);
    setState(() {});
    Map<String, dynamic> res = {"report_id": "$id"};
    deleteReportAPI(res).then((value) {
      toast(locale.lblReport + " " + locale.lblDeletedSuccessfully);
    }).catchError((e) {
      toast(e.toString());
    }).whenComplete(() {
      appStore.setLoading(false);
      setState(() {});
    });
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    getDisposeStatusBarColor();
    super.dispose();
  }

  Widget buildBodyWidget() {
    return FutureBuilder<List<ReportData>>(
      future: future,
      builder: (_, snap) {
        if (snap.hasData) {
          if (snap.data.validate().isEmpty) return NoDataFoundWidget(text: locale.lblNoReportsFound).center();
          return ListView.builder(
            padding: EdgeInsets.only(bottom: 60, top: 16, left: 16, right: 16),
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: snap.data!.validate().length,
            itemBuilder: (context, index) {
              return ReportComponent(
                reportData: snap.data![index],
                isForMyReportScreen: true,
              );
            },
          );
        }
        if (snap.connectionState == ConnectionState.waiting) return LoaderWidget();
        return snapWidgetHelper(snap, loadingWidget: LoaderWidget());
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(locale.lblMyReports, textColor: Colors.white, systemUiOverlayStyle: defaultSystemUiOverlayStyle(context), elevation: 0, color: appPrimaryColor),
      body: buildBodyWidget(),
    );
  }
}
