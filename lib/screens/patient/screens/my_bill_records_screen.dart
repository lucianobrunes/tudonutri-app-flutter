import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/loader_widget.dart';
import 'package:kivicare_flutter/components/no_data_found_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/bill_list_model.dart';
import 'package:kivicare_flutter/network/bill_repository.dart';
import 'package:kivicare_flutter/screens/patient/components/bill_component.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:nb_utils/nb_utils.dart';

class MyBillRecordsScreen extends StatefulWidget {
  @override
  _MyBillRecordsScreenState createState() => _MyBillRecordsScreenState();
}

class _MyBillRecordsScreenState extends State<MyBillRecordsScreen> {
  Future<List<BillListData>>? future;

  List<BillListData> patientBillList = [];

  int page = 1;

  bool isLastPage = false;

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    future = getBillListApi(billList: patientBillList, page: page, lastPageCallback: (b) => isLastPage = b);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    getDisposeStatusBarColor();
    super.dispose();
  }

  Widget buildBodyWidget() {
    return SnapHelperWidget<List<BillListData>>(
      future: future,
      defaultErrorMessage: locale.lblNoDataFound,
      loadingWidget: Align(alignment: Alignment.center, child: LoaderWidget()),
      onSuccess: (snap) {
        if (snap.isNotEmpty)
          return AnimatedListView(
            itemCount: snap.length,
            onSwipeRefresh: () async {
              page = 1;
              init();
              setState(() {});
              return await 2.seconds.delay;
            },
            onNextPage: () {
              if (!isLastPage) {
                page++;
                init();
                setState(() {});
              }
            },
            disposeScrollController: true,
            padding: EdgeInsets.fromLTRB(16, 16, 16, 80),
            shrinkWrap: true,
            listAnimationType: listAnimationType,
            slideConfiguration: SlideConfiguration(verticalOffset: 400),
            itemBuilder: (_, index) => BillComponent(billData: snap[index], index: index),
          );
        else
          return Center(child: NoDataFoundWidget(text: '${locale.lblNo} ${locale.lblBillRecords}', iconSize: 140));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(
        //Todo
        locale.lblBillingRecords,
        textColor: Colors.white,
        systemUiOverlayStyle: defaultSystemUiOverlayStyle(context),
        elevation: 0,
        color: appPrimaryColor,
      ),
      body: buildBodyWidget(),
    );
  }
}
