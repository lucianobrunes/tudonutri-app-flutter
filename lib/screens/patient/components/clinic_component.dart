import 'package:flutter/material.dart';
import 'package:kivicare_flutter/model/clinic_list_model.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:nb_utils/nb_utils.dart';

import 'package:kivicare_flutter/components/status_widget.dart';

class ClinicComponent extends StatelessWidget {
  final Clinic clinicData;
  final bool isCheck;
  final Function(bool)? onTap;

  ClinicComponent({required this.clinicData, this.onTap, this.isCheck = false});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.zero,
      child: Stack(
        children: [
          Container(
            width: context.width() / 2 - 28,
            padding: EdgeInsets.only(top: 32, right: 16, left: 16),
            decoration: boxDecorationDefault(color: context.cardColor),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                12.height,
                Text(clinicData.name.validate(), style: boldTextStyle(size: 14)),
                4.height,
                Wrap(
                  spacing: 4,
                  crossAxisAlignment: WrapCrossAlignment.start,
                  children: [
                    ic_location.iconImage(size: 16).paddingAll(4),
                    Text(clinicData.city.validate(), style: secondaryTextStyle()),
                  ],
                ),
                16.height,
                Column(
                  children: [],
                ),
              ],
            ),
          ),
          Positioned(
            top: 8,
            right: 8,
            child: StatusWidget(
              status: clinicData.status.validate(),
              isClinicStatus: true,
              borderRadius: radius(),
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
            ),
          ),
          Positioned(
            bottom: 8,
            right: 8,
            child: Container(
              padding: EdgeInsets.zero,
              decoration: boxDecorationDefault(shape: BoxShape.circle, color: isCheck ? Colors.green : context.cardColor, border: Border.all(width: 2, color: Colors.green)),
              child: isCheck ? Icon(Icons.check, color: Colors.white, size: 16) : Container(padding: EdgeInsets.all(isCheck ? 0 : 8), decoration: boxDecorationDefault(shape: BoxShape.circle, color: context.cardColor)),
            ).onTap(() {
              onTap!.call(!isCheck);
            }),
          )
        ],
      ),
    );
  }
}
