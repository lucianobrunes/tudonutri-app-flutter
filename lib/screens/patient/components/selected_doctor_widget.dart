import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/user_model.dart';
import 'package:kivicare_flutter/network/doctor_list_repository.dart';
import 'package:nb_utils/nb_utils.dart';

class SelectedDoctorWidget extends StatefulWidget {
  int clinicId;
  int doctorId;

  SelectedDoctorWidget({required this.clinicId, required this.doctorId});

  @override
  State<SelectedDoctorWidget> createState() => _SelectedDoctorWidgetState();
}

class _SelectedDoctorWidgetState extends State<SelectedDoctorWidget> {
  Future<UserModel>? future;

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    future = getSelectedDoctorAPI(clinicId: widget.clinicId.validate(), doctorId: widget.doctorId.validate());
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<UserModel>(
      future: future,
      builder: (context, snap) {
        return Container(
          decoration: boxDecorationDefault(color: context.cardColor),
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('${locale.lblDoctor}: ', style: secondaryTextStyle()),
              8.height,
              snap.hasData
                  ? Marquee(
                      child: Text("${snap.data!.displayName.validate().validate()}", style: boldTextStyle(size: 18)),
                    )
                  : Text('${locale.lblLoading} ${locale.lblDoctor}...', style: secondaryTextStyle()),
            ],
          ),
        );
      },
    );

    return Observer(builder: (context) {
      if (appointmentAppStore.mDoctorSelected != null)
        return Container(
          decoration: boxDecorationDefault(color: context.cardColor),
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Doctor: ', style: secondaryTextStyle()),
              8.height,
              Marquee(
                child: Text("${appointmentAppStore.mDoctorSelected?.displayName.validate()}", style: boldTextStyle(size: 18)),
              ),
            ],
          ),
        );

      return Offstage();
    });
    return Container(
      decoration: boxDecorationDefault(color: context.cardColor),
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Doctor: ', style: secondaryTextStyle()),
          8.height,
          Marquee(
            child: Observer(builder: (context) {
              return Text("Dr. ${appointmentAppStore.mDoctorSelected!.displayName.validate()}", style: boldTextStyle(size: 18));
            }),
          ),
        ],
      ),
    );
  }
}
