import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kivicare_flutter/components/price_widget.dart';
import 'package:kivicare_flutter/components/role_widget.dart';
import 'package:kivicare_flutter/components/side_date_widget.dart';
import 'package:kivicare_flutter/components/status_widget.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/bill_list_model.dart';
import 'package:kivicare_flutter/model/encounter_model.dart';
import 'package:kivicare_flutter/screens/doctor/screens/bill_details_screen.dart';
import 'package:kivicare_flutter/screens/encounter/screen/encounter_dashboard_screen.dart';
import 'package:kivicare_flutter/screens/doctor/screens/generate_bill_screen.dart';
import 'package:kivicare_flutter/screens/encounter/screen/patient_encounter_dashboard_screen.dart';
import 'package:kivicare_flutter/utils/colors.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:kivicare_flutter/utils/extensions/string_extensions.dart';
import 'package:kivicare_flutter/utils/images.dart';
import 'package:nb_utils/nb_utils.dart';

class BillComponent extends StatelessWidget {
  final BillListData billData;
  final int index;

  BillComponent({required this.billData, required this.index});

  @override
  Widget build(BuildContext context) {
    return Slidable(
      key: ValueKey(billData),
      endActionPane: ActionPane(
        extentRatio: 0.6,
        motion: ScrollMotion(),
        children: [
          SlidableAction(
            onPressed: (BuildContext context) {
              if (billData.paymentStatus.validate()) {
                BillDetailsScreen(encounterId: billData.id.validate())
                    .launch(context);
              } else {
                if (isPatient()) {
                  BillDetailsScreen(
                          encounterId: billData.encounterId.validate().toInt())
                      .launch(context);
                } else {
                  GenerateBillScreen(
                    data: EncounterModel(
                      id: billData.encounterId.validate().toString(),
                      paymentStatus:
                          billData.paymentStatus.getIntBool().toString(),
                      billId: billData.id.validate().toString(),
                    ),
                  ).launch(context);
                }
              }
            },
            backgroundColor: primaryColor,
            foregroundColor: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(defaultRadius),
                bottomLeft: Radius.circular(defaultRadius)),
            icon: FontAwesomeIcons.moneyBillTransfer,
            label: locale.lblBillDetails,
          ),
          SlidableAction(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(defaultRadius),
                bottomRight: Radius.circular(defaultRadius)),
            onPressed: (context) {
              if (isPatient()) {
                PatientEncounterDashboardScreen(
                  id: billData.encounterId.validate().toString(),
                  isPaymentDone: billData.paymentStatus.validate(),
                ).launch(context);
              } else {
                EncounterDashboardScreen(
                        encounterId: billData.encounterId.validate().toString())
                    .launch(context);
              }
            },
            backgroundColor: appSecondaryColor,
            icon: FontAwesomeIcons.gaugeHigh,
            foregroundColor: Colors.white,
            spacing: 16,
            padding: EdgeInsets.all(8),
            label: locale.lblEncounterDashboard,
          ),
        ],
      ),
      child: Container(
        decoration: boxDecorationDefault(color: context.cardColor),
        padding: EdgeInsets.all(12),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SideDateWidget(
                    tempDate: DateTime.parse(billData.createdAt.validate())),
                SizedBox(
                  height: 60,
                  child: VerticalDivider(
                    color: Colors.grey.withOpacity(0.5),
                    width: 14,
                    thickness: 1,
                  ),
                ),
                4.width,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RoleWidget(
                      isShowReceptionist: true,
                      isShowDoctor: true,
                      child: Text(billData.patientName.validate(),
                          style: boldTextStyle()),
                    ),
                    RoleWidget(
                      isShowPatient: false,
                      isShowReceptionist: true,
                      isShowDoctor: true,
                      child: 2.height,
                    ),
                    RoleWidget(
                      isShowPatient: true,
                      isShowDoctor: true,
                      child: Text('${billData.clinicName.validate()}',
                          style: isDoctor()
                              ? secondaryTextStyle()
                              : boldTextStyle()),
                    ),
                    RoleWidget(
                      isShowPatient: true,
                      child: Text(
                          '${billData.doctorName.validate().split(" ").first.capitalizeFirstLetter().prefixText(value: 'Dr.')}',
                          style:
                              secondaryTextStyle(size: isPatient() ? 14 : 16)),
                    ),
                  ],
                ).expand(),
                10.width,
                StatusWidget(
                    status: billData.paymentStatus.getIntBool().toString(),
                    isPaymentStatus: true),
              ],
            ),
            16.height,
            DottedBorderWidget(
              radius: defaultRadius,
              color: context.primaryColor,
              child: Container(
                padding: EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(locale.lblTotal,
                                style: secondaryTextStyle(size: 12))
                            .expand(),
                        4.height,
                        Text(locale.lblDiscount,
                                style: secondaryTextStyle(size: 12))
                            .expand(),
                        4.height,
                        Text(locale.lblAmountDue,
                                style: secondaryTextStyle(size: 12))
                            .expand(),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        PriceWidget(
                                price:
                                    billData.totalAmount.validate().toString(),
                                textSize: 14)
                            .expand(),
                        PriceWidget(
                                price: billData.discount.validate().toString(),
                                textSize: 14)
                            .expand(),
                        PriceWidget(
                                price:
                                    billData.actualAmount.validate().toString(),
                                textSize: 14)
                            .expand(),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    ).paddingSymmetric(vertical: 8);
    return Container(
      decoration: boxDecorationDefault(color: context.cardColor),
      margin: EdgeInsets.all(4),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 6),
                decoration: boxDecorationDefault(
                  color: getEncounterStatusColor(0.toString()).withOpacity(0.2),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(defaultRadius),
                      bottomRight: Radius.circular(defaultRadius)),
                ),
                child: Text("${getPaymentStatus(0.toString())}".toUpperCase(),
                    style: boldTextStyle(
                        size: 10,
                        color: getEncounterStatusColor(0.toString()),
                        letterSpacing: 1)),
              ),
              Wrap(
                spacing: 12,
                crossAxisAlignment: WrapCrossAlignment.center,
                children: [
                  ic_bill.iconImage().onTap(() {}),
                  FaIcon(
                    FontAwesomeIcons.gaugeHigh,
                    size: 18,
                    color: appSecondaryColor,
                  ).withWidth(16).onTap(() {}),
                  FaIcon(
                    FontAwesomeIcons.pen,
                    size: 18,
                    color: appSecondaryColor,
                  ).onTap(() {})
                ],
              ).paddingOnly(right: 12),
            ],
          ),
          Container(
            padding: EdgeInsets.fromLTRB(8, 2, 8, 16),
            child: Row(
              children: [
                SideDateWidget(tempDate: DateTime.now()),
                SizedBox(
                  height: 140,
                  child: VerticalDivider(
                    color: Colors.grey.withOpacity(0.5),
                    width: 24,
                    thickness: 1,
                    indent: 4,
                    endIndent: 1,
                  ),
                ),
                6.width,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('#id',
                        style: boldTextStyle(color: context.primaryColor)),
                    RoleWidget(
                      isShowPatient: true,
                      isShowReceptionist: true,
                      isShowDoctor: true,
                      child: Text('PatientName', style: boldTextStyle()),
                    ),
                    RoleWidget(
                      isShowPatient: true,
                      child: Text(
                        'Dr. First ( clinic Name )',
                        style: boldTextStyle(size: isPatient() ? 14 : 16),
                      ),
                    ),
                    16.height,
                    DottedBorderWidget(
                      radius: defaultRadius,
                      color: context.primaryColor,
                      child: Container(
                        padding: EdgeInsets.all(8),
                        child: Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(locale.lblTotal + ' : ',
                                    style: secondaryTextStyle()),
                                4.height,
                                Text(locale.lblDiscount + ' : ',
                                    style: secondaryTextStyle()),
                                4.height,
                                Text(locale.lblAmountDue + ' : ',
                                    style: secondaryTextStyle()),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(locale.lblTotal,
                                    style: boldTextStyle(size: 14)),
                                4.height,
                                Text("locale.lblDiscount",
                                    style: boldTextStyle(size: 14)),
                                4.height,
                                Text(locale.lblAmountDue,
                                    style: boldTextStyle(size: 14)),
                              ],
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
