import 'package:flutter/material.dart';
import 'package:kivicare_flutter/components/cached_image_widget../../../model/service_model.dart';
import 'package:kivicare_flutter/components/image_border_component.dart';
import 'package:nb_utils/nb_utils.dart';

class CategoryWidget extends StatelessWidget {
  final ServiceData data;
  final int index;
  final double? width;
  final bool hideMoreButton;

  const CategoryWidget({Key? key, required this.data, required this.index, this.width, this.hideMoreButton = true}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = 60;
    return Container(
      width: width ?? context.width() / 4 - 22,
      child: Column(
        children: [
          ImageBorder(
            src: data.image.validate(),
            height: height,
          ),
          8.height,
          Text(data.name.validate(), textAlign: TextAlign.center, style: secondaryTextStyle(), maxLines: 2, overflow: TextOverflow.ellipsis),
        ],
      ),
    );
  }
}
