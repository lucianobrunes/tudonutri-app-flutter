import 'package:kivicare_flutter/locale/base_language_key.dart';

class LanguageHi extends BaseLanguage {
  @override
  String get appName => "TudoNutri";

  @override
  String get lblWalkThroughTitle1 => "Bem-vindo";

  @override
  String get lblWalkThroughTitle2 => "Encontre médicos";

  @override
  String get lblWalkThroughTitle3 => "Evite tempo de espera";

  @override
  String get lblWalkThroughTitle4 => "Converse com médicos";

  @override
  String get lblWalkThroughSubTitle1 =>
      "O aplicativo TudoNutri é a solução pronta e definitiva de Registro Eletrônico de Saúde (EHR) para médicos, profissionais da área médica, clínicas e gerenciamento de pacientes. Quatro modelos exclusivos para médicos, administradores de clínicas, recepcionistas e pacientes.";

  @override
  String get lblWalkThroughSubTitle2 =>
      "Construa seu aplicativo móvel de forma útil listando médicos. Você pode encontrar médicos mais próximos a você com esse incrível modelo de Médicos. Adicione e gerencie a lista de médicos com o TudoNutri.";

  @override
  String get lblWalkThroughSubTitle3 =>
      "Gerencie consultas em clínicas ou hospitais, agende pacientes com antecedência com a interface inteligente do TudoNutri. Evite atrasos ou esperas longas em filas.";

  @override
  String get lblWalkThroughSubTitle4 =>
      "Os pacientes podem ter consultas diretas com o médico, tornando o primeiro diagnóstico preciso e pontual. As notificações por SMS do TudoNutri enviam alertas para o número registrado do paciente.";

  @override
  String get lblWalkThroughSkipButton => "Pular";

  @override
  String get lblWalkThroughNextButton => "Próximo";

  @override
  String get lblWalkThroughGetStartedButton => "Começar";

  @override
  String get lblSignIn => "Entrar";

  @override
  String get lblLogOut => "Sair";

  @override
  String get lblEmail => "E-mail";

  @override
  String get lblPassword => "Senha";

  @override
  String get lblOldPassword => "Senha Antiga";

  @override
  String get lblNewPassword => "Nova Senha";

  @override
  String get lblConfirmPassword => "Confirmar Senha";

  @override
  String get lblForgotPassword => "Esqueceu a Senha?";

  @override
  String get lblSignUp => "Registrar-se";

  @override
  String get lblBasicDetails => "Detalhes Básicos";

  @override
  String get lblOtherDetails => "Outros Detalhes";

  @override
  String get lblSubmit => "Enviar";

  @override
  String get lblFirstName => "Nome";

  @override
  String get lblLastName => "Sobrenome";

  @override
  String get lblContactNumber => "Número de Contato";

  @override
  String get lblDOB => "Data de Nascimento";

  @override
  String get lblSelectBloodGroup => "Selecionar Grupo Sanguíneo";

  @override
  String get lblAddress => "Endereço";

  @override
  String get lblCity => "Cidade";

  @override
  String get lblCountry => "País";

  @override
  String get lblPostalCode => "Código Postal";

  @override
  String get lblSettings => "Configurações";

  @override
  String get lblChangePassword => "Alterar Senha";

  @override
  String get lblTermsAndCondition => "Termos e Condições";

  @override
  String get lblLanguage => "Idioma";

  @override
  String get lblAboutUs => "Sobre Nós";

  @override
  String get lblRateUs => "Avalie-nos";

  @override
  String get lblSave => "Salvar";

  @override
  String get lblDegree => "Grau";

  @override
  String get lblUniversity => "Universidade";

  @override
  String get lblYear => "Ano";

  @override
  String get lblSearch => "Buscar";

  @override
  String get lblCancel => "Cancelar";

  @override
  String get lblDoctor => "Médico";

  @override
  String get lblDescription => "Descrição";

  @override
  String get lblPrescription => "Prescrição";

  @override
  String get lblFrequency => "Frequência";

  @override
  String get lblDuration => "Duração";

  @override
  String get lblInstruction => "Instrução";

  @override
  String get lblSignInToContinue =>
      "Bem-vindo de volta! Faça login na sua conta";

  @override
  String get lblNewMember => "Não tem uma conta? ";

  @override
  String get lblDone => "Concluído";

  @override
  String get lblSignUpAsPatient => "Crie sua conta no TudoNutri";

  @override
  String get lblAlreadyAMember => "Já é membro?";

  @override
  String get lblLogin => "Login";

  @override
  String get lblDashboard => "Painel de Controle";

  @override
  String get lblAppointments => "Consultas";

  @override
  String get lblAppointment => "Consulta";

  @override
  String get lblPatients => "Pacientes";

  @override
  String get lblTotalPatient => "Total de Pacientes";

  @override
  String get lblTotalVisitedPatients => "Total de pacientes atendidos";

  @override
  String get lblTotalAppointment => "Total de Consultas";

  @override
  String get lblTotalVisitedAppointment => "Total de consultas atendidas";

  @override
  String get lblTodayAppointments => "Consultas de Hoje";

  @override
  String get lblTotalTodayAppointments => "Total de consultas de hoje";

  @override
  String get lblWeeklyAppointments => "Consultas Semanais";

  @override
  String get lblTodaySAppointments => "Suas Consultas";

  @override
  String get lblAppointmentDeleted => "Consulta excluída";

  @override
  String get lblDate => "Data";

  @override
  String get lblConfirmAppointment => "Confirmar Consulta";

  @override
  String get lblSelectDateTime => "Selecione Data e Hora";

  @override
  String get lblSelectServices => "Selecione os Serviços";

  @override
  String get lblBook => "Agendar";

  @override
  String get lblNoAppointmentForToday => "Nenhuma consulta para hoje";

  @override
  String get lblCheckIn => "Check-in";

  @override
  String get lblCheckOut => "Check-out";

  @override
  String get lblAreDeleteAppointment =>
      "Tem certeza de que deseja excluir a consulta?";

  @override
  String get lblYouCannotStart => "Você não pode começar";

  @override
  String get lblPrescriptionAdded => "Prescrição adicionada";

  @override
  String get lblUpdatedSuccessfully => "Atualizado com sucesso";

  @override
  String get lblPrescriptionDeleted => "Prescrição excluída";

  @override
  String get lblAddPrescription => "Adicionar Prescrição";

  @override
  String get lblName => "Nome";

  @override
  String get lblPrescriptionDurationIsRequired =>
      "Duração da prescrição é obrigatória";

  @override
  String get lblDurationInDays => "Duração (em dias)";

  @override
  String get lblAddNewPrescription => "Adicionar Nova Prescrição";

  @override
  String get lblEditPrescriptionDetail => "Editar Detalhes da Prescrição";

  @override
  String get lblAreYouSure => "Tem certeza?";

  @override
  String get lblDays => "dias";

  @override
  String get lblAppointmentIsConfirmed => "Seu agendamento está confirmado";

  @override
  String get lblThanksForBooking => "Obrigado por agendar";

  @override
  String get lblAppointmentConfirmation => "nós confirmamos o seu agendamento";

  @override
  String get lblNoPatientFound => "Nenhum paciente encontrado";

  @override
  String get lblDeleteRecordConfirmation =>
      "Tem certeza de que deseja excluir todos os registros de ";

  @override
  String get lblAllRecordsFor => "Todos os registros de";

  @override
  String get lblAreDeleted => "foram excluídos";

  @override
  String get lblEncounters => "Encontros";

  @override
  String get lblDelete => "Excluir";

  @override
  String get lblMale => "Masculino";

  @override
  String get lblFemale => "Feminino";

  @override
  String get lblOther => "Outro";

  @override
  String get lblMinimumAgeRequired => "Idade mínima requerida é 18.";

  @override
  String get lblCurrentAgeIs => " Sua idade atual é";

  @override
  String get lblGender1 => "Gênero";

  @override
  String get lblSpecialization => "Especialização";

  @override
  String get lblExperience => "Experiência";

  @override
  String get lblZoomConfiguration => "Configuração do Zoom";

  @override
  String get lblTelemed => "Telemedicina";

  @override
  String get lblAPIKeyCannotBeEmpty => "A chave da API não pode estar vazia";

  @override
  String get lblAPIKey => "Chave da API";

  @override
  String get lblAPISecret => "Segredo da API";

  @override
  String get lblAPISecretCannotBeEmpty =>
      "O segredo da API não pode estar vazio";

  @override
  String get lblZoomConfigurationGuide => "Guia de configuração do Zoom";

  @override
  String get lblSignUpOrSignIn => "Cadastre-se ou faça login aqui:";

  @override
  String get lblZoomMarketPlacePortal => "Portal do Zoom Market Place";

  @override
  String get lbl1 => "1.";

  @override
  String get lbl2 => "2.";

  @override
  String get lblClickOnDevelopButton =>
      "Clique/passe o mouse sobre o botão Desenvolver na barra de navegação à direita e clique em construir aplicativo";

  @override
  String get lblCreateApp => "Criar aplicativo";

  @override
  String get lb13 => "3.";

  @override
  String get lblChooseAppTypeToJWT => "Escolha o tipo de aplicativo para JWT";

  @override
  String get lbl4 => "4.";

  @override
  String get lblMandatoryMessage =>
      "Preencha as informações obrigatórias e na tag Credenciais do aplicativo você pode ver a chave da API e o segredo da API.";

  @override
  String get lbl5 => "5.";

  @override
  String get lblCopyAndPasteAPIKey =>
      "Copie e cole a chave da API e o segredo da API aqui e clique no botão salvar e você está pronto para começar.";

  @override
  String get lblEncounterClosed => "Encontro Encerrado";

  @override
  String get lblChangedTo => "Alterado para";

  @override
  String get lblEncounterWillBeClosed => "O encontro será encerrado";

  @override
  String get lblEncounterDate => "Data do encontro";

  @override
  String get lblClinicName => "Nome da clínica";

  @override
  String get lblDoctorName => "Nome do médico";

  @override
  String get lblDesc => "Descrição";

  @override
  String get lblAddNewQualification => "Adicionar Nova Qualificação";

  @override
  String get lblAddBillItem => "Adicionar Item";

  @override
  String get lblServiceIsRequired => "Serviço é obrigatório";

  @override
  String get lblOne => "1";

  @override
  String get lblPrice => "Preço";

  @override
  String get lblQuantity => "Quantidade";

  @override
  String get lblTotal => "Total";

  @override
  String get lblEncounterUpdated => "Encontro Atualizado";

  @override
  String get lblAddNewEncounter => "Adicionar Novo Encontro";

  @override
  String get lblEditEncounterDetail => "Editar Detalhes do Encontro";

  @override
  String get lblHolidayOf => "Feriado de";

  @override
  String get lblModuleIsRequired => "Módulo é obrigatório";

  @override
  String get lblScheduleDate => "Data de Agendamento";

  @override
  String get lblLeaveFor => "Deixar para";

  @override
  String get lblAddHoliday => "Adicionar Feriado";

  @override
  String get lblEditHolidays => "Editar Feriados";

  @override
  String get lblAreYouSureToDelete => "Tem certeza de que deseja excluir?";

  @override
  String get lblNewPatientAddedSuccessfully =>
      "Novo paciente adicionado com sucesso";

  @override
  String get lblPatientDetailUpdatedSuccessfully =>
      "Detalhes do paciente atualizados com sucesso";

  @override
  String get lblBasicInformation => "Informações Básicas";

  @override
  String get lblFirstNameIsRequired => "O primeiro nome é obrigatório";

  @override
  String get lblLastNameIsRequired => "O sobrenome é obrigatório";

  @override
  String get lblEmailIsRequired => "O email é obrigatório";

  @override
  String get lblAddNewPatient => "Adicionar Novo Paciente";

  @override
  String get lblEditPatientDetail => "Editar Detalhes do Paciente";

  @override
  String get lblCategory => "Categoria";

  @override
  String get lblCharges => "Cobrança";

  @override
  String get lblSelectDoctor => "Selecionar Médico";

  @override
  String get lblAddService => "Adicionar Novo Serviço";

  @override
  String get lblEditService => "Editar Serviço";

  @override
  String get lblSelectWeekdays => "Selecionar Dias da Semana";

  @override
  String get lblSessionAddedSuccessfully => "Sessão adicionada com sucesso";

  @override
  String get lblSessionUpdatedSuccessfully => "Sessão atualizada com sucesso";

  @override
  String get lblSessionDeleted => "Sessão Excluída";

  @override
  String get lblPleaseSelectTime => "Por favor, selecione um horário";

  @override
  String get lblStartAndEndTimeNotSame =>
      "O horário de início e término não podem ser iguais";

  @override
  String get lblTimeNotBeforeMorningStartTime =>
      "O horário não pode ser anterior ao horário de início da manhã";

  @override
  String get lblTimeNotBeforeEveningStartTime =>
      "O horário não pode ser anterior ao horário de início da tarde";

  @override
  String get lblTimeShouldBeInMultiplyOf5 => "O horário deve ser múltiplo de 5";

  @override
  String get lblTimeSlotInMinute => "Intervalo de tempo (em minutos)";

  @override
  String get lblTimeSlotRequired => "Intervalo de tempo obrigatório";

  @override
  String get lblWeekDays => "Dias da semana";

  @override
  String get lblMorningSession => "Sessão da manhã";

  @override
  String get lblStartTime => "Horário de início";

  @override
  String get lblEndTime => "Horário de término";

  @override
  String get lblSelectStartTimeFirst =>
      "Selecione o horário de início primeiro";

  @override
  String get lblEveningSession => "Sessão da tarde";

  @override
  String get lblAddSession => "Adicionar Sessão";

  @override
  String get lblEditSession => "Editar Sessão";

  @override
  String get lblInvoiceDetail => "Detalhes da Fatura";

  @override
  String get lblClinicDetails => "Detalhes da Clínica";

  @override
  String get lblPatientDetails => "Detalhes do Paciente";

  @override
  String get lblServices => "Serviços";

  @override
  String get lblDiscount => "Desconto";

  @override
  String get lblAmountDue => "Valor Devido";

  @override
  String get lblInvoiceId => "ID da Fatura";

  @override
  String get lblCreatedAt => "Criado em";

  @override
  String get lblPaymentStatus => "Status de Pagamento";

  @override
  String get lblPatientName => "Nome do Paciente";

  @override
  String get lblGender2 => "Gênero";

  @override
  String get lblSRNo => "Nº SR";

  @override
  String get lblItemName => "NOME DO ITEM";

  @override
  String get lblPRICE => "PREÇO";

  @override
  String get lblQUANTITY => "QUANTIDADE";

  @override
  String get lblTOTAL => "TOTAL";

  @override
  String get lblServicesSelected => "Serviços selecionados";

  @override
  String get lblPatientNameIsRequired => "O nome do paciente é obrigatório";

  @override
  String get lblDoctorSessions => "Todas as Sessões";

  @override
  String get lblEditProfile => "Editar Perfil";

  @override
  String get lblQualification => "Qualificação";

  @override
  String get lblEncounterDashboard => "Painel de Encontros";

  @override
  String get lblEncounterDetails => "Detalhes do Encontro";

  @override
  String get lblProblems => "Problemas";

  @override
  String get lblObservation => "Observação";

  @override
  String get lblNotes => "Notas";

  @override
  String get lblBillAddedSuccessfully => "Fatura Adicionada com Sucesso";

  @override
  String get lblAtLeastSelectOneBillItem =>
      "Selecione pelo menos um item da fatura";

  @override
  String get lblGenerateInvoice => "Gerar Fatura";

  @override
  String get lblSERVICES => "SERVIÇOS";

  @override
  String get lblPayableAmount => "Valor a Pagar";

  @override
  String get lblSaveAndCloseEncounter => "Salvar e Fechar Encontro";

  @override
  String get lblHolidays => "Feriados";

  @override
  String get lblClinic => "Clínica";

  @override
  String get lblAfter => "Após";

  @override
  String get lblWasOffFor => "Esteve de folga por";

  @override
  String get lblYourHolidays => "Seus Feriados";

  @override
  String get lblNoServicesFound => "Nenhum serviço encontrado";

  @override
  String get lblNoDataFound => "Nenhum dado encontrado";

  @override
  String get lblTelemedServicesUpdated =>
      "Serviços de Telemedicina Atualizados";

  @override
  String get lblOn => "Ligado";

  @override
  String get lblOff => "Desligado";

  @override
  String get lblNoAppointments => "Sem consultas";

  @override
  String get lblSelectClinic => "Selecionar Clínica";

  @override
  String get lblEnter => "Entrar";

  @override
  String get lblFieldIsRequired => "Campo é obrigatório";

  @override
  String get lblHoliday => "Feriado";

  @override
  String get lblClinicHoliday => "Feriados da Clínica";

  @override
  String get lblSessions => "Sessões";

  @override
  String get lblClinicSessions => "Sessões da Clínica";

  @override
  String get lblClinicServices => "Serviços";

  @override
  String get lblVideoConsulting => "Consulta por Vídeo";

  @override
  String get lblYourEncounters => "Seus Encontros";

  @override
  String get lblSelectTheme => "Selecionar Tema";

  @override
  String get lblChooseYourAppTheme => "Escolha o tema do aplicativo";

  @override
  String get lblClinicTAndC => "Termos e Condições da Clínica";

  @override
  String get lblAboutKiviCare => "Sobre o TudoNutri";

  @override
  String get lblYourReviewCounts => "Contagem das suas avaliações";

  @override
  String get lblAppVersion => "Versão do Aplicativo";

  @override
  String get lblHelpAndSupport => "Ajuda e Suporte";

  @override
  String get lblSubmitYourQueriesHere => "Envie suas perguntas aqui";

  @override
  String get lblShareKiviCare => "Compartilhar TudoNutri";

  @override
  String get lblLogout => "Sair";

  @override
  String get lblThanksForVisiting => "Obrigado por visitar";

  @override
  String get lblAreYouSureToLogout => "Tem certeza de que deseja sair?";

  @override
  String get lblGeneralSetting => "Configurações Gerais";

  @override
  String get lblAppSettings => "Configurações do Aplicativo";

  @override
  String get lblVersion => "Versão";

  @override
  String get lblContactUs => "Contate-nos";

  @override
  String get lblAboutUsDes =>
      "O TudoNutri é um aplicativo completo de gerenciamento de consultas e registros de clínicas/hospitais para médicos e pacientes. Ele ajuda os pacientes a marcar consultas facilmente a qualquer momento. Usando o aplicativo TudoNutri, você pode construir, gerenciar e rastrear todos os registros de seus pacientes, como informações pessoais, relatórios médicos, medicação, histórico de visitas, notas clínicas, histórico do paciente e outras anotações. As consultas de seus pacientes podem ser facilmente gerenciadas usando o aplicativo TudoNutri. Todos os registros médicos de seus pacientes estarão disponíveis para você consultar imediatamente. Portanto, não há mais necessidade de procurar por papéis para ver o histórico anterior de seus pacientes, tudo está prontamente disponível para seu diagnóstico.";

  @override
  String get lblPurchase => "Comprar";

  @override
  String get lblDemoUserPasswordNotChanged =>
      "A senha do usuário de demonstração não pode ser alterada";

  @override
  String get lblPasswordLengthMessage => "A senha deve ter mais de";

  @override
  String get lblBothPasswordMatched => "Ambas as senhas devem ser iguais";

  @override
  String get lblVisited => "Visitado";

  @override
  String get lblBooked => "Agendado";

  @override
  String get lblCompleted => "Concluído";

  @override
  String get lblCancelled => "Cancelado";

  @override
  String get lblYes => "Sim";

  @override
  String get lblPayment => "Pagamento WooCommerce";

  @override
  String get lblError => "Erro";

  @override
  String get lblRegisteredSuccessfully => "Registrado com sucesso";

  @override
  String get lblBirthDateIsRequired => "Data de nascimento é obrigatória";

  @override
  String get lblBloodGroupIsRequired => "Grupo sanguíneo é obrigatório";

  @override
  String get lblAppointmentBookedSuccessfully =>
      "Consulta agendada com sucesso, por favor verifique seu e-mail.";

  @override
  String get lblSelectedSlots => "Horários selecionados";

  @override
  String get lblSession => "Sessão";

  @override
  String get lblTimeSlotIsBooked => "Horário já está reservado";

  @override
  String get lblAppointmentDate => "Data da consulta";

  @override
  String get lblViewDetails => "Ver detalhes";

  @override
  String get lblDoctorDetails => "Detalhes do Médico";

  @override
  String get lblAreYouWantToDeleteDoctor =>
      "Você tem certeza que deseja excluir o médico?";

  @override
  String get lblDoctorDeleted => "Médico excluído";

  @override
  String get lblYearsExperience => "anos";

  @override
  String get lblYearsOfExperience => "anos de experiência";

  @override
  String get lblAvailableOn => "Disponível para os seguintes dias:";

  @override
  String get lblHealth => "Saúde";

  @override
  String get lblReadMore => "...Leia Mais";

  @override
  String get lblReadLess => " Leia Menos";

  @override
  String get lblBy => "por";

  @override
  String get lblNews => "Notícias";

  @override
  String get lblUpcomingAppointments => "Consultas Agendadas";

  @override
  String get lblViewAll => "Ver todos";

  @override
  String get lblTopDoctors => "Melhores Médicos";

  @override
  String get lblExpertsHealthTipsAndAdvice =>
      "Dicas e Conselhos de Saúde dos Especialistas";

  @override
  String get lblArticlesByHighlyQualifiedDoctors =>
      "Artigos de médicos altamente qualificados sobre saúde diária.. ";

  @override
  String get lblChooseYourDoctor => "Escolha seu médico";

  @override
  String get lblAddNewAppointment => "Adicionar nova consulta";

  @override
  String get lblSelectOneDoctor => "Selecione um médico";

  @override
  String get lblClinicDoctor => "Médico da clínica";

  @override
  String get lblPatientDashboard => "Painel do Paciente";

  @override
  String get lblFeedsAndArticles => "Notícias e Artigos";

  @override
  String get lblPatientsEncounter => "Encontro com Pacientes";

  @override
  String get lblNoEncounterFound => "Nenhum encontro encontrado";

  @override
  String get lblSelectSpecialization => "Selecione uma especialização";

  @override
  String get lblAddDoctorProfile => "Adicionar perfil do médico";

  @override
  String get lblMedicalReport => "Relatório médico";

  @override
  String get lblNewMedicalReport => "+ Novo relatório médico";

  @override
  String get lblGoogleCalendarConfiguration =>
      "Configuração do Google Calendar";

  @override
  String get lblRememberMe => "Lembrar-me";

  @override
  String get lblChooseYourClinic => "Escolha sua clínica";

  @override
  String get lblAll => "Todos";

  @override
  String get lblLatest => "Mais recente";

  @override
  String get lblMon => "Seg";

  @override
  String get lblTue => "Ter";

  @override
  String get lblWed => "Qua";

  @override
  String get lblThu => "Qui";

  @override
  String get lblFri => "Sex";

  @override
  String get lblSat => "Sáb";

  @override
  String get lblSun => "Dom";

  @override
  String get lblNoReportWasSelected => "Nenhum relatório foi selecionado";

  @override
  String get lblAddReportScreen => "Adicionar relatório";

  @override
  String get lblDateCantBeNull => "A data não pode estar vazia";

  @override
  String get lblUploadReport => "Enviar relatório";

  @override
  String get lblConnectWithGoogle => "Conectar com o";

  @override
  String get lblDisconnect => "Desconectar";

  @override
  String get lblAreYouSureYouWantToDisconnect =>
      "Tem certeza de que deseja desconectar?";

  @override
  String get lblLight => "Claro";

  @override
  String get lblDark => "Escuro";

  @override
  String get lblSystemDefault => "Padrão do Sistema";

  @override
  String get lblNA => "N/D";

  @override
  String get lblAddedNewEncounter => "Novo Encontro Adicionado";

  @override
  String get lblCantEditDate => "Você não pode editar a data que já passou";

  @override
  String get lblNoTitle => "Sem título";

  @override
  String get lblSelectOneClinic => "Selecione uma clínica";

  @override
  String get lblPast => "Passado";

  @override
  String get lblAddMedicalReport => "Adicionar Relatório Médico";

  @override
  String get lblSendPrescriptionOnMail => "Enviar prescrição por e-mail";

  @override
  String get lblYouAreConnectedWithTheGoogleCalender =>
      "Você está conectado ao Google Calendar.";

  @override
  String get lblPleaseConnectWithYourGoogleAccountToGetAppointmentsInGoogleCalendarAutomatically =>
      "Por favor, conecte-se à sua Conta do Google para obter as consultas no Google Calendar automaticamente.";

  @override
  String get lblGoogleMeet => "Google Meet";

  @override
  String get lblYouCanUseOneMeetingServiceAtTheTimeWeAreDisablingZoomService =>
      "Você pode usar apenas um serviço de reunião de cada vez. Estamos desativando o serviço do Zoom.";

  @override
  String get lblYouCanUseOneMeetingServiceAtTheTimeWeAreDisablingGoogleMeetService =>
      "Você pode usar apenas um serviço de reunião de cada vez. Estamos desativando o serviço do Google Meet.";

  @override
  String get lblFilesSelected => "Arquivos selecionados";

  @override
  String get lblService => "Serviço";

  @override
  String get lblTime => "Horário";

  @override
  String get lblAppointmentSummary => "Resumo da Consulta";

  @override
  String get lblEncounter => "Encontro";

  @override
  String get lblMedicalReports => "Relatórios Médicos";

  @override
  String get lblConnectedWith => "Conectado com";

  @override
  String get lblContact => "Contato";

  @override
  String get lblQrScanner => "Leitor de QR";

  @override
  String get lblLoginSuccessfully => "Login realizado com sucesso!! ??";

  @override
  String get lblWrongUser => "Usuário incorreto";

  @override
  String get lblMorning => "";

  @override
  String get lblEvening => "";

  @override
  String get lblShare => "Compartilhar";

  @override
  String get lblNoMatch => "Nenhuma visualização correspondente";

  @override
  String get lblNoDataSubTitle =>
      "Não encontramos nada relacionado à sua busca";

  @override
  String get lblEdit => "Editar";

  @override
  String get lblSwipeMassage =>
      "Deslize para a esquerda para editar ou excluir";

  @override
  String get lblReachUsMore => "Entre em contato conosco";

  @override
  String get lblAddressDetail => "Detalhes do Endereço";

  @override
  String get lblChangeYourClinic => "Alterar sua clínica";

  @override
  String get lblYourBills => "Obtenha detalhes de todas as suas contas";

  @override
  String get lblYourReports => "Encontre seus relatórios enviados";

  @override
  String get lblBillRecords => "Registros de Contas";

  @override
  String get lblMyBills => "Minhas Contas";

  @override
  String get lblRevenue => "Receita";

  @override
  String get lblBuyIt => "Comprar";

  @override
  String get lblTryIt => "Experimentar";

  @override
  String get lblYouAreJustOneStepAwayFromHavingAHandsOnBackendDemo =>
      "Você está a apenas um passo de ter uma demonstração prática do backend.";

  @override
  String get lblChooseYourRole => "Escolha seu papel";

  @override
  String get lblEnterYourEmailAddressAsWellAsTheTemporaryLink =>
      "Digite seu endereço de e-mail e o link temporário";

  @override
  String get lblClickOnThatAndScanItFromTheApp =>
      "clique nele e escaneie-o no aplicativo";

  @override
  String get lblYouWillSeeAQRForAppOptionOnTheRightHandCorner =>
      "Você verá uma opção QR para App no canto superior direito,";

  @override
  String get lblEnjoyTheFlawlessKivicareSystemWithEase =>
      "Aproveite o sistema TudoNutri sem falhas com facilidade.";

  @override
  String get lblCamera => "Câmera";

  @override
  String get lblGallery => "Galeria";

  @override
  String get lblRemoveImage => "Remover Imagem";

  @override
  String get lblCanNotBeEmpty => "Não pode estar vazio";

  @override
  String get lblNoConnection => "Sem conexão";

  @override
  String get lblYourInternetConnectionWasInterrupted =>
      "Sua conexão com a Internet foi interrompida";

  @override
  String get lblPlease => "Por favor,";

  @override
  String get lblRetry => "tente novamente";

  @override
  String get lblAfternoon => "Olá";

  @override
  String get lblGood => "Olá";

  @override
  String get lblNight => "Olá";

  @override
  String get lblNoSlotAvailable => "Nenhum horário disponível";

  @override
  String get lblPleaseChooseAnotherDay => "Por favor, escolha outro dia";

  @override
  String? get lblPleaseCloseTheEncounterToCheckoutPatient =>
      "Por favor, feche o encontro para finalizar o atendimento ao paciente";

  @override
  String get lblRemove => "Remover";

  @override
  String get lblAResetPasswordLinkWillBeSentToTheAboveEnteredEmailAddress =>
      "Um link para redefinir a senha será enviado para o endereço de e-mail fornecido acima";

  @override
  String get lblAreYouSureYouWantToChangeThePassword =>
      "Tem certeza de que deseja alterar a senha?";

  @override
  String get lblEnterYourEmailAddress => "Digite seu endereço de e-mail";

  @override
  String get lblHowToGenerateQRCode => "Como gerar um código QR?";

  @override
  String get lblStepsToGenerateQRCode => "Passos para gerar o código QR";

  @override
  String get lblOpenTheDemoUrlInWeb =>
      "Abra a URL de demonstração no navegador";

  @override
  String get lblAreYouSureYouWantToSubmitTheForm =>
      "Tem certeza de que deseja enviar o formulário?";

  @override
  String get lblMore => "Mais";

  @override
  String get lblRatingsAndReviews => "Avaliações e Comentários";

  @override
  String get lblViewFile => "Visualizar Arquivo";

  @override
  String get lblLoading => "Carregando";

  @override
  String get lblAnErrorOccurredWhileCheckingInternetConnectivity =>
      "Ocorreu um erro ao verificar a conectividade com a Internet";

  @override
  String get lblBloodGroup => "Grupo Sanguíneo";

  @override
  String get lblChooseAction => "Escolher Ação";

  @override
  String get lblConnecting => "Conectando";

  @override
  String get lblMyClinic => "Minha Clínica";

  @override
  String get lblMyReports => "Meus Relatórios";

  @override
  String get lblNoReviewsFound => "Nenhuma avaliação encontrada";

  @override
  String get lblPleaseCheckYourNumber => "Verifique seu número";

  @override
  String get lblYourReviews => "Suas Avaliações";

  @override
  String get lblConnected => "Conectado";

  @override
  String get lblNetworkStatus => "Status da Rede";

  @override
  String get lblOffline => "Offline";

  @override
  String get lblUnknown => "Desconhecido";

  @override
  String get lblSelectAppointmentDate => "Selecionar Data do Agendamento";

  @override
  String get lblScanToTest => "Escaneie para testar";

  @override
  String get lblPleaseSelectPaymentStatus =>
      "Selecione o status de pagamento primeiro";

  @override
  String get lblWhatYourCustomersSaysAboutYou =>
      "O que seus clientes dizem sobre você";

  @override
  String get lblFriday => "Sexta-feira";

  @override
  String get lblMonday => "Segunda-feira";

  @override
  String get lblSaturday => "Sábado";

  @override
  String get lblSunday => "Domingo";

  @override
  String get lblThursday => "Quinta-feira";

  @override
  String get lblTuesday => "Terça-feira";

  @override
  String get lblWednesday => "Quarta-feira";

  @override
  String get lblChange => "Alterar";

  @override
  String get lblChangingStatusFrom => "Alterando o status de";

  @override
  String get lblPleaseSelectDoctor => "Por favor, selecione o médico";

  @override
  String get lblClose => "Fechar";

  @override
  String get lblAllTheAppointmentOnSelectedDateWillBeCancelled =>
      "Todos os agendamentos na data selecionada serão cancelados.";

  @override
  String get lblApr => "Abr";

  @override
  String get lblArabic => "Árabe";

  @override
  String get lblAug => "Ago";

  @override
  String get lblDec => "Dez";

  @override
  String get lblEnglish => "Inglês";

  @override
  String get lblFeb => "Fev";

  @override
  String get lblFrench => "Francês";

  @override
  String get lblGerman => "Alemão";

  @override
  String get lblHindi => "Brazil";

  @override
  String get lblJan => "Jan";

  @override
  String get lblJul => "Jul";

  @override
  String get lblJun => "Jun";

  @override
  String get lblMar => "Mar";

  @override
  String get lblMay => "Mai";

  @override
  String get lblNov => "Nov";

  @override
  String get lblOct => "Out";

  @override
  String get lblSep => "Set";

  @override
  String get lblToday => "Hoje";

  @override
  String get lblTomorrow => "Amanhã";

  @override
  String get lblYesterday => "Ontem";

  @override
  String get lblNoQualificationsFound => "Nenhuma qualificação encontrada";

  @override
  String get lblActive => "Ativo";

  @override
  String get lblInActive => "Inativo";

  @override
  String get lblOpen => "Aberto";

  @override
  String get lblPaid => "Pago";

  @override
  String get lblUnPaid => "Não pago";

  @override
  String get lblComplete => "Completo";

  @override
  String get lblClosed => "Fechado";

  @override
  String get lblChooseYourFavouriteClinic => "Escolha sua clínica favorita";

  @override
  String get lblAvailableSession => "Detalhes da sessão disponível";

  @override
  String get lblGetYourAllBillsHere => "Histórico de todas as contas";

  @override
  String get lblServicesYouProvide => "Serviços que você oferece";

  @override
  String get lblYourAllEncounters => "Histórico de todos os encontros";

  @override
  String get lblScheduledHolidays => "Feriados agendados";

  @override
  String get lblNotSelected => "Não selecionado";

  @override
  String get lblStatus => "Status";

  @override
  String get lblMultipleSelection => "Seleção múltipla";

  @override
  String get lblAdded => "Adicionado";

  @override
  String get lblAddedSuccessfully => "Adicionado com sucesso";

  @override
  String get lblInvalidURL => "URL inválida: ";

  @override
  String get lblMedicalHistoryHasBeen => "O histórico médico foi";

  @override
  String get lblReport => "Relatório";

  @override
  String get lblSuccessfully => "Com sucesso";

  @override
  String get lblInvalidDayOfMonth => "Dia do mês inválido";

  @override
  String get lblConnectionReEstablished => "Conexão restabelecida";

  @override
  String get lblToMobileData => "para dados móveis";

  @override
  String get lblToWifi => "para Wi-Fi";

  @override
  String get lblMultipleSelectionIsAvailableForThisService =>
      "Seleção múltipla está disponível para este serviço";

  @override
  String get lblNote => "Observação";

  @override
  String get lblTelemedServiceIsAvailable =>
      "Serviço de telemedicina está disponível";

  @override
  String get lblToCloseTheEncounterInvoicePaymentIsMandatory =>
      "Para fechar o encontro, o pagamento da fatura é obrigatório";

  @override
  String get lblUpdate => "Atualizar";

  @override
  String get lblBillDetails => "Detalhes da fatura";

  @override
  String get lblChooseImage => "Escolher imagem";

  @override
  String get lblApril => "Abril";

  @override
  String get lblAugust => "Agosto";

  @override
  String get lblDecember => "Dezembro";

  @override
  String get lblFebruary => "Fevereiro";

  @override
  String get lblJanuary => "Janeiro";

  @override
  String get lblJuly => "Julho";

  @override
  String get lblJune => "Junho";

  @override
  String get lblMarch => "Março";

  @override
  String get lblMonthly => "Mensalmente";

  @override
  String get lblNovember => "Novembro";

  @override
  String get lblOctober => "Outubro";

  @override
  String get lblSeptember => "Setembro";

  @override
  String get lblWeekly => "Semanalmente";

  @override
  String get lblYearly => "Anualmente";

  @override
  String get lblChangeSignature => "Alterar Assinatura";

  @override
  String get lblClear => "Limpar";

  @override
  String get lblUndo => "Desfazer";

  @override
  String get lblAreYouSureYouWantToUpdateDetails =>
      "Tem certeza de que deseja atualizar os detalhes?";

  @override
  String get lblSignature => "Assinatura";

  @override
  String get lblAreYouSureYouWantTo => "Tem certeza de que deseja";

  @override
  String get lblAdd => "Adicionar";

  @override
  String get lblSelectYearOfGraduation => "Selecione o ano de formatura";

  @override
  String get lblSelect => "Selecionar";

  @override
  String get lblPayBill => "Pagar Conta";

  @override
  String get lblPleaseCheckYourEmailInboxToSetNewPassword =>
      "Verifique sua caixa de entrada de e-mails para definir uma nova senha";

  @override
  String get lblReview => "Avaliação";

  @override
  String get lblBillingRecords => "Registros de Faturamento";

  @override
  String get lblAppointmentCount => "Contagem de Consultas";

  @override
  String get lblNoRecordsFound => "Nenhum registro encontrado";

  @override
  String get lblNoAppointmentsFound => "Nenhuma consulta encontrada";

  @override
  String get lblSelectPatient => "Selecionar Paciente";

  @override
  String get lblNoReportsFound => "Nenhum relatório encontrado";

  @override
  String get lblSpecialities => "Especialidades";

  @override
  String get lblKnowWhatYourPatientsSaysAboutYou =>
      "Saiba o que seus pacientes dizem sobre você";

  @override
  String get lblSchedule => "Agenda";

  @override
  String get lblIsThisTeleMedService => "Este é um serviço de telemedicina?";

  @override
  String get lblAllowMultiSelectionWhileBooking =>
      "Permitir seleção múltipla durante a reserva?";

  @override
  String get lblNo => "Não";

  @override
  String get lblSetStatus => "Definir Status";

  @override
  String get lblFound => "encontrado";

  @override
  String get lblDUpdatedSuccessfully => "Atualizado com sucesso";

  @override
  String get lblDeletedSuccessfully => "Excluído com sucesso";

  @override
  String? get lblPleaseGiveYourRating => "Por favor, dê a sua avaliação";

  @override
  String get lblEnterYourReviews => "Digite sua avaliação (opcional)";
}
