import 'package:kivicare_flutter/locale/base_language_key.dart';

class LanguageDe extends BaseLanguage {
  @override
  String get appName => 'Kivicare';

  @override
  String get lblWalkThroughTitle1 => 'Willkommen';

  @override
  String get lblWalkThroughTitle2 => 'Ärzte finden';

  @override
  String get lblWalkThroughTitle3 => 'Wartenzeit vermeiden';

  @override
  String get lblWalkThroughTitle4 => 'Sprechen Sie mit Ärzten';

  @override
  String get lblWalkThroughSubTitle1 =>
      '""Die Kivicare-App ist die EHR-Lösung für die vorbereitete und ultimative elektronische Gesundheitsakten (ELEPONISIERUNGSBEDEUTUNG) für Ärzte, Mediziner, Klinik und Patientenmanagement. Vier einzigartige Modelle für Arzt, Klinikadministratoren, Empfangsanlagen und Patienten.';

  @override
  String get lblWalkThroughSubTitle2 =>
      'Erstellen Sie Ihre mobile App einfallsreich, indem Sie Ärzte auflisten. Sie können einen Arzt finden, der Ihnen mit diesem erstaunlichen Ärztemodell am nächsten liegt. Fügen Sie die Ärzteliste mit Kivicare hinzu und verwalten Sie sie.';

  @override
  String get lblWalkThroughSubTitle3 =>
      'Verwalten Sie Termine in der Klinik oder im Krankenhaus und planen Sie Patienten im Voraus mit intelligenter Kivicare -Schnittstelle. Vermeiden Sie Verzögerungen oder längeres Warten in Warteschlangen';

  @override
  String get lblWalkThroughSubTitle4 =>
      'Patienten können direkt mit dem Arzt konsultieren, was die erste Diagnose genau und rechtzeitig darstellt. Die SMS -Benachrichtigung von Kivicare sendet Benachrichtigungen über die registrierte Nummer des Patienten.';

  @override
  String get lblWalkThroughSkipButton => 'Überspringen';

  @override
  String get lblWalkThroughNextButton => 'Nächste';

  @override
  String get lblWalkThroughGetStartedButton => 'Loslegen';

  @override
  String get lblSignIn => 'Anmelden';

  @override
  String get lblLogOut => 'Ausloggen';

  @override
  String get lblEmail => 'Email';

  @override
  String get lblPassword => 'Passwort';

  @override
  String get lblOldPassword => 'Altes Passwort';

  @override
  String get lblNewPassword => 'Neues Kennwort';

  @override
  String get lblConfirmPassword => 'Bestätige das Passwort';

  @override
  String get lblForgotPassword => 'Passwort vergessen?';

  @override
  String get lblSignUp => 'Anmeldung';

  @override
  String get lblBasicDetails => 'Grundlegende Details';

  @override
  String get lblOtherDetails => 'Andere Details';

  @override
  String get lblSubmit => 'Einreichen';

  @override
  String get lblFirstName => 'Vorname';

  @override
  String get lblLastName => 'Familienname, Nachname';

  @override
  String get lblContactNumber => 'Kontakt Nummer';

  @override
  String get lblDOB => 'Dob';

  @override
  String get lblSelectBloodGroup => 'Wählen Sie Blutgruppe';

  @override
  String get lblAddress => 'Adresse';

  @override
  String get lblCity => 'Stadt';

  @override
  String get lblCountry => 'Land';

  @override
  String get lblPostalCode => 'Postleitzahl';

  @override
  String get lblSettings => 'Einstellungen';

  @override
  String get lblChangePassword => 'Kennwort ändern';

  @override
  String get lblTermsAndCondition => 'Allgemeine Geschäftsbedingungen';

  @override
  String get lblLanguage => 'Sprache';

  @override
  String get lblAboutUs => 'Über uns';

  @override
  String get lblRateUs => 'Bewerten Sie uns';

  @override
  String get lblSave => 'Speichern';

  @override
  String get lblDegree => 'Grad';

  @override
  String get lblUniversity => 'Universität';

  @override
  String get lblYear => 'Jahr';

  @override
  String get lblSearch => 'Suchen';

  @override
  String get lblCancel => 'Stornieren';

  @override
  String get lblDoctor => 'Arzt';

  @override
  String get lblDescription => 'Beschreibung';

  @override
  String get lblPrescription => 'Verschreibung';

  @override
  String get lblFrequency => 'Frequenz';

  @override
  String get lblDuration => 'Dauer';

  @override
  String get lblInstruction => 'Anweisung';

  @override
  String get lblSignInToContinue =>
      'Willkommen zurück, melden Sie sich in Ihrem Konto an';

  @override
  String get lblNewMember => 'Sie haben kein Konto?';

  @override
  String get lblDone => 'Erledigt';

  @override
  String get lblSignUpAsPatient => 'Erstellen Sie Ihr Kivicare -Konto';

  @override
  String get lblAlreadyAMember => 'Schon ein Mitglied?';

  @override
  String get lblLogin => 'Anmeldung';

  @override
  String get lblDashboard => 'Armaturenbrett';

  @override
  String get lblAppointments => 'Termine';

  @override
  String get lblAppointment => 'Termine';

  @override
  String get lblPatients => 'Patienten';

  @override
  String get lblTotalPatient => 'Totaler Patient';

  @override
  String get lblTotalVisitedPatients => 'Gesamt besuchte Patienten';

  @override
  String get lblTotalAppointment => 'Totalernen Sie.';

  @override
  String get lblTotalVisitedAppointment => 'Total besuchter Termin';

  @override
  String get lblTodayAppointments => 'Heute ernennen.';

  @override
  String get lblTotalTodayAppointments => 'Insgesamt heute Termine';

  @override
  String get lblWeeklyAppointments => 'Wöchentliche Termine';

  @override
  String get lblTodaySAppointments => 'Ihre Termine';

  @override
  String get lblAppointmentDeleted => 'Termin gelöscht';

  @override
  String get lblDate => 'Datum';

  @override
  String get lblConfirmAppointment => 'Termin bestätigen';

  @override
  String get lblSelectDateTime => 'Wählen Sie Datum und Uhrzeit auswählen';

  @override
  String get lblSelectServices => 'Dienste auswählen';

  @override
  String get lblBook => 'Buch';

  @override
  String get lblNoAppointmentForToday => 'Kein Termin für heute';

  @override
  String get lblCheckIn => 'Einchecken';

  @override
  String get lblCheckOut => 'Kasse';

  @override
  String get lblAreDeleteAppointment =>
      'Sind Sie sicher, dass Sie einen Termin löschen möchten?';

  @override
  String get lblYouCannotStart => 'Sie können nicht anfangen';

  @override
  String get lblPrescriptionAdded => 'Rezept hinzugefügt';

  @override
  String get lblUpdatedSuccessfully => 'Erfolgreich geupdated';

  @override
  String get lblPrescriptionDeleted => 'Verschreibungspflichtig gelöscht';

  @override
  String get lblAddPrescription => 'Rezept hinzufügen';

  @override
  String get lblName => 'Name';

  @override
  String get lblPrescriptionDurationIsRequired =>
      'Verschreibungsdauer ist erforderlich';

  @override
  String get lblDurationInDays => 'Dauer (in Tagen)';

  @override
  String get lblAddNewPrescription => 'Neues Rezept hinzufügen';

  @override
  String get lblEditPrescriptionDetail => 'Verschreibungsdetails bearbeiten';

  @override
  String get lblAreYouSure => 'Bist du dir sicher ?';

  @override
  String get lblDays => 'Tage';

  @override
  String get lblAppointmentIsConfirmed => 'Ihr Termin ist bestätigt';

  @override
  String get lblThanksForBooking => 'Danke fürs Buchung';

  @override
  String get lblAppointmentConfirmation =>
      'Wir haben Sie für Ihren Termin bestätigt';

  @override
  String get lblNoPatientFound => 'Kein Patient gefunden';

  @override
  String get lblDeleteRecordConfirmation =>
      'Sind Sie sicher, dass Sie alle Datensätze löschen möchten';

  @override
  String get lblAllRecordsFor => 'Alle Aufzeichnungen für';

  @override
  String get lblAreDeleted => 'werden gelöscht';

  @override
  String get lblEncounters => 'Begegnungen';

  @override
  String get lblDelete => 'Löschen';

  @override
  String get lblMale => 'Männlich';

  @override
  String get lblFemale => 'Weiblich';

  @override
  String get lblOther => 'Andere';

  @override
  String get lblMinimumAgeRequired =>
      'Das erforderliche Mindestalter beträgt 18.';

  @override
  String get lblCurrentAgeIs => 'Ihr aktuelles Alter ist';

  @override
  String get lblGender1 => 'Geschlecht';

  @override
  String get lblSpecialization => 'Spezialisierung';

  @override
  String get lblExperience => 'Erfahrung';

  @override
  String get lblZoomConfiguration => 'Zoomkonfiguration';

  @override
  String get lblTelemed => 'Telemed';

  @override
  String get lblAPIKeyCannotBeEmpty => 'API -Schlüssel kann nicht leer sein';

  @override
  String get lblAPIKey => 'API-Schlüssel';

  @override
  String get lblAPISecret => 'API -Geheimnis';

  @override
  String get lblAPISecretCannotBeEmpty => 'API -Geheimnis kann nicht leer sein';

  @override
  String get lblZoomConfigurationGuide => 'Zoom -Konfigurationshandbuch';

  @override
  String get lblSignUpOrSignIn =>
      'Melden Sie sich hier an oder melden Sie sich an:';

  @override
  String get lblZoomMarketPlacePortal => 'Zoom Marktplatz Portal';

  @override
  String get lbl1 => '1';

  @override
  String get lbl2 => '2';

  @override
  String get lblClickOnDevelopButton =>
      'Klicken Sie auf die Schaltfläche Entwickeln auf der Navigationsleiste auf die Schaltfläche ""Entwickeln"" und klicken Sie auf die Build -App';

  @override
  String get lblCreateApp => 'App erstellen';

  @override
  String get lb13 => '3';

  @override
  String get lblChooseAppTypeToJWT => 'Wählen Sie Ihren App -Typ in JWT aus';

  @override
  String get lbl4 => '4';

  @override
  String get lblMandatoryMessage =>
      'Füllen Sie die obligatorischen Informationen aus und im Tag -Tag -Tag können Sie den API -Schlüssel und das API -Geheimnis sehen.';

  @override
  String get lbl5 => '5';

  @override
  String get lblCopyAndPasteAPIKey =>
      'Kopieren Sie die API -Taste und das API -Geheimnis hier und klicken Sie hier auf die Schaltfläche Speichern. Sie sind bereit zu gehen.';

  @override
  String get lblEncounterClosed => 'Begegnung geschlossen';

  @override
  String get lblChangedTo => 'Gewechselt zu';

  @override
  String get lblEncounterWillBeClosed => 'Begegnung wird geschlossen';

  @override
  String get lblEncounterDate => 'Begegnung Datum';

  @override
  String get lblClinicName => 'Klinikname';

  @override
  String get lblDoctorName => 'Arztname';

  @override
  String get lblDesc => 'Vor';

  @override
  String get lblAddNewQualification => 'Neue Qualifikation hinzufügen';

  @override
  String get lblAddBillItem => 'Artikel hinzufügen';

  @override
  String get lblServiceIsRequired => 'Service ist erforderlich';

  @override
  String get lblOne => '1';

  @override
  String get lblPrice => 'Preis';

  @override
  String get lblQuantity => 'Menge';

  @override
  String get lblTotal => 'Gesamt';

  @override
  String get lblEncounterUpdated => 'Begegnung aktualisiert';

  @override
  String get lblAddNewEncounter => 'Fügen Sie eine neue Begegnung hinzu';

  @override
  String get lblEditEncounterDetail => 'Bearbeiten von Begegnungen Details';

  @override
  String get lblHolidayOf => 'Urlaub von';

  @override
  String get lblModuleIsRequired => 'Modul ist erforderlich';

  @override
  String get lblScheduleDate => 'Zeitplandatum';

  @override
  String get lblLeaveFor => 'Auflassen';

  @override
  String get lblAddHoliday => 'Fügen Sie Urlaub hinzu';

  @override
  String get lblEditHolidays => 'Feiertage bearbeiten';

  @override
  String get lblAreYouSureToDelete =>
      'Sind Sie sicher, dass Sie löschen möchten?';

  @override
  String get lblNewPatientAddedSuccessfully =>
      'Neuer Patient fügte erfolgreich hinzu';

  @override
  String get lblPatientDetailUpdatedSuccessfully =>
      'Patientendetails erfolgreich aktualisiert';

  @override
  String get lblBasicInformation => 'Grundinformation';

  @override
  String get lblFirstNameIsRequired => 'Vorname ist erforderlich';

  @override
  String get lblLastNameIsRequired => 'Nachname ist erforderlich';

  @override
  String get lblEmailIsRequired => 'E-Mail ist erforderlich';

  @override
  String get lblAddNewPatient => 'Neuen Patienten hinzufügen';

  @override
  String get lblEditPatientDetail => 'Patientendetail bearbeiten';

  @override
  String get lblCategory => 'Kategorie';

  @override
  String get lblCharges => 'Aufladung';

  @override
  String get lblSelectDoctor => 'Arzt auswählen';

  @override
  String get lblAddService => 'Neuen Service hinzufügen';

  @override
  String get lblEditService => 'Service bearbeiten';

  @override
  String get lblSelectWeekdays => 'Wochentage auswählen';

  @override
  String get lblSessionAddedSuccessfully => 'Sitzung erfolgreich hinzugefügt';

  @override
  String get lblSessionUpdatedSuccessfully =>
      'Sitzung erfolgreich aktualisiert';

  @override
  String get lblSessionDeleted => 'Sitzung gelöscht';

  @override
  String get lblPleaseSelectTime => 'Bitte wählen Sie Zeit';

  @override
  String get lblStartAndEndTimeNotSame =>
      'Start- und Endzeit kann nicht dasselbe sein';

  @override
  String get lblTimeNotBeforeMorningStartTime =>
      'Die Zeit kann nicht vor dem Morgenstart sein';

  @override
  String get lblTimeNotBeforeEveningStartTime =>
      'Die Zeit kann nicht vor dem Abend für den Abend sein';

  @override
  String get lblTimeShouldBeInMultiplyOf5 =>
      'Die Zeit sollte sich in 5 multiplizieren';

  @override
  String get lblTimeSlotInMinute => 'Zeitfenster (in Minute)';

  @override
  String get lblTimeSlotRequired => 'Zeitfenster erforderlich';

  @override
  String get lblWeekDays => 'Wochentage';

  @override
  String get lblMorningSession => 'Morgensitzung';

  @override
  String get lblStartTime => 'Startzeit';

  @override
  String get lblEndTime => 'Endzeit';

  @override
  String get lblSelectStartTimeFirst => 'Wählen Sie zuerst die Startzeit aus';

  @override
  String get lblEveningSession => 'Abendsitzung';

  @override
  String get lblAddSession => 'Sitzung hinzufügen';

  @override
  String get lblEditSession => 'Sitzung bearbeiten';

  @override
  String get lblInvoiceDetail => 'Rechnungsdetail';

  @override
  String get lblClinicDetails => 'Klinikdetails';

  @override
  String get lblPatientDetails => 'Patientendetails';

  @override
  String get lblServices => 'Dienstleistungen';

  @override
  String get lblDiscount => 'Rabatt';

  @override
  String get lblAmountDue => 'Offener Betrag';

  @override
  String get lblInvoiceId => 'Rechnungs -ID';

  @override
  String get lblCreatedAt => 'Hergestellt in';

  @override
  String get lblPaymentStatus => 'Zahlungsstatus';

  @override
  String get lblPatientName => 'Patientenname';

  @override
  String get lblGender2 => 'Geschlecht';

  @override
  String get lblSRNo => 'Sr -Nr';

  @override
  String get lblItemName => 'ARTIKELNAME';

  @override
  String get lblPRICE => 'PREIS';

  @override
  String get lblQUANTITY => 'MENGE';

  @override
  String get lblTOTAL => 'GESAMT';

  @override
  String get lblServicesSelected => 'Dienste ausgewählt';

  @override
  String get lblPatientNameIsRequired => 'Der Patientenname ist erforderlich';

  @override
  String get lblDoctorSessions => 'Alle Sitzung';

  @override
  String get lblEditProfile => 'Profil bearbeiten';

  @override
  String get lblQualification => 'Qualifikation';

  @override
  String get lblEncounterDashboard => 'Begegnung Das Dashboard';

  @override
  String get lblEncounterDetails => 'Begegnung mit Details';

  @override
  String get lblProblems => 'Problem';

  @override
  String get lblObservation => 'Überwachung';

  @override
  String get lblNotes => 'Anmerkungen';

  @override
  String get lblBillAddedSuccessfully => 'Bill fügte erfolgreich hinzu';

  @override
  String get lblAtLeastSelectOneBillItem =>
      'Um mindestens einen Rechnungselement zu sparen';

  @override
  String get lblGenerateInvoice => 'Rechnung erstellen';

  @override
  String get lblSERVICES => 'DIENSTLEISTUNGEN';

  @override
  String get lblPayableAmount => 'Zu zahlender Betrag';

  @override
  String get lblSaveAndCloseEncounter => 'Speichern & Schließung begegnen';

  @override
  String get lblHolidays => 'Feiertage';

  @override
  String get lblClinic => 'Klinik';

  @override
  String get lblAfter => 'Nach';

  @override
  String get lblWasOffFor => 'War ausgeschaltet für';

  @override
  String get lblYourHolidays => 'Deine Ferien';

  @override
  String get lblNoServicesFound => 'Keine Dienste gefunden';

  @override
  String get lblNoDataFound => 'Keine Daten gefunden';

  @override
  String get lblTelemedServicesUpdated => 'Telemed Services aktualisiert';

  @override
  String get lblOn => 'An';

  @override
  String get lblOff => 'Aus';

  @override
  String get lblNoAppointments => 'Keine Termine';

  @override
  String get lblSelectClinic => 'Klinik auswählen';

  @override
  String get lblEnter => 'Eingeben';

  @override
  String get lblFieldIsRequired => 'Feld ist erforderlich';

  @override
  String get lblHoliday => 'Urlaub';

  @override
  String get lblClinicHoliday => 'Klinikurlaub';

  @override
  String get lblSessions => 'Sitzungen';

  @override
  String get lblClinicSessions => 'Kliniksitzungen';

  @override
  String get lblClinicServices => 'Dienstleistungen';

  @override
  String get lblVideoConsulting => 'Videoberatung';

  @override
  String get lblYourEncounters => 'Ihre Begegnungen';

  @override
  String get lblSelectTheme => 'Thema wählen';

  @override
  String get lblChooseYourAppTheme => 'Wählen Sie Ihr App -Thema aus';

  @override
  String get lblClinicTAndC => 'Klinik T & C';

  @override
  String get lblAboutKiviCare => 'Über Kivicare';

  @override
  String get lblYourReviewCounts => 'Ihre Bewertung zählt';

  @override
  String get lblAppVersion => 'App Version';

  @override
  String get lblHelpAndSupport => 'Hilfe Unterstützung';

  @override
  String get lblSubmitYourQueriesHere => 'Senden Sie hier Ihre Fragen';

  @override
  String get lblShareKiviCare => 'Teile Kivicare';

  @override
  String get lblLogout => 'Ausloggen';

  @override
  String get lblThanksForVisiting => 'Danke für den Besuch';

  @override
  String get lblAreYouSureToLogout =>
      'Sind Sie sicher, dass Sie sich abmelden möchten?';

  @override
  String get lblGeneralSetting => 'Allgemeine Einstellungen';

  @override
  String get lblAppSettings => 'App Einstellungen';

  @override
  String get lblVersion => 'Ausführung';

  @override
  String get lblContactUs => 'Kontaktiere uns';

  @override
  String get lblAboutUsDes =>
      '""Kivicare ist eine vollständige Klinik-/Krankenhaustermin- und Rekordmanagement für Ärzte und Patienten App. Es hilft den Patienten, ihren Arzttermin zu jedem Zeitpunkt leicht zu buchen Informationen, medizinische Berichte, Medikamente, Besuch der Geschichte, klinische Notizen, Patientenanamnese und andere Notizen. Termine für Ihre Patienten können einfach mit der Kivicare -App verwaltet werden. Alle Ihre medizinischen Unterlagen für Patienten können Sie sofort betrachten. Also nicht mehr Wenn Sie durch Papiere jonglieren, um die Vorgeschichte Ihrer Patienten zu sehen, ist alles für Ihre Diagnose verfügbar.';

  @override
  String get lblPurchase => 'Kaufen';

  @override
  String get lblDemoUserPasswordNotChanged =>
      'Demo -Benutzer -Passwort kann nicht geändert werden';

  @override
  String get lblPasswordLengthMessage => 'Passwortlänge sollte mehr als sein';

  @override
  String get lblBothPasswordMatched => 'Beide Passwort sollten übereinstimmen';

  @override
  String get lblVisited => 'hat besucht';

  @override
  String get lblBooked => 'Gebucht';

  @override
  String get lblCompleted => 'Vollendet';

  @override
  String get lblCancelled => 'Abgesagt';

  @override
  String get lblYes => 'Ja';

  @override
  String get lblPayment => 'Woocommerce -Zahlung';

  @override
  String get lblError => 'Fehler';

  @override
  String get lblRegisteredSuccessfully => 'Erfolgreich registriert';

  @override
  String get lblBirthDateIsRequired => 'Geburtsdatum ist erforderlich';

  @override
  String get lblBloodGroupIsRequired => 'Blutgruppe ist erforderlich';

  @override
  String get lblAppointmentBookedSuccessfully =>
      'Termin erfolgreich gebucht, lesen Sie bitte Ihre E -Mails.';

  @override
  String get lblSelectedSlots => 'Ausgewählte Slots';

  @override
  String get lblSession => 'Sitzung';

  @override
  String get lblTimeSlotIsBooked => 'Zeitfenster ist gebucht';

  @override
  String get lblAppointmentDate => 'Termin';

  @override
  String get lblViewDetails => 'Sicht';

  @override
  String get lblDoctorDetails => 'Doktionsdetails';

  @override
  String get lblAreYouWantToDeleteDoctor =>
      'Sind Sie sicher, dass Sie Arzt löschen möchten?';

  @override
  String get lblDoctorDeleted => 'Arzt gelöscht';

  @override
  String get lblYearsExperience => 'Jahr';

  @override
  String get lblYearsOfExperience => 'langjährige Erfahrung';

  @override
  String get lblAvailableOn => 'Verfügbar für diese Wochentage:';

  @override
  String get lblHealth => 'Gesundheit';

  @override
  String get lblReadMore => '...Weiterlesen';

  @override
  String get lblReadLess => 'Lese weniger';

  @override
  String get lblBy => 'von';

  @override
  String get lblNews => 'Nachricht';

  @override
  String get lblUpcomingAppointments => 'Bevorstehende Termine';

  @override
  String get lblViewAll => 'Alle ansehen';

  @override
  String get lblTopDoctors => 'Top -Ärzte';

  @override
  String get lblExpertsHealthTipsAndAdvice =>
      'Experten gesundheitliche Tipps und Ratschläge';

  @override
  String get lblArticlesByHighlyQualifiedDoctors =>
      'Artikel von hochqualifizierten Ärzten über die alltägliche Gesundheit.';

  @override
  String get lblChooseYourDoctor => 'Wählen Sie Ihren Arzt';

  @override
  String get lblAddNewAppointment => 'Neuen Termin hinzufügen';

  @override
  String get lblSelectOneDoctor => 'Wählen Sie einen Arzt aus';

  @override
  String get lblClinicDoctor => 'Klinikarzt';

  @override
  String get lblPatientDashboard => 'Armaturenbrett';

  @override
  String get lblFeedsAndArticles => 'Feeds & Artikel';

  @override
  String get lblPatientsEncounter => 'Patienten begegnen';

  @override
  String get lblNoEncounterFound => 'Keine Begegnung gefunden';

  @override
  String get lblSelectSpecialization => 'Wählen Sie Spezialisierung';

  @override
  String get lblAddDoctorProfile => 'Arztprofil hinzufügen';

  @override
  String get lblMedicalReport => 'Medizinischer Bericht';

  @override
  String get lblNewMedicalReport => 'Neuer medizinischer Bericht';

  @override
  String get lblGoogleCalendarConfiguration => 'Google Kalender';

  @override
  String get lblRememberMe => 'Mich erinnern';

  @override
  String get lblChooseYourClinic => 'Wählen Sie Ihre Klinik';

  @override
  String get lblAll => 'Alle';

  @override
  String get lblLatest => 'Neueste';

  @override
  String get lblMon => 'Mon';

  @override
  String get lblTue => 'Di';

  @override
  String get lblWed => 'Heiraten';

  @override
  String get lblThu => 'Do';

  @override
  String get lblFri => 'Fr';

  @override
  String get lblSat => 'Sa';

  @override
  String get lblSun => 'Sonne';

  @override
  String get lblNoReportWasSelected => 'Es wurde kein Bericht ausgewählt';

  @override
  String get lblAddReportScreen => 'Bericht hinzufügen';

  @override
  String get lblDateCantBeNull => 'Datum kann nicht leer sein';

  @override
  String get lblUploadReport => 'Bericht hochladen';

  @override
  String get lblConnectWithGoogle => 'Verbinden mit';

  @override
  String get lblDisconnect => 'Trennen';

  @override
  String get lblAreYouSureYouWantToDisconnect =>
      'Sind Sie sicher, dass Sie die Verbindung trennen möchten?';

  @override
  String get lblLight => 'Licht';

  @override
  String get lblDark => 'Dunkel';

  @override
  String get lblSystemDefault => 'Systemfehler';

  @override
  String get lblNA => 'N / A';

  @override
  String get lblAddedNewEncounter => 'Neue Begegnung hinzugefügt';

  @override
  String get lblCantEditDate =>
      'Sie können das bereits übergebene Datum bearbeiten';

  @override
  String get lblNoTitle => 'Kein Titel';

  @override
  String get lblSelectOneClinic => 'Wählen Sie eine Klinik aus';

  @override
  String get lblPast => 'Vergangenheit';

  @override
  String get lblAddMedicalReport => 'Medizinischen Bericht hinzufügen';

  @override
  String get lblSendPrescriptionOnMail => 'Rezept per Post senden';

  @override
  String get lblYouAreConnectedWithTheGoogleCalender =>
      'Sie sind mit dem Google -Kalender verbunden.';

  @override
  String get lblPleaseConnectWithYourGoogleAccountToGetAppointmentsInGoogleCalendarAutomatically =>
      'Bitte verbinden Sie sich mit Ihrem Google -Konto, um Termine im Google -Kalender automatisch zu erhalten.';

  @override
  String get lblGoogleMeet => 'Google Meet';

  @override
  String get lblYouCanUseOneMeetingServiceAtTheTimeWeAreDisablingZoomService =>
      'Sie können zu diesem Zeitpunkt einen Besprechungsdienst nutzen. Wir deaktivieren den Zoom -Service.';

  @override
  String get lblYouCanUseOneMeetingServiceAtTheTimeWeAreDisablingGoogleMeetService =>
      'Sie können zu diesem Zeitpunkt einen Besprechungsdienst nutzen. Wir deaktivieren Google Meet Service.';

  @override
  String get lblFilesSelected => 'Dateien ausgewählt';

  @override
  String get lblService => 'Service';

  @override
  String get lblTime => 'Zeit';

  @override
  String get lblAppointmentSummary => 'Terminzusammenfassung';

  @override
  String get lblEncounter => 'Begegnen';

  @override
  String get lblMedicalReports => 'Medizinischer Bericht';

  @override
  String get lblConnectedWith => 'Verbunden mit';

  @override
  String get lblContact => 'Kontakt';

  @override
  String get lblQrScanner => 'QR -Scanner';

  @override
  String get lblLoginSuccessfully => 'Anmeldung erfolgreich !! 🎉';

  @override
  String get lblWrongUser => 'Falscher Benutzer';

  @override
  String get lblMorning => 'Morgen';

  @override
  String get lblEvening => 'Abend';

  @override
  String get lblShare => 'Aktie';

  @override
  String get lblNoMatch => 'Keine passenden Ansichten';

  @override
  String get lblNoDataSubTitle => 'Wir konnten nichts mit Ihrer Suche finden';

  @override
  String get lblEdit => 'Bearbeiten';

  @override
  String get lblSwipeMassage =>
      'Links wischen, um zu bearbeiten oder zu löschen';

  @override
  String get lblReachUsMore => 'Erreichen Sie uns mehr';

  @override
  String get lblAddressDetail => 'Adressen Details';

  @override
  String get lblChangeYourClinic => 'Ändern Sie Ihre Klinik';

  @override
  String get lblYourBills => 'Holen Sie sich alle Rechnungsdetails';

  @override
  String get lblYourReports => 'Finden Sie Ihre hochgeladenen Berichte';

  @override
  String get lblBillRecords => 'Bill Records';

  @override
  String get lblMyBills => 'Meine Rechnungen';

  @override
  String get lblRevenue => 'Einnahmen';

  @override
  String get lblBuyIt => 'Kauf es';

  @override
  String get lblTryIt => 'Versuch es';

  @override
  String get lblYouAreJustOneStepAwayFromHavingAHandsOnBackendDemo =>
      'Sie sind nur einen Schritt von einer praktischen Backend-Demo entfernt.';

  @override
  String get lblChooseYourRole => 'Wählen Sie Ihre Rolle';

  @override
  String get lblEnterYourEmailAddressAsWellAsTheTemporaryLink =>
      'Geben Sie Ihre E -Mail -Adresse sowie den temporären Link ein';

  @override
  String get lblClickOnThatAndScanItFromTheApp =>
      'Klicken Sie darauf und scannen Sie es aus der App';

  @override
  String get lblYouWillSeeAQRForAppOptionOnTheRightHandCorner =>
      'Sie sehen eine QR für App -Option in der rechten Ecke.';

  @override
  String get lblEnjoyTheFlawlessKivicareSystemWithEase =>
      'Genießen! Das makellose Kivicare -System mit Leichtigkeit.';

  @override
  String get lblCamera => 'Kamera';

  @override
  String get lblGallery => 'Galerie';

  @override
  String get lblRemoveImage => 'Entferne Bild';

  @override
  String get lblCanNotBeEmpty => 'Kann nicht leer sein';

  @override
  String get lblNoConnection => 'Keine Verbindung';

  @override
  String get lblYourInternetConnectionWasInterrupted =>
      'Ihre Internetverbindung wurde unterbrochen';

  @override
  String get lblPlease => 'Bitte';

  @override
  String get lblRetry => 'wiederholen';

  @override
  String get lblAfternoon => 'Guten Tag';

  @override
  String get lblGood => 'Gut';

  @override
  String get lblNight => 'Gute Nacht';

  @override
  String get lblNoSlotAvailable => 'Kein Slot verfügbar';

  @override
  String get lblPleaseChooseAnotherDay => 'Bitte wählen Sie einen weiteren Tag';

  @override
  String get lblPleaseCloseTheEncounterToCheckoutPatient =>
      'Bitte schließen Sie die Begegnung, um Patient zu überprüfen';

  @override
  String get lblRemove => 'entfernen';

  @override
  String get lblAResetPasswordLinkWillBeSentToTheAboveEnteredEmailAddress =>
      'Ein Link zum Zurücksetzen des Kennworts wird an die oben eingegebene E -Mail -Adresse gesendet';

  @override
  String get lblAreYouSureYouWantToChangeThePassword =>
      'Sind Sie sicher, dass Sie das Passwort ändern möchten?';

  @override
  String get lblEnterYourEmailAddress => 'Geben sie ihre E-Mailadresse ein';

  @override
  String get lblHowToGenerateQRCode => 'Wie generiere ich QR -Code?';

  @override
  String get lblStepsToGenerateQRCode => 'Schritte zum Generieren des QR -Code';

  @override
  String get lblOpenTheDemoUrlInWeb => 'Öffnen Sie die Demo -URL im Web';

  @override
  String get lblAreYouSureYouWantToSubmitTheForm =>
      'Sind Sie sicher, dass Sie das Formular einreichen möchten?';

  @override
  String get lblMore => 'Mehr';

  @override
  String get lblRatingsAndReviews => 'Bewertungen und Bewertungen';

  @override
  String get lblViewFile => 'Datei ansehen';

  @override
  String get lblLoading => 'Wird geladen';

  @override
  String get lblAnErrorOccurredWhileCheckingInternetConnectivity =>
      'Bei der Überprüfung der Internetkonnektivität trat ein Fehler auf';

  @override
  String get lblBloodGroup => 'Blutgruppe';

  @override
  String get lblChooseAction => 'Aktion wählen';

  @override
  String get lblConnecting => 'Verbinden';

  @override
  String get lblMyClinic => 'Meine Klinik';

  @override
  String get lblMyReports => 'Meine Berichte';

  @override
  String get lblNoReviewsFound => 'Keine Bewertungen gefunden';

  @override
  String get lblPleaseCheckYourNumber => 'Bitte überprüfen Sie Ihre Nummer';

  @override
  String get lblYourReviews => 'Ihre Bewertungen';

  @override
  String get lblConnected => 'In Verbindung gebracht';

  @override
  String get lblNetworkStatus => 'Netzwerkstatus';

  @override
  String get lblOffline => 'Offline';

  @override
  String get lblUnknown => 'Unbekannt';

  @override
  String get lblSelectAppointmentDate => 'Wählen Sie Termindatum';

  @override
  String get lblScanToTest => 'Scannen, um zu testen';

  @override
  String get lblPleaseSelectPaymentStatus =>
      'Bitte wählen Sie den Zahlungsstatus zuerst aus';

  @override
  String get lblWhatYourCustomersSaysAboutYou =>
      'Was Ihre Kunden über Sie sagen';

  @override
  String get lblFriday => 'Freitag';

  @override
  String get lblMonday => 'Montag';

  @override
  String get lblSaturday => 'Samstag';

  @override
  String get lblSunday => 'Sonntag';

  @override
  String get lblThursday => 'Donnerstag';

  @override
  String get lblTuesday => 'Dienstag';

  @override
  String get lblWednesday => 'Mittwoch';

  @override
  String get lblChange => 'Ändern';

  @override
  String get lblChangingStatusFrom => 'Status ändern von';

  @override
  String get lblPleaseSelectDoctor => 'Bitte wählen Sie Arzt';

  @override
  String get lblClose => 'Schließen';

  @override
  String get lblAllTheAppointmentOnSelectedDateWillBeCancelled =>
      'Der gesamte Termin am ausgewählten Datum wird storniert.';

  @override
  String get lblApr => 'Apr';

  @override
  String get lblArabic => 'Arabisch';

  @override
  String get lblAug => 'August';

  @override
  String get lblDec => 'Dez';

  @override
  String get lblEnglish => 'Englisch';

  @override
  String get lblFeb => 'Feb';

  @override
  String get lblFrench => 'Französisch';

  @override
  String get lblGerman => 'Deutsch';

  @override
  String get lblHindi => 'Brazil';

  @override
  String get lblJan => 'Jan';

  @override
  String get lblJul => 'Jul';

  @override
  String get lblJun => 'Jun';

  @override
  String get lblMar => 'Beschädigen';

  @override
  String get lblMay => 'Dürfen';

  @override
  String get lblNov => 'Nov.';

  @override
  String get lblOct => 'Oktober';

  @override
  String get lblSep => 'Sep';

  @override
  String get lblToday => 'Heute';

  @override
  String get lblTomorrow => 'Morgen';

  @override
  String get lblYesterday => 'Gestern';

  @override
  String get lblNoQualificationsFound => 'Keine Qualifikationen gefunden';

  @override
  String get lblActive => 'Aktiv';

  @override
  String get lblInActive => 'Inaktiv';

  @override
  String get lblOpen => 'Offen';

  @override
  String get lblPaid => 'Bezahlt';

  @override
  String get lblUnPaid => 'Unbezahlt';

  @override
  String get lblComplete => 'Vollständig';

  @override
  String get lblClosed => 'Geschlossen';

  @override
  String get lblChooseYourFavouriteClinic => 'Wählen Sie Ihre Lieblingsklinik';

  @override
  String get lblAvailableSession => 'Verfügbare Sitzungsdetails';

  @override
  String get lblGetYourAllBillsHere => 'Geschichte aller Bill Records';

  @override
  String get lblServicesYouProvide => 'Dienstleistungen, die Sie anbieten';

  @override
  String get lblYourAllEncounters => 'Geschichte aller Begegnungen';

  @override
  String get lblScheduledHolidays => 'Geplante Feiertage';

  @override
  String get lblNotSelected => 'Nicht ausgewählt';

  @override
  String get lblStatus => 'Status';

  @override
  String get lblMultipleSelection => 'Mehrfachauswahl';

  @override
  String get lblAdded => 'Hinzugefügt';

  @override
  String get lblAddedSuccessfully => 'Erfolgreich hinzugefügt';

  @override
  String get lblInvalidURL => 'Ungültige URL:';

  @override
  String get lblMedicalHistoryHasBeen => 'Krankengeschichte war';

  @override
  String get lblReport => 'Bericht';

  @override
  String get lblSuccessfully => 'Erfolgreich';

  @override
  String get lblInvalidDayOfMonth => 'Ungültiger Tag des Monats';

  @override
  String get lblConnectionReEstablished => 'Verbindung wieder hergestellt';

  @override
  String get lblToMobileData => 'mobile Daten';

  @override
  String get lblToWifi => 'nach WiFi';

  @override
  String get lblMultipleSelectionIsAvailableForThisService =>
      'Für diesen Service steht eine Option mit mehreren Auswahlmöglichkeiten zur Verfügung';

  @override
  String get lblNote => 'Notiz';

  @override
  String get lblTelemedServiceIsAvailable => 'Telemed Service ist verfügbar';

  @override
  String get lblToCloseTheEncounterInvoicePaymentIsMandatory =>
      'Um die Begegnung zu schließen, ist die Rechnungszahlung obligatorisch';

  @override
  String get lblUpdate => 'Aktualisieren';

  @override
  String get lblBillDetails => 'Rechnungsdetails';

  @override
  String get lblChooseImage => 'Wählen Sie Bild';

  @override
  String get lblApril => 'April';

  @override
  String get lblAugust => 'August';

  @override
  String get lblDecember => 'Dezember';

  @override
  String get lblFebruary => 'Februar';

  @override
  String get lblJanuary => 'Januar';

  @override
  String get lblJuly => 'Juli';

  @override
  String get lblJune => 'Juni';

  @override
  String get lblMarch => 'Marsch';

  @override
  String get lblMonthly => 'Monatlich';

  @override
  String get lblNovember => 'November';

  @override
  String get lblOctober => 'Oktober';

  @override
  String get lblSeptember => 'September';

  @override
  String get lblWeekly => 'Wöchentlich';

  @override
  String get lblYearly => 'Jährlich';

  @override
  String get lblChangeSignature => 'Signatur ändern';

  @override
  String get lblClear => 'Klar';

  @override
  String get lblUndo => 'Rückgängig machen';

  @override
  String get lblAreYouSureYouWantToUpdateDetails =>
      'Sind Sie sicher, dass Sie die Details aktualisieren möchten?';

  @override
  String get lblSignature => 'Unterschrift';

  @override
  String get lblAreYouSureYouWantTo => 'Bist du sicher, dass du das willst';

  @override
  String get lblAdd => 'Hinzufügen';

  @override
  String get lblSelectYearOfGraduation => 'Wählen Sie das Abschlussjahr aus';

  @override
  String get lblSelect => 'Wählen';

  @override
  String get lblPayBill => 'Rechnung bezahlen';

  @override
  String get lblPleaseCheckYourEmailInboxToSetNewPassword =>
      'Bitte überprüfen Sie Ihren E -Mail -Posteingang, um ein neues Passwort festzulegen';

  @override
  String get lblReview => 'Rezension';

  @override
  String get lblBillingRecords => 'Abrechnungsunterlagen';

  @override
  String get lblAppointmentCount => 'Terminzahl';

  @override
  String get lblNoRecordsFound => 'Keine Aufzeichnungen gefunden';

  @override
  String get lblNoAppointmentsFound => 'Keine Termine gefunden';

  @override
  String get lblSelectPatient => 'Patienten auswählen';

  @override
  String get lblNoReportsFound => 'Keine Berichte gefunden';

  @override
  String get lblSpecialities => 'Spezialitäten';

  @override
  String get lblKnowWhatYourPatientsSaysAboutYou =>
      'Wissen Sie, was Ihre Patienten über Sie sagen';

  @override
  String get lblSchedule => 'Zeitplan';

  @override
  String get lblIsThisTeleMedService => 'Ist das ein Telemedienst?';

  @override
  String get lblAllowMultiSelectionWhileBooking =>
      'Multi Selection während der Buchung zulassen?';

  @override
  String get lblNo => 'NEIN';

  @override
  String get lblSetStatus => 'Status festlegen';

  @override
  String get lblFound => 'gefunden';

  @override
  String get lblDUpdatedSuccessfully => 'Erfolgreich geupdated';

  @override
  String get lblDeletedSuccessfully => 'Erfolgreich gelöscht';

  @override
  String get lblPleaseGiveYourRating => 'Bitte geben Sie Ihre Bewertung an';

  @override
  String get lblEnterYourReviews => 'Geben Sie Ihre Bewertung ein (optional)';
}
