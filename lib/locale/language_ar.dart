import 'package:kivicare_flutter/locale/base_language_key.dart';

class LanguageAr extends BaseLanguage {
  @override
  String get appName => 'كيفيكاري';

  @override
  String get lblWalkThroughTitle1 => 'مرحباً';

  @override
  String get lblWalkThroughTitle2 => 'العثور على الأطباء';

  @override
  String get lblWalkThroughTitle3 => 'تجنب وقت الانتظار';

  @override
  String get lblWalkThroughTitle4 => 'تحدث مع الأطباء';

  @override
  String get lblWalkThroughSubTitle1 =>
      '""تطبيق Kivicare هو حل السجلات الصحية الإلكترونية الجاهزة والمطلقة (EHR) للأطباء والمهنيين الطبيين وإدارة العيادة وإدارة المرضى.';

  @override
  String get lblWalkThroughSubTitle2 =>
      'قم ببناء تطبيق الهاتف المحمول الخاص بك عن طريق سرد الأطباء. يمكنك العثور على طبيب أقرب إليك مع نموذج الأطباء المذهلين هذا. إضافة وإدارة قائمة الأطباء مع Kivicare.';

  @override
  String get lblWalkThroughSubTitle3 =>
      'إدارة المواعيد في العيادة أو المستشفى ، أو جدولة المرضى في وقت مبكر مع واجهة Kivicare الذكية. تجنب أي تأخيرات أو أكثر في الانتظار في قوائم الانتظار';

  @override
  String get lblWalkThroughSubTitle4 =>
      'يمكن للمرضى الحصول على استشارات مباشرة مع الطبيب الذي يجعل التشخيص الأول دقيق وفي الوقت المناسب. يرسل إشعار الرسائل القصيرة من Kivicare تنبيهات على الرقم المسجل للمريض.';

  @override
  String get lblWalkThroughSkipButton => 'يتخطى';

  @override
  String get lblWalkThroughNextButton => 'التالي';

  @override
  String get lblWalkThroughGetStartedButton => 'البدء';

  @override
  String get lblSignIn => 'تسجيل الدخول';

  @override
  String get lblLogOut => 'تسجيل خروج';

  @override
  String get lblEmail => 'بريد إلكتروني';

  @override
  String get lblPassword => 'كلمة المرور';

  @override
  String get lblOldPassword => 'كلمة المرور القديمة';

  @override
  String get lblNewPassword => 'كلمة المرور الجديدة';

  @override
  String get lblConfirmPassword => 'تأكيد كلمة المرور';

  @override
  String get lblForgotPassword => 'هل نسيت كلمة السر؟';

  @override
  String get lblSignUp => 'اشتراك';

  @override
  String get lblBasicDetails => 'تفاصيل أساسية';

  @override
  String get lblOtherDetails => 'تفاصيل أخرى';

  @override
  String get lblSubmit => 'يُقدِّم';

  @override
  String get lblFirstName => 'الاسم الأول';

  @override
  String get lblLastName => 'اسم العائلة';

  @override
  String get lblContactNumber => 'رقم الاتصال';

  @override
  String get lblDOB => 'دوب';

  @override
  String get lblSelectBloodGroup => 'حدد FLODER FROLE';

  @override
  String get lblAddress => 'عنوان';

  @override
  String get lblCity => 'مدينة';

  @override
  String get lblCountry => 'دولة';

  @override
  String get lblPostalCode => 'رمز بريدي';

  @override
  String get lblSettings => 'إعدادات';

  @override
  String get lblChangePassword => 'تغيير كلمة المرور';

  @override
  String get lblTermsAndCondition => 'الشروط والشرط';

  @override
  String get lblLanguage => 'لغة';

  @override
  String get lblAboutUs => 'معلومات عنا';

  @override
  String get lblRateUs => 'قيمنا';

  @override
  String get lblSave => 'يحفظ';

  @override
  String get lblDegree => 'درجة';

  @override
  String get lblUniversity => 'جامعة';

  @override
  String get lblYear => 'سنة';

  @override
  String get lblSearch => 'يبحث';

  @override
  String get lblCancel => 'يلغي';

  @override
  String get lblDoctor => 'طبيب';

  @override
  String get lblDescription => 'وصف';

  @override
  String get lblPrescription => 'روشتة';

  @override
  String get lblFrequency => 'تكرار';

  @override
  String get lblDuration => 'مدة';

  @override
  String get lblInstruction => 'تعليمات';

  @override
  String get lblSignInToContinue =>
      'مرحبًا بك مرة أخرى ، قم بتسجيل الدخول إلى حسابك';

  @override
  String get lblNewMember => 'ليس لديك حساب؟';

  @override
  String get lblDone => 'منتهي';

  @override
  String get lblSignUpAsPatient => 'إنشاء حساب Kivicare الخاص بك';

  @override
  String get lblAlreadyAMember => 'عضوا فعلا؟';

  @override
  String get lblLogin => 'تسجيل الدخول';

  @override
  String get lblDashboard => 'لوحة القيادة';

  @override
  String get lblAppointments => 'تعيينات';

  @override
  String get lblAppointment => 'تعيينات';

  @override
  String get lblPatients => 'مرضى';

  @override
  String get lblTotalPatient => 'إجمالي المريض';

  @override
  String get lblTotalVisitedPatients => 'إجمالي زيارة المرضى';

  @override
  String get lblTotalAppointment => 'إجمالي التعيين.';

  @override
  String get lblTotalVisitedAppointment => 'إجمالي موعد زار';

  @override
  String get lblTodayAppointments => 'اليوم تعيين.';

  @override
  String get lblTotalTodayAppointments => 'إجمالي مواعيد اليوم';

  @override
  String get lblWeeklyAppointments => 'المواعيد الأسبوعية';

  @override
  String get lblTodaySAppointments => 'مواعيدك';

  @override
  String get lblAppointmentDeleted => 'تم حذف الموعد';

  @override
  String get lblDate => 'تاريخ';

  @override
  String get lblConfirmAppointment => 'تأكيد الموعد';

  @override
  String get lblSelectDateTime => 'حدد التاريخ والوقت';

  @override
  String get lblSelectServices => 'حدد الخدمات';

  @override
  String get lblBook => 'كتاب';

  @override
  String get lblNoAppointmentForToday => 'لا يوجد موعد لهذا اليوم';

  @override
  String get lblCheckIn => 'تحقق في';

  @override
  String get lblCheckOut => 'الدفع';

  @override
  String get lblAreDeleteAppointment => 'هل أنت متأكد من أنك تريد حذف الموعد؟';

  @override
  String get lblYouCannotStart => 'لا يمكنك البدء';

  @override
  String get lblPrescriptionAdded => 'وأضاف وصفة طبية';

  @override
  String get lblUpdatedSuccessfully => 'تم التحديث بنجاح';

  @override
  String get lblPrescriptionDeleted => 'وصفة طبية تم حذفها';

  @override
  String get lblAddPrescription => 'إضافة وصفة طبية';

  @override
  String get lblName => 'اسم';

  @override
  String get lblPrescriptionDurationIsRequired => 'مطلوب مدة الوصفة الطبية';

  @override
  String get lblDurationInDays => 'المدة (في الأيام)';

  @override
  String get lblAddNewPrescription => 'أضف وصفة طبية جديدة';

  @override
  String get lblEditPrescriptionDetail => 'تحرير تفاصيل الوصفة الطبية';

  @override
  String get lblAreYouSure => 'هل أنت متأكد ؟';

  @override
  String get lblDays => 'أيام';

  @override
  String get lblAppointmentIsConfirmed => 'تم تأكيد موعدك';

  @override
  String get lblThanksForBooking => 'شكرا للحجز';

  @override
  String get lblAppointmentConfirmation => 'لقد تم تأكيدك لموعدك';

  @override
  String get lblNoPatientFound => 'لم يجد مريض';

  @override
  String get lblDeleteRecordConfirmation =>
      'هل أنت متأكد أنك تريد حذف جميع السجلات';

  @override
  String get lblAllRecordsFor => 'جميع السجلات ل';

  @override
  String get lblAreDeleted => 'يتم حذفها';

  @override
  String get lblEncounters => 'لقاءات';

  @override
  String get lblDelete => 'يمسح';

  @override
  String get lblMale => 'ذكر';

  @override
  String get lblFemale => 'أنثى';

  @override
  String get lblOther => 'آخر';

  @override
  String get lblMinimumAgeRequired => 'الحد الأدنى للسن المطلوب هو 18.';

  @override
  String get lblCurrentAgeIs => 'سنك الحالي';

  @override
  String get lblGender1 => 'جنس';

  @override
  String get lblSpecialization => 'تخصص';

  @override
  String get lblExperience => 'خبرة';

  @override
  String get lblZoomConfiguration => 'تكبير التكبير';

  @override
  String get lblTelemed => 'عن بعد';

  @override
  String get lblAPIKeyCannotBeEmpty => 'مفتاح API لا يمكن أن يكون فارغًا';

  @override
  String get lblAPIKey => 'مفتاح API';

  @override
  String get lblAPISecret => 'API سر';

  @override
  String get lblAPISecretCannotBeEmpty => 'سر API لا يمكن أن يكون فارغا';

  @override
  String get lblZoomConfigurationGuide => 'دليل تكوين التكبير';

  @override
  String get lblSignUpOrSignIn => 'قم بالتسجيل أو تسجيل الدخول هنا:';

  @override
  String get lblZoomMarketPlacePortal => 'Zoom Market Place Place Portal';

  @override
  String get lbl1 => '1';

  @override
  String get lbl2 => '2';

  @override
  String get lblClickOnDevelopButton =>
      'انقر/تحوم على زر تطوير على اليمين في شريط التنقل وانقر على إنشاء تطبيق';

  @override
  String get lblCreateApp => 'إنشاء التطبيق';

  @override
  String get lb13 => '3';

  @override
  String get lblChooseAppTypeToJWT => 'اختر نوع التطبيق الخاص بك إلى JWT';

  @override
  String get lbl4 => '4';

  @override
  String get lblMandatoryMessage =>
      'املأ المعلومات الإلزامية وفي علامة بيانات اعتماد التطبيق ، يمكنك رؤية مفتاح API و Secret API.';

  @override
  String get lbl5 => '5';

  @override
  String get lblCopyAndPasteAPIKey =>
      'نسخ ولصق مفتاح API و Secret API هنا وانقر على زر حفظ وأنت جاهز للذهاب.';

  @override
  String get lblEncounterClosed => 'لقاء مغلق';

  @override
  String get lblChangedTo => 'تغير إلى';

  @override
  String get lblEncounterWillBeClosed => 'سيتم إغلاق اللقاء';

  @override
  String get lblEncounterDate => 'تاريخ لقاء';

  @override
  String get lblClinicName => 'اسم العيادة';

  @override
  String get lblDoctorName => 'اسم الطبيب';

  @override
  String get lblDesc => 'DESC';

  @override
  String get lblAddNewQualification => 'إضافة مؤهل جديد';

  @override
  String get lblAddBillItem => 'اضافة عنصر';

  @override
  String get lblServiceIsRequired => 'الخدمة مطلوبة';

  @override
  String get lblOne => '1';

  @override
  String get lblPrice => 'سعر';

  @override
  String get lblQuantity => 'كمية';

  @override
  String get lblTotal => 'المجموع';

  @override
  String get lblEncounterUpdated => 'مواجهة تحديث';

  @override
  String get lblAddNewEncounter => 'أضف لقاء جديد';

  @override
  String get lblEditEncounterDetail => 'تحرير التفاصيل اللقاء';

  @override
  String get lblHolidayOf => 'عطلة';

  @override
  String get lblModuleIsRequired => 'الوحدة المطلوبة';

  @override
  String get lblScheduleDate => 'تاريخ الجدول الزمني';

  @override
  String get lblLeaveFor => 'غادر ل';

  @override
  String get lblAddHoliday => 'أضف عطلة';

  @override
  String get lblEditHolidays => 'تحرير العطل';

  @override
  String get lblAreYouSureToDelete => 'هل أنت متأكد أنك تريد حذف؟';

  @override
  String get lblNewPatientAddedSuccessfully => 'أضاف مريض جديد بنجاح';

  @override
  String get lblPatientDetailUpdatedSuccessfully =>
      'تم تحديث تفاصيل المريض بنجاح';

  @override
  String get lblBasicInformation => 'معلومات اساسية';

  @override
  String get lblFirstNameIsRequired => 'الإسم الأول مطلوب';

  @override
  String get lblLastNameIsRequired => 'إسم العائلة مطلوب';

  @override
  String get lblEmailIsRequired => 'البريد الالكتروني مطلوب';

  @override
  String get lblAddNewPatient => 'أضف مريض جديد';

  @override
  String get lblEditPatientDetail => 'تحرير تفاصيل المريض';

  @override
  String get lblCategory => 'فئة';

  @override
  String get lblCharges => 'تكلفة';

  @override
  String get lblSelectDoctor => 'حدد الطبيب';

  @override
  String get lblAddService => 'أضف خدمة جديدة';

  @override
  String get lblEditService => 'تحرير الخدمة';

  @override
  String get lblSelectWeekdays => 'حدد أيام الأسبوع';

  @override
  String get lblSessionAddedSuccessfully => 'تمت إضافة الجلسة بنجاح';

  @override
  String get lblSessionUpdatedSuccessfully => 'تم تحديث الجلسة بنجاح';

  @override
  String get lblSessionDeleted => 'تم حذف الجلسة';

  @override
  String get lblPleaseSelectTime => 'الرجاء تحديد الوقت';

  @override
  String get lblStartAndEndTimeNotSame =>
      'لا يمكن أن يكون وقت البدء والنهاية هو نفسه';

  @override
  String get lblTimeNotBeforeMorningStartTime =>
      'لا يمكن أن يكون الوقت قبل وقت بدء الصباح';

  @override
  String get lblTimeNotBeforeEveningStartTime =>
      'لا يمكن أن يكون الوقت قبل وقت بدء المساء';

  @override
  String get lblTimeShouldBeInMultiplyOf5 => 'يجب أن يكون الوقت مضاعفًا من 5';

  @override
  String get lblTimeSlotInMinute => 'فتحة الوقت (في الدقيقة)';

  @override
  String get lblTimeSlotRequired => 'فتحة الوقت المطلوبة';

  @override
  String get lblWeekDays => 'أيام الأسبوع';

  @override
  String get lblMorningSession => 'الجلسة الصباحية';

  @override
  String get lblStartTime => 'وقت البدء';

  @override
  String get lblEndTime => 'وقت النهاية';

  @override
  String get lblSelectStartTimeFirst => 'حدد وقت البدء أولاً';

  @override
  String get lblEveningSession => 'جلسة المساء';

  @override
  String get lblAddSession => 'أضف الجلسة';

  @override
  String get lblEditSession => 'تحرير الجلسة';

  @override
  String get lblInvoiceDetail => 'تفاصيل الفاتورة';

  @override
  String get lblClinicDetails => 'تفاصيل العيادة';

  @override
  String get lblPatientDetails => 'تفاصيل المريض';

  @override
  String get lblServices => 'خدمات';

  @override
  String get lblDiscount => 'تخفيض';

  @override
  String get lblAmountDue => 'المبلغ المستحق';

  @override
  String get lblInvoiceId => 'هوية صوتية';

  @override
  String get lblCreatedAt => 'أنشئت في';

  @override
  String get lblPaymentStatus => 'حالة السداد';

  @override
  String get lblPatientName => 'اسم المريض';

  @override
  String get lblGender2 => 'جنس';

  @override
  String get lblSRNo => 'الأب رقم';

  @override
  String get lblItemName => 'اسم العنصر';

  @override
  String get lblPRICE => 'سعر';

  @override
  String get lblQUANTITY => 'كمية';

  @override
  String get lblTOTAL => 'المجموع';

  @override
  String get lblServicesSelected => 'الخدمات المحددة';

  @override
  String get lblPatientNameIsRequired => 'مطلوب اسم المريض';

  @override
  String get lblDoctorSessions => 'كل الجلسة';

  @override
  String get lblEditProfile => 'تعديل الملف الشخصي';

  @override
  String get lblQualification => 'مؤهل';

  @override
  String get lblEncounterDashboard => 'لقاء لوحة القيادة';

  @override
  String get lblEncounterDetails => 'تواجه تفاصيل';

  @override
  String get lblProblems => 'مشكلة';

  @override
  String get lblObservation => 'ملاحظة';

  @override
  String get lblNotes => 'ملحوظات';

  @override
  String get lblBillAddedSuccessfully => 'أضاف بيل بنجاح';

  @override
  String get lblAtLeastSelectOneBillItem => 'لحفظ ATLEAST حدد عنصر فاتورة واحد';

  @override
  String get lblGenerateInvoice => 'توليد فاتورة';

  @override
  String get lblSERVICES => 'خدمات';

  @override
  String get lblPayableAmount => 'مبلغ مستحق الدفع';

  @override
  String get lblSaveAndCloseEncounter => 'حفظ وإغلاق اللقاء';

  @override
  String get lblHolidays => 'العطلات';

  @override
  String get lblClinic => 'عيادة';

  @override
  String get lblAfter => 'بعد';

  @override
  String get lblWasOffFor => 'كان خارج ل';

  @override
  String get lblYourHolidays => 'إجازتك';

  @override
  String get lblNoServicesFound => 'لم يتم العثور على خدمات';

  @override
  String get lblNoDataFound => 'لاتوجد بيانات';

  @override
  String get lblTelemedServicesUpdated => 'الخدمات عن بعد تحديثها';

  @override
  String get lblOn => 'على';

  @override
  String get lblOff => 'عن';

  @override
  String get lblNoAppointments => 'لا مواعيد';

  @override
  String get lblSelectClinic => 'حدد العيادة';

  @override
  String get lblEnter => 'يدخل';

  @override
  String get lblFieldIsRequired => 'الحقل مطلوب';

  @override
  String get lblHoliday => 'عطلة';

  @override
  String get lblClinicHoliday => 'العطلة العيادة';

  @override
  String get lblSessions => 'جلسات';

  @override
  String get lblClinicSessions => 'جلسات العيادة';

  @override
  String get lblClinicServices => 'خدمات';

  @override
  String get lblVideoConsulting => 'استشارات الفيديو';

  @override
  String get lblYourEncounters => 'لقاءاتك';

  @override
  String get lblSelectTheme => 'اختر نمطا';

  @override
  String get lblChooseYourAppTheme => 'اختر موضوع التطبيق الخاص بك';

  @override
  String get lblClinicTAndC => 'عيادة T&C';

  @override
  String get lblAboutKiviCare => 'حول كيفيكاري';

  @override
  String get lblYourReviewCounts => 'تهم مراجعتك';

  @override
  String get lblAppVersion => 'نسخة التطبيق';

  @override
  String get lblHelpAndSupport => 'ساعد لدعم';

  @override
  String get lblSubmitYourQueriesHere => 'أرسل استفساراتك هنا';

  @override
  String get lblShareKiviCare => 'مشاركة Kivicare';

  @override
  String get lblLogout => 'تسجيل خروج';

  @override
  String get lblThanksForVisiting => 'شكرا لزيارتكم';

  @override
  String get lblAreYouSureToLogout => 'هل أنت متأكد أنك تريد تسجيل الخروج';

  @override
  String get lblGeneralSetting => 'الاعدادات العامة';

  @override
  String get lblAppSettings => 'إعدادات التطبيقات';

  @override
  String get lblVersion => 'إصدار';

  @override
  String get lblContactUs => 'اتصل بنا';

  @override
  String get lblAboutUsDes =>
      '""Kivicare هو موعد كامل للعيادة/المستشفى وإدارة السجلات للأطباء والمرضى. إنه يساعد المرضى على حجز مواعيد الطبيب الخاصة بهم بسهولة في أي وقت المعلومات ، والتقارير الطبية ، والأدوية ، وتاريخ زيارة ، والملاحظات السريرية ، تاريخ المريض وغيرها يتنقل كل شيء من خلال الأوراق لرؤية التاريخ السابق لمرضاك ، كل شيء متاح بسهولة لتشخيصك.';

  @override
  String get lblPurchase => 'شراء';

  @override
  String get lblDemoUserPasswordNotChanged =>
      'لا يمكن تغيير كلمة مرور المستخدمين التجريبيين';

  @override
  String get lblPasswordLengthMessage => 'يجب أن يكون طول كلمة المرور أكثر من';

  @override
  String get lblBothPasswordMatched => 'يجب أن تتوافق كلا كلمة المرور';

  @override
  String get lblVisited => 'زار';

  @override
  String get lblBooked => 'حجز';

  @override
  String get lblCompleted => 'مكتمل';

  @override
  String get lblCancelled => 'ألغيت';

  @override
  String get lblYes => 'نعم';

  @override
  String get lblPayment => 'دفع WooCommerce';

  @override
  String get lblError => 'خطأ';

  @override
  String get lblRegisteredSuccessfully => 'مسجل بنجاح';

  @override
  String get lblBirthDateIsRequired => 'تاريخ الميلاد مطلوب';

  @override
  String get lblBloodGroupIsRequired => 'مطلوب مجموعة الدم';

  @override
  String get lblAppointmentBookedSuccessfully =>
      'تم حجز الموعد بنجاح ، يرجى التحقق من بريدك الإلكتروني.';

  @override
  String get lblSelectedSlots => 'فتحات مختارة';

  @override
  String get lblSession => 'حصة';

  @override
  String get lblTimeSlotIsBooked => 'تم حجز فتحة الوقت';

  @override
  String get lblAppointmentDate => 'تاريخ الموعد';

  @override
  String get lblViewDetails => 'منظر';

  @override
  String get lblDoctorDetails => 'تفاصيل الطبيب';

  @override
  String get lblAreYouWantToDeleteDoctor => 'هل أنت متأكد أنك تريد حذف الطبيب؟';

  @override
  String get lblDoctorDeleted => 'حذف الطبيب';

  @override
  String get lblYearsExperience => 'عام';

  @override
  String get lblYearsOfExperience => 'سنوات من الخبرة';

  @override
  String get lblAvailableOn => 'متاح لهذا الأسبوع:';

  @override
  String get lblHealth => 'صحة';

  @override
  String get lblReadMore => '...اقرأ أكثر';

  @override
  String get lblReadLess => 'أقرأ أقل';

  @override
  String get lblBy => 'بواسطة';

  @override
  String get lblNews => 'أخبار';

  @override
  String get lblUpcomingAppointments => 'المواعيد القادمة';

  @override
  String get lblViewAll => 'عرض الكل';

  @override
  String get lblTopDoctors => 'كبار الأطباء';

  @override
  String get lblExpertsHealthTipsAndAdvice => 'نصائح ونصائح صحية الخبراء';

  @override
  String get lblArticlesByHighlyQualifiedDoctors =>
      'مقالات من قبل الأطباء المؤهلين تأهيلا عاليا على الصحة اليومية ..';

  @override
  String get lblChooseYourDoctor => 'اختر طبيبك';

  @override
  String get lblAddNewAppointment => 'إضافة موعد جديد';

  @override
  String get lblSelectOneDoctor => 'حدد طبيب واحد';

  @override
  String get lblClinicDoctor => 'طبيب العيادة';

  @override
  String get lblPatientDashboard => 'لوحة القيادة';

  @override
  String get lblFeedsAndArticles => 'الأعلاف والمقالات';

  @override
  String get lblPatientsEncounter => 'يواجه المرضى';

  @override
  String get lblNoEncounterFound => 'لم يتم العثور على لقاء';

  @override
  String get lblSelectSpecialization => 'حدد التخصص';

  @override
  String get lblAddDoctorProfile => 'إضافة ملف تعريف الطبيب';

  @override
  String get lblMedicalReport => 'تقرير طبي';

  @override
  String get lblNewMedicalReport => 'تقرير طبي جديد';

  @override
  String get lblGoogleCalendarConfiguration => 'تقويم جوجل';

  @override
  String get lblRememberMe => 'تذكرنى';

  @override
  String get lblChooseYourClinic => 'اختر عيادتك';

  @override
  String get lblAll => 'الجميع';

  @override
  String get lblLatest => 'أحدث';

  @override
  String get lblMon => 'الاثنين';

  @override
  String get lblTue => 'الثلاثاء';

  @override
  String get lblWed => 'تزوج';

  @override
  String get lblThu => 'الخميس';

  @override
  String get lblFri => 'الجمعة';

  @override
  String get lblSat => 'قعد';

  @override
  String get lblSun => 'شمس';

  @override
  String get lblNoReportWasSelected => 'لم يتم اختيار أي تقرير';

  @override
  String get lblAddReportScreen => 'إضافة تقرير';

  @override
  String get lblDateCantBeNull => 'لا يمكن أن يكون التاريخ فارغًا';

  @override
  String get lblUploadReport => 'تقرير التحميل';

  @override
  String get lblConnectWithGoogle => 'متصل مع';

  @override
  String get lblDisconnect => 'قطع الاتصال';

  @override
  String get lblAreYouSureYouWantToDisconnect => 'هل أنت متأكد أنك تريد فصل؟';

  @override
  String get lblLight => 'ضوء';

  @override
  String get lblDark => 'مظلم';

  @override
  String get lblSystemDefault => 'النظام الافتراضي';

  @override
  String get lblNA => 'ن/أ';

  @override
  String get lblAddedNewEncounter => 'أضيفت لقاء جديد';

  @override
  String get lblCantEditDate => 'لا يمكنك تحرير التاريخ الذي تم تمريره بالفعل';

  @override
  String get lblNoTitle => 'بلا عنوان';

  @override
  String get lblSelectOneClinic => 'حدد عيادة واحدة';

  @override
  String get lblPast => 'ماضي';

  @override
  String get lblAddMedicalReport => 'إضافة تقرير طبي';

  @override
  String get lblSendPrescriptionOnMail => 'أرسل وصفة طبية على البريد';

  @override
  String get lblYouAreConnectedWithTheGoogleCalender =>
      'أنت متصل بتقويم Google.';

  @override
  String get lblPleaseConnectWithYourGoogleAccountToGetAppointmentsInGoogleCalendarAutomatically =>
      'يرجى الاتصال بحساب Google الخاص بك للحصول على مواعيد في تقويم Google تلقائيًا.';

  @override
  String get lblGoogleMeet => 'لقاء جوجل';

  @override
  String get lblYouCanUseOneMeetingServiceAtTheTimeWeAreDisablingZoomService =>
      'يمكنك استخدام خدمة اجتماع واحدة في ذلك الوقت. نحن نقوم بتعطيل خدمة التكبير.';

  @override
  String get lblYouCanUseOneMeetingServiceAtTheTimeWeAreDisablingGoogleMeetService =>
      'يمكنك استخدام خدمة اجتماع واحدة في ذلك الوقت. نحن نقوم بتعطيل خدمة Google Meet.';

  @override
  String get lblFilesSelected => 'الملفات المحددة';

  @override
  String get lblService => 'خدمة';

  @override
  String get lblTime => 'وقت';

  @override
  String get lblAppointmentSummary => 'ملخص التعيين';

  @override
  String get lblEncounter => 'يقابل';

  @override
  String get lblMedicalReports => 'تقرير طبي';

  @override
  String get lblConnectedWith => 'متصل مع';

  @override
  String get lblContact => 'اتصال';

  @override
  String get lblQrScanner => 'الماسح الضوئي QR';

  @override
  String get lblLoginSuccessfully => 'تسجيل الدخول بنجاح !! 🎉';

  @override
  String get lblWrongUser => 'مستخدم خاطئ';

  @override
  String get lblMorning => 'صباح';

  @override
  String get lblEvening => 'مساء';

  @override
  String get lblShare => 'يشارك';

  @override
  String get lblNoMatch => 'لا توجد وجهات نظر مطابقة';

  @override
  String get lblNoDataSubTitle => 'لم نتمكن من العثور على أي شيء يتعلق ببحثك';

  @override
  String get lblEdit => 'يحرر';

  @override
  String get lblSwipeMassage => 'انتقد اليسار لتحرير أو حذف';

  @override
  String get lblReachUsMore => 'تصل إلينا أكثر';

  @override
  String get lblAddressDetail => 'تفاصيل العنوان';

  @override
  String get lblChangeYourClinic => 'تغيير عيادتك';

  @override
  String get lblYourBills => 'احصل على تفاصيل جميع الفواتير';

  @override
  String get lblYourReports => 'ابحث عن تقاريرك التي تم تحميلها';

  @override
  String get lblBillRecords => 'سجلات الفاتورة';

  @override
  String get lblMyBills => 'فواتيري';

  @override
  String get lblRevenue => 'ربح';

  @override
  String get lblBuyIt => 'اشتريها';

  @override
  String get lblTryIt => 'جربها';

  @override
  String get lblYouAreJustOneStepAwayFromHavingAHandsOnBackendDemo =>
      'أنت على بعد خطوة واحدة على بعد خطوة واحدة من التوضيح العملي.';

  @override
  String get lblChooseYourRole => 'اختر دورك';

  @override
  String get lblEnterYourEmailAddressAsWellAsTheTemporaryLink =>
      'أدخل عنوان بريدك الإلكتروني وكذلك الرابط المؤقت';

  @override
  String get lblClickOnThatAndScanItFromTheApp =>
      'انقر على ذلك وفحصه من التطبيق';

  @override
  String get lblYouWillSeeAQRForAppOptionOnTheRightHandCorner =>
      'سترى خيار QR للتطبيق في الزاوية اليمنى ،';

  @override
  String get lblEnjoyTheFlawlessKivicareSystemWithEase =>
      'يتمتع! نظام Kivicare الذي لا تشوبه شائبة بسهولة.';

  @override
  String get lblCamera => 'آلة تصوير';

  @override
  String get lblGallery => 'صالة عرض';

  @override
  String get lblRemoveImage => 'إزالة الصورة';

  @override
  String get lblCanNotBeEmpty => 'لايمكن ان يكون فارغا';

  @override
  String get lblNoConnection => 'لا يوجد اتصال';

  @override
  String get lblYourInternetConnectionWasInterrupted =>
      'تمت مقاطعة اتصال الإنترنت الخاص بك';

  @override
  String get lblPlease => 'لو سمحت';

  @override
  String get lblRetry => 'إعادة المحاولة';

  @override
  String get lblAfternoon => 'مساء الخير';

  @override
  String get lblGood => 'جيد';

  @override
  String get lblNight => 'طاب مساؤك';

  @override
  String get lblNoSlotAvailable => 'لا توجد فتحة متاحة';

  @override
  String get lblPleaseChooseAnotherDay => 'الرجاء اختيار يوم آخر';

  @override
  String get lblPleaseCloseTheEncounterToCheckoutPatient =>
      'يرجى إغلاق اللقاء للتغلب على المريض';

  @override
  String get lblRemove => 'يزيل';

  @override
  String get lblAResetPasswordLinkWillBeSentToTheAboveEnteredEmailAddress =>
      'سيتم إرسال رابط كلمة مرور إعادة تعيين إلى عنوان البريد الإلكتروني الذي تم إدخاله أعلاه';

  @override
  String get lblAreYouSureYouWantToChangeThePassword =>
      'هل أنت متأكد من أنك تريد تغيير كلمة المرور؟';

  @override
  String get lblEnterYourEmailAddress => 'أدخل عنوان بريدك الالكتروني';

  @override
  String get lblHowToGenerateQRCode => 'كيفية توليد رمز الاستجابة السريعة؟';

  @override
  String get lblStepsToGenerateQRCode => 'خطوات لإنشاء رمز الاستجابة السريعة';

  @override
  String get lblOpenTheDemoUrlInWeb => 'افتح عنوان URL التجريبي في الويب';

  @override
  String get lblAreYouSureYouWantToSubmitTheForm =>
      'هل أنت متأكد أنك تريد إرسال النموذج؟';

  @override
  String get lblMore => 'أكثر';

  @override
  String get lblRatingsAndReviews => 'التقييمات والمراجعات';

  @override
  String get lblViewFile => 'استعراض الملف';

  @override
  String get lblLoading => 'تحميل';

  @override
  String get lblAnErrorOccurredWhileCheckingInternetConnectivity =>
      'حدث خطأ أثناء التحقق من اتصال الإنترنت';

  @override
  String get lblBloodGroup => 'فصيلة الدم';

  @override
  String get lblChooseAction => 'اختر الإجراء';

  @override
  String get lblConnecting => 'توصيل';

  @override
  String get lblMyClinic => 'عيادتي';

  @override
  String get lblMyReports => 'تقاريري';

  @override
  String get lblNoReviewsFound => 'لم يتم العثور على مراجعات';

  @override
  String get lblPleaseCheckYourNumber => 'يرجى التحقق من رقمك';

  @override
  String get lblYourReviews => 'مراجعاتك';

  @override
  String get lblConnected => 'متصل';

  @override
  String get lblNetworkStatus => 'حالة الشبكة';

  @override
  String get lblOffline => 'غير متصل على الانترنت';

  @override
  String get lblUnknown => 'مجهول';

  @override
  String get lblSelectAppointmentDate => 'حدد موعد الموعد';

  @override
  String get lblScanToTest => 'فحص للاختبار';

  @override
  String get lblPleaseSelectPaymentStatus => 'الرجاء تحديد حالة الدفع أولاً';

  @override
  String get lblWhatYourCustomersSaysAboutYou => 'ماذا يقول عملاؤك عنك';

  @override
  String get lblFriday => 'جمعة';

  @override
  String get lblMonday => 'الاثنين';

  @override
  String get lblSaturday => 'السبت';

  @override
  String get lblSunday => 'الأحد';

  @override
  String get lblThursday => 'يوم الخميس';

  @override
  String get lblTuesday => 'يوم الثلاثاء';

  @override
  String get lblWednesday => 'الأربعاء';

  @override
  String get lblChange => 'يتغير';

  @override
  String get lblChangingStatusFrom => 'تغيير الحالة من';

  @override
  String get lblPleaseSelectDoctor => 'الرجاء تحديد الطبيب';

  @override
  String get lblClose => 'يغلق';

  @override
  String get lblAllTheAppointmentOnSelectedDateWillBeCancelled =>
      'سيتم إلغاء جميع الموعد في التاريخ المحدد.';

  @override
  String get lblApr => 'أبريل';

  @override
  String get lblArabic => 'عربي';

  @override
  String get lblAug => 'أغسطس';

  @override
  String get lblDec => 'ديسمبر';

  @override
  String get lblEnglish => 'إنجليزي';

  @override
  String get lblFeb => 'فبراير';

  @override
  String get lblFrench => 'فرنسي';

  @override
  String get lblGerman => 'ألمانية';

  @override
  String get lblHindi => 'Brazil';

  @override
  String get lblJan => 'يناير';

  @override
  String get lblJul => 'يوليو';

  @override
  String get lblJun => 'يونيو';

  @override
  String get lblMar => 'مارس';

  @override
  String get lblMay => 'يمكن';

  @override
  String get lblNov => 'نوفمبر';

  @override
  String get lblOct => 'أكتوبر';

  @override
  String get lblSep => 'سبتمبر';

  @override
  String get lblToday => 'اليوم';

  @override
  String get lblTomorrow => 'غداً';

  @override
  String get lblYesterday => 'أمس';

  @override
  String get lblNoQualificationsFound => 'لم يتم العثور على مؤهلات';

  @override
  String get lblActive => 'نشيط';

  @override
  String get lblInActive => 'غير نشط';

  @override
  String get lblOpen => 'يفتح';

  @override
  String get lblPaid => 'مدفوع';

  @override
  String get lblUnPaid => 'غير مدفوع الأجر';

  @override
  String get lblComplete => 'مكتمل';

  @override
  String get lblClosed => 'مغلق';

  @override
  String get lblChooseYourFavouriteClinic => 'اختر عيادتك المفضلة';

  @override
  String get lblAvailableSession => 'تفاصيل الجلسة المتاحة';

  @override
  String get lblGetYourAllBillsHere => 'تاريخ جميع سجلات الفاتورة';

  @override
  String get lblServicesYouProvide => 'الخدمات التي تقدمها';

  @override
  String get lblYourAllEncounters => 'تاريخ جميع اللقاءات';

  @override
  String get lblScheduledHolidays => 'العطلات المجدولة';

  @override
  String get lblNotSelected => 'لم يتم اختياره';

  @override
  String get lblStatus => 'حالة';

  @override
  String get lblMultipleSelection => 'اختيار متعددة';

  @override
  String get lblAdded => 'وأضاف';

  @override
  String get lblAddedSuccessfully => 'اضيف بنجاح';

  @override
  String get lblInvalidURL => 'URL غير صالح:';

  @override
  String get lblMedicalHistoryHasBeen => 'كان التاريخ الطبي';

  @override
  String get lblReport => 'تقرير';

  @override
  String get lblSuccessfully => 'بنجاح';

  @override
  String get lblInvalidDayOfMonth => 'يوم غير صالح من الشهر';

  @override
  String get lblConnectionReEstablished => 'إعادة تأسيس الاتصال';

  @override
  String get lblToMobileData => 'إلى بيانات الهاتف المحمول';

  @override
  String get lblToWifi => 'إلى wifi';

  @override
  String get lblMultipleSelectionIsAvailableForThisService =>
      'يتوفر خيار الاختيار المتعدد لهذه الخدمة';

  @override
  String get lblNote => 'ملحوظة';

  @override
  String get lblTelemedServiceIsAvailable => 'الخدمة المتوفرة عن بعد متوفرة';

  @override
  String get lblToCloseTheEncounterInvoicePaymentIsMandatory =>
      'لإغلاق اللقاء ، يكون دفع الفاتورة إلزاميًا';

  @override
  String get lblUpdate => 'تحديث';

  @override
  String get lblBillDetails => 'تفاصيل فاتورة';

  @override
  String get lblChooseImage => 'اختر صورة';

  @override
  String get lblApril => 'أبريل';

  @override
  String get lblAugust => 'أغسطس';

  @override
  String get lblDecember => 'ديسمبر';

  @override
  String get lblFebruary => 'شهر فبراير';

  @override
  String get lblJanuary => 'يناير';

  @override
  String get lblJuly => 'يوليو';

  @override
  String get lblJune => 'يونيو';

  @override
  String get lblMarch => 'يمشي';

  @override
  String get lblMonthly => 'شهريا';

  @override
  String get lblNovember => 'شهر نوفمبر';

  @override
  String get lblOctober => 'اكتوبر';

  @override
  String get lblSeptember => 'سبتمبر';

  @override
  String get lblWeekly => 'أسبوعي';

  @override
  String get lblYearly => 'سنوي';

  @override
  String get lblChangeSignature => 'تغيير التوقيع';

  @override
  String get lblClear => 'واضح';

  @override
  String get lblUndo => 'الغاء التحميل';

  @override
  String get lblAreYouSureYouWantToUpdateDetails =>
      'هل أنت متأكد من أنك تريد تحديث التفاصيل؟';

  @override
  String get lblSignature => 'إمضاء';

  @override
  String get lblAreYouSureYouWantTo => 'هل أنت متأكد أنك تريد';

  @override
  String get lblAdd => 'يضيف';

  @override
  String get lblSelectYearOfGraduation => 'حدد سنة التخرج';

  @override
  String get lblSelect => 'يختار';

  @override
  String get lblPayBill => 'فاتورة الأجرة';

  @override
  String get lblPleaseCheckYourEmailInboxToSetNewPassword =>
      'يرجى التحقق من صندوق بريدك الإلكتروني لتعيين كلمة مرور جديدة';

  @override
  String get lblReview => 'مراجعة';

  @override
  String get lblBillingRecords => 'سجلات الفواتير';

  @override
  String get lblAppointmentCount => 'عدد التعيين';

  @override
  String get lblNoRecordsFound => 'لا توجد سجلات';

  @override
  String get lblNoAppointmentsFound => 'لم يتم العثور على مواعيد';

  @override
  String get lblSelectPatient => 'حدد المريض';

  @override
  String get lblNoReportsFound => 'لم يتم العثور على تقارير';

  @override
  String get lblSpecialities => 'التخصصات';

  @override
  String get lblKnowWhatYourPatientsSaysAboutYou =>
      'تعرف على ما يقوله مرضاك عنك';

  @override
  String get lblSchedule => 'جدول';

  @override
  String get lblIsThisTeleMedService => 'هل هذه خدمة عبر عن بُعد؟';

  @override
  String get lblAllowMultiSelectionWhileBooking =>
      'السماح باختيار متعدد أثناء الحجز؟';

  @override
  String get lblNo => 'لا';

  @override
  String get lblSetStatus => 'تعيين الحالة';

  @override
  String get lblFound => 'وجد';

  @override
  String get lblDUpdatedSuccessfully => 'تم التحديث بنجاح';

  @override
  String get lblDeletedSuccessfully => 'حذف بنجاح';

  @override
  String get lblPleaseGiveYourRating => 'من فضلك أعطي تصنيفك';

  @override
  String get lblEnterYourReviews => 'أدخل مراجعتك (اختياري)';
}
