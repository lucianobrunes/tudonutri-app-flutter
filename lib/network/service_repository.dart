//Start Service API
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:kivicare_flutter/config.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/base_response.dart';
import 'package:kivicare_flutter/model/service_model.dart';
import 'package:kivicare_flutter/network/auth_repository.dart';
import 'package:kivicare_flutter/network/network_utils.dart';
import 'package:kivicare_flutter/utils/common.dart';
import 'package:nb_utils/nb_utils.dart';

Future<ServiceListModel> getServiceResponseAPI({int? id}) async {
  String doctorId = id != null ? "doctor_id=${id.validate()}" : "";
  return ServiceListModel.fromJson(await (handleResponse(await buildHttpResponse('kivicare/api/v1/service/get-list?$doctorId'))));
}

Future<BaseResponses> deleteServiceDataAPI(Map request) async {
  return BaseResponses.fromJson(await handleResponse(await buildHttpResponse('kivicare/api/v1/service/delete-service', request: request, method: HttpMethod.POST)));
}

Future<BaseResponses> saveServiceAPI({required Map<String, dynamic> data, File? serviceImage}) async {
  var multiPartRequest = await getMultiPartRequest('kivicare/api/v1/service/add-service');

  multiPartRequest.fields.addAll(await getMultipartFields(val: data));

  if (serviceImage != null) {
    multiPartRequest.files.add(await MultipartFile.fromPath('image', serviceImage.path));
  }

  log("Multi Part Request : ${jsonEncode(multiPartRequest.fields)} ${multiPartRequest.files.map((e) => e.field + ": " + e.filename.validate())}");

  multiPartRequest.headers.addAll(buildHeaderTokens());

  appStore.setLoading(true);

  return await sendMultiPartRequestNew(multiPartRequest).then((value) {
    return BaseResponses.fromJson(value);
  }).catchError((e) {
    appStore.setLoading(false);
    throw e;
  });
}

Future<List<ServiceData>> getServiceListWithPaginationAPI({int? clinicId, required int page, required List<ServiceData> serviceList, Function(bool)? lastPageCallback, Function(int)? getTotalService}) async {
  appStore.setLoading(true);
  List<String> param = [];

  if (isReceptionist()) {
    param.add('?clinic_id=${userStore.userClinicId}');

    param.add('limit=$PER_PAGE');
  } else if (isDoctor() || isPatient()) {
    param.add('?limit=$PER_PAGE');
    param.add('page=$page');
  }

  ServiceListModel res = ServiceListModel.fromJson(await handleResponse(await buildHttpResponse('kivicare/api/v1/service/get-list${param.validate().join('&')}')));
  getTotalService?.call(res.total.validate().toInt());

  if (page == 1) serviceList.clear();

  lastPageCallback?.call(res.serviceData.validate().length != PER_PAGE);

  serviceList.addAll(res.serviceData.validate());

  appStore.setLoading(false);
  return serviceList;
}

//End Service API
