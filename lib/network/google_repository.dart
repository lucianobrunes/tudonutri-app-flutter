import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/response_model.dart';
import 'package:kivicare_flutter/model/user_model.dart';
import 'package:kivicare_flutter/network/network_utils.dart';
import 'package:nb_utils/nb_utils.dart';

// /kivicare/api/v1/doctor/get-zoom-configuration

//region Google Calender
Future<ResponseModel> connectGoogleCalendarAPI({required Map<String, dynamic> request}) async {
  return ResponseModel.fromJson(await (handleResponse(await buildHttpResponse('kivicare/api/v1/google-calendar/connect-doctor', request: request, method: HttpMethod.POST))));
}

Future<ResponseModel> disconnectGoogleCalendarAPI() async {
  return ResponseModel.fromJson(await (handleResponse(await buildHttpResponse('kivicare/api/v1/google-calendar/disconnect-doctor'))));
}

//region Google Calender
Future<ResponseModel> connectMeetAPI({required Map<String, dynamic> request}) async {
  return ResponseModel.fromJson(await (handleResponse(await buildHttpResponse('kivicare/api/v1/doctor/connect-googlemeet-doctor', request: request, method: HttpMethod.POST))));
}

Future<ResponseModel> disconnectMeetAPI({required Map<String, dynamic> request}) async {
  return ResponseModel.fromJson(await (handleResponse(await buildHttpResponse('kivicare/api/v1/doctor/disconnect-googlemeet-doctor', request: request, method: HttpMethod.POST))));
}

Future<UserModel> getConfigurationAPI() async {
  UserModel value = UserModel.fromJson(await (handleResponse(await buildHttpResponse('kivicare/api/v1/user/get-configuration'))));

  appStore.setUserProEnabled(value.isKivicareProOnName.validate(), initialize: true);
  appStore.setUserTelemedOn(value.isTelemedActive.validate(), initialize: true);
  appStore.setUserMeetService(value.isKivicareGoogleMeetActive.validate(), initialize: true);
  appStore.setUserEnableGoogleCal(value.isEnableGoogleCal.validate(), initialize: true);
  appStore.setUserDoctorGoogleCal(value.isEnableDoctorGCal.validate(), initialize: true);
  appStore.setTelemedType(value.telemedType.validate().toString(), initialize: true);
  appStore.setGlobalDateFormat(value.globalDateFormat.validate().toString(), initialize: true);
  appStore.setRestrictAppointmentPost(value.restrictAppointment!.post.validate().toInt(), initialize: true);
  appStore.setRestrictAppointmentPre(value.restrictAppointment!.pre.validate().toInt(), initialize: true);
  return value;
}

//endregion
