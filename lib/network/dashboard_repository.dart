import 'dart:convert';

import 'package:kivicare_flutter/model/dashboard_model.dart';
import 'package:kivicare_flutter/model/encounter_model.dart';
import 'package:kivicare_flutter/model/static_data_model.dart';
import 'package:kivicare_flutter/model/upcoming_appointment_model.dart';
import 'package:kivicare_flutter/network/network_utils.dart';
import 'package:kivicare_flutter/utils/cached_value.dart';

Future<DashboardModel> getUserDashBoardAPI() async {
  DashboardModel res =  DashboardModel.fromJson(await (handleResponse(await buildHttpResponse('kivicare/api/v1/user/get-dashboard?page=1&limit=5'))));
  cachedDashboardModel = res;
  return res;
}

Future<EncounterModel> getEncounterDetailsDashBoardAPI({required int encounterId}) async {
  return EncounterModel.fromJson(await (handleResponse(await buildHttpResponse('kivicare/api/v1/encounter/get-encounter-detail?id=$encounterId'))));
}

Future<StaticDataModel> getStaticDataResponseAPI(String req) async {
  return StaticDataModel.fromJson(await (handleResponse(await buildHttpResponse('kivicare/api/v1/staticdata/get-list?type=$req'))));
}
// get appointment count

Future<List<WeeklyAppointment>> getAppointmentCountAPI({required Map request}) async {
  Iterable it = Iterable.empty();

  it = await handleResponse(await buildHttpResponse('kivicare/api/v1/doctor/get-appointment-count', request: request, method: HttpMethod.POST));

  return it.map((e) => WeeklyAppointment.fromJson(e)).toList();
}
