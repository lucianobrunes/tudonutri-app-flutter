import 'package:kivicare_flutter/config.dart';
import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/base_response.dart';
import 'package:kivicare_flutter/model/clinic_list_model.dart';
import 'package:kivicare_flutter/network/network_utils.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:nb_utils/nb_utils.dart';

Future<Clinic> getSelectedClinicAPI({int? page, required String clinicId}) async {
  ClinicListModel res = ClinicListModel.fromJson(await (handleResponse(await buildHttpResponse('kivicare/api/v1/clinic/get-list?page=${page != null ? page : ''}'))));
  appointmentAppStore.setSelectedClinic(res.clinicData.validate().firstWhere((element) => element.id.validate() == clinicId));
  return res.clinicData.validate().firstWhere((element) => element.id.validate() == clinicId);
}

Future<List<Clinic>> getClinicListAPI({required int page, required List<Clinic> appointmentList, Function(bool)? lastPageCallback, Function(int)? getTotalPatient}) async {
  ClinicListModel res = ClinicListModel.fromJson(await (handleResponse(await buildHttpResponse(getEndPoint(endPoint: 'kivicare/api/v1/clinic/get-list', page: page)))));
  appStore.setLoading(true);

  getTotalPatient?.call(res.total.validate().toInt());

  if (page == 1) appointmentList.clear();

  lastPageCallback?.call(res.clinicData.validate().length != PER_PAGE);

  appointmentList.addAll(res.clinicData.validate());

  appStore.setLoading(false);
  log(appointmentList.length);

  return appointmentList;
}

Future switchClinicApi({required Map req}) async {
  return (await handleResponse(await buildHttpResponse('kivicare/api/v1/patient/switch-clinic', request: req, method: HttpMethod.POST)));
}
