import 'package:kivicare_flutter/main.dart';
import 'package:kivicare_flutter/model/bill_list_model.dart';
import 'package:kivicare_flutter/model/patient_bill_model.dart';
import 'package:kivicare_flutter/network/network_utils.dart';
import 'package:kivicare_flutter/utils/app_common.dart';
import 'package:nb_utils/nb_utils.dart';

Future<PatientBillModule> getBillDetailsAPI({int? encounterId}) async {
  return PatientBillModule.fromJson(await (handleResponse(await buildHttpResponse('kivicare/api/v1/bill/bill-details?encounter_id=$encounterId'))));
}

Future addPatientBillAPI(Map request) async {
  return await handleResponse(await buildHttpResponse('kivicare/api/v1/bill/add-bill', request: request, method: HttpMethod.POST));
}

///Todo change endPoints
Future<List<BillListData>> getBillListApi({
  int? page,
  required List<BillListData> billList,
  Function(bool)? lastPageCallback,
}) async {
  List<String> param = [];

  appStore.setLoading(true);

  BillListModel res = BillListModel.fromJson(
    await handleResponse(await buildHttpResponse(getEndPoint(endPoint: 'kivicare/api/v1/bill/list', page: page, params: param))),
  );

  if (page == 1) billList.clear();

  lastPageCallback?.call(res.billListData.validate().length != 10);

  billList.addAll(res.billListData.validate());

  appStore.setLoading(false);
  return billList;
}
