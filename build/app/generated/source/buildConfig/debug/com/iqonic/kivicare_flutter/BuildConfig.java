/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.iqonic.kivicare_flutter;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.brunes.tudonutri";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 19;
  public static final String VERSION_NAME = "7.0.0";
}
